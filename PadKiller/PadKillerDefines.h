// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#define SERVICE_NAME   L"PK Listener"

#define SERVICE_PORT_W L"17639"
#define SERVICE_PORT_A "17639"

#define CLIENT_PORT_W  L"17640"
#define CLIENT_PORT_A  "17640"

#define EVENT_SERVER_PORT_W  L"17641"
#define EVENT_SERVER_PORT_A  "17641"

#define COMMAND_LISTENER_PORT_W  L"17642"
#define COMMAND_LISTENER_PORT_A  "17642"

#define CONTROL_LISTENER_PORT_W  L"17643"
#define CONTROL_LISTENER_PORT_A  "17643"

#define TIMER_CLIENT_ABANDONED_TIMEOUT    10000

#define MAX_STRING_SIZE 4096

//
// Do NOT forget to modify the Android constants if you change ANY of these enums
// Also, do NOT forget that the enums are numbered consequeitively, the comments have NO bearing on the value(s)
//

enum pkCommands {
   NOCOMMAND = 0,
   RECEIVEIMAGE = 1,
   START,               // = 2,
   STOP,                // = 3,
   CLEARHOTSPOTS,       // = 4,
   HOTSPOT,             // = 5,
   CLEAREVERYTHING,     // = 6,
   CREATEBUTTON,        // = 7,
   POSITION,            // = 8,
   RESIZE,              // = 9,
   QUERYPARAMETER,      // = 10,
   HIDE,                // = 11,
   SHOW,                // = 12,
   DESTROYCONTROL,      // = 13,
   SETPARAMETER,        // = 14,
   BACKGROUND,          // = 15,
   CLEARINK,            // = 16,
   CLEARSETTINGS,       // = 17,
   CLEARBACKGROUND,     // = 18,
   HIDECONTROL,         // = 19,
   SHOWCONTROL,         // = 20,
   HIDECONTROLLIST,     // = 21,
   SHOWCONTROLLIST,     // = 22,
   DISPLAYPDF,          // = 23,
   CREATEDROPDOWNBOX,   // = 24,
   SELECTITEM,          // = 25,
   CREATELABEL,         // = 26,
   SETLABEL,            // = 27,
   CREATECHECKBOX,      // = 28,
   SETOPTIONSTATE,      // = 29,
   CREATERADIOBUTTON,   // = 30,
   CREATETEXTBOX,       // = 31,
   CREATEENTRYFIELD,    // = 32,
   TAKEKEYSTROKE        // = 33
};

enum pkEvents {
   PENDOWN = 1,
   PENUP,               // = 2,
   PENPOINT,            // = 3,
   OPTIONSELECTED,      // = 4,
   OPTIONUNSELECTED,    // = 5
   DEVICEREADY,         // = 6,
   CONFIGURATIONCHANGED,// = 7,
   ITEMSELECTED,        // = 8,
   TEXTCHANGED,         // = 9,
   KEYSTROKE            // = 10
};

enum pkParameters {
   PARM_NOPARAMETER = 0,
   PARM_BOUNDS_PIXELS = 1,
   PARM_BOUNDS_INCHES,           // = 2
   PARM_CLIENT_ADDRESS,          // = 3,
   PARM_INK_COLOR,               // = 4,
   PARM_INK_WEIGHT,              // = 5,
   PARM_SHOW_INK,                // = 6,
   PARM_SEND_POINTS,             // = 7,
   PARM_WIDTH_PIXELS,            // = 8,
   PARM_HEIGHT_PIXELS,           // = 9,
   PARM_WIDTH_INCHES,            // = 10,
   PARM_HEIGHT_INCHES,           // = 11,
   PARM_DEVICE_WIDTH_INCHES,     // = 12,
   PARM_DEVICE_HEIGHT_INCHES,    // = 13,
   PARM_DEVICE_WIDTH_PIXELS,     // = 14,
   PARM_DEVICE_HEIGHT_PIXELS,    // = 15,
   PARM_IMAGE,                   // = 16,
   PARM_FONT_FAMILY,             // = 17,
   PARM_FONT_SIZE,               // = 18,
   PARM_CONTROL_BOUNDS,          // = 19,
   PARM_PDF,                     // = 20,
   PARM_UI_HOST_WIDTH,           // = 21,
   PARM_UI_HOST_HEIGHT,          // = 22,
   PARM_CONTROL_TEXT,            // = 23,
   PARM_CONTROL_TEXT_BOUNDS,     // = 24,
   PARM_CONTROL_TEXT_LINE,       // = 25,
   PARM_CONTROL_POSITION,        // = 26,
   PARM_UPDATES_SHOWING,         // = 27,
   PARM_CONTROL_TEXT_FONT_HEIGHT // = 28
};
