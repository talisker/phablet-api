// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkListener.h"

#include "pkListenerData.h"

   static long isInitialized = 0L;

   VOID WINAPI servicePK(DWORD argc,LPTSTR *argv) {

   hServiceStatus = RegisterServiceCtrlHandlerEx(SERVICE_NAME,serviceHandler,NULL);

   serviceStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS; 
   serviceStatus.dwServiceSpecificExitCode = 0;    

   reportStatus(SERVICE_START_PENDING,NO_ERROR,3000);

   unsigned threadAddress;
   hServiceThread = (HANDLE)_beginthreadex(NULL,0,serviceLoop,NULL,CREATE_SUSPENDED,&threadAddress);

   reportStatus(SERVICE_RUNNING,NO_ERROR,0);

   HKEY hKey = NULL;

   if ( ! isInitialized ) {

      isInitialized = 1L;

      if ( ERROR_SUCCESS == RegOpenKeyEx(HKEY_LOCAL_MACHINE,L"System",0,KEY_SET_VALUE,&hKey) ) {

         HKEY hKey2;

         if ( ERROR_SUCCESS == RegOpenKeyEx(hKey,L"CurrentControlSet\\Services\\EventLog\\Application",0,KEY_SET_VALUE,&hKey2) ) {

            HKEY hKey3;
            RegCreateKeyEx(hKey2,SERVICE_NAME,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&hKey3,NULL);

            wchar_t szTemp[MAX_PATH];
            GetModuleFileName(NULL,szTemp,MAX_PATH);

            DWORD cb = 2 * ((DWORD)wcslen(szTemp) + 1);
            RegSetValueEx(hKey3,L"EventMessageFile",0L,REG_EXPAND_SZ,(BYTE *)szTemp,cb);

            RegCloseKey(hKey3);
            RegCloseKey(hKey2);

         }

         RegCloseKey(hKey);

      }

   }

   ResumeThread(hServiceThread);

   return;
   }
