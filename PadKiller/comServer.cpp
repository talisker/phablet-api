// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkListener.h"

   OLECHAR wstrModuleName[MAX_PATH];

   Factory factory(CLSID_PadKiller);

   Factory::Factory(REFCLSID myObj) : refCount(0),myObject(myObj) {};

   extern "C" int registerServer() {

   wchar_t *OBJECT_NAME;
   wchar_t *OBJECT_NAME_V;
   wchar_t *OBJECT_VERSION;

   GUID OBJECT_LIBID;
   wchar_t *OBJECT_DESCRIPTION;

   OBJECT_NAME = L"InnoVisioNate.PadKiller";
   OBJECT_NAME_V = L"InnoVisioNate.PadKiller.1";
   OBJECT_VERSION = L"1.0";

   memcpy(&OBJECT_LIBID,&LIBID_PadKiller,sizeof(GUID));
   OBJECT_DESCRIPTION = L"InnoVisioNate PadKiller";

   ITypeLib *ptLib;
   HKEY keyHandle,clsidHandle;
   DWORD disposition;
   wchar_t szTemp[256],szCLSID[256];
   LPOLESTR oleString;
   HRESULT rc;

   StringFromCLSID(CLSID_PadKiller,&oleString);
   //WideCharToMultiByte(CP_ACP,0,oleString,-1,szCLSID,256,0,0);
   wcscpy(szCLSID,oleString);
  
   if ( S_OK != LoadTypeLib(wstrModuleName,&ptLib) )
      rc = ResultFromScode(SELFREG_E_TYPELIB);
   else
      if ( S_OK != RegisterTypeLib(ptLib,wstrModuleName,NULL) )
         rc = ResultFromScode(SELFREG_E_TYPELIB);

   RegOpenKeyEx(HKEY_CLASSES_ROOT,L"CLSID",0,KEY_CREATE_SUB_KEY,&keyHandle);
  
      RegCreateKeyEx(keyHandle,szCLSID,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&clsidHandle,&disposition);
      wsprintf(szTemp,OBJECT_DESCRIPTION);
      RegSetValueEx(clsidHandle,NULL,0,REG_SZ,(BYTE *)szTemp,2 * (DWORD)wcslen(szTemp));
  
      wsprintf(szTemp,L"Control");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      wsprintf(szTemp,L"");
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,2 * (DWORD)wcslen(szTemp));
  
      wsprintf(szTemp,L"ProgID");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      wsprintf(szTemp,OBJECT_NAME_V);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,2 * (DWORD)wcslen(szTemp));
  
      wsprintf(szTemp,L"InprocServer");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)wstrModuleName,2 * (DWORD)wcslen(wstrModuleName));
  
      wsprintf(szTemp,L"InprocServer32");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)wstrModuleName,2 * (DWORD)wcslen(wstrModuleName));
      RegSetValueEx(keyHandle,L"ThreadingModel",0,REG_SZ,(BYTE *)L"Free",10);
//      RegSetValueEx(keyHandle,L"ThreadingModel",0,REG_SZ,(BYTE *)L"Apartment",18);
  
      wsprintf(szTemp,L"LocalServer32");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)wstrModuleName,2 * (DWORD)wcslen(wstrModuleName));
    
      wsprintf(szTemp,L"TypeLib");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
    
      StringFromCLSID(OBJECT_LIBID,&oleString);
      //WideCharToMultiByte(CP_ACP,0,oleString,-1,szTemp,256,0,0);//W2A(oleString,szTemp,256);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)oleString,2 * (DWORD)wcslen(oleString));
        
      wsprintf(szTemp,L"ToolboxBitmap32");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
//      wsprintf(szTemp,"%s, 1",szModuleName);
//      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,strlen(szModuleName));
  
      wsprintf(szTemp,L"Version");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      wsprintf(szTemp,OBJECT_VERSION);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,2 * (DWORD)wcslen(szTemp));
  
      wsprintf(szTemp,L"MiscStatus");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      wsprintf(szTemp,L"0");
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,2 * (DWORD)wcslen(szTemp));
  
      wsprintf(szTemp,L"1");
      RegCreateKeyEx(keyHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      wsprintf(szTemp,L"%ld",
                 OLEMISC_ALWAYSRUN |
                 OLEMISC_ACTIVATEWHENVISIBLE | 
                 OLEMISC_RECOMPOSEONRESIZE | 
                 OLEMISC_INSIDEOUT |
                 OLEMISC_SETCLIENTSITEFIRST |
                 OLEMISC_CANTLINKINSIDE );
//wsprintf(szTemp,"%ld",131473L);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,2 * (DWORD)wcslen(szTemp));
  
   RegCreateKeyEx(HKEY_CLASSES_ROOT,OBJECT_NAME,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegCreateKeyEx(keyHandle,L"CurVer",0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      wsprintf(szTemp,OBJECT_NAME_V);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,2 * (DWORD)wcslen(szTemp));
  
   RegCreateKeyEx(HKEY_CLASSES_ROOT,OBJECT_NAME_V,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegCreateKeyEx(keyHandle,L"CLSID",0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szCLSID,2 * (DWORD)wcslen(szCLSID));
    RegOpenKeyEx(HKEY_CLASSES_ROOT,L"CLSID",0,KEY_CREATE_SUB_KEY,&keyHandle);

   RegOpenKeyEx(HKEY_CLASSES_ROOT,L"Interface",0,KEY_CREATE_SUB_KEY,&keyHandle);

   StringFromCLSID(IID_IPadKiller,&oleString);
   wcscpy(szCLSID,oleString);

      RegCreateKeyEx(keyHandle,szCLSID,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&clsidHandle,&disposition);
      RegSetValueEx(clsidHandle,NULL,0,REG_SZ,(BYTE *)L"IPadKiller",2 * (DWORD)wcslen(L"IPadKiller"));

      RegCreateKeyEx(clsidHandle,L"TypeLib",0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      StringFromCLSID(OBJECT_LIBID,&oleString);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)oleString,2 * (DWORD)wcslen(oleString));

      RegCreateKeyEx(clsidHandle,L"ProxyStubClsid32",0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)L"{00020424-0000-0000-C000-000000000046}",2 * (DWORD)wcslen(L"{00020424-0000-0000-C000-000000000046}"));

      RegCreateKeyEx(clsidHandle,L"ProxyStubClsid",0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)L"{00020424-0000-0000-C000-000000000046}",2 * (DWORD)wcslen(L"{00020424-0000-0000-C000-000000000046}"));

      RegCreateKeyEx(clsidHandle,L"NumMethods",0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)L"1",2);

   return 0;
   }



   STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID * ppObject) {
   *ppObject = NULL;
   if ( rclsid == CLSID_PadKiller )
      return factory.QueryInterface(riid,ppObject);
   return CLASS_E_CLASSNOTAVAILABLE;
   }

   long __stdcall Factory::QueryInterface(REFIID iid, void **ppv) { 
   *ppv = NULL; 
   if ( iid == IID_IUnknown || iid == IID_IClassFactory ) 
      *ppv = this; 
   else
      return E_NOINTERFACE; 
   AddRef(); 
   return S_OK; 
   } 
  
  
   unsigned long __stdcall Factory::AddRef() { 
   return ++refCount; 
   } 
  
  
   unsigned long __stdcall Factory::Release() { 
   return --refCount;
   }


   HRESULT STDMETHODCALLTYPE Factory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv) { 
   *ppv = NULL; 
   PadKiller *pk = new PadKiller();
   return pk -> QueryInterface(riid,ppv);
   } 
  
  
   long __stdcall Factory::LockServer(int fLock) { 
   return S_OK; 
   }