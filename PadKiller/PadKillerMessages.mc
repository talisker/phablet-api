
; // ***** PadKiller.mc *****

MessageIdTypedef=DWORD

SeverityNames=(
   Success=0x0:STATUS_SEVERITY_SUCCESS
   Informational=0x1:STATUS_SEVERITY_INFORMATIONAL
   Warning=0x2:STATUS_SEVERITY_WARNING
   Error=0x3:STATUS_SEVERITY_ERROR
   )


FacilityNames=(
   System=0x0:FACILITY_SYSTEM
   Runtime=0x2:FACILITY_RUNTIME
   Stubs=0x3:FACILITY_STUBS
   Io=0x4:FACILITY_IO_ERROR_CODE
   )

LanguageNames=(English=0x409:MSG00409)

; // The following are message definitions.

MessageId=0x1
Severity=Informational
Facility=Runtime
SymbolicName=EVENTLOG_EVENT_STOPPED
Language=English
The PK Listener Service has been Stopped.
.

MessageId=0x4
Severity=Error
Facility=Runtime
SymbolicName=COULDNT_START
Language=English
The PK Listener Service could not be started.
.

MessageId=0x5
Severity=Informational
Facility=Runtime
SymbolicName=EVENTLOG_EVENT_SERVICE_STOPPING
Language=English
The PK Listener service is shutting down.
.

MessageId=0x6
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_NO_SOCKET_SUPPORT_INITIALIZE
Language=English
.

MessageId=0x7
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_NO_CREATE_SOCKET
Language=English
.

MessageId=0x8
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_NO_BIND_SOCKET
Language=English
.

MessageId=0x9
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_NO_LISTEN_PORT
Language=English
.

MessageId = 0xA
Severity = Error
Facility = Runtime
SymbolicName = EVENTLOG_ERROR_ACCEPT
Language = English
.

MessageId = 0xB
Severity = Error
Facility = Runtime
SymbolicName = EVENTLOG_ERROR_SOCKET_RECIEVE_ERROR
Language = English
.

MessageId = 512
Severity = Error
Facility = Runtime
SymbolicName = EVENTLOG_ERROR_WRONG_BYTES_SENT
Language = English
.

MessageId = 513
Severity = Error
Facility = Runtime
SymbolicName = EVENTLOG_ERROR_ERROR
Language = English
.

MessageId = 1024
Severity = Informational
Facility = Runtime
SymbolicName = EVENTLOG_INFORMATION_INFORMATION
Language = English
.

MessageId = 1025
Severity = Informational
Facility = Runtime
SymbolicName = EVENTLOG_INFORMATION_WAITING_CONNECTION
Language = English
.

MessageId = 1026
Severity = Informational
Facility = Runtime
SymbolicName = EVENTLOG_INFORMATION_WAITING_COMMAND
Language = English
.

MessageId = 1027
Severity = Informational
Facility = Runtime
SymbolicName = EVENTLOG_INFORMATION_CONNECTION_ACCEPTED
Language = English
.

MessageId = 1028
Severity = Informational
Facility = Runtime
SymbolicName = EVENTLOG_INFORMATION_FILE_RECIEVED
Language = English
.

