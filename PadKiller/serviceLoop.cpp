// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkListener.h"

#define DEFINE_DATA

#include "pkListenerData.h"

#include "PadKillerMessages.h"

extern SERVICE_STATUS serviceStatus;

   unsigned int __stdcall serviceLoop(void *) {

   WCHAR szwInput[1024];
   char szHostName[NI_MAXHOST];

   addrinfo addressInfo;
   addrinfo *pResolvedAddressInfo;

   memset(&addressInfo,0,sizeof(addrinfo));

   addressInfo.ai_flags = AI_PASSIVE;
   addressInfo.ai_family = AF_INET;
   addressInfo.ai_socktype = SOCK_STREAM;
   addressInfo.ai_protocol = IPPROTO_TCP;
   addressInfo.ai_addrlen = 0;
   addressInfo.ai_addr = NULL;
   addressInfo.ai_canonname = NULL;
   addressInfo.ai_next = NULL;

   getaddrinfo(NULL,SERVICE_PORT_A,&addressInfo,&pResolvedAddressInfo);

   getnameinfo(pResolvedAddressInfo -> ai_addr,(socklen_t)pResolvedAddressInfo -> ai_addrlen,szHostName,NI_MAXHOST,NULL,0L,0L);

   clientListenerSocket = socket(pResolvedAddressInfo -> ai_family,pResolvedAddressInfo -> ai_socktype,pResolvedAddressInfo -> ai_protocol);

   if ( INVALID_SOCKET == clientListenerSocket ) {
      swprintf(message,L"The PK Listener could not create a socket. RC = %ld",WSAGetLastError());
      logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_NO_CREATE_SOCKET);
      reportStatus(SERVICE_STOPPED,NO_ERROR,0);
      freeaddrinfo(pResolvedAddressInfo);
      return 0;
   }

   if ( SOCKET_ERROR == bind(clientListenerSocket,pResolvedAddressInfo -> ai_addr,(int)pResolvedAddressInfo -> ai_addrlen) ) {
      swprintf(message,L"The PK Listener could not bind the socket on port %s. RC = %ld",SERVICE_PORT_W,WSAGetLastError());
      logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_NO_BIND_SOCKET);
      reportStatus(SERVICE_STOPPED,NO_ERROR,0);
      freeaddrinfo(pResolvedAddressInfo);
      return 0;
   }

   freeaddrinfo(pResolvedAddressInfo);

   if ( SOCKET_ERROR == listen(clientListenerSocket,SOMAXCONN) ) {
      swprintf(message,L"The PK Listener could not listen on the socket on port %s. RC = %ld",SERVICE_PORT_W,WSAGetLastError());
      logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_NO_LISTEN_PORT);
      reportStatus(SERVICE_STOPPED,NO_ERROR,0);
      return 0;
   }

   BSTR bstrHostName = SysAllocStringLen(NULL,NI_MAXHOST);

   MultiByteToWideChar(CP_ACP,0,szHostName,-1,bstrHostName,NI_MAXHOST);

   bool shutdownRequested = false;

   while ( ! ( SERVICE_STOPPED == serviceStatus.dwCurrentState ) ) {

      swprintf(message,L"The PK Listener is awaiting connections on %s:%s.",bstrHostName,SERVICE_PORT_W);
      logEvent(message,NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_INFORMATION_WAITING_CONNECTION);

      sockaddr_in clientSockAddr = {0};

      int clientSockAddrSize = sizeof(sockaddr);

      clientCommandSocket = accept(clientListenerSocket,(sockaddr *)&clientSockAddr,&clientSockAddrSize);

      if ( INVALID_SOCKET == clientCommandSocket ) {
         swprintf(message,L"The PK Listener failed during socket accept on port %s. RC = %ld",SERVICE_PORT_W,WSAGetLastError());
         logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_ACCEPT);
         reportStatus(SERVICE_STOPPED,NO_ERROR,0);
         return 0;
      }

      DWORD cBytes = NI_MAXHOST;

      WSAAddressToString((sockaddr *)&clientSockAddr,sizeof(sockaddr_in),NULL,szwClientAddress,&cBytes);

      wchar_t *p = wcsrchr(szwClientAddress,':');
      if ( p )
         *p = '\0';

      WideCharToMultiByte(CP_ACP,0,szwClientAddress,-1,szClientAddress,NI_MAXHOST,0,0);

      swprintf(message,L"The PK Listener has accepted a connection on %s from %s.",SERVICE_PORT_W,szwClientAddress);

      logEvent(message,NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_INFORMATION_CONNECTION_ACCEPTED);

      if ( processInfo.hProcess ) {
//         if ( processInfo.hProcess ) 
//            CloseHandle(processInfo.hProcess);
//         if ( processInfo.hThread )
//            CloseHandle(processInfo.hThread);
         TerminateProcess(processInfo.hProcess,0L);
         memset(&processInfo,0,sizeof(processInfo));
      }

      if ( ! launchPadKillerHost() ) {
         swprintf(message,L"The system was not able to launch the pkHost application");
         logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_ERROR);
         return 0;
      }

      swprintf(szwInput,L"started: commands: %ls events: %ls control: %ls",COMMAND_LISTENER_PORT_W,EVENT_SERVER_PORT_W,CONTROL_LISTENER_PORT_W);

      send(clientCommandSocket,(char *)szwInput,2 * (DWORD)wcslen(szwInput),0L);

      bool clientConnected = true;

      while ( clientConnected ) {

         swprintf(message,L"The PK Listener is awaiting the next command.");

         logEvent(message,NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_INFORMATION_WAITING_COMMAND);

         memset(szwInput,0,sizeof(szwInput));

         long rc = recv(clientCommandSocket,(char *)szwInput,8,MSG_WAITALL);

         if ( rc < 0 ) {
            swprintf(message,L"The PK Listener encountered an error recieving data on port %s. RC = %ld",SERVICE_PORT_W,WSAGetLastError());
            logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_SOCKET_RECIEVE_ERROR);
            break;
         }

         if ( 0 == rc )
            break;

         long expectedBytes = _wtol(szwInput);

         memset(szwInput,0,sizeof(szwInput));

         rc = recv(clientCommandSocket,(char *)szwInput,expectedBytes,MSG_WAITALL);

         if ( 0 == _wcsnicmp(L"disconnect",szwInput,10) ) {
            clientConnected = false;
            break;
         }

      }

      shutdown(clientCommandSocket,SD_BOTH);
      closesocket(clientCommandSocket);
      clientCommandSocket = INVALID_SOCKET;

      if ( shutdownRequested )
         break;

   }

   swprintf(message,L"The PK Listener is shutting down.");
   logEvent(message,NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_EVENT_SERVICE_STOPPING);

   return 0;
   }
