// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkListener.h"

#include <ShlObj.h>

#include "pkListenerData.h"

#include "PadKiller_i.c"

   extern "C" int registerServer();

   extern Factory factory;

   extern OLECHAR wstrModuleName[];

   HINSTANCE hInstance = NULL;

   extern "C" LRESULT __stdcall comHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

   extern "C" int main(int argc,char *argv[]) {

   MultiByteToWideChar(CP_ACP,0,argv[0],-1,wstrModuleName,MAX_PATH);

   CoInitialize(NULL);

   registerServer();

   DWORD dwClassObject;

   IUnknown *pIUnknownFactory;
  
   HRESULT rcFactory = factory.QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknownFactory));

   HRESULT rc = CoRegisterClassObject(CLSID_PadKiller,pIUnknownFactory,CLSCTX_SERVER,0*REGCLS_SINGLEUSE | REGCLS_MULTIPLEUSE,&dwClassObject);

   if ( argc < 2 || strcmp("-Embedding",argv[1]) ) {

      SERVICE_TABLE_ENTRY dispatchTable[] = { {SERVICE_NAME,(LPSERVICE_MAIN_FUNCTION)servicePK}, {NULL,NULL} };
   
      hModule = GetModuleHandle(NULL);
   
      WSADATA wsaData;
   
      if ( WSAStartup(MAKEWORD(2,2),&wsaData) ) {
         TCHAR message[256];
         swprintf(message,L"The PK Listener service could not initialize the windows socket library. RC = %ld",WSAGetLastError());
         logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_NO_SOCKET_SUPPORT_INITIALIZE);
         reportStatus(SERVICE_STOPPED,NO_ERROR,0);
         CoRevokeClassObject(dwClassObject);
         return 0;
      }
   
      if ( ! StartServiceCtrlDispatcher(dispatchTable) )
         logEvent(L"The " SERVICE_NAME L" could not be started.",NULL,EVENTLOG_INFORMATION_TYPE,COULDNT_START); 

   } else {

      WNDCLASS gClass;
      
      memset(&gClass,0,sizeof(WNDCLASS));
      gClass.style = CS_BYTEALIGNCLIENT | CS_BYTEALIGNWINDOW;
      gClass.lpfnWndProc = comHandler;
      gClass.cbClsExtra = 32;
      gClass.cbWndExtra = 32;
      gClass.hInstance = hModule;
      gClass.hIcon = NULL;
      gClass.hCursor = NULL;
      gClass.hbrBackground = 0;
      gClass.lpszMenuName = NULL;
      gClass.lpszClassName = L"padKiller";

      RegisterClass(&gClass);

      HWND hwndMain = NULL;

      hwndMain = CreateWindowEx(0L,L"padKiller",L"",0L,0,0,0,0,NULL,NULL,hInstance,NULL);

      MSG qMessage;

      while ( GetMessage(&qMessage,(HWND)NULL,0L,0L) ) 
         DispatchMessage(&qMessage);

   }

   CoRevokeClassObject(dwClassObject);

   return 0;
   }


   DWORD WINAPI serviceHandler(DWORD dwControl,DWORD dwEventType,LPVOID pEventData,LPVOID pContext) {

   switch ( dwControl ) {

   case SERVICE_CONTROL_STOP:
      reportStatus(SERVICE_STOPPED,NO_ERROR,0);
      TerminateThread(hServiceThread,0L);
      logEvent(L"The PK Listener service is closing down.",NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_EVENT_SERVICE_STOPPING);
      return NO_ERROR;

   }

   reportStatus(SERVICE_RUNNING,NO_ERROR,0);

   return NO_ERROR;
   }


   void reportStatus(DWORD dwCurrentState,DWORD dwWin32ExitCode,DWORD dwWaitHint) {
   static DWORD dwCheckPoint = 1;
   serviceStatus.dwCurrentState = dwCurrentState;
   serviceStatus.dwWin32ExitCode = dwWin32ExitCode;
   serviceStatus.dwWaitHint = dwWaitHint;
   if ( SERVICE_START_PENDING == dwCurrentState )
      serviceStatus.dwControlsAccepted = 0;
   else 
      serviceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;
   if ( SERVICE_RUNNING == dwCurrentState || SERVICE_STOPPED == dwCurrentState )
      serviceStatus.dwCheckPoint = 0;
    else 
      serviceStatus.dwCheckPoint = dwCheckPoint++;
   SetServiceStatus(hServiceStatus,&serviceStatus);
   return;
   }


   void logEvent(LPTSTR szFunction,LPTSTR additionalInfo,DWORD eventType,DWORD eventNumber) { 
   HANDLE hEventSource = RegisterEventSource(NULL,SERVICE_NAME);
   if ( NULL == hEventSource ) 
      return;
   LPCTSTR lpszStrings[2];
   lpszStrings[0] = szFunction;
   if ( additionalInfo ) {
      lpszStrings[1] = additionalInfo;
      ReportEvent(hEventSource,(WORD)eventType,0,eventNumber,NULL,2,0,lpszStrings,NULL);
   } else 
      ReportEvent(hEventSource,(WORD)eventType,0,eventNumber,NULL,1,0,lpszStrings,NULL);
   DeregisterEventSource(hEventSource);
   return;
   }


   int GetLocation(HWND hwnd,long key,wchar_t *szFolderLocation) {

   ITEMIDLIST *ppItemIDList;
   IShellFolder *pIShellFolder;
   LPCITEMIDLIST pcParentIDList;

   HRESULT wasInitialized = OleInitialize(NULL);

   szFolderLocation[0] = '\0';

   HRESULT rc = SHGetFolderLocation(hwnd,key,NULL,0,&ppItemIDList);

   if ( S_OK != rc ) {
      szFolderLocation[0] = '\0';
      return 0;
   }

   rc = SHBindToParent(ppItemIDList, IID_IShellFolder, (void **) &pIShellFolder, &pcParentIDList);

   if ( S_OK == rc ) {

      STRRET strRet;
      rc = pIShellFolder -> GetDisplayNameOf(pcParentIDList,SHGDN_FORPARSING,&strRet);
      pIShellFolder -> Release();
      if ( S_OK == rc ) {
         wcscpy(szFolderLocation,strRet.pOleStr);
      } else {
         szFolderLocation[0] = '\0';
         return 0;
      }

   } else {
      szFolderLocation[0] = '\0';
      return 0;
   }

   if ( S_FALSE == wasInitialized )
      CoUninitialize();

   return 0;
   }


   extern "C" int __cdecl GetCommonAppDataLocation(HWND hwnd,wchar_t *szFolderLocation) {
   GetLocation(hwnd,CSIDL_COMMON_APPDATA,szFolderLocation);
   return 0;
   }
 

   extern "C" LRESULT __stdcall comHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   switch ( msg ) {

   case WM_CREATE: {
      CREATESTRUCT *pc = reinterpret_cast<CREATESTRUCT *>(lParam);
      }
      break;
   
   default:
      break;

   }

   return DefWindowProc(hwnd,msg,wParam,lParam);
   }