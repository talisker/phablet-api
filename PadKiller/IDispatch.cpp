// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkListener.h"

#include <tchar.h>
#include <olectl.h>

   extern OLECHAR wstrModuleName[];

   ITypeInfo *pITypeInfo = NULL;

   HRESULT __stdcall PadKiller::QueryInterface(REFIID riid, void** ppv) {
   *ppv = 0;

   if ( IID_IUnknown == riid )
      *ppv = this;
   else

      if ( IID_IPadKiller == riid )
         *ppv = static_cast<IPadKiller *>(this);
      else

         return E_NOINTERFACE;

   AddRef();

   return S_OK;
   }


   ULONG __stdcall PadKiller::AddRef() {
   return refCount++;
   }


   ULONG __stdcall PadKiller::Release() { 
   if ( --refCount == 0 ) {
      delete this;
      return 0;
   }
   return refCount;
   }


   // IDispatch

   STDMETHODIMP PadKiller::GetTypeInfoCount(UINT * pctinfo) { 
   *pctinfo = 1;
   return S_OK;
   } 


   long __stdcall PadKiller::GetTypeInfo(UINT itinfo,LCID lcid,ITypeInfo **pptinfo) { 
   *pptinfo = NULL; 
   if ( itinfo != 0 ) 
      return DISP_E_BADINDEX; 
   if ( NULL == pITypeInfo ) {
      ITypeLib *ptLib;
      LoadTypeLib(wstrModuleName,&ptLib);
      ptLib -> GetTypeInfoOfGuid(IID_IPadKiller,&pITypeInfo);
      ptLib -> Release();
   }
   *pptinfo = pITypeInfo;
   pITypeInfo -> AddRef();
   return S_OK; 
   } 
 

   STDMETHODIMP PadKiller::GetIDsOfNames(REFIID riid,OLECHAR** rgszNames,UINT cNames,LCID lcid, DISPID* rgdispid) { 
   return DispGetIDsOfNames(pITypeInfo,rgszNames,cNames,rgdispid);
   }


   STDMETHODIMP PadKiller::Invoke(DISPID dispidMember, REFIID riid, LCID lcid, 
                                           WORD wFlags,DISPPARAMS FAR* pdispparams, VARIANT FAR* pvarResult,
                                           EXCEPINFO FAR* pexcepinfo, UINT FAR* puArgErr) { 
   return DispInvoke(this,pITypeInfo,dispidMember,wFlags,pdispparams,pvarResult,pexcepinfo,puArgErr); 
   }


