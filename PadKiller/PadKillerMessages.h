 // ***** PadKiller.mc *****
 // The following are message definitions.
//
//  Values are 32 bit values laid out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//
#define FACILITY_SYSTEM                  0x0
#define FACILITY_RUNTIME                 0x2
#define FACILITY_STUBS                   0x3
#define FACILITY_IO_ERROR_CODE           0x4


//
// Define the severity codes
//
#define STATUS_SEVERITY_SUCCESS          0x0
#define STATUS_SEVERITY_INFORMATIONAL    0x1
#define STATUS_SEVERITY_WARNING          0x2
#define STATUS_SEVERITY_ERROR            0x3


//
// MessageId: EVENTLOG_EVENT_STOPPED
//
// MessageText:
//
// The PK Listener Service has been Stopped.
//
#define EVENTLOG_EVENT_STOPPED           ((DWORD)0x40020001L)

//
// MessageId: COULDNT_START
//
// MessageText:
//
// The PK Listener Service could not be started.
//
#define COULDNT_START                    ((DWORD)0xC0020004L)

//
// MessageId: EVENTLOG_EVENT_SERVICE_STOPPING
//
// MessageText:
//
// The PK Listener service is shutting down.
//
#define EVENTLOG_EVENT_SERVICE_STOPPING  ((DWORD)0x40020005L)

//
// MessageId: EVENTLOG_ERROR_NO_SOCKET_SUPPORT_INITIALIZE
//
// MessageText:
//
//  EVENTLOG_ERROR_NO_SOCKET_SUPPORT_INITIALIZE
//
#define EVENTLOG_ERROR_NO_SOCKET_SUPPORT_INITIALIZE ((DWORD)0xC0020006L)

//
// MessageId: EVENTLOG_ERROR_NO_CREATE_SOCKET
//
// MessageText:
//
//  EVENTLOG_ERROR_NO_CREATE_SOCKET
//
#define EVENTLOG_ERROR_NO_CREATE_SOCKET  ((DWORD)0xC0020007L)

//
// MessageId: EVENTLOG_ERROR_NO_BIND_SOCKET
//
// MessageText:
//
//  EVENTLOG_ERROR_NO_BIND_SOCKET
//
#define EVENTLOG_ERROR_NO_BIND_SOCKET    ((DWORD)0xC0020008L)

//
// MessageId: EVENTLOG_ERROR_NO_LISTEN_PORT
//
// MessageText:
//
//  EVENTLOG_ERROR_NO_LISTEN_PORT
//
#define EVENTLOG_ERROR_NO_LISTEN_PORT    ((DWORD)0xC0020009L)

//
// MessageId: EVENTLOG_ERROR_ACCEPT
//
// MessageText:
//
//  EVENTLOG_ERROR_ACCEPT
//
#define EVENTLOG_ERROR_ACCEPT            ((DWORD)0xC002000AL)

//
// MessageId: EVENTLOG_ERROR_SOCKET_RECIEVE_ERROR
//
// MessageText:
//
//  EVENTLOG_ERROR_SOCKET_RECIEVE_ERROR
//
#define EVENTLOG_ERROR_SOCKET_RECIEVE_ERROR ((DWORD)0xC002000BL)

//
// MessageId: EVENTLOG_ERROR_WRONG_BYTES_SENT
//
// MessageText:
//
//  EVENTLOG_ERROR_WRONG_BYTES_SENT
//
#define EVENTLOG_ERROR_WRONG_BYTES_SENT  ((DWORD)0xC0020200L)

//
// MessageId: EVENTLOG_ERROR_ERROR
//
// MessageText:
//
//  EVENTLOG_ERROR_ERROR
//
#define EVENTLOG_ERROR_ERROR             ((DWORD)0xC0020201L)

//
// MessageId: EVENTLOG_INFORMATION_INFORMATION
//
// MessageText:
//
//  EVENTLOG_INFORMATION_INFORMATION
//
#define EVENTLOG_INFORMATION_INFORMATION ((DWORD)0x40020400L)

//
// MessageId: EVENTLOG_INFORMATION_WAITING_CONNECTION
//
// MessageText:
//
//  EVENTLOG_INFORMATION_WAITING_CONNECTION
//
#define EVENTLOG_INFORMATION_WAITING_CONNECTION ((DWORD)0x40020401L)

//
// MessageId: EVENTLOG_INFORMATION_WAITING_COMMAND
//
// MessageText:
//
//  EVENTLOG_INFORMATION_WAITING_COMMAND
//
#define EVENTLOG_INFORMATION_WAITING_COMMAND ((DWORD)0x40020402L)

//
// MessageId: EVENTLOG_INFORMATION_CONNECTION_ACCEPTED
//
// MessageText:
//
//  EVENTLOG_INFORMATION_CONNECTION_ACCEPTED
//
#define EVENTLOG_INFORMATION_CONNECTION_ACCEPTED ((DWORD)0x40020403L)

//
// MessageId: EVENTLOG_INFORMATION_FILE_RECIEVED
//
// MessageText:
//
//  EVENTLOG_INFORMATION_FILE_RECIEVED
//
#define EVENTLOG_INFORMATION_FILE_RECIEVED ((DWORD)0x40020404L)

