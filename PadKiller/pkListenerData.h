// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

VOID WINAPI servicePK(DWORD,LPTSTR *);
DWORD WINAPI serviceHandler(DWORD dwControl,DWORD dwEventType,LPVOID pEventData,LPVOID pContext);

extern "C" int __cdecl GetCommonAppDataLocation(HWND hwnd,wchar_t *szFolderLocation);

unsigned int __stdcall serviceLoop(void *);

void reportStatus(DWORD dwCurrentState,DWORD dwWin32ExitCode,DWORD dwWaitHint);
void logEvent(LPTSTR szFunction,LPTSTR additionalInfo,DWORD eventType,DWORD eventNumber);

//unsigned int __stdcall eventListener(void *);

//HRESULT sendImage(char *pszFileName,long x,long y,long cx,long cy);
//HRESULT sendStart(char *pszResult,long resultSize);
//HRESULT sendStop(char *pszResult,long resultSize);
//void startEvents();
//void stopEvents();
//HRESULT forwardCommand(char *pszCommand,char *pszResult,long resultSize);
//HRESULT connectClientEventSink();
//HRESULT connectHostEventSource();
//HRESULT connectHostCommandListener();

BOOL launchPadKillerHost();

#ifdef DEFINE_DATA

   HINSTANCE hModule = NULL;

   SERVICE_STATUS_HANDLE hServiceStatus = NULL;
   SERVICE_STATUS serviceStatus = {0};

   HANDLE hServiceThread = NULL;

   TCHAR message[NI_MAXHOST];

   wchar_t szwClientAddress[NI_MAXHOST];
   char szClientAddress[NI_MAXHOST];

   PROCESS_INFORMATION processInfo;

   HANDLE hEventListener = NULL;

   SOCKET clientEventSocket = INVALID_SOCKET;
   SOCKET hostCommandListenerSocket = INVALID_SOCKET;
   SOCKET hostEventSourceSocket = INVALID_SOCKET;
   long eventListenerShutdown = 0L;

   SOCKET clientListenerSocket = INVALID_SOCKET;
   SOCKET clientCommandSocket = INVALID_SOCKET;

#else

   extern HINSTANCE hModule;

   extern SERVICE_STATUS_HANDLE hServiceStatus;
   extern SERVICE_STATUS serviceStatus;

   extern HANDLE hServiceThread;

   extern TCHAR message[];

   extern wchar_t szwClientAddress[];
   extern char szClientAddress[];

   extern PROCESS_INFORMATION processInfo;

   extern HANDLE hEventListener;

   extern SOCKET clientEventSocket;
   extern SOCKET hostCommandListenerSocket;
   extern SOCKET hostEventSourceSocket;
   extern long eventListenerShutdown;

   extern SOCKET clientListenerSocket;
   extern SOCKET clientCommandSocket;

#endif