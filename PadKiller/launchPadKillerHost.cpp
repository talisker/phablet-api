// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkListener.h"

#include <WtsApi32.h>

#include "pkListenerData.h"

   BOOL launchPadKillerHost() {

   BOOL bReturn = TRUE;

   HANDLE hToken,hTokenDup;

   WTSQueryUserToken(WTSGetActiveConsoleSessionId(),&hToken);

   void *pEnvironment = NULL;

   if ( ! DuplicateTokenEx(hToken,MAXIMUM_ALLOWED,NULL,SecurityIdentification,TokenPrimary, &hTokenDup) ) {
      swprintf(message,L"DuplicateTokenEx failed");
      logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_ERROR);
      return FALSE;
   }

   if ( ! CreateEnvironmentBlock(&pEnvironment, hTokenDup, FALSE) ) {
      swprintf(message,L"CreateEnvironmentBlock failed");
      logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_ERROR);
      CloseHandle(hTokenDup);
      return FALSE;
   }

   STARTUPINFO startUpInfo;

   ZeroMemory(&startUpInfo,sizeof(STARTUPINFO));
   ZeroMemory(&processInfo,sizeof(PROCESS_INFORMATION));

   startUpInfo.cb = sizeof(STARTUPINFO);
   startUpInfo.wShowWindow = SW_SHOW;
   startUpInfo.lpDesktop = L"Winsta0\\Default";
   startUpInfo.dwFlags = STARTF_USESHOWWINDOW;
   startUpInfo.wShowWindow = SW_SHOW;

   wchar_t szExecutableName[MAX_PATH];

   GetModuleFileName(NULL,szExecutableName,MAX_PATH);

   wchar_t *p = wcsrchr(szExecutableName,'\\');

   wsprintf(p,L"\\pkHostApp.exe");

   swprintf(message,L"The PK Listener is launching: %s",szExecutableName);
   logEvent(message,NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_INFORMATION_INFORMATION);

   if ( ! CreateProcessAsUser(hTokenDup,szExecutableName,szExecutableName,NULL,NULL,TRUE,
                                    CREATE_UNICODE_ENVIRONMENT | CREATE_PRESERVE_CODE_AUTHZ_LEVEL,
                                    pEnvironment,NULL,&startUpInfo,&processInfo) )  {
      swprintf(message,L"Create Process as user failed");
      logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_ERROR);
      bReturn = FALSE;
   } else {
      swprintf(message,L"Create Process as user succeeded");
      logEvent(message,NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_INFORMATION_INFORMATION);
   }

   RevertToSelf( );

   CloseHandle(hToken);

   if ( pEnvironment )
      DestroyEnvironmentBlock(pEnvironment);

   CloseHandle(hTokenDup);

   return bReturn;
}