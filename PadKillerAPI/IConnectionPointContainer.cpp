// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   pkAPI::_IConnectionPointContainer::_IConnectionPointContainer(pkAPI *pp) : 
     pParent(pp)
   { 
   return;
   }

   pkAPI::_IConnectionPointContainer::~_IConnectionPointContainer() {
   return;
   }


   HRESULT pkAPI::_IConnectionPointContainer::QueryInterface(REFIID riid,void **ppv) {
   return pParent -> QueryInterface(riid,ppv);
   }


   STDMETHODIMP_(ULONG) pkAPI::_IConnectionPointContainer::AddRef() {
   return pParent -> AddRef();
   }

   STDMETHODIMP_(ULONG) pkAPI::_IConnectionPointContainer::Release() {
   return pParent -> Release();
   }


   STDMETHODIMP pkAPI::_IConnectionPointContainer::EnumConnectionPoints(IEnumConnectionPoints **ppEnum) {

   _IConnectionPoint *connectionPoints[1];

   *ppEnum = NULL;
 
   if ( pParent -> pEnumConnectionPoints )
      delete pParent -> pEnumConnectionPoints;
 
   connectionPoints[0] = pParent -> pConnectionPointExtended;
   pParent -> pEnumConnectionPoints = new _IEnumConnectionPoints(pParent,connectionPoints,1);
 
   return pParent -> pEnumConnectionPoints -> QueryInterface(IID_IEnumConnectionPoints,(void **)ppEnum);
   }
 
 
   STDMETHODIMP pkAPI::_IConnectionPointContainer::FindConnectionPoint(REFIID riid,IConnectionPoint **ppCP) {
   *ppCP = NULL;
   if ( riid == IID_IPhabletSignaturePadEvents )
      return pParent -> pConnectionPointExtended -> QueryInterface(IID_IConnectionPoint,(void **)ppCP);
   if ( riid == IID_IVisioLoggerSignaturePadEvents )
      return pParent -> pConnectionPointVisioLogger -> QueryInterface(IID_IConnectionPoint,(void **)ppCP);
   if ( riid == IID_ISignaturePadEvents )
      return pParent -> pConnectionPointCursiVision -> QueryInterface(IID_IConnectionPoint,(void **)ppCP);
   return CONNECT_E_NOCONNECTION;
   }


   void pkAPI::_IConnectionPointContainer::fire_PenUp(long x,long y) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pConnectionPointExtended -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents * pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> PenUp(x,y);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }


   void pkAPI::_IConnectionPointContainer::fire_PenDown(long x,long y) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pConnectionPointExtended -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents * pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> PenDown(x,y);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }


   void pkAPI::_IConnectionPointContainer::fire_PenPoint(long x,long y,float inkWeight) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pConnectionPointExtended -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents * pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> PenPoint(x,y,inkWeight);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }


   void pkAPI::_IConnectionPointContainer::fire_OptionSelected(long optionNumber) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pConnectionPointExtended -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents * pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> OptionSelected(optionNumber);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }

   void pkAPI::_IConnectionPointContainer::fire_OptionUnSelected(long optionNumber) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pConnectionPointExtended -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents * pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> OptionUnSelected(optionNumber);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }

   void pkAPI::_IConnectionPointContainer::fire_DeviceReady() {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pConnectionPointExtended -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents * pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> DeviceReady();
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }

   void pkAPI::_IConnectionPointContainer::fire_ConfigurationChanged() {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pConnectionPointExtended -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents * pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> ConfigurationChanged();
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }

   void pkAPI::_IConnectionPointContainer::fire_ItemSelected(long controlId,BSTR bstrItem) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pConnectionPointExtended -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents * pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> ItemSelected(controlId,bstrItem);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }

   void pkAPI::_IConnectionPointContainer::fire_TextChanged(long controlId,BSTR oldText,BSTR newText) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pConnectionPointExtended -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents * pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> TextChanged(controlId,oldText,newText);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }

   void pkAPI::_IConnectionPointContainer::fire_KeyStroke(long controlId,WPARAM keyStroke) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pConnectionPointExtended -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents * pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> KeyStroke(controlId,keyStroke);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }