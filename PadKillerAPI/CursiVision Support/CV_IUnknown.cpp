// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "..\pkAPI.h"

   pkAPI::_ICursiVisionSignaturePad::_ICursiVisionSignaturePad(pkAPI *pp) :

      pParent(pp),

      pICursiVisionServices(NULL),

      pIConnectionPointContainer(NULL),

      hwndCursiVisionFrameWindow(NULL),

      isActive(false),

      signatureTimeOutPeriod(0.0),

      pSignatureDataX(NULL),
      pSignatureDataY(NULL)

   {

   memset(&originPoint,0,sizeof(originPoint));
   memset(&baselinePoint,0,sizeof(baselinePoint));

   pIConnectionPointContainer = new _ICursiVisionSignaturePad::_IConnectionPointContainer(this);

   return;
   }


   pkAPI::_ICursiVisionSignaturePad::~_ICursiVisionSignaturePad() {

   if ( pSignatureDataX )
      delete pSignatureDataX;

   if ( pSignatureDataY )
      delete pSignatureDataY;

   return;
   }

   // IUnknown

   long __stdcall pkAPI::_ICursiVisionSignaturePad::QueryInterface(REFIID riid,void **ppv) {

   if ( riid == IID_IUnknown )
      *ppv = static_cast<IUnknown *>(this);
   else

   if ( riid == IID_ISignaturePad )
      *ppv = static_cast<ISignaturePad *>(this);
   else

   if ( riid == IID_IConnectionPoint ) 
      *ppv = static_cast<IConnectionPoint *>(pParent -> pConnectionPointCursiVision);
   else

   if ( riid == IID_IConnectionPointContainer ) 
      *ppv = static_cast<IConnectionPointContainer *>(pIConnectionPointContainer);
   else

   if ( riid == IID_IGPropertyPageClient )
      return pParent -> pIGPropertyPageClient -> QueryInterface(riid,ppv);
   else

      return E_NOINTERFACE;

   AddRef();

   return S_OK;
   }

   unsigned long __stdcall pkAPI::_ICursiVisionSignaturePad::AddRef() {
   return pParent -> AddRef();
   }

   unsigned long __stdcall pkAPI::_ICursiVisionSignaturePad::Release() { 
   return pParent -> Release();
   }