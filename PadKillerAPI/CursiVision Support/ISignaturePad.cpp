// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "..\pkAPI.h"

   long pkAPI::_ICursiVisionSignaturePad::HostHandle(HWND hwnd) {
   return S_OK;
   }


   long pkAPI::_ICursiVisionSignaturePad::Load(char *pszDeviceName,HWND hwndNewClient,void *pvICursiVisionServices) {

   pICursiVisionServices = reinterpret_cast<ICursiVisionServices *>(pvICursiVisionServices);

   if ( NULL == pszDeviceName && NULL == hwndNewClient ) {
      if ( pParent -> IsConnected() )
         return S_OK;
      return pParent -> pingServer();
   }

   pParent -> Connect(NULL);

   return S_OK;
   }


   long pkAPI::_ICursiVisionSignaturePad::Start() {
   pParent -> Start();
   return S_OK;
   }


   long pkAPI::_ICursiVisionSignaturePad::Unload() {
   pParent -> ClearBackground();
   Stop();
   return S_OK;
   }

   long pkAPI::_ICursiVisionSignaturePad::Stop() {
   pParent -> Stop();
   pParent -> Disconnect();
   return S_OK;
   }

   void pkAPI::_ICursiVisionSignaturePad::PrepareForPage(RECT *pRect) {
   return;
   }

   long pkAPI::_ICursiVisionSignaturePad::Width() {
   return pParent -> getPadWidth();
   }

   long pkAPI::_ICursiVisionSignaturePad::Height() {
   return pParent -> getPadHeight();
   }

   long pkAPI::_ICursiVisionSignaturePad::LCDWidth() {
   return pParent -> getPadWidth();
   }

   long pkAPI::_ICursiVisionSignaturePad::LCDHeight() {
   return pParent -> getPadHeight();
   }

   long pkAPI::_ICursiVisionSignaturePad::GetPixelBits(HDC hdc) {
   return GetDeviceCaps(hdc,BITSPIXEL);
   }

   long pkAPI::_ICursiVisionSignaturePad::MaximumX() {
   return Width();
   }

   long pkAPI::_ICursiVisionSignaturePad::MaximumY() {
   return Height();
   }

   long pkAPI::_ICursiVisionSignaturePad::MinimumX() {
   return 0;
   }

   long pkAPI::_ICursiVisionSignaturePad::MinimumY() {
   return 0;
   }

   long pkAPI::_ICursiVisionSignaturePad::MaximumSignableY() {
   return MaximumY();
   }

   long pkAPI::_ICursiVisionSignaturePad::FireOption(long optionNumber) {
   pIConnectionPointContainer -> fire_OptionSelected(optionNumber);
   return S_OK;
   }


   long pkAPI::_ICursiVisionSignaturePad::ShowProperties() {
   return pParent -> ShowProperties();
   }

   void pkAPI::_ICursiVisionSignaturePad::OverlaySignatureBitmapHandle(UINT_PTR hBitmap,HDC hdc,long x,long y,long cx,long cy) {
   pParent -> AreaBitmapHandle(hBitmap,x,y,cx,cy);
   return;
   }

   void pkAPI::_ICursiVisionSignaturePad::DisplaySignatureBitmapHandle(UINT_PTR hBitmap,HDC hdc,long x,long y,long cx,long cy) {
   pParent -> put_BackgroundBitmapHandle(hBitmap);
   return;
   }

   void pkAPI::_ICursiVisionSignaturePad::DisplaySignatureBitmapFile(char *pszImageFile,BOOL populatePad,BOOL isLastPage) {

   return;
   }

   void pkAPI::_ICursiVisionSignaturePad::DisplayHotSpotFile(char *pszFile,long eventID,long x,long y,long cx,long cy) {
   return;
   }

   void pkAPI::_ICursiVisionSignaturePad::DisplayHotSpotHandle(UINT_PTR hBitmap,HDC hdc,long eventID,long x,long y,long cx,long cy) {
   return;
   }

   void pkAPI::_ICursiVisionSignaturePad::DisplayOk(long eventID) {
   pParent -> RemoveControl(eventID);
   pParent -> CreateButton(L"Ok",eventID,0,0,0);
   RECT rcButton = {0};
   pParent -> get_ControlBounds(eventID,&rcButton);
   rcButton.left = Width() - (rcButton.right - rcButton.left) - 4;
   rcButton.right = Width() - 8;
   rcButton.bottom = 4 + (rcButton.bottom - rcButton.top);
   rcButton.top = 4;
   POINT pt;
   pt.x = rcButton.left;
   pt.y = rcButton.top;
   pParent -> put_ControlPosition(eventID,pt);
   pParent -> ShowControl(eventID);
   return;
   }

   void pkAPI::_ICursiVisionSignaturePad::DisplayClear(long eventID) {
   pParent -> RemoveControl(eventID);
   pParent -> CreateButton(L"Clear",eventID,0,0,0);
   RECT rcButton = {0};
   pParent -> get_ControlBounds(eventID,&rcButton);
   rcButton.right = 8 + rcButton.right - rcButton.left;
   rcButton.left = 8;
   rcButton.bottom = 4 + (rcButton.bottom - rcButton.top);
   rcButton.top = 4;
   POINT pt;
   pt.x = rcButton.left;
   pt.y = rcButton.top;
   pParent-> put_ControlPosition(eventID,pt);
   pParent -> ShowControl(eventID);
   return;
   }

   void pkAPI::_ICursiVisionSignaturePad::DisplayCancel(long eventID) {
   return;
   }

   short pkAPI::_ICursiVisionSignaturePad::KeyPadQueryHotSpot(short keyCode) {
   return 0;
   }

   void pkAPI::_ICursiVisionSignaturePad::SetBackgroundFile(BSTR bstrFileName) {
   return;
   }

   void pkAPI::_ICursiVisionSignaturePad::SetBackgroundHandle(OLE_HANDLE hBitmap) {
   return;
   }

   void pkAPI::_ICursiVisionSignaturePad::GetRect(RECT *pRect) {
   if ( ! pRect )
      return;
   pRect -> left = 0;
   pRect -> top = 0;
   pRect -> right = Width();
   pRect -> bottom = Height();
   return;
   }

   void pkAPI::_ICursiVisionSignaturePad::GetHotSpot(long hotSpotNumber,RECT *) {
   return;
   }

   long pkAPI::_ICursiVisionSignaturePad::ClearInk() {
   pParent -> ClearInk();
   return S_OK;
   }

   long pkAPI::_ICursiVisionSignaturePad::SignatureData() {
   return S_OK;
   }

   void pkAPI::_ICursiVisionSignaturePad::ClearSignatureData() {
   return;
   }

   void pkAPI::_ICursiVisionSignaturePad::ClearTablet() {
   pParent -> ClearEverything();
   pParent -> put_BackgroundBitmapFile(NULL);
   return;
   }

   void pkAPI::_ICursiVisionSignaturePad::TabletState(long ts) {
   return;
   }

   BOOL pkAPI::_ICursiVisionSignaturePad::EnableTablet() {
   pParent -> Start();
   return TRUE;
   }

   BOOL pkAPI::_ICursiVisionSignaturePad::DisableTablet() {
   pParent -> Stop();
   return TRUE;
   }

   void pkAPI::_ICursiVisionSignaturePad::DeleteSignatureData() {
   return;
   }

   void pkAPI::_ICursiVisionSignaturePad::LCDRefresh(long,long x,long y,long cx,long cy) {
   pParent -> ClearArea(x,y,cx,cy);
   return;
   }