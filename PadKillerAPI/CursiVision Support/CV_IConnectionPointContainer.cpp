// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "..\pkAPI.h"

   pkAPI::_ICursiVisionSignaturePad::_IConnectionPointContainer::_IConnectionPointContainer(_ICursiVisionSignaturePad *pp) : 
     pParent(pp)
   { 
   return;
   }

   pkAPI::_ICursiVisionSignaturePad::_IConnectionPointContainer::~_IConnectionPointContainer() {
   return;
   }


   HRESULT pkAPI::_ICursiVisionSignaturePad::_IConnectionPointContainer::QueryInterface(REFIID riid,void **ppv) {
   return pParent -> QueryInterface(riid,ppv);
   }


   STDMETHODIMP_(ULONG) pkAPI::_ICursiVisionSignaturePad::_IConnectionPointContainer::AddRef() {
   return pParent -> AddRef();
   }

   STDMETHODIMP_(ULONG) pkAPI::_ICursiVisionSignaturePad::_IConnectionPointContainer::Release() {
   return pParent -> Release();
   }


   STDMETHODIMP pkAPI::_ICursiVisionSignaturePad::_IConnectionPointContainer::EnumConnectionPoints(IEnumConnectionPoints **ppEnum) {

   _IConnectionPoint *connectionPoints[1];

   *ppEnum = NULL;
 
   if ( pParent -> pParent -> pEnumConnectionPoints ) 
      delete pParent -> pParent -> pEnumConnectionPoints;
 
   connectionPoints[0] = pParent -> pParent -> pConnectionPointCursiVision;

   pParent -> pParent -> pEnumConnectionPoints = new _IEnumConnectionPoints(pParent -> pParent,connectionPoints,1);
 
   return pParent -> pParent -> pEnumConnectionPoints -> QueryInterface(IID_IEnumConnectionPoints,(void **)ppEnum);
   }
 
 
   STDMETHODIMP pkAPI::_ICursiVisionSignaturePad::_IConnectionPointContainer::FindConnectionPoint(REFIID riid,IConnectionPoint **ppCP) {
   *ppCP = NULL;
   if ( riid == IID_ISignaturePadEvents )
      return pParent -> pParent -> pConnectionPointCursiVision -> QueryInterface(IID_IConnectionPoint,(void **)ppCP);
   return CONNECT_E_NOCONNECTION;
   }


   void pkAPI::_ICursiVisionSignaturePad::_IConnectionPointContainer::fire_PenUp(long,long) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pParent -> pConnectionPointCursiVision -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      ISignaturePadEvents * pClient = reinterpret_cast<ISignaturePadEvents *>(connectData.pUnk);
      pClient -> PenUp();
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   fire_PenPoint(0,0,0.0f);
   return;
   }


   void pkAPI::_ICursiVisionSignaturePad::_IConnectionPointContainer::fire_PenDown(long,long) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pParent -> pConnectionPointCursiVision -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      ISignaturePadEvents * pClient = reinterpret_cast<ISignaturePadEvents *>(connectData.pUnk);
      pClient -> PenDown();
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   fire_PenPoint(0,0,0.0f);
   return;
   }


   void pkAPI::_ICursiVisionSignaturePad::_IConnectionPointContainer::fire_PenPoint(long x,long y,float inkWeight) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pParent -> pConnectionPointCursiVision -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) 
         break;
      ISignaturePadEvents * pClient = reinterpret_cast<ISignaturePadEvents *>(connectData.pUnk);
      pClient -> PenPoint(x,y,inkWeight);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }


   void pkAPI::_ICursiVisionSignaturePad::_IConnectionPointContainer::fire_OptionSelected(long optionNumber) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pParent -> pConnectionPointCursiVision -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) 
      return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) 
         break;
      ISignaturePadEvents * pClient = reinterpret_cast<ISignaturePadEvents *>(connectData.pUnk);
      pClient -> OptionSelected(optionNumber);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }