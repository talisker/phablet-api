// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"
#include <stddef.h>

   WNDPROC pkAPI::defaultSwatchHandler = NULL;
   WNDPROC pkAPI::defaultTextHandler = NULL;

   HBITMAP ConvertBitmap(HDC hdcSource,HBITMAP hBitmap,long bitCount);

   pkAPI::pkAPI(IUnknown *pUnkOuter,bool isVisioLogger,bool isVisioLoggerKiosk,bool isExt,bool isCV,bool instantiateOleObject) :

      connectionPointContainer(this),
      pConnectionPointExtended(NULL),
      pEnumConnectionPoints(NULL),

      isExtended(isExt),
      isCursiVision(isCV),

      pIGProperties(NULL),
      pIGPropertiesClient(NULL),
      pIGPropertyPageClient(NULL),

      pIOleClientSite_MySite(NULL),
      pIOleInPlaceSite_MySite(NULL),

      pIVisioLoggerSignaturePad(NULL),
      pIVisioLoggerKioskPad(NULL),

      pConnectionPointVisioLogger(NULL),
      pConnectionPointVisioLoggerKiosk(NULL),

      pICursiVisionSignaturePad(NULL),
      pConnectionPointCursiVision(NULL),

      hwndSettings(NULL),

      hwndSite(NULL),

      hwndClientProvidedSite(NULL),

      startProperties(0x00),

      inkWeight(2L),
      inkColor(RGB(0,0,0)),
      pixelWidth(0L),
      pixelHeight(0L),
      widthInInches(4.0),
      heightInInches(0.0),
      fontSize(12.0f),

      endProperties(0x00),

      connectAttempts(0),

      cachedPadWidth(-1L),
      cachedPadHeight(-1L),

      pIOleObject(NULL),

      pProperties(NULL),

      propertiesSize(0L),

      gdiplusToken(NULL),
      hdcGDIPlus(NULL),
      pGDIPlusGraphic(NULL),
      pGDIPlusPen(NULL),
      gdiplusStartupInput(0L),

      pTheEncoder(NULL),
      pTheEncoderParameters(NULL),

      scaleInchesWidthToPixels(0.0),
      scaleInchesHeightToPixels(0.0),

      hwndBackground(NULL),

      pIOleClientSite_HTML_Host(NULL),
      pIOleDocumentSite_HTML_Host(NULL),
      pIOleInPlaceSite_HTML_Host(NULL),
      pIOleInPlaceFrame_HTML_Host(NULL),

      pIOleObject_HTML(NULL),
      pIOleInPlaceObject_HTML(NULL),
      pIOleInPlaceActiveObject_HTML(NULL),

      pIWebBrowser(NULL),

      refCount(0)

   {
 
   memset(szwFontFamily,0,sizeof(szwFontFamily));
   memset(szDeviceIpOrNetworkName,0,sizeof(szDeviceIpOrNetworkName));
   memset(szPDFFileName,0,sizeof(szPDFFileName));

   memset(szwCommandsPort,0,sizeof(szwCommandsPort));
   memset(szwEventSourcePort,0,sizeof(szwEventSourcePort));
   memset(szwControlPort,0,sizeof(szwControlPort));

   memset(&sizelDisplay,0,sizeof(SIZEL));

   pConnectionPointExtended = new _IConnectionPoint(this,IID_IPhabletSignaturePadEvents);

   pProperties = (BYTE *)&inkWeight;

   propertiesSize = offsetof(pkAPI,endProperties) - offsetof(pkAPI,startProperties);

   char szTemp[MAX_PATH];

   GetCommonAppDataLocation(NULL,szTemp);

   if ( instantiateOleObject ) {

      pIOleObject = new _IOleObject(this); 

      sprintf(szApplicationDataDirectory,"%s\\pkAPI",szTemp);

      DWORD fileAttributes = GetFileAttributes(szApplicationDataDirectory);

      if ( fileAttributes == INVALID_FILE_ATTRIBUTES || ! ( fileAttributes & FILE_ATTRIBUTE_DIRECTORY ) )
         szApplicationDataDirectory[0] = '\0';

   } else {

      if ( isVisioLogger || isVisioLoggerKiosk ) {
         sprintf(szApplicationDataDirectory,"%s\\%s",szTemp,"VisioLogger");
         CreateDirectory(szApplicationDataDirectory,NULL);
         sprintf(szApplicationDataDirectory,"%s\\%s\\Settings",szTemp,"VisioLogger");
         CreateDirectory(szApplicationDataDirectory,NULL);
         sprintf(szApplicationDataDirectory,"%s\\%s",szTemp,"VisioLogger");
      } else {
         if ( isExtended ) {
            sprintf(szApplicationDataDirectory,"%s\\%s",szTemp,"CursiVisionPK");
            CreateDirectory(szApplicationDataDirectory,NULL);
            sprintf(szApplicationDataDirectory,"%s\\%s\\Settings",szTemp,"CursiVisionPK");
            CreateDirectory(szApplicationDataDirectory,NULL);
            sprintf(szApplicationDataDirectory,"%s\\%s",szTemp,"CursiVisionPK");
         } else if ( isCursiVision ) {
            sprintf(szApplicationDataDirectory,"%s\\%s",szTemp,"CursiVision");
            CreateDirectory(szApplicationDataDirectory,NULL);
            sprintf(szApplicationDataDirectory,"%s\\%s\\Settings",szTemp,"CursiVision");
            CreateDirectory(szApplicationDataDirectory,NULL);
            sprintf(szApplicationDataDirectory,"%s\\%s",szTemp,"CursiVision");
         } else
            szApplicationDataDirectory[0] = '\0';
      }

      HRESULT rc = E_FAIL;

      if ( isVisioLogger || isVisioLoggerKiosk )
         rc = CoCreateInstance(CLSID_InnoVisioNateVisioLoggerProperties,NULL,CLSCTX_ALL,IID_IGProperties,reinterpret_cast<void **>(&pIGProperties));
      else
         rc = CoCreateInstance(CLSID_InnoVisioNateProperties,NULL,CLSCTX_ALL,IID_IGProperties,reinterpret_cast<void **>(&pIGProperties));

      pIGPropertiesClient = new _IGPropertiesClient(this);

      pIGProperties -> Advise(static_cast<IGPropertiesClient *>(pIGPropertiesClient));

      pIGPropertyPageClient = new _IGPropertyPageClient(this);

      pIGProperties -> AdvisePropertyPageClient(pIGPropertyPageClient,true);

      char szRootName[MAX_PATH];      

      strcpy(szRootName,szModuleName);

      char *p = strrchr(szModuleName,'\\');
      if ( ! p )
         p = strrchr(szModuleName,'/');
      if ( p ) {
         strcpy(szRootName,p + 1);
      }

      p = strrchr(szRootName,'.');
      if ( p )
         *p = '\0';

      sprintf(szTemp,"%s\\Settings\\%s.settings",szApplicationDataDirectory,szRootName);

      BSTR bstrFileName = SysAllocStringLen(NULL,MAX_PATH);
      MultiByteToWideChar(CP_ACP,0,szTemp,-1,bstrFileName,MAX_PATH);

      pIGProperties -> put_FileName(bstrFileName);

      SysFreeString(bstrFileName);

      pIGProperties -> Add(L"properties",NULL);
      pIGProperties -> DirectAccess(L"properties",TYPE_BINARY,&startProperties,propertiesSize);

      short bSuccess;
      pIGProperties -> LoadFile(&bSuccess);
      if ( ! bSuccess )
         pIGPropertiesClient -> InitNew();
      
   }

   WSADATA wsaData = {0};

   if ( WSAStartup(MAKEWORD(2,2),&wsaData) ) {
      MessageBox(NULL,"There was an error starting the eventSink, WSAStartup failed","Note",MB_OK | MB_ICONEXCLAMATION);
      return;
   }

   if ( isVisioLogger ) {

      pIVisioLoggerSignaturePad = new _IVisioLoggerSignaturePad(this);
      pConnectionPointVisioLogger = new _IConnectionPoint(this,IID_IVisioLoggerSignaturePadEvents);

   }

   if ( isVisioLoggerKiosk ) {

      pIVisioLoggerKioskPad = new _IVisioLoggerKioskPad(this);
      pConnectionPointVisioLoggerKiosk = new _IConnectionPoint(this,IID_IVisioLoggerKioskPadEvents);

   }

   if ( isCursiVision ) {

      pICursiVisionSignaturePad = new _ICursiVisionSignaturePad(this);
      pConnectionPointCursiVision = new _IConnectionPoint(this,IID_ISignaturePadEvents);      

   }

   hProcessingDone = CreateSemaphore(NULL,0,1,NULL);

   return;
   }


   pkAPI::~pkAPI() {

   Disconnect();

   if ( hHeartbeatThread )
      TerminateThread(hHeartbeatThread,0);

   if ( pIGProperties ) {
      pIGProperties -> Save();
      pIGProperties -> Release();
   }

   if ( pConnectionPointExtended )
      delete pConnectionPointExtended;

   if ( pConnectionPointVisioLogger )
      delete pConnectionPointVisioLogger;

   if ( pIVisioLoggerSignaturePad )
      delete pIVisioLoggerSignaturePad;

   if ( pConnectionPointCursiVision )
      delete pConnectionPointCursiVision;

   if ( pICursiVisionSignaturePad )
      delete pICursiVisionSignaturePad;

   pStaticObject = NULL;

   if ( ! ( INVALID_HANDLE_VALUE == hMutexConnection ) )
      CloseHandle(hMutexConnection);

   return;
   }


   void pkAPI::initializeMyWindow(bool isTransparent,bool isSunkenBorder,HWND hwndContainer) {

   WNDCLASS gClass;
   
   memset(&gClass,0,sizeof(WNDCLASS));
   gClass.style = CS_BYTEALIGNCLIENT | CS_BYTEALIGNWINDOW;
   gClass.lpfnWndProc = siteHandler;
   gClass.cbClsExtra = 32;
   gClass.cbWndExtra = 32;
   gClass.hInstance = hModule;
   gClass.hIcon = NULL;
   gClass.hCursor = NULL;
   gClass.hbrBackground = 0;
   gClass.lpszMenuName = NULL;
   gClass.lpszClassName = "pkAPI";
  
   RegisterClass(&gClass);

   hwndSite = CreateWindowEx(isTransparent ? WS_EX_TRANSPARENT : isSunkenBorder ? WS_EX_CLIENTEDGE : 0L,"pkAPI","",WS_CHILD | WS_VISIBLE,0,0,0,0,hwndContainer,NULL,hModule,(void *)this);

   return;
   }


   long pkAPI::getPadWidth() {

   if ( 0 == pixelWidth && ! IsConnected() ) {
      BSTR bstrName = SysAllocStringLen(NULL,MAX_PATH);
      MultiByteToWideChar(CP_ACP,0,szDeviceIpOrNetworkName,-1,bstrName,256);
      long rc = Connect(bstrName);
      SysFreeString(bstrName);
      return pixelWidth;
   }

   WCHAR szwResult[32];

   queryValue(PARM_WIDTH_PIXELS,szwResult,32);

   if ( ! szwResult[0] )
      return pixelWidth;

   pixelWidth = _wtol(wcstok(szwResult,L" ,",NULL));

   if ( pIGProperties )
      pIGProperties -> Save();

   scaleInchesWidthToPixels = (float)pixelWidth / (float)widthInInches;

   return pixelWidth;
   }


   void pkAPI::setPadWidth(long value) {

   pixelWidth = value;

   if ( ! IsConnected() ) 
      return;

   WCHAR szwResult[32];

   swprintf(szwResult,L"%ld",pixelWidth);

   setValue(PARM_WIDTH_PIXELS,szwResult);

   return;
   }


   long pkAPI::getPadHeight() {

   if ( 0 == pixelHeight && ! IsConnected() ) {
      BSTR bstrName = SysAllocStringLen(NULL,MAX_PATH);
      MultiByteToWideChar(CP_ACP,0,szDeviceIpOrNetworkName,-1,bstrName,256);
      long rc = Connect(bstrName);
      SysFreeString(bstrName); 
      return pixelHeight;
   }

   WCHAR szwResult[32];

   queryValue(PARM_HEIGHT_PIXELS,szwResult,32);

   if ( ! szwResult[0] )
      return pixelHeight;

   pixelHeight = _wtol(wcstok(szwResult,L" ,",NULL));

   if ( pIGProperties )
      pIGProperties -> Save();

   if ( 0 == pixelWidth )
      getPadWidth();

   double aspect = (double)pixelHeight / (double)pixelWidth;

   heightInInches = aspect * widthInInches;

   scaleInchesHeightToPixels = (float)pixelHeight / (float)heightInInches;

   return pixelHeight;
   }


   void pkAPI::setPadHeight(long value) {

   pixelHeight = value;

   if ( ! IsConnected() ) 
      return;

   WCHAR szwResult[32];

   swprintf(szwResult,L"%ld",pixelHeight);

   setValue(PARM_HEIGHT_PIXELS,szwResult);

   return;
   }

   double pkAPI::getPadWidthInInches() {
   return widthInInches;
   }


   void pkAPI::teardownGDIPlus() {

   if ( ! gdiplusToken )
      return;

   if ( hdcGDIPlus )
      ReleaseDC(hwndBackground,hdcGDIPlus);

   hwndBackground = NULL;

   if ( pGDIPlusPen )
      delete pGDIPlusPen;

   if ( pGDIPlusGraphic )
      delete pGDIPlusGraphic;

   pGDIPlusPen = NULL;

   pGDIPlusGraphic = NULL;

   return;
   }


   void pkAPI::setupGDIPlus(HWND hwndDrawing) {

   teardownGDIPlus();
   
   Gdiplus::GdiplusStartup(&gdiplusToken,&gdiplusStartupInput,NULL);

   hwndBackground = hwndDrawing;

   if ( pIOleObject -> runTransparent ) 
      hwndBackground = pIOleObject -> hwndContainer;

   hdcGDIPlus = GetDC(hwndBackground);

   COLORREF color = (COLORREF)inkColor;

   float weight = (float)inkWeight;

   if ( 0.0f == weight )
      weight = 1.0f;

   pGDIPlusPen = new Gdiplus::Pen(Gdiplus::Color(GetRValue(color),GetGValue(color),GetBValue(color)),weight);

   pGDIPlusPen -> SetLineCap(Gdiplus::LineCapSquare,Gdiplus::LineCapSquare,Gdiplus::DashCapFlat);

   pGDIPlusGraphic = new Gdiplus::Graphics(hdcGDIPlus);

   pGDIPlusGraphic -> SetSmoothingMode(Gdiplus::SmoothingMode::SmoothingModeHighQuality);

   RECT rcClient = {0};

   GetWindowRect(hwndBackground,&rcClient);

   float scaleX = (float)((double)(rcClient.right - rcClient.left) / (double)getPadWidth());
   float scaleY = (float)((double)(rcClient.bottom - rcClient.top) / (double)getPadHeight());

   pGDIPlusGraphic -> ScaleTransform(scaleX,scaleY);

   return;
   }


   HRESULT pkAPI::postConnectDetermineScale() {

   if ( ! pIOleObject ) 
      return S_OK;

   if ( pIOleObject -> renderSignature && ! hwndBackground && hwndSite ) 
      setupGDIPlus(hwndSite);

   double aspectRatio = (double) getPadWidth() / (double)getPadHeight();

   sizelDisplay.cy = (long)((double)sizelDisplay.cx / aspectRatio);

   SetWindowPos(hwndSite,HWND_TOP,0,0,sizelDisplay.cx,sizelDisplay.cy,SWP_NOMOVE);

   pIOleObject -> cxScale = (double)sizelDisplay.cx / (double) getPadWidth();

   pIOleObject -> cyScale = (double)sizelDisplay.cy / (double) getPadHeight();

   return S_OK;
   }


   int pixelsToHiMetric(SIZEL *pPixels,SIZEL *phiMetric) {
   HDC hdc = GetDC(0);
   int pxlsX,pxlsY;
 
   pxlsX = GetDeviceCaps(hdc,LOGPIXELSX);
   pxlsY = GetDeviceCaps(hdc,LOGPIXELSY);
   ReleaseDC(0,hdc);
 
   phiMetric -> cx = PIXELS_TO_HIMETRIC(pPixels -> cx,pxlsX);
   phiMetric -> cy = PIXELS_TO_HIMETRIC(pPixels -> cy,pxlsY);
   return TRUE;
   }

   int hiMetricToPixel(SIZEL *phiMetric,SIZEL *pPixels) {
   HDC hdc = GetDC(0);
   int pxlsX,pxlsY;

   pxlsX = GetDeviceCaps(hdc,LOGPIXELSX);
   pxlsY = GetDeviceCaps(hdc,LOGPIXELSY);
   ReleaseDC(0,hdc);

   pPixels -> cx = HIMETRIC_TO_PIXELS(phiMetric -> cx,pxlsX);
   pPixels -> cy = HIMETRIC_TO_PIXELS(phiMetric -> cy,pxlsY);

   return TRUE;
   }