// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "..\pkAPI.h"

   pkAPI::_IVisioLoggerSignaturePad::_IConnectionPointContainer::_IConnectionPointContainer(_IVisioLoggerSignaturePad *pp) : 
     pParent(pp)
   { 
   return;
   }

   pkAPI::_IVisioLoggerSignaturePad::_IConnectionPointContainer::~_IConnectionPointContainer() {
   return;
   }


   HRESULT pkAPI::_IVisioLoggerSignaturePad::_IConnectionPointContainer::QueryInterface(REFIID riid,void **ppv) {
   return pParent -> QueryInterface(riid,ppv);
   }


   STDMETHODIMP_(ULONG) pkAPI::_IVisioLoggerSignaturePad::_IConnectionPointContainer::AddRef() {
   return pParent -> AddRef();
   }

   STDMETHODIMP_(ULONG) pkAPI::_IVisioLoggerSignaturePad::_IConnectionPointContainer::Release() {
   return pParent -> Release();
   }


   STDMETHODIMP pkAPI::_IVisioLoggerSignaturePad::_IConnectionPointContainer::EnumConnectionPoints(IEnumConnectionPoints **ppEnum) {

   _IConnectionPoint *connectionPoints[1];

   *ppEnum = NULL;
 
   if ( pParent -> pParent -> pEnumConnectionPoints ) 
      delete pParent -> pParent -> pEnumConnectionPoints;
 
   connectionPoints[0] = pParent -> pParent -> pConnectionPointVisioLogger;

   pParent -> pParent -> pEnumConnectionPoints = new _IEnumConnectionPoints(pParent -> pParent,connectionPoints,1);
 
   return pParent -> pParent -> pEnumConnectionPoints -> QueryInterface(IID_IEnumConnectionPoints,(void **)ppEnum);
   }
 
 
   STDMETHODIMP pkAPI::_IVisioLoggerSignaturePad::_IConnectionPointContainer::FindConnectionPoint(REFIID riid,IConnectionPoint **ppCP) {
   *ppCP = NULL;
   if ( riid == IID_IVisioLoggerSignaturePadEvents )
      return pParent -> pParent -> pConnectionPointVisioLogger -> QueryInterface(IID_IConnectionPoint,(void **)ppCP);
   return CONNECT_E_NOCONNECTION;
   }


   void pkAPI::_IVisioLoggerSignaturePad::_IConnectionPointContainer::fire_PenUp(long x,long y) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pParent -> pConnectionPointVisioLogger -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IVisioLoggerSignaturePadEvents * pClient = reinterpret_cast<IVisioLoggerSignaturePadEvents *>(connectData.pUnk);
      pClient -> PenUp(x,y);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }


   void pkAPI::_IVisioLoggerSignaturePad::_IConnectionPointContainer::fire_PenDown(long x,long y) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pParent -> pConnectionPointVisioLogger -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IVisioLoggerSignaturePadEvents * pClient = reinterpret_cast<IVisioLoggerSignaturePadEvents *>(connectData.pUnk);
      pClient -> PenDown(x,y);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }


   void pkAPI::_IVisioLoggerSignaturePad::_IConnectionPointContainer::fire_PenPoint(long x,long y,float inkWeight) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pParent -> pConnectionPointVisioLogger -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) 
         break;
      IVisioLoggerSignaturePadEvents * pClient = reinterpret_cast<IVisioLoggerSignaturePadEvents *>(connectData.pUnk);
      pClient -> PenPoint(x,y,inkWeight);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }


   void pkAPI::_IVisioLoggerSignaturePad::_IConnectionPointContainer::fire_OptionSelected(long optionNumber) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> pParent -> pConnectionPointVisioLogger -> EnumConnections(&pIEnum);
   if ( ! pIEnum ) 
      return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) 
         break;
      IVisioLoggerSignaturePadEvents * pClient = reinterpret_cast<IVisioLoggerSignaturePadEvents *>(connectData.pUnk);
      pClient -> OptionSelected(optionNumber);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }