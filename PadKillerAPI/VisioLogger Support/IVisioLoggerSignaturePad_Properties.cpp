// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <stddef.h>

#include "..\pkAPI.h"

   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_IsConnected(long *pResult) {
   *pResult = 1L;
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_HasLCD(long *pResult) {
   *pResult = 1L;
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_Width(long *pResult) {
   return pParent -> get_Width(pResult);
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::put_Width(long value) {
   return pParent -> put_Width(value);
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_Height(long *pResult) {
   return pParent -> get_Height(pResult);
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::put_Height(long value) {
   return pParent -> put_Height(value);
   }


   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_PadScaleX(double *pResult) {
   *pResult = 1.0;
   return S_OK;
   }


   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_PadScaleY(double *pResult) {
   *pResult = 1.0;
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_MenuMode(long *pResult) {
   *pResult = menuMode ? 1 : 0;
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::put_MenuMode(long mm) {
   menuMode = ( 1L == mm  ? true : false );
   if ( menuMode ) {
      pParent -> setValue(PARM_SEND_POINTS,0L);
      pParent -> setValue(PARM_SHOW_INK,0L);
   } else {
      pParent -> setValue(PARM_SEND_POINTS,1L);
      pParent -> setValue(PARM_SHOW_INK,1L);
   }
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_SmallFontSize(long *pResult) {
   *pResult = 16L;
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_MediumFontSize(long *pResult) {
   *pResult = 20L;
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_LargeFontSize(long *pResult) {
   *pResult = 32L;
   return S_OK;
   }


   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_SmallFontPixels(long *pResult) {
   *pResult = 20L;
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_MediumFontPixels(long *pResult) {
   *pResult = 28L;
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_LargeFontPixels(long *pResult) {
   *pResult = 48L;
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::put_BitDepth(long) {
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::get_NativeSizeImage(UINT_PTR *pImageHandle) {
   return pParent -> get_NativeSizeImage(pImageHandle);
   }