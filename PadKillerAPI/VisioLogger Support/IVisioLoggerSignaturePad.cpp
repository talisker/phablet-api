// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <stddef.h>
#include <process.h>
#include <time.h>

#include "..\pkAPI.h"


   HRESULT pkAPI::_IVisioLoggerSignaturePad::Clear() {
   pParent -> ClearEverything();
   return S_OK;
   }


   HRESULT pkAPI::_IVisioLoggerSignaturePad::ClearDisplay() {
   pParent -> ClearEverything();
   return S_OK;
   }


   HRESULT pkAPI::_IVisioLoggerSignaturePad::ShowBitmap(long x,long y,long width,long height,UINT_PTR hBitmap,long duration) {
   long padWidth;
   pParent -> get_Width(&padWidth);
   if ( x || y || ! ( width == padWidth ) )
      pParent -> AreaBitmapHandle(hBitmap,x,y,width,height);
   else
      pParent -> put_BackgroundBitmapHandle(hBitmap);
   return S_OK;
   }


   HRESULT pkAPI::_IVisioLoggerSignaturePad::AddBitmap(long x,long y,long width,long height,UINT_PTR hBitmap,long duration) {
   pParent -> AreaBitmapHandle(hBitmap,x,y,width,height);
   return S_OK;
   }


   HRESULT pkAPI::_IVisioLoggerSignaturePad::FlushBitmap() {
   return S_OK;
   }


   HRESULT pkAPI::_IVisioLoggerSignaturePad::Start() {
   pParent -> Start();
   pParent -> Show();
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::Stop() {
   pParent -> Stop();
   //pParent -> Hide();
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::Kill() {
   return Stop();
   }

   HRESULT pkAPI::_IVisioLoggerSignaturePad::ShowProperties() {
   pParent -> ShowProperties();
   return S_OK;
   }
