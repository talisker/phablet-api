// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "..\pkAPI.h"

   pkAPI::_IVisioLoggerKioskPad::_IVisioLoggerKioskPad(pkAPI *pp) :
      pParent(pp)
   {
   pIConnectionPointContainer = new _IVisioLoggerKioskPad::_IConnectionPointContainer(this);
   return;
   }

   pkAPI::_IVisioLoggerKioskPad::~_IVisioLoggerKioskPad() {
   delete pIConnectionPointContainer;
   return;
   }

   // IUnknown

   long __stdcall pkAPI::_IVisioLoggerKioskPad::QueryInterface(REFIID riid,void **ppv) {

   if ( riid == IID_IUnknown )
      *ppv = static_cast<IUnknown *>(this);
   else

   if ( riid == IID_IVisioLoggerKioskPad )
      *ppv = static_cast<IVisioLoggerKioskPad *>(this);
   else

   if ( riid == IID_IGPropertyPageClient )
      return pParent -> QueryInterface(riid,ppv);
   else

   if ( riid == IID_IGPropertiesClient )
      return pParent -> QueryInterface(riid,ppv);
   else

   if ( riid == IID_IConnectionPoint ) 
      *ppv = static_cast<IConnectionPoint *>(pParent -> pConnectionPointVisioLoggerKiosk);
   else

   if ( riid == IID_IConnectionPointContainer ) 
      *ppv = static_cast<IConnectionPointContainer *>(pIConnectionPointContainer);
   else

      return E_NOINTERFACE;

   AddRef();

   return S_OK;
   }

   unsigned long __stdcall pkAPI::_IVisioLoggerKioskPad::AddRef() {
   return pParent -> AddRef();
   }

   unsigned long __stdcall pkAPI::_IVisioLoggerKioskPad::Release() { 
   return pParent -> Release();
   }