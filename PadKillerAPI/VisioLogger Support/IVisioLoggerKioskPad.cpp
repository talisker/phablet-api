// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "..\pkAPI.h"

   HRESULT pkAPI::_IVisioLoggerKioskPad::Connect(BSTR address) {
   return pParent -> Connect(address);
   }


   HRESULT pkAPI::_IVisioLoggerKioskPad::HostWidth(long cx) {
   WCHAR szwValue[128];
   swprintf(szwValue,L"%ld %ld %ld",SETPARAMETER,PARM_UI_HOST_WIDTH,cx);
   if ( ! ( S_OK == pParent -> sendCommand(szwValue) ) )
      return E_FAIL;
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerKioskPad::HostHeight(long cy) {
   WCHAR szwValue[128];
   swprintf(szwValue,L"%ld %ld %ld",SETPARAMETER,PARM_UI_HOST_HEIGHT,cy);
   if ( ! ( S_OK == pParent -> sendCommand(szwValue) ) )
      return E_FAIL;
   return S_OK;
   }

   HRESULT pkAPI::_IVisioLoggerKioskPad::put_Background(UINT_PTR hBitmap) {
   pParent -> put_BackgroundBitmapHandle(hBitmap);
   return S_OK;
   }
