// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "..\pkAPI.h"

   pkAPI::_IVisioLoggerSignaturePad::_IVisioLoggerSignaturePad(pkAPI *pp) :
      pParent(pp),
      menuMode(false)
   {
   pIConnectionPointContainer = new _IVisioLoggerSignaturePad::_IConnectionPointContainer(this);
   return;
   }

   pkAPI::_IVisioLoggerSignaturePad::~_IVisioLoggerSignaturePad() {
   delete pIConnectionPointContainer;
   return;
   }

   // IUnknown

   long __stdcall pkAPI::_IVisioLoggerSignaturePad::QueryInterface(REFIID riid,void **ppv) {

   if ( riid == IID_IUnknown )
      *ppv = static_cast<IUnknown *>(this);
   else

   if ( riid == IID_IVisioLoggerSignaturePad )
      *ppv = static_cast<IVisioLoggerSignaturePad *>(this);
   else

   if ( riid == IID_IGPropertyPageClient )
      return pParent -> QueryInterface(riid,ppv);
   else

   if ( riid == IID_IGPropertiesClient )
      return pParent -> QueryInterface(riid,ppv);
   else

   if ( riid == IID_IConnectionPoint ) 
      *ppv = static_cast<IConnectionPoint *>(pParent -> pConnectionPointVisioLogger);
   else

   if ( riid == IID_IConnectionPointContainer ) 
      *ppv = static_cast<IConnectionPointContainer *>(pIConnectionPointContainer);
   else

      return E_NOINTERFACE;

   AddRef();

   return S_OK;
   }

   unsigned long __stdcall pkAPI::_IVisioLoggerSignaturePad::AddRef() {
   return pParent -> AddRef();
   }

   unsigned long __stdcall pkAPI::_IVisioLoggerSignaturePad::Release() { 
   return pParent -> Release();
   }