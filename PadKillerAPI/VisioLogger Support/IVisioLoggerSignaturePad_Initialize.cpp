// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "..\pkAPI.h"

   HRESULT pkAPI::_IVisioLoggerSignaturePad::Initialize(BSTR padName,UINT_PTR hwndHost) {

 #if 0
   pParent -> pixelWidth = width;
   pParent -> pixelHeight = height;

   WideCharToMultiByte(CP_ACP,0,padName,-1,pParent -> szDeviceIpOrNetworkName,MAX_PATH,0,0);

   pParent -> Connect(padName);
#endif

   pParent -> initializeMyWindow(true,false,(HWND)hwndHost);

   pParent -> Connect(NULL);

   return S_OK;
   }
