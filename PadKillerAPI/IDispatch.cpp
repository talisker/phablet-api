// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   // IDispatch

   STDMETHODIMP pkAPI::GetTypeInfoCount(UINT * pctinfo) { 
   *pctinfo = 1;
   return S_OK;
   } 


   long __stdcall pkAPI::GetTypeInfo(UINT itinfo,LCID lcid,ITypeInfo **pptinfo) { 

   *pptinfo = NULL; 

   if ( itinfo != 0 ) 
      return DISP_E_BADINDEX; 

   if ( ! pITypeInfo_IBasicSignaturePad ) {

   }

   *pptinfo = pITypeInfo_IBasicSignaturePad;

   pITypeInfo_IBasicSignaturePad -> AddRef();

   return S_OK; 
   } 
 

   STDMETHODIMP pkAPI::GetIDsOfNames(REFIID riid,OLECHAR** rgszNames,UINT cNames,LCID lcid, DISPID* rgdispid) { 
   return DispGetIDsOfNames(pITypeInfo_IBasicSignaturePad, rgszNames, cNames, rgdispid); 
   }


   STDMETHODIMP pkAPI::Invoke(DISPID dispidMember, REFIID riid, LCID lcid, 
                                                                        WORD wFlags,DISPPARAMS FAR* pdispparams, VARIANT FAR* pvarResult,
                                                                         EXCEPINFO FAR* pexcepinfo, UINT FAR* puArgErr) {
   IDispatch *ppv;
   QueryInterface(IID_IDispatch,reinterpret_cast<void**>(&ppv));
   HRESULT hr = pITypeInfo_IBasicSignaturePad -> Invoke(ppv,dispidMember,wFlags,pdispparams,pvarResult,pexcepinfo,puArgErr);
   ppv -> Release();
   return hr;
   }

