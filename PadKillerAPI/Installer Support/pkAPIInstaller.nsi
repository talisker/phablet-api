
!define ARCH x64
!define CONFIGURATION Release
!define OUTFILE pkAPI64.exe


!define PRODUCT_NAME "PK API"

!define PRODUCT_VERSION "4.00"
!define PRODUCT_PUBLISHER "InnoVisioNate"
!define PRODUCT_WEB_SITE "http://www.innovisionate.com"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\pkAPI.ocx"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_MINOR_VERSION "0"
!define PRODUCT_MAJOR_VERSION "2"

!include "MUI.nsh"

!include "WinVer.nsh"

!include WordFunc.nsh
!insertmacro VersionCompare
!include LogicLib.nsh

!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

!insertmacro MUI_PAGE_WELCOME

!insertmacro MUI_PAGE_INSTFILES

!define MUI_FINISHPAGE_RUN_NOTCHECKED

;!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

!define MULTIUSER_EXECUTIONLEVEL Admin

!include MultiUser.nsh

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"

InstallDir "$PROGRAMFILES\InnoVisioNate\pkAPI"

InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""

ShowInstDetails show

ShowUnInstDetails show

Var PostInstallDLL

Page custom installedAPK "" " - Install the APK"

Page custom installedSamples "" " - Installed sample applications"

!ifdef INNER

  !echo "Inner invocation"

  OutFile "D:\temp\tempinstaller.exe"

!else

  !echo "Outer invocation"

  !system "$\"${NSISDIR}\makensis$\" /DINNER pkAPIInstaller.nsi" = 0

  !system "D:\temp\tempinstaller.exe" = 2

  !system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "D:\temp\PKAPIUninst.exe"'

   OutFile "${OUTFILE}"

!endif

Function .onInit

!ifdef INNER
  WriteUninstaller "D:\temp\PKAPIUninst.exe"
  Quit
!endif

!insertmacro MUI_LANGDLL_DISPLAY

!insertmacro MULTIUSER_INIT

   SetShellVarContext all
   
${If} 'x64' == '${ARCH}'

   StrCpy $INSTDIR "$PROGRAMFILES64\InnoVisioNate\pkAPI"

   StrCpy $PostInstallDLL "PostInstall32"

${Else}

   strCpy $PostInstallDLL "PostInstall"

${EndIf}

   SetOutPath "$INSTDIR"

   File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PostInstall*.dll"

   Return
   
FunctionEnd


Section "InnoVisioNate" SEC01
  
  SetOutPath "$INSTDIR"

  SetDetailsView show
 
  SetOverwrite ifnewer

!ifdef INNER
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkAPI.ocx"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PostInstall.dll"'
${If} 'x64' == '${ARCH}'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkAPI32.ocx"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PostInstall32.dll"'
${EndIf}
!endif

  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkAPI.ocx"

${If} 'x64' == '${ARCH}'
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkAPI32.ocx"
  RegDll "pkAPI32.ocx"
${EndIf}

  File "PhabletInstaller1.ini"
  File "PhabletInstaller2.ini"

  RegDll "pkAPI.ocx"
  
  CreateDirectory "$INSTDIR\Samples"
  
  SetOutPath "$INSTDIR\Samples"
  
;  File "\Phablet-API-Samples\${CONFIGURATION}\*.exe"
;  File "\Phablet-API-Samples\${CONFIGURATION}\*.dll"
;  File "\Phablet-API-Samples\${CONFIGURATION}\*.config"
  File "\Phablet-API-Samples\Debug\*.exe"
  File "\Phablet-API-Samples\Debug\*.dll"
  File "\Phablet-API-Samples\Debug\*.config"

;     ${If} 'debug' == '${Configuration}'
     
  File "C:\Windows\SysWOW64\vcruntime140d.dll"
  File "C:\Windows\SysWOW64\ucrtbased.dll"

;     ${Else}
;
;        File "C:\Windows\SysWOW64\vcruntime140.dll"
;        File "C:\Windows\SysWOW64\ucrtbase.dll"
;
;     ${EndIf}

  CreateDirectory "$APPDATA\pkAPI"

  CreateDirectory "$APPDATA\pkAPI\Documentation"

  CreateDirectory "$APPDATA\pkAPI\Samples"

  SetOutPath "$APPDATA\pkAPI\Samples\C++ Simple Signature Client"

  File /r /x *.user /x Debug /x *.ncb /x Release "\Phablet-API-Samples\C++ Simple Signature Client\*.*"
  
  File "\Common\Include\pkAPI_i.*"

  SetOutPath "$APPDATA\pkAPI\Samples\vbSample - ezSign"

  File /r /x *.user /x *.ncb /x obj /x bin "\Phablet-API-Samples\vbSample - ezSign\*.*"

  SetOutPath "$APPDATA\pkAPI\Samples\vbSample - Phablet GUI"

  File /r /x *.user /x *.ncb /x obj /x bin "\Phablet-API-Samples\vbSample - Phablet GUI\*.*"
  
  SetOutPath "$APPDATA\pkAPI\Samples"
  
  File "\Phablet-API-Samples\*.sln"

  SetOutPath "$APPDATA\pkAPI\Documentation"
  
  File "\Phablet-API\PadKillerAPI\Documentation\*.*"
  
  SetOutPath "$APPDATA\pkAPI"
  
  File "\Phablet-API\PhabletSignaturePad\app\build\outputs\apk\PhabletSignaturePad.apk"

  SetOutPath "$INSTDIR"

  System::Call '$PostInstallDLL::fixupLinks(t "$APPDATA\pkAPI\Documentation",t "PhabletSignaturePad") i .r1'

  System::Call '$PostInstallDLL::GrantFullAccess(t "$APPDATA\pkAPI") i .r1'

SectionEnd

Function installedAPK

  GetDlgItem $1 $HWNDPARENT 1

  ${NSD_SetText} $1 "More"

  InstallOptions::dialog "$INSTDIR\PhabletInstaller1.ini"
  
  Delete "$INSTDIR\PhabletInstaller1.ini"

FunctionEnd

Function installedSamples

  GetDlgItem $1 $HWNDPARENT 1

  ${NSD_SetText} $1 "Finish"

  InstallOptions::dialog "$INSTDIR\PhabletInstaller2.ini"

  Delete "$INSTDIR\PhabletInstaller2.ini"

FunctionEnd

Section -Post

!ifndef INNER
  SetOutPath $INSTDIR
  File "D:\temp\PKAPIUninst.exe"
  !system 'del "D:\temp\PKAPIUninst.exe"'
  !system 'del "D:\temp\tempinstaller.exe"'
!endif

  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\PKAPIUninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\PKAPIUninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\pkAPI.ocx"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "VersionMajor" "${PRODUCT_MAJOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "VersionMinor" "${PRODUCT_MINOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "MajorVersion" "${PRODUCT_MAJOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "MinorVersion" "${PRODUCT_MINOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "InstallLocation" "$INSTDIR"

SectionEnd


Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) has been removed."
FunctionEnd

Function un.onInit
!insertmacro MULTIUSER_UNINIT
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Are you sure you want to remove $(^Name)?" IDYES +2
  Abort
FunctionEnd

Section Uninstall

  UnRegDLL "$INSTDIR\pkAPI.ocx"
  
${If} 'x64' == '${ARCH}'
  UnRegDLL "$INSTDIR\pkAPI32.ocx"
  Delete "$INSTDIR\pkAPI32.ocx"
${EndIf}

  Delete "$INSTDIR\PKAPIUninst.exe"
  
  Delete "$INSTDIR\Samples\*.*"
  
  RMDir "$INSTDIR\Samples"

  Delete "$APPDATA\pkAPI\Documentation\*.*"
  
  Delete "$APPDATA\pkAPI\*.*"

  RMDir "$APPDATA\pkAPI\Documentation"

  Delete "$INSTDIR\pkAPI.ocx"
  
  Delete "$INSTDIR\pkAPI.ocx"

  Delete "$INSTDIR\PostInstall.dll"

  RMDir "$INSTDIR"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
  DeleteRegKey HKLM "Software\InnoVisioNate\PK"
  SetAutoClose true
  
  System::Call 'Shell32::SHChangeNotify(i 0x8000000, i 0, i 0, i 0)'

SectionEnd

