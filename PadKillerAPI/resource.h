// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#define IDD_PAD_PROPERTIES             1800
#define IDDI_PAD_PROPERTIES_ACCEPT     1801
#define IDDI_PAD_PROPERTIES_HOSTNAME   1802
#define IDDI_PAD_PIXEL_WIDTH_LABEL     1803
#define IDDI_PAD_PIXEL_WIDTH           1804
#define IDDI_PAD_PIXEL_HEIGHT_LABEL    1806
#define IDDI_PAD_PIXEL_HEIGHT          1807
#define IDDI_PAD_NOT_CONNECTED         1808
#define IDDI_PAD_INCH_LABEL_WIDTH      1809
#define IDDI_PAD_INCH_DIMENSIONS_LABEL 1810
#define IDDI_PAD_INCH_WIDTH            1811
#define IDDI_PAD_INCH_INSTRUCTIONS     1812
#define IDDI_PAD_CONNECT               1813
#define IDDI_PAD_DIMENSIONS_INFO       1814

#define IDDI_PAD_TAB_CONTROL           1815
#define IDDI_ALWAYS_AVAILABLE_TEXT_ITEM   1816

#define IDD_PAD_INK_PROPERTIES         1000

#define IDDI_INK_PROPERTIES_SET        1820

#define IDD_ACTIVEX_CONTROL_PROPERTIES    2000
#define IDDI_ACTIVEX_RENDER_SIGNATURE     2001
#define IDDI_ACTIVEX_TRANSPARENT          2002
#define IDDI_ACTIVEX_RENDER_BACKGROUND    2003
#define IDDI_ACTIVEX_RENDER_CONTROLS      2004
#define IDDI_ACTIVEX_SUNKEN_BORDER        2005

#define IDDI_ACTIVEX_RUNTIME_COMMAND_BASE 2100

#define IDD_DOCUMENTATION                 3000
#define IDDI_DOCUMENTATION_PANEL          3001

#define PROPERTY_PAGE_WIDTH         360
#define PROPERTY_PAGE_HEIGHT        380

#define IDDI_INK_PROPERTIES_OK                  1001
#define IDDI_INK_PROPERTIES_CANCEL              1002
#define IDDI_INK_PROPERTIES_COLOR_CHOOSE        1003
#define IDDI_INK_PROPERTIES_COLOR_SWATCH        1004
#define IDDI_INK_PROPERTIES_WEIGHT              1005
#define IDDI_INK_PROPERTIES_WEIGHT_SPIN         1006

#define WM_PEN_UP_EVENT                (WM_USER + 1)
#define WM_PEN_DOWN_EVENT              (WM_USER + 2)
#define WM_PEN_POINT_EVENT             (WM_USER + 3)
#define WM_OPTION_EVENT                (WM_USER + 4)
#define WM_OPTION_UNSELECTED_EVENT     (WM_USER + 5)
#define WM_DEVICE_READY_EVENT          (WM_USER + 6)
#define WM_CONFIGURATION_CHANGED_EVENT (WM_USER + 7)
#define WM_ITEM_SELECTED_EVENT         (WM_USER + 8)
#define WM_TEXT_CHANGED_EVENT          (WM_USER + 9)
