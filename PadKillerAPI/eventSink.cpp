// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   void pkAPI::openEventSink() {
   closeEventSink();
   unsigned threadAddress;
   hEventSinkThread = (HANDLE)_beginthreadex(NULL,16384,_eventSink,(void *)this,CREATE_SUSPENDED,&threadAddress);
   ResumeThread(hEventSinkThread);
   WaitForSingleObject(hProcessingDone,INFINITE);
   return;
   }

   void pkAPI::closeEventSink() {
   if ( hEventSinkThread ) {
      eventSinkStopRequested = 1L;
      if ( ! ( INVALID_SOCKET == eventSinkSocket ) ) {
         shutdown(eventSinkSocket,SD_BOTH);
         closesocket(eventSinkSocket);
      }
      eventSinkSocket = INVALID_SOCKET;
      WaitForSingleObject(hEventSinkThread,INFINITE);
      hEventSinkThread = NULL;
   }
   return;
   }

   unsigned int __stdcall pkAPI::_eventSink(void *p) {

   pkAPI *pThis = reinterpret_cast<pkAPI *>(p);

   WCHAR szwInput[32768];

   eventSinkSocket = pThis -> connectPort(pThis -> szDeviceIpOrNetworkName,pThis -> szwEventSourcePort);

   bool clientConnected = true;

   eventSinkStopRequested = 0L;

   ReleaseSemaphore(hProcessingDone,1,NULL);

   while ( clientConnected && ! eventSinkStopRequested ) {

      memset(szwInput,0,sizeof(szwInput));

      long rc = recv(eventSinkSocket,(char *)szwInput,32768,0L);

      if ( rc < 0 )
         break;

      send(eventSinkSocket,(char *)L"ok",4,0);

      pkEvents eventId = (pkEvents)_wtol(szwInput);

      switch ( eventId ) {
      case PENUP: {
         wcstok(szwInput,L" ",NULL);
         long x = _wtol(wcstok(NULL,L" ",NULL));
         long y = _wtol(wcstok(NULL,L" ",NULL));
         pThis -> penUp(x,y);
         }
         break;

      case PENDOWN: {
         wcstok(szwInput,L" ",NULL);
         long x = _wtol(wcstok(NULL,L" ",NULL));
         long y = _wtol(wcstok(NULL,L" ",NULL));
         pThis -> penDown(x,y);
         }
         break;

      case PENPOINT: {
         wcstok(szwInput,L" ",NULL);
         long x = _wtol(wcstok(NULL,L" ",NULL));
         long y = _wtol(wcstok(NULL,L" ",NULL));
         float inkWeight = 0.0;
         WCHAR *p = wcstok(NULL,L" ",NULL);
         if ( p )
            inkWeight = (float)_wtof(p);
         pThis -> penPoint(x,y,inkWeight);
         }
         break;

      case OPTIONSELECTED: {
         wcstok(szwInput,L" ",NULL);
         pThis -> optionSelected(_wtol(wcstok(NULL,L" ",NULL)));
         }
         break;

      case OPTIONUNSELECTED: {
         wcstok(szwInput,L" ",NULL);
         WCHAR *p = wcstok(NULL,L" ",NULL);
         if ( p )
            pThis -> optionUnSelected(_wtol(p));
         }
         break;

      case DEVICEREADY: 
         pThis -> deviceReady();
         break;

      case CONFIGURATIONCHANGED: 
         pThis -> configurationChanged();
         break;

      case ITEMSELECTED: {
         wcstok(szwInput,L" ",NULL);
         long controlId = _wtol(wcstok(NULL,L" ",NULL));
         WCHAR *pszItem = wcstok(NULL,L"��",NULL);
         pThis -> itemSelected(controlId,pszItem);
         }
         break;

      case TEXTCHANGED: {

         WCHAR *pNew = wcsrchr(szwInput,L'�') + 1;
         WCHAR *p = wcschr(pNew,L'�');
         *p = L'\0';

         WCHAR *pszOldText = NULL;
         WCHAR *pszNewText = NULL;

         if ( wcsstr(szwInput,L"�� �") ) {
            pszOldText = new WCHAR[1];
            pszOldText[0] = L'\0';
         }

         wcstok(szwInput,L" ",NULL);

         long controlId = _wtol(wcstok(NULL,L" ",NULL));

         if ( ! pszOldText ) {
            p = wcstok(NULL,L"��",NULL);
            pszOldText = new WCHAR[wcslen(p) + 1];
            wcscpy(pszOldText,p);
         }

         pszNewText = new WCHAR[wcslen(pNew) + 1];
         wcscpy(pszNewText,pNew);

         pThis -> textChanged(controlId,pszOldText,pszNewText);
         }
         break;

      case KEYSTROKE: {
Beep(2000,100);
         }
         break;

      default:
         break;
      }

   }

   if ( ! ( INVALID_SOCKET == eventSinkSocket ) ) {
      shutdown(eventSinkSocket,SD_BOTH);
      closesocket(eventSinkSocket);
   }

   eventSinkSocket = INVALID_SOCKET;

   eventSinkStopRequested = 0L;

   return 0L;
   }