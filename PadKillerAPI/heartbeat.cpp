// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   long timerClientAbandoned = TIMER_CLIENT_ABANDONED_TIMEOUT / 8;

   void pkAPI::startHeartbeat() {
   long rc = 0;
   char szResponse[256];
   if ( INVALID_SOCKET == controlSocket ) {
      controlSocket = connectPort(szDeviceIpOrNetworkName,szwControlPort);
      if ( INVALID_SOCKET == controlSocket )
         return;
      memset(szResponse,0,sizeof(szResponse));
      rc = recv(controlSocket,szResponse,256,0);
      char *p = strtok(szResponse,"heartbeats:");
      if ( p )
         timerClientAbandoned = atol(p) / 8;
   }
   stopHeartbeat(true);
   unsigned threadAddress;
   hHeartbeatThread = (HANDLE)_beginthreadex(NULL,0,_heartbeat,(void *)this,CREATE_SUSPENDED,&threadAddress);
   ResumeThread(hHeartbeatThread);
   WaitForSingleObject(hProcessingDone,INFINITE);
   return;
   }

   
   void pkAPI::stopHeartbeat(bool waitForCompletion) {
   if ( INVALID_SOCKET == controlSocket )
      return;
   if ( hHeartbeatThread ) {
      heartbeatStopRequested = 1L;
      if ( ! ( INVALID_SOCKET == controlSocket ) ) {
         shutdown(controlSocket,SD_BOTH);
         closesocket(controlSocket);
      }
      controlSocket = INVALID_SOCKET;
      if ( waitForCompletion ) {
         WaitForSingleObject(hHeartbeatThread,INFINITE);
         hHeartbeatThread = NULL;
      }
      return;
   }
   heartbeatStopRequested = 0L;
   return;
   }


   unsigned int __stdcall pkAPI::_heartbeat(void *p) {

   pkAPI *pThis = reinterpret_cast<pkAPI *>(p);

   ReleaseSemaphore(hProcessingDone,1,NULL);

   long interimCount = 0L;

   while ( ! heartbeatStopRequested ) {

      interimCount++;

      if ( interimCount % 4 ) {
         Sleep(timerClientAbandoned);
         continue;
      }

      interimCount = 0L;

      char szCommand[256];

      sprintf(szCommand,"0009");

      long rc = send(controlSocket,szCommand,4,0);

      if ( 0 > rc )
         break;

      sprintf(szCommand,"heartbeat");

      rc = send(controlSocket,szCommand,9,0);

      if ( 0 > rc )
         break;

      rc = recv(controlSocket,szCommand,256,0);

      if ( 0 > rc )
         break;

      Sleep(timerClientAbandoned);

   }

   heartbeatStopRequested = 0L;

   hHeartbeatThread = NULL;

   return 0L;
   }