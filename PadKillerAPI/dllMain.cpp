// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#define DEFINE_DATA

#include "pkAPI.h"

#include "pkAPI_i.c"
#include "SignaturePad_i.c"
#include "pkAPI_VisioLogger_i.c"

#include "properties_i.c"

char OBJECT_NAME[][128] = {"InnoVisioNate.PhabletSignaturePad",
                                 "InnoVisioNate.PhabletSignaturePadPropertyPage",
                                 "InnoVisioNate.PhabletSignaturePadRuntimePropertyPage",
                                 "InnoVisioNate.PhabletSignaturePadDocumentationPropertyPage",
                                 "InnoVisioNate.ExtendedPhabletSignaturePad",
                                 "InnoVisioNate.VisioLoggerPhabletSignaturePad",
                                 "InnoVisioNate.InnoVisioNateSignaturePad",
                                 "InnoVisioNate.VisioLoggerPhabletKioskPad"};

char OBJECT_NAME_V[][128] = {"InnoVisioNate.PhabletSignaturePad.1",
                                 "InnoVisioNate.PhabletSignaturePadPropertyPage.1",
                                 "InnoVisioNate.PhabletSignaturePadRuntimePropertyPage.1",
                                 "InnoVisioNate.PhabletSignaturePadDocumentationPropertyPage.1",
                                 "InnoVisioNate.ExtendedPhabletSignaturePad.1",
                                 "InnoVisioNate.VisioLoggerPhabletSignaturePad.1",
                                 "InnoVisioNate.InnoVisioNateSignaturePad.1",
                                 "InnoVisioNate.VisioLoggerPhabletKioskPad.1"};

char OBJECT_VERSION[][128] = {"1.0","1.0","1.0","1.0","1.0","1.0","1.0"};

char OBJECT_DESCRIPTION[][128] = {"InnoVisioNate Phablet Signature Pad Object",
                                    "InnoVisioNate Phablet Signature Pad Property Page",
                                    "InnoVisioNate Phablet Signature Pad Runtime Property Page",
                                    "InnoVisioNate Phablet Signature Pad Documentation Property Page",
                                    "InnoVisioNate Extended Phablet Signature Pad",
                                    "InnoVisioNate Phablet Signature Pad for Visio Logger",
                                    "InnoVisioNate Phablet Signature Pad for CursiVIsion",
                                    "InnoVisioNate Phablet Kiosk Pad for Visio Logger"};

CLSID OBJECT_CLSID[] = {CLSID_PhabletSignaturePad,
                                 CLSID_PhabletSignaturePadPropertyPage,
                                 CLSID_PhabletSignaturePadRuntimePropertyPage,
                                 CLSID_PhabletSignaturePadDocumentationPropertyPage,
                                 CLSID_ExtendedPhabletSignaturePad,
                                 CLSID_SignaturePad,
                                 CLSID_CursiVisionSignaturePad,
                                 CLSID_KioskPad,
                                 GUID_NULL};

CLSID OBJECT_LIBID[] = {LIBID_PhabletSignaturePad,LIBID_InnoVisioNatePhabletSignaturePad};

#define DEFINE_DATA

   extern "C" BOOL WINAPI DllMain(HINSTANCE hI, DWORD dwReason, LPVOID) {

   switch ( dwReason ) {

   case DLL_PROCESS_ATTACH: {

      hModule = hI;

      GetModuleFileName(hModule,szModuleName,1024);

      memset(wstrModuleName,0,sizeof(wstrModuleName));

      MultiByteToWideChar(CP_ACP, 0, szModuleName, -1, wstrModuleName,(DWORD)strlen(szModuleName));  

      ITypeLib *ptLib;
      LoadTypeLib(wstrModuleName,&ptLib);
      if ( ptLib )
         HRESULT hr = ptLib -> GetTypeInfoOfGuid(IID_IBasicSignaturePad,&pITypeInfo_IBasicSignaturePad);

      }

      break;
  
   case DLL_PROCESS_DETACH:
      if ( pStaticObject )
         pStaticObject -> Release();
      pStaticObject = NULL;
      break;
  
   }
  
   return TRUE;
   }
  
  
   extern "C" int  __cdecl GetDocumentsLocation(HWND hwnd,char *szFolderLocation) {
   GetLocation(hwnd,CSIDL_PERSONAL,szFolderLocation);
   return 0;
   }

   extern "C" int __cdecl GetCommonAppDataLocation(HWND hwnd,char *szFolderLocation) {
   GetLocation(hwnd,CSIDL_COMMON_APPDATA,szFolderLocation);
   return 0;
   }

   int GetLocation(HWND hwnd,long key,char *szFolderLocation) {

   ITEMIDLIST *ppItemIDList;
   IShellFolder *pIShellFolder;
   LPCITEMIDLIST pcParentIDList;

   HRESULT wasInitialized = CoInitialize(NULL);

   szFolderLocation[0] = '\0';

   HRESULT rc = SHGetFolderLocation(hwnd,key,NULL,0,&ppItemIDList);
 
   if ( S_OK != rc ) 
      return 0;

   rc = SHBindToParent(ppItemIDList, IID_IShellFolder, (void **) &pIShellFolder, &pcParentIDList);
   if ( S_OK != rc )
      return 0;

   STRRET strRet;
   rc = pIShellFolder -> GetDisplayNameOf(pcParentIDList,SHGDN_FORPARSING,&strRet);
   pIShellFolder -> Release();
   if ( S_OK != rc ) 
      return 0;

   WideCharToMultiByte(CP_ACP,0,strRet.pOleStr,-1,szFolderLocation,MAX_PATH,0,0);

   if ( S_FALSE == wasInitialized )
      CoUninitialize();

   return 0;
   }

  
   class Factory : public IClassFactory {
   public:
  
      STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
      STDMETHOD_ (ULONG, AddRef)();
      STDMETHOD_ (ULONG, Release)();
      STDMETHOD (CreateInstance)(IUnknown *punkOuter, REFIID riid, void **ppv);
      STDMETHOD (LockServer)(BOOL fLock);
  
      Factory(REFCLSID myObj) : refCount(0),myObject(myObj) {};
      ~Factory() {};
  
   private:
      CLSID myObject;
      int refCount;
   };
  
  
   static Factory factory(OBJECT_CLSID[0]);
   static Factory propertyPageFactory(OBJECT_CLSID[1]);
   static Factory runtimePropertyPageFactory(OBJECT_CLSID[2]);
   static Factory documentationPropertyPageFactory(OBJECT_CLSID[3]);
   static Factory extendedFactory(OBJECT_CLSID[4]);
   static Factory visioLoggerFactory(OBJECT_CLSID[5]);
   static Factory cursiVisionFactory(OBJECT_CLSID[6]);
   static Factory visioLoggerKioskFactory(OBJECT_CLSID[7]);
  
   STDAPI DllCanUnloadNow(void) {
   return S_OK;
   }
  
  
  
   STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID * ppObject) {
   *ppObject = NULL;
   if ( rclsid == OBJECT_CLSID[0] )
      return factory.QueryInterface(riid,ppObject);
   if ( rclsid == OBJECT_CLSID[1] )
      return propertyPageFactory.QueryInterface(riid,ppObject);
   if ( rclsid == OBJECT_CLSID[2] )
      return runtimePropertyPageFactory.QueryInterface(riid,ppObject);
   if ( rclsid == OBJECT_CLSID[3] )
      return documentationPropertyPageFactory.QueryInterface(riid,ppObject);
   if ( rclsid == OBJECT_CLSID[4] )
      return extendedFactory.QueryInterface(riid,ppObject);
   if ( rclsid == OBJECT_CLSID[5] )
      return visioLoggerFactory.QueryInterface(riid,ppObject);
   if ( rclsid == OBJECT_CLSID[6] )
      return cursiVisionFactory.QueryInterface(riid,ppObject);
   if ( rclsid == OBJECT_CLSID[7] )
      return visioLoggerKioskFactory.QueryInterface(riid,ppObject);
   return CLASS_E_CLASSNOTAVAILABLE;
   }
  
  
   STDAPI DllRegisterServer() {

   HRESULT rc = S_OK;
   ITypeLib *ptLib;
   HKEY keyHandle,clsidHandle;
   DWORD disposition;
   char szTemp[256],szCLSID[256];
   LPOLESTR oleString;
  
   for ( long libIndex = 0; libIndex < 2; libIndex++ ) {

      wchar_t szLib[256];

      wsprintfW(szLib,L"%s\\%ld",wstrModuleName,libIndex + 1);

      if ( S_OK != (rc = LoadTypeLib(szLib,&ptLib)) ) {
         rc = ResultFromScode(SELFREG_E_TYPELIB);
         wchar_t szMessage[256];
         wsprintfW(szMessage,L"The type library in %s did not load and therefore will not register properly.\n\nThe error code from windows is %ld",szLib,rc);
         MessageBoxW(NULL,szMessage,L"Error",MB_ICONEXCLAMATION);
         return E_FAIL;
      } else {
         if ( S_OK != (rc = RegisterTypeLib(ptLib,szLib,NULL)) ) {
            wchar_t szMessage[256];
            wsprintfW(szMessage,L"The type library in %s did not register properly.\n\nThe error code from windows is %ld",szLib,rc);
            MessageBoxW(NULL,szMessage,L"Error",MB_ICONEXCLAMATION);
            return E_FAIL;
         }
      }

   }

   for ( long objIndex = 0; 1; objIndex++ ) {

      if ( GUID_NULL == OBJECT_CLSID[objIndex] )
         break;

      StringFromCLSID(OBJECT_CLSID[objIndex],&oleString);

      WideCharToMultiByte(CP_ACP,0,oleString,-1,szCLSID,256,0,0);

      rc = RegOpenKeyEx(HKEY_CLASSES_ROOT,"CLSID",0,KEY_CREATE_SUB_KEY,&keyHandle);
    
         rc = RegCreateKeyEx(keyHandle,szCLSID,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&clsidHandle,&disposition);
         sprintf(szTemp,OBJECT_DESCRIPTION[objIndex]);
         rc = RegSetValueEx(clsidHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
    
         sprintf(szTemp,"Control");
         rc = RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
         sprintf(szTemp,"");
         RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
    
         sprintf(szTemp,"ProgID");
         rc = RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
         sprintf(szTemp,OBJECT_NAME_V[objIndex]);
         rc = RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
    
         sprintf(szTemp,"InprocServer");
         RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
         RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szModuleName,(DWORD)strlen(szModuleName));
    
         sprintf(szTemp,"InprocServer32");
         rc = RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
         rc = RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szModuleName,(DWORD)strlen(szModuleName));
//         RegSetValueEx(keyHandle,"ThreadingModel",0,REG_SZ,(BYTE *)"Free",5);
         rc = RegSetValueEx(keyHandle,"ThreadingModel",0,REG_SZ,(BYTE *)"Apartment",9);
    
         sprintf(szTemp,"LocalServer");
         rc = RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
         rc = RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szModuleName,(DWORD)strlen(szModuleName));
       
         sprintf(szTemp,"TypeLib");
         rc = RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
       
         StringFromCLSID(OBJECT_LIBID[objIndex],&oleString);
         WideCharToMultiByte(CP_ACP,0,oleString,-1,szTemp,256,0,0);
         RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
           
         sprintf(szTemp,"ToolboxBitmap32");
         RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
// //      sprintf(szTemp,"%s, 1",szModuleName);
// //      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szModuleName));
    
         sprintf(szTemp,"Version");
         RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
         sprintf(szTemp,OBJECT_VERSION[objIndex]);
         RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
    
         sprintf(szTemp,"MiscStatus");
         RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
         sprintf(szTemp,"0");
         RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
    
         sprintf(szTemp,"1");
         RegCreateKeyEx(keyHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
         sprintf(szTemp,"%ld",
                    OLEMISC_ALWAYSRUN |
                    OLEMISC_ACTIVATEWHENVISIBLE | 
                    OLEMISC_RECOMPOSEONRESIZE | 
                    OLEMISC_INSIDEOUT |
                    OLEMISC_SETCLIENTSITEFIRST |
                    OLEMISC_CANTLINKINSIDE );
//s//sprintf(szTemp,"%ld",131473L);
         RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
    
      RegCreateKeyEx(HKEY_CLASSES_ROOT,OBJECT_NAME[objIndex],0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
         RegCreateKeyEx(keyHandle,"CurVer",0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
         sprintf(szTemp,OBJECT_NAME_V[objIndex]);
         RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
    
      RegCreateKeyEx(HKEY_CLASSES_ROOT,OBJECT_NAME_V[objIndex],0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
         RegCreateKeyEx(keyHandle,"CLSID",0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
         RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szCLSID,(DWORD)strlen(szCLSID));

   }

   return S_OK;
   }
  
  
   STDAPI DllUnregisterServer() {

   HRESULT rc = S_OK;
   HKEY keyHandle;
   char szCLSID[256];
   LPOLESTR oleString;
  
   for ( long libIndex = 0; libIndex < 2; libIndex++ ) {

      ITypeLib *ptLib;
      wchar_t szLib[256];

      wsprintfW(szLib,L"%s\\%ld",wstrModuleName,libIndex + 1);

      if ( S_OK != (rc = LoadTypeLib(szLib,&ptLib)) ) {
         rc = ResultFromScode(SELFREG_E_TYPELIB);
         wchar_t szMessage[256];
         wsprintfW(szMessage,L"The type library in %s did not load and therefore will not Un-register properly.\n\nThe error code from windows is %ld",szLib,rc);
         MessageBoxW(NULL,szMessage,L"Error",MB_ICONEXCLAMATION);
         return E_FAIL;
      } else {
         if ( S_OK != (rc = UnRegisterTypeLib(OBJECT_LIBID[libIndex],1,0,LANG_NEUTRAL,SYS_WIN32)) ) {
            wchar_t szMessage[256];
            wsprintfW(szMessage,L"The type library in %s did not Un-register properly.\n\nThe error code from windows is %ld",szLib,rc);
            MessageBoxW(NULL,szMessage,L"Error",MB_ICONEXCLAMATION);
            return E_FAIL;
         }
      }

   }

   for ( long objIndex = 0; 1; objIndex++ ) {

      if ( GUID_NULL == OBJECT_CLSID[objIndex] )
         break;

      StringFromCLSID(OBJECT_CLSID[objIndex],&oleString);
      WideCharToMultiByte(CP_ACP,0,oleString,-1,szCLSID,256,0,0);
 
      rc = RegOpenKeyEx(HKEY_CLASSES_ROOT,"CLSID",0,KEY_CREATE_SUB_KEY,&keyHandle);

      rc = SHDeleteKey(keyHandle,szCLSID);

      rc = SHDeleteKey(HKEY_CLASSES_ROOT,OBJECT_NAME[objIndex]);

      rc = SHDeleteKey(HKEY_CLASSES_ROOT,OBJECT_NAME_V[objIndex]);

   }

   return S_OK;
   }
  
  
   long __stdcall Factory::QueryInterface(REFIID iid, void **ppv) { 
   *ppv = NULL; 
   if ( iid == IID_IUnknown || iid == IID_IClassFactory ) 
      *ppv = this; 
   else 
      return E_NOINTERFACE; 
   AddRef(); 
   return S_OK; 
   } 
  
  
   unsigned long __stdcall Factory::AddRef() { 
   return ++refCount; 
   } 
  
  
   unsigned long __stdcall Factory::Release() { 
   return --refCount;
   } 

   HRESULT STDMETHODCALLTYPE Factory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv) { 

   *ppv = NULL; 

   if ( OBJECT_CLSID[0] == myObject ) {
      if ( ! pStaticObject )
         pStaticObject = new pkAPI(pUnkOuter,false,false,false,false,true);
      return pStaticObject -> QueryInterface(riid,ppv);

   } else if ( OBJECT_CLSID[1] == myObject ) {
      if ( ! pStaticObject )
         pStaticObject = new pkAPI(pUnkOuter,false,false,false,false,true);
      return pStaticObject -> GetIOleObject() -> PropertyPage(0) -> QueryInterface(riid,ppv);

   } else if ( OBJECT_CLSID[2] == myObject ) {
      if ( ! pStaticObject )
         pStaticObject = new pkAPI(pUnkOuter,false,false,false,false,true);
      return pStaticObject -> GetIOleObject() -> PropertyPage(1) -> QueryInterface(riid,ppv);

   } else if ( OBJECT_CLSID[3] == myObject ) {
      if ( ! pStaticObject )
         pStaticObject = new pkAPI(pUnkOuter,false,false,false,false,true);
      return pStaticObject -> GetIOleObject() -> PropertyPage(2) -> QueryInterface(riid,ppv);

   } else if ( OBJECT_CLSID[4] == myObject ) {
      if ( ! pStaticObject )
         pStaticObject = new pkAPI(pUnkOuter,false,false,true,false,false);
      return pStaticObject -> QueryInterface(riid,ppv);

   } else if ( OBJECT_CLSID[5] == myObject ) {
      if ( ! pStaticObject )
         pStaticObject = new pkAPI(pUnkOuter,true,false,false,false,false);
      return pStaticObject -> QueryInterface(riid,ppv);

   } else if ( OBJECT_CLSID[6] == myObject ) {
      if ( ! pStaticObject )
         pStaticObject = new pkAPI(pUnkOuter,false,false,false,true,false);
      return pStaticObject -> QueryInterface(riid,ppv);

   } else if ( OBJECT_CLSID[7] == myObject ) {
      if ( ! pStaticObject )
         pStaticObject = new pkAPI(pUnkOuter,false,true,false,false,false);
      return pStaticObject -> QueryInterface(riid,ppv);
   }

   return E_NOINTERFACE;
   } 
  
  
   long __stdcall Factory::LockServer(int fLock) { 
   return S_OK; 
   }