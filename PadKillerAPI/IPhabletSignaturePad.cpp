// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   HRESULT pkAPI::put_WidthInInches(double value) {
   widthInInches = value;
   return S_OK;
   }

   HRESULT pkAPI::get_WidthInInches(double *pValue) {
   if ( ! pValue )
      return E_POINTER;
   *pValue = widthInInches;
   return S_OK;
   }