// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

#include <shlobj.h>

#define LOAD_CONTROLS                                                   \
{                                                                       \
PUT_STRING(p -> szDeviceIpOrNetworkName,IDDI_PAD_PROPERTIES_HOSTNAME)   \
PUT_LONG(p -> pixelWidth,IDDI_PAD_PIXEL_WIDTH)                          \
PUT_LONG(p -> pixelHeight,IDDI_PAD_PIXEL_HEIGHT)                        \
PUT_DOUBLE(p -> widthInInches,IDDI_PAD_INCH_WIDTH)                      \
}

#define UNLOAD_CONTROLS                                                 \
{                                                                       \
GET_STRING(p -> szDeviceIpOrNetworkName,IDDI_PAD_PROPERTIES_HOSTNAME)   \
GET_LONG(p -> pixelWidth,IDDI_PAD_PIXEL_WIDTH)                          \
GET_LONG(p -> pixelHeight,IDDI_PAD_PIXEL_HEIGHT)                        \
GET_DOUBLE(p -> widthInInches,IDDI_PAD_INCH_WIDTH)                      \
}

   HWND settingsHandles[128];
   long settingsHandlesCount = 0L;
   HWND hwndTabs = NULL;
   HWND hwndDocumentation = NULL;

   long entryPadSelection = -1L;

   char szFeatureDescriptions[][64] = {
               "The InnoVisioNate Phablet signature Pad",
               "signotec",
               "Wacom LCD",
               "Scriptel",
               "Windows Tablet PC OS"};

   PAD_IMPLEMENTATION_FILES

   extern "C" void disableAll(HWND hwnd,long *pExceptions);
   extern "C" void enableAll(HWND hwnd,long *pExceptions);

   BOOL CALLBACK setParent(HWND hwnd,LPARAM newParent);
   BOOL CALLBACK moveDown(HWND hwndTest,LPARAM lParam);
   BOOL CALLBACK loadSettingsHandles(HWND hwndTest,LPARAM lParam);

   TCITEM tcItemPhabletInstructions = {0};

   LRESULT CALLBACK pkAPI::settingsHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   pkAPI *p = (pkAPI *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   switch ( msg ) {

   case WM_INITDIALOG: {

      PROPSHEETPAGE *pPage = reinterpret_cast<PROPSHEETPAGE *>(lParam);

      p = (pkAPI *)pPage -> lParam;

      SetWindowLongPtr(hwnd,GWLP_USERDATA,(ULONG_PTR)p);

      if ( p -> pIGProperties ) {
         p -> pIGProperties -> Push();
         p -> pIGProperties -> Push();
      }

      if ( p -> pIOleObject || p -> pIVisioLoggerSignaturePad ) {

         DestroyWindow(GetDlgItem(hwnd,IDDI_PAD_INCH_DIMENSIONS_LABEL));
         DestroyWindow(GetDlgItem(hwnd,IDDI_PAD_INCH_LABEL_WIDTH));
         DestroyWindow(GetDlgItem(hwnd,IDDI_PAD_INCH_WIDTH));
         DestroyWindow(GetDlgItem(hwnd,IDDI_PAD_INCH_INSTRUCTIONS));

      }

      if ( p -> isExtended || p -> isCursiVision || p -> pIVisioLoggerSignaturePad ) {

         RECT rcTop;
         GetWindowRect(GetDlgItem(hwnd,IDDI_PAD_DIMENSIONS_INFO),&rcTop);

         DestroyWindow(GetDlgItem(hwnd,IDDI_PAD_DIMENSIONS_INFO));
         DestroyWindow(GetDlgItem(hwnd,IDDI_PAD_PIXEL_WIDTH_LABEL));
         DestroyWindow(GetDlgItem(hwnd,IDDI_PAD_PIXEL_WIDTH));
         DestroyWindow(GetDlgItem(hwnd,IDDI_PAD_PIXEL_HEIGHT_LABEL));
         DestroyWindow(GetDlgItem(hwnd,IDDI_PAD_PIXEL_HEIGHT));
         DestroyWindow(GetDlgItem(hwnd,IDDI_PAD_NOT_CONNECTED));
         DestroyWindow(GetDlgItem(hwnd,IDDI_PAD_CONNECT));

         RECT rcControl,rcParent;

         GetWindowRect(hwnd,&rcParent);

         GetWindowRect(GetDlgItem(hwnd,IDDI_PAD_INCH_DIMENSIONS_LABEL),&rcControl);

         long deltaY = rcControl.top - rcTop.top;

         SetWindowPos(GetDlgItem(hwnd,IDDI_PAD_INCH_DIMENSIONS_LABEL),HWND_TOP,rcControl.left - rcParent.left,rcControl.top - deltaY - rcParent.top,0,0,SWP_NOSIZE);

         GetWindowRect(GetDlgItem(hwnd,IDDI_PAD_INCH_LABEL_WIDTH),&rcControl);
         SetWindowPos(GetDlgItem(hwnd,IDDI_PAD_INCH_LABEL_WIDTH),HWND_TOP,rcControl.left - rcParent.left,rcControl.top - deltaY - rcParent.top,0,0,SWP_NOSIZE);

         GetWindowRect(GetDlgItem(hwnd,IDDI_PAD_INCH_WIDTH),&rcControl);
         SetWindowPos(GetDlgItem(hwnd,IDDI_PAD_INCH_WIDTH),HWND_TOP,rcControl.left - rcParent.left,rcControl.top - deltaY - rcParent.top,0,0,SWP_NOSIZE);

         GetWindowRect(GetDlgItem(hwnd,IDDI_PAD_INCH_INSTRUCTIONS),&rcControl);
         SetWindowPos(GetDlgItem(hwnd,IDDI_PAD_INCH_INSTRUCTIONS),HWND_TOP,rcControl.left - rcParent.left,rcControl.top - deltaY - rcParent.top,0,0,SWP_NOSIZE);
      
         if ( p -> isCursiVision ) {

            char szTemp[1024];

            for ( long k = IDDI_PAD_MODEL_MIN; k <= IDDI_PAD_MODEL_MAX; k++ ) {
               char szModel[128];
               GetWindowText(GetDlgItem(hwnd,k),szModel,128);
               if ( strstr(szModel,p -> pICursiVisionSignaturePad -> DeviceProductName()) ) {
                  SendMessage(GetDlgItem(hwnd,k),BM_SETCHECK,BST_CHECKED,0L);
                  entryPadSelection = k;
                  break;
               }
            }

            LoadString(hModule,IDDI_PAD_MODEL_CHOICE_LABEL2,szTemp,sizeof(szTemp));

            SetDlgItemText(hwnd,IDDI_PAD_MODEL_CHOICE_LABEL2,szTemp);

            if ( ! p -> pICursiVisionSignaturePad -> pICursiVisionServices -> IsAdministrator() ) {
               pkAPI::defaultTextHandler = (WNDPROC)SetWindowLongPtr(GetDlgItem(hwnd,IDDI_PAD_MODEL_CHOICE_NONADMIN),GWLP_WNDPROC,(ULONG_PTR)pkAPI::redTextHandler);
               LoadString(hModule,IDDI_PAD_MODEL_CHOICE_NONADMIN,szTemp,sizeof(szTemp));
               SetDlgItemText(hwnd,IDDI_PAD_MODEL_CHOICE_NONADMIN,szTemp);
               DestroyWindow(GetDlgItem(hwnd,IDDI_PAD_MODEL_CHOICE_LABEL2));
               EnableWindow(GetDlgItem(hwnd,IDDI_PAD_MODEL_CHOICE_LABEL1),FALSE);
               for ( long k = IDDI_PAD_MODEL_MIN; k <= IDDI_PAD_MODEL_MAX; k++ ) 
                  EnableWindow(GetDlgItem(hwnd,k),FALSE);
            } else
               DestroyWindow(GetDlgItem(hwnd,IDDI_PAD_MODEL_CHOICE_NONADMIN));


         }

      } else if ( p -> IsConnected() ) {

         RECT rcParent,rcLabel,rcInchLabel,rcControl;

         GetWindowRect(hwnd,&rcParent);

         GetWindowRect(GetDlgItem(hwnd,IDDI_PAD_NOT_CONNECTED),&rcLabel);
         GetWindowRect(GetDlgItem(hwnd,IDDI_PAD_INCH_DIMENSIONS_LABEL),&rcInchLabel);
         GetWindowRect(GetDlgItem(hwnd,IDDI_PAD_INCH_WIDTH),&rcControl);

         ShowWindow(GetDlgItem(hwnd,IDDI_PAD_NOT_CONNECTED),SW_HIDE);
         ShowWindow(GetDlgItem(hwnd,IDDI_PAD_CONNECT),SW_HIDE);

         long deltaY = rcControl.top - rcInchLabel.top + 16;

         GetWindowRect(GetDlgItem(hwnd,IDDI_PAD_INCH_DIMENSIONS_LABEL),&rcControl);
         SetWindowPos(GetDlgItem(hwnd,IDDI_PAD_INCH_DIMENSIONS_LABEL),HWND_TOP,rcControl.left - rcParent.left,rcControl.top - rcParent.top - deltaY,0,0,SWP_NOSIZE);

         GetWindowRect(GetDlgItem(hwnd,IDDI_PAD_INCH_LABEL_WIDTH),&rcControl);
         SetWindowPos(GetDlgItem(hwnd,IDDI_PAD_INCH_LABEL_WIDTH),HWND_TOP,rcControl.left - rcParent.left,rcControl.top - rcParent.top - deltaY,0,0,SWP_NOSIZE);

         GetWindowRect(GetDlgItem(hwnd,IDDI_PAD_INCH_WIDTH),&rcControl);
         SetWindowPos(GetDlgItem(hwnd,IDDI_PAD_INCH_WIDTH),HWND_TOP,rcControl.left - rcParent.left,rcControl.top - rcParent.top - deltaY,0,0,SWP_NOSIZE);

         GetWindowRect(GetDlgItem(hwnd,IDDI_PAD_INCH_INSTRUCTIONS),&rcControl);
         SetWindowPos(GetDlgItem(hwnd,IDDI_PAD_INCH_INSTRUCTIONS),HWND_TOP,rcControl.left - rcParent.left,rcControl.top - rcParent.top - deltaY,0,0,SWP_NOSIZE);

      } 
   
#ifdef ALLOW_PAD_SELECTION
      bool allowPadSelection = true;
#else
      bool allowPadSelection = false;
#endif

      if ( ! p -> isCursiVision || ! allowPadSelection ) {

         ShowWindow(GetDlgItem(hwnd,IDDI_PAD_MODEL_CHOICE_LABEL1),SW_HIDE);
         for ( long k = IDDI_PAD_MODEL_MIN; k <= IDDI_PAD_MODEL_MAX; k++ ) 
            ShowWindow(GetDlgItem(hwnd,k),SW_HIDE);

         ShowWindow(GetDlgItem(hwnd,IDDI_PAD_MODEL_CHOICE_LABEL2),SW_HIDE);

      } else {

         settingsHandlesCount = 0L;

         EnumChildWindows(hwnd,moveDown,32);
         EnumChildWindows(hwnd,loadSettingsHandles,0L);

         hwndTabs = CreateWindowEx(0L,WC_TABCONTROL,"",WS_CHILD | WS_VISIBLE,0,0,0,0,hwnd,(HMENU)IDDI_PAD_TAB_CONTROL,hModule,NULL);

         SendMessage(hwndTabs,WM_SETFONT,(WPARAM)GetStockObject(DEFAULT_GUI_FONT),0L);

         TCITEM tcItem = {0};

         tcItem.pszText = "Settings";
         tcItem.mask = TCIF_TEXT;

         SendMessage(hwndTabs,TCM_INSERTITEM,0L,(LPARAM)&tcItem);

         if ( IDDI_PAD_MODEL_PHABLET == entryPadSelection ) {
            tcItemPhabletInstructions.pszText = "Setting up the Pad";
            tcItemPhabletInstructions.mask = TCIF_TEXT;
            SendMessage(hwndTabs,TCM_INSERTITEM,1L,(LPARAM)&tcItemPhabletInstructions);
         }

         if ( IsWindow(GetDlgItem(hwnd,IDDI_PAD_MODEL_CHOICE_NONADMIN)) )
            SetWindowLongPtr(GetDlgItem(hwnd,IDDI_PAD_MODEL_CHOICE_NONADMIN),GWLP_WNDPROC,(ULONG_PTR)pkAPI::redTextHandler);

         HFONT hFontRadioButton = (HFONT)SendDlgItemMessage(hwnd,IDDI_PAD_MODEL_PHABLET,WM_GETFONT,0L,0L);

         SendDlgItemMessage(hwnd,IDDI_PAD_MODEL_PHABLET_NOTE1,WM_SETFONT,(WPARAM)hFontRadioButton,MAKELPARAM(TRUE,TRUE));

         DLGTEMPLATE *pdx = (DLGTEMPLATE *)LoadResource(hModule,FindResource(hModule,MAKEINTRESOURCE(IDD_DOCUMENTATION),RT_DIALOG));

         PROPSHEETPAGE pPage = {0};
         pPage.hInstance = hModule;
         pPage.lParam = (LPARAM)p;

         hwndDocumentation = CreateDialogIndirectParam(hModule,pdx,hwndTabs,(DLGPROC)pkAPI::documentationSettingsHandler,(LPARAM)&pPage);

         ShowWindow(hwndDocumentation,SW_HIDE);

      }

      LOAD_CONTROLS;

      }
      return LRESULT(FALSE);

   case WM_SIZE: {
      
      if ( ! p -> isCursiVision )
         break;

      HWND hwndTabs = GetDlgItem(hwnd,IDDI_PAD_TAB_CONTROL);
      RECT rcSize = {0};
      GetClientRect(hwnd,&rcSize);
      SetWindowPos(hwndTabs,HWND_TOP,8,8,rcSize.right - rcSize.left - 16,rcSize.bottom - rcSize.top - 16,0L);
      SetWindowPos(hwndDocumentation,HWND_TOP,8,32,rcSize.right - rcSize.left - 32,rcSize.bottom - rcSize.top - 64,0L);

      }
      break;

   case WM_COMMAND: {

      if ( IDDI_PAD_MODEL_MIN <= LOWORD(wParam) && LOWORD(wParam) <= IDDI_PAD_MODEL_MAX ) {
         if ( BST_CHECKED != SendMessage((HWND)lParam,BM_GETCHECK,0L,0L) ) 
            break;
         if ( ! ( IDDI_PAD_MODEL_PHABLET == LOWORD(wParam) ) ) {
            tcItemPhabletInstructions.mask = 0L;
            SendMessage(hwnd,WM_SETREDRAW,(WPARAM)FALSE,0L);
            SendMessage(hwndTabs,TCM_DELETEITEM,1L,0L);
            SendMessage(hwnd,WM_SETREDRAW,(WPARAM)TRUE,0L);
            InvalidateRect(hwnd,NULL,TRUE);
         } else if ( 0 == tcItemPhabletInstructions.mask ) {
            tcItemPhabletInstructions.pszText = "Setting up the Pad";
            tcItemPhabletInstructions.mask = TCIF_TEXT;
            SendMessage(hwndTabs,TCM_INSERTITEM,1L,(LPARAM)&tcItemPhabletInstructions);
            InvalidateRect(hwnd,NULL,TRUE);
         }
         long exceptions[32];
         memset(exceptions,0,32 * sizeof(long));
         for ( long j = IDDI_PAD_MODEL_MIN; j <= IDDI_PAD_MODEL_MAX; j++ )
            exceptions[j - IDDI_PAD_MODEL_MIN] = j;
         exceptions[IDDI_PAD_MODEL_MAX - IDDI_PAD_MODEL_MIN + 1] = IDDI_PAD_MODEL_CHOICE_LABEL1;
         exceptions[IDDI_PAD_MODEL_MAX - IDDI_PAD_MODEL_MIN + 2] = IDDI_PAD_MODEL_CHOICE_LABEL2;
         if ( LOWORD(wParam) != entryPadSelection )
            disableAll(hwnd,exceptions);
         else {
            exceptions[0] = 0;
            enableAll(hwnd,exceptions);
         }
         break;
      }

      switch ( LOWORD(wParam) ) {

      case IDDI_PAD_CONNECT: {
         GetDlgItemText(hwnd,IDDI_PAD_PROPERTIES_HOSTNAME,p -> szDeviceIpOrNetworkName,MAX_PATH);
         if ( ! p -> szDeviceIpOrNetworkName[0] )
            strcpy(p -> szDeviceIpOrNetworkName,"localhost");
         if ( E_FAIL == p -> connectServer() ) {
            char szMessage[1024];
            sprintf(szMessage,"There was an error connecting to %s on port %s. Is the computer visible on the network, or is a firewall preventing access to this port ?",p -> szDeviceIpOrNetworkName,SERVICE_PORT_A);
            MessageBox(NULL,szMessage,"Error",MB_ICONEXCLAMATION | MB_OK);
            break;
         }
         char szTemp[32];
         sprintf(szTemp,"%ld",p -> getPadWidth());
         SetDlgItemText(hwnd,IDDI_PAD_PIXEL_WIDTH,szTemp);
         sprintf(szTemp,"%ld",p -> getPadHeight());
         SetDlgItemText(hwnd,IDDI_PAD_PIXEL_HEIGHT,szTemp);
         p -> disconnectServer();
         }
         break;

      case IDDI_PAD_PROPERTIES_ACCEPT:
         UNLOAD_CONTROLS;
         break;

      case IDDI_INK_PROPERTIES_SET:
         DialogBoxParam(hModule,MAKEINTRESOURCE(IDD_PAD_INK_PROPERTIES),hwnd,(DLGPROC)pkAPI::inkHandler,(ULONG_PTR)p);
         break;

      default:
         break;
      }

      }
      break;


   case WM_NOTIFY: {

      NMHDR *pNotifyHeader = (NMHDR *)lParam;

      switch ( pNotifyHeader -> code ) {

      case PSN_SETACTIVE: {
         LOAD_CONTROLS
         }
         break;

      case PSN_KILLACTIVE:
         SetWindowLongPtr(pNotifyHeader -> hwndFrom,DWLP_MSGRESULT,FALSE);
         break;

      case PSN_APPLY: {

         PSHNOTIFY *pNotify = (PSHNOTIFY *)lParam;

         UNLOAD_CONTROLS;

         bool saveProperties = false;
         for ( long k = IDDI_PAD_MODEL_MIN; k <= IDDI_PAD_MODEL_MAX; k++ ) {
            long isSelected = (long)SendMessage(GetDlgItem(hwnd,k),BM_GETCHECK,0L,0L);
            if ( isSelected ) {
               if ( entryPadSelection == k ) {
                  saveProperties = true;
                  break;
               }
               if ( p -> pICursiVisionSignaturePad -> pICursiVisionServices )
                  p -> pICursiVisionSignaturePad -> pICursiVisionServices -> RegisterSignaturePad(wszFiles[k - IDDI_PAD_MODEL_MIN]);
               break;
            }
         }

         if ( p -> pIGProperties ) {

            if ( pNotify -> lParam ) {
               if ( saveProperties )
                  p -> pIGProperties -> Save();
               p -> pIGProperties -> Discard();
               p -> pIGProperties -> Discard();
            } else {
               p -> pIGProperties -> Discard();
               p -> pIGProperties -> Push();
            }
         }

         SetWindowLongPtr(pNotifyHeader -> hwndFrom,DWLP_MSGRESULT,PSNRET_NOERROR);

         }
         break;

      case PSN_RESET: {
         if ( p -> pIGProperties ) {
            p -> pIGProperties -> Pop();
            p -> pIGProperties -> Pop();
         }
         }
         break;

      case TTN_NEEDTEXT: {
         NMTTDISPINFO *pNotify = (NMTTDISPINFO *)lParam;
         long k = 0L;
         if ( pNotify -> uFlags & TTF_IDISHWND )
            k = (long)SendMessage((HWND)pNotify -> hdr.idFrom,TBM_GETPOS,0L,0L);
         else
            k = (long)SendDlgItemMessage(hwnd,(int)pNotify -> hdr.idFrom,TBM_GETPOS,0L,0L);
         }
         return (LRESULT)0;

      case TCN_SELCHANGING: {
         long action = SW_SHOW;
         if ( 0 == SendMessage(GetDlgItem(hwnd,IDDI_PAD_TAB_CONTROL),TCM_GETCURSEL,0L,0L) ) 
            action = SW_HIDE;
         for ( long k = 0; k < settingsHandlesCount; k++ )
            ShowWindow(settingsHandles[k],action);
         ShowWindow(hwndDocumentation,action == SW_HIDE ? SW_SHOW : SW_HIDE);
         }
         return (LRESULT)0;

      default:
         break;
      }

      }
      break;

   default:
      break;
   }

   return LRESULT(FALSE);
   }


   static DWORD entryInkColor = -1L;


   LRESULT CALLBACK pkAPI::inkHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   pkAPI *p = (pkAPI *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   switch ( msg ) {

   case WM_INITDIALOG: {

      SetWindowLongPtr(hwnd,GWLP_USERDATA,lParam);

      p = (pkAPI *)lParam;

      SetWindowLongPtr(GetDlgItem(hwnd,IDDI_INK_PROPERTIES_COLOR_SWATCH),GWLP_USERDATA,(ULONG_PTR)p);
      pkAPI::defaultSwatchHandler = (WNDPROC)SetWindowLongPtr(GetDlgItem(hwnd,IDDI_INK_PROPERTIES_COLOR_SWATCH),GWLP_WNDPROC,(ULONG_PTR)pkAPI::swatchHandler);
      EnableWindow(GetDlgItem(hwnd,IDDI_INK_PROPERTIES_COLOR_SWATCH),FALSE);

      char szX[32];
      sprintf(szX,"%ld",p -> inkWeight);
      SetWindowText(GetDlgItem(hwnd,IDDI_INK_PROPERTIES_WEIGHT),szX);

      RECT rcWeight;

      GetWindowRect(GetDlgItem(hwnd,IDDI_INK_PROPERTIES_WEIGHT),&rcWeight);

      HWND weightSpinner = CreateWindowEx(0L,UPDOWN_CLASS,"",WS_VISIBLE | WS_CHILD | UDS_SETBUDDYINT | UDS_ALIGNRIGHT,0,0,30,rcWeight.bottom - rcWeight.top,
                                              hwnd,(HMENU)IDDI_INK_PROPERTIES_WEIGHT_SPIN,hModule,(void *)p);

      SendMessage(weightSpinner,UDM_SETRANGE,0L,MAKELONG(16,1));

      SendMessage(weightSpinner,UDM_SETPOS,0L,MAKELONG((short)p -> inkWeight,0));
      SendMessage(weightSpinner,UDM_SETBUDDY,(WPARAM)GetDlgItem(hwnd,IDDI_INK_PROPERTIES_WEIGHT),0L);

      entryInkColor = p -> inkColor;

      POINT ptl;
      GetCursorPos(&ptl);

      SetWindowPos(hwnd,HWND_TOP,ptl.x + 32,ptl.y + 32,0,0,SWP_NOSIZE);

      }
      return LRESULT(FALSE);

   case WM_COMMAND:

      switch ( LOWORD(wParam) ) {
      case IDDI_INK_PROPERTIES_OK:
         char szX[32];
         GetWindowText(GetDlgItem(hwnd,IDDI_INK_PROPERTIES_WEIGHT),szX,32);
         p -> inkWeight = atol(szX);
         EndDialog(hwnd,1L);
         break;

      case IDDI_INK_PROPERTIES_CANCEL:
         p -> inkColor = entryInkColor;
         EndDialog(hwnd,0L);
         break;

      case IDDI_INK_PROPERTIES_COLOR_CHOOSE: {
         CHOOSECOLOR chooseColor = {0};
         COLORREF customColors[16];
         memset(customColors,0xFFFF,16 * sizeof(COLORREF));
         chooseColor.lStructSize = sizeof(CHOOSECOLOR);
         chooseColor.hwndOwner = hwnd;
         chooseColor.hInstance = (HWND)hModule;
         chooseColor.rgbResult = p -> inkColor;
         chooseColor.Flags = CC_RGBINIT | CC_FULLOPEN;
         chooseColor.lpCustColors = customColors;
         if ( ChooseColor(&chooseColor) ) {
            p -> inkColor = chooseColor.rgbResult;
            InvalidateRect(GetDlgItem(hwnd,IDDI_INK_PROPERTIES_COLOR_SWATCH),NULL,TRUE);  
         }
         }
         break;
      }

   default:
      break;

   }
   return (LRESULT)0;
   }


   LRESULT CALLBACK pkAPI::redTextHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   SignaturePad *p = (SignaturePad *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   switch ( msg ) {

   case WM_PAINT: {
      PAINTSTRUCT ps = {0};
      BeginPaint(hwnd,&ps);
#ifdef ALLOW_PAD_SELECTION
      char szText[1024];
      GetWindowText(hwnd,szText,1024);
      HFONT hGUIFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
      SelectObject(ps.hdc,hGUIFont);
      SetTextColor(ps.hdc,RGB(255,0,0));
      SetBkColor(ps.hdc,GetSysColor(COLOR_MENU));
      DrawText(ps.hdc,szText,(DWORD)strlen(szText),&ps.rcPaint,DT_TOP);
#endif
      EndPaint(hwnd,&ps);
      }
      break;

   default:
      break;

   }

   return CallWindowProc(pkAPI::defaultTextHandler,hwnd,msg,wParam,lParam);
   }

   LRESULT CALLBACK pkAPI::swatchHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   pkAPI *p = (pkAPI *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   switch ( msg ) {

   case WM_PAINT: {
      PAINTSTRUCT ps = {0};
      HBRUSH hb = CreateSolidBrush((COLORREF)p -> inkColor);
      BeginPaint(hwnd,&ps);
      FillRect(ps.hdc,&ps.rcPaint,hb);
      DeleteObject(hb);
      EndPaint(hwnd,&ps);
      }
      return (LRESULT)0L;

   default:
      break;

   }

   return CallWindowProc(pkAPI::defaultSwatchHandler,hwnd,msg,wParam,lParam);
   }


   BOOL CALLBACK doDisable(HWND hwndTest,LPARAM lParam);
   BOOL CALLBACK doEnable(HWND hwndTest,LPARAM lParam);

   extern "C" void enableAll(HWND hwnd,long *pExceptions) {
   EnumChildWindows(hwnd,doEnable,(LPARAM)pExceptions);
   return;
   }

   extern "C" void disableAll(HWND hwnd,long *pExceptions) {
   EnumChildWindows(hwnd,doDisable,(LPARAM)pExceptions);
   return;
   }

   BOOL CALLBACK doDisable(HWND hwndTest,LPARAM lParam) {
   long *pExceptions = (long *)lParam;
   long id = (long)GetWindowLongPtr(hwndTest,GWL_ID);
   for ( long k = 0; 1; k++ ) {
      if ( ! pExceptions[k] )
         break;
      if ( id == pExceptions[k] ) {
         EnableWindow(hwndTest,TRUE);
         return TRUE;
      }
   }
   EnableWindow(hwndTest,FALSE);
   return TRUE;
   }


   BOOL CALLBACK doEnable(HWND hwndTest,LPARAM lParam) {
   long *pExceptions = (long *)lParam;
   long id = (long)GetWindowLongPtr(hwndTest,GWL_ID);
   for ( long k = 0; 1; k++ ) {
      if ( ! pExceptions[k] )
         break;
      if ( id == pExceptions[k] ) {
         EnableWindow(hwndTest,FALSE);
         return TRUE;
      }
   }
   EnableWindow(hwndTest,TRUE);
   return TRUE;
   }

   BOOL CALLBACK setParent(HWND hwndTest,LPARAM lParam) {
   SetParent(hwndTest,(HWND)lParam);
   return TRUE;
   }

   BOOL CALLBACK moveDown(HWND hwndTest,LPARAM lParam) {
   RECT rc = {0};
   RECT rcParent = {0};
   GetWindowRect(hwndTest,&rc);
   GetWindowRect(GetParent(hwndTest),&rcParent);
   SetWindowPos(hwndTest,HWND_TOP,rc.left - rcParent.left,rc.top - rcParent.top + (int)lParam,0,0,SWP_NOSIZE);
   return TRUE;
   }

   BOOL CALLBACK loadSettingsHandles(HWND hwndTest,LPARAM lParam) {
   settingsHandles[settingsHandlesCount] = hwndTest;
   settingsHandlesCount++;
   return TRUE;
   }