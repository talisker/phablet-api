// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   // IUnknown

   long __stdcall pkAPI::QueryInterface(REFIID riid,void **ppv) {

   *ppv = NULL;

   if ( pIOleObject_HTML ) {

      if ( IID_IOleInPlaceFrame == riid ) 
         *ppv = static_cast<IOleInPlaceFrame *>(pIOleInPlaceFrame_HTML_Host);
      else

      if ( IID_IOleInPlaceSite == riid ) 
         *ppv = static_cast<IOleInPlaceSite *>(pIOleInPlaceSite_HTML_Host);
      else

      if ( IID_IOleInPlaceSiteEx == riid ) 
         *ppv = static_cast<IOleInPlaceSiteEx *>(pIOleInPlaceSite_HTML_Host);
      else

      if ( IID_IOleDocumentSite == riid ) 
         *ppv = static_cast<IOleDocumentSite *>(pIOleDocumentSite_HTML_Host);

      if ( *ppv ) {
         AddRef();
         return S_OK;
      }

   }

   if ( riid == IID_IUnknown )
      *ppv = static_cast<IUnknown *>(this);
   else

   if ( riid == IID_IDispatch )
      *ppv = static_cast<IDispatch *>(this);
   else

   if ( riid == IID_IBasicSignaturePad )
      *ppv = static_cast<IBasicSignaturePad *>(this);
   else

   if ( riid == IID_IPhabletSignaturePad )
      *ppv = static_cast<IPhabletSignaturePad *>(this);
   else

   if ( riid == IID_IGPropertyPageClient )
      return pIGPropertyPageClient -> QueryInterface(riid,ppv);
   else

   if ( riid == IID_IGPropertiesClient )
      return pIGPropertiesClient -> QueryInterface(riid,ppv);
   else

   if ( IID_IPersistStream == riid ) 
      return pIGProperties -> QueryInterface(riid,ppv);
   else

   if ( riid == IID_IConnectionPointContainer && ! pIVisioLoggerSignaturePad && ! pICursiVisionSignaturePad && ! pIVisioLoggerKioskPad ) 
      *ppv = static_cast<IConnectionPointContainer *>(&connectionPointContainer);
   else

   if ( riid == IID_IConnectionPoint && ! pIVisioLoggerSignaturePad && ! pICursiVisionSignaturePad && ! pIVisioLoggerKioskPad ) 
      *ppv = static_cast<IConnectionPoint *>(pConnectionPointExtended);

   else {

      if ( pIVisioLoggerSignaturePad )
         return pIVisioLoggerSignaturePad -> QueryInterface(riid,ppv);

      if ( pIVisioLoggerKioskPad )
         return pIVisioLoggerKioskPad -> QueryInterface(riid,ppv);

      if ( pICursiVisionSignaturePad )
         return pICursiVisionSignaturePad -> QueryInterface(riid,ppv);

      if ( isExtended ) 

         return E_NOINTERFACE;

      else {

         if ( fromOleObject )
            return E_NOINTERFACE;

         if ( ! pIOleObject )
            pIOleObject = new _IOleObject(this);

         return pIOleObject -> QueryInterface(riid,ppv);
      }

   }

   AddRef();

   return S_OK;
   }

   unsigned long __stdcall pkAPI::AddRef() {
   refCount++;
   return refCount;
   }

   unsigned long __stdcall pkAPI::Release() { 
   refCount--;
   if ( ! refCount ) {
      delete this;
      return 0;
   }
   return refCount;
   }