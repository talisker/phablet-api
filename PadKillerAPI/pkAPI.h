// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#include <Ws2tcpip.h>
#include <windows.h>
#include <stdio.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <olectl.h>
#include <process.h>
#include <stddef.h>

#include <list>
#include <map>

#ifndef RC_INVOKED
#include <Gdiplus.h>
#include <wingdi.h>
#endif

#include "..\PadKiller\PadKillerDefines.h"

#include "resource.h"

#include "pkAPI_i.h"
#include "SignaturePad_i.h"
#include "pkAPI_VisioLogger_i.h"

#include "PDFEnabler_i.h"
#include "PrintingSupport_i.h"
#include "CursiVision_i.h"

#include "signaturePadModelIds.h"

#include "properties_i.h"

//using namespace visioLogger;

#define COMMAND_SIZE    32768
#define RESPONSE_SIZE   32768
#define ALLOW_PAD_SELECTION

#define DONT_SHOW_COMM_ERROR_NOTIFICATION

   class pkAPI : public IPhabletSignaturePad {

   public:

      pkAPI(IUnknown *pUnkOuter,bool isVisioLogger,bool isVisioLoggerKiosk,bool isExtended,bool isCursiVision,bool instantiateOleObject);

      ~pkAPI();

      //   IUnknown

      STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
      STDMETHOD_ (ULONG, AddRef)();
      STDMETHOD_ (ULONG, Release)();

      // IDispatch

      STDMETHOD(GetTypeInfoCount)(UINT *pctinfo);
      STDMETHOD(GetTypeInfo)(UINT itinfo, LCID lcid, ITypeInfo **pptinfo);
      STDMETHOD(GetIDsOfNames)(REFIID riid, LPOLESTR *rgszNames, UINT cNames, LCID lcid, DISPID *rgdispid);
      STDMETHOD(Invoke)(DISPID dispidMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS *pdispparams, VARIANT *pvarResult, EXCEPINFO *pexcepinfo, UINT *puArgErr);

   private:

      // IBasicSignaturePad

      long __stdcall Connect(BSTR serverIPorNetworkName);
      long __stdcall Disconnect();

      HRESULT __stdcall get_IsConnected(VARIANT_BOOL *isAvailable);

      long __stdcall Start();
      long __stdcall Stop();

      HRESULT __stdcall put_IPOrNetworkName(BSTR);
      HRESULT __stdcall get_IPOrNetworkName(BSTR *);

      HRESULT __stdcall put_Width(long v);
      HRESULT __stdcall get_Width(long *pV);

      HRESULT __stdcall put_DeviceWidth(long v);
      HRESULT __stdcall get_DeviceWidth(long *pV);

      HRESULT __stdcall put_WidthInches(float v);
      HRESULT __stdcall get_WidthInches(float *pV);

      HRESULT __stdcall put_Height(long v);
      HRESULT __stdcall get_Height(long *pV);

      HRESULT __stdcall put_DeviceHeight(long v);
      HRESULT __stdcall get_DeviceHeight(long *pV);

      HRESULT __stdcall put_HeightInches(float v);
      HRESULT __stdcall get_HeightInches(float *pV);

      HRESULT __stdcall put_Bounds(RECT *);
      HRESULT __stdcall get_Bounds(RECT *);

      HRESULT __stdcall put_BoundsInches(RECTF *);
      HRESULT __stdcall get_BoundsInches(RECTF *);

      HRESULT __stdcall put_InkColor(long value);
      HRESULT __stdcall get_InkColor(long *pValue);

      HRESULT __stdcall put_InkWeight(long value);
      HRESULT __stdcall get_InkWeight(long *pValue);

      HRESULT __stdcall put_FontSize(float value);
      HRESULT __stdcall get_FontSize(float *pValue);

      HRESULT __stdcall put_FontFamily(BSTR fontFamily);
      HRESULT __stdcall get_FontFamily(BSTR *pFontFamily);

      HRESULT __stdcall get_Image(UINT_PTR *pImageInControlWindowDimensions);
      HRESULT __stdcall get_NativeSizeImage(UINT_PTR *pImageInPadDimensions);

      HRESULT __stdcall get_ImageFile(BSTR *pBSTRImageFileInControlWindowDimensions);
      HRESULT __stdcall get_NativeSizeImageFile(BSTR *pBSTRImageFileInPadDimensions);

      HRESULT __stdcall put_BackgroundBitmapFile(BSTR imageFile);
      HRESULT __stdcall put_BackgroundBitmapHandle(UINT_PTR hBitmap);

      HRESULT __stdcall put_PDFFile(BSTR pdfFile);
      HRESULT __stdcall get_PDFFile(BSTR *pPdfFile);

      HRESULT __stdcall put_ShowUpdates(VARIANT_BOOL showUpdates);
      HRESULT __stdcall get_ShowUpdates(VARIANT_BOOL *pShowUpdates);

      HRESULT __stdcall AreaBitmapHandle(UINT_PTR hBitmap,long x,long y,long cx,long cy);

      HRESULT __stdcall CreateButton(BSTR buttonText,long eventID,long x,long y,BOOL isVisible);
      HRESULT __stdcall CreateDropDownBox(BSTR commaDelimitedItems,long controlID,long x,long y,BOOL isVisible,VARIANT selectedItem);
      HRESULT __stdcall CreateLabel(BSTR labelText,long controlID,long x,long y,BOOL isVisible,VARIANT fontFace,VARIANT fontSize);
      HRESULT __stdcall CreateLabelInches(BSTR labelText,long controlID,float x,float y,BOOL isVisible,VARIANT fontFace,VARIANT fontSize);
      HRESULT __stdcall CreateCheckBox(BSTR labelText,long controlID,long x,long y,BOOL isVisible,BOOL isChecked);
      HRESULT __stdcall CreateRadioButton(BSTR labelText,long controlID,long x,long y,BOOL isVisible,long groupNumber,BOOL isChecked);
      HRESULT __stdcall CreateTextBox(BSTR theText,long controlID,long x,long y,long width,long maxHeight,BOOL isVisible,VARIANT fontFace,VARIANT fontSizeInPoints);
      HRESULT __stdcall CreateEntryField(BSTR theText,long controlID,long x,long y,long width,BOOL isVisible,VARIANT fontFace,VARIANT fontSizeInPoints);

      HRESULT __stdcall RemoveControl(long eventID);

      HRESULT __stdcall get_ControlBounds(long eventId,RECT *pBounds);

      HRESULT __stdcall put_ControlPosition(long eventId,POINT point);
      HRESULT __stdcall get_ControlPosition(long eventId,POINT *pPoint);

      HRESULT __stdcall put_ControlText(long eventId,BSTR theText);
      HRESULT __stdcall get_ControlText(long eventId,BSTR *pResult);

      HRESULT __stdcall get_ControlTextBounds(long controlId,RECT *pBounds);

      HRESULT __stdcall get_ControlFontHeightPixels(long controlId,float *pResult);

      HRESULT __stdcall ClearInk();
      HRESULT __stdcall ClearEverything();
      HRESULT __stdcall ClearSettings();
      HRESULT __stdcall ClearBackground();

      HRESULT __stdcall ClearArea(long x,long y,long width,long height) { return E_NOTIMPL; };

      HRESULT __stdcall FireOption(long optionNumber);

      HRESULT __stdcall FireSelection(long controlId,BSTR theSelection);

      HRESULT __stdcall FireKeyStroke(long controlId,WPARAM keyStroke);

      HRESULT __stdcall Hide() { return sendCommand(HIDE); };
      HRESULT __stdcall Show() { return sendCommand(SHOW); };

      HRESULT __stdcall ShowProperties();

      HRESULT __stdcall HideControl(long controlID);
      HRESULT __stdcall ShowControl(long controlID);

      HRESULT __stdcall HideControlList(BSTR controlIds);
      HRESULT __stdcall ShowControlList(BSTR controlIds);

      HRESULT __stdcall put_DrawingWindowHandle(HWND hwndDrawingWindow);

      // IPhabletSignaturePad

      HRESULT __stdcall put_WidthInInches(double value);
      HRESULT __stdcall get_WidthInInches(double *pValue);

//      PhabletSignaturePad IConnectionPointContainer

     struct _IConnectionPointContainer : public IConnectionPointContainer {

     public:

        STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
        STDMETHOD_ (ULONG, AddRef)();
        STDMETHOD_ (ULONG, Release)();

        STDMETHOD(FindConnectionPoint)(REFIID riid,IConnectionPoint **);
        STDMETHOD(EnumConnectionPoints)(IEnumConnectionPoints **);

        _IConnectionPointContainer(pkAPI *pp);
        ~_IConnectionPointContainer();

        void fire_PenUp(long x,long y);
        void fire_PenDown(long x,long y);
        void fire_PenPoint(long x,long y,float inkWeight);
        void fire_OptionSelected(long optionNumber);
        void fire_OptionUnSelected(long optionNumber);
        void fire_DeviceReady();
        void fire_ConfigurationChanged();
        void fire_ItemSelected(long controlId,BSTR theItem);
        void fire_TextChanged(long controlId,BSTR oldText,BSTR newText);
        void fire_KeyStroke(long controlId,WPARAM keyStroke);

      private:

		  pkAPI *pParent;

      } connectionPointContainer;


      class _ICursiVisionSignaturePad : public ISignaturePad {

      public:

         _ICursiVisionSignaturePad(pkAPI *pp);
         ~_ICursiVisionSignaturePad();

         //   IUnknown

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

      private:

         long __stdcall TakeFrameWindow(HWND hwnd) { hwndCursiVisionFrameWindow = hwnd; return S_OK; };

         long __stdcall HostHandle(HWND hwnd);

         long __stdcall Load(char *pszOPOSName,HWND hwndClientHost,void *pvICursiVisionServices);
         long __stdcall Start();

         long __stdcall Unload();
         long __stdcall Stop();

         BOOL __stdcall IsActive() { return isActive; };

         long __stdcall Width();
         long __stdcall Height();
         long __stdcall LCDWidth();
         long __stdcall LCDHeight();

         double __stdcall WidthInInches() { return pParent -> widthInInches; };
         double __stdcall HeightInInches() { 
            if ( 0 == pParent -> heightInInches ) {
               double aspect = LCDWidth() / LCDHeight();
               pParent -> heightInInches = pParent -> widthInInches / aspect;
            }
            return pParent -> heightInInches; 
         };

         double __stdcall PadToLCDScaleX() { return 1.0; };
         double __stdcall PadToLCDScaleY() { return 1.0; };

         double __stdcall PadTimeout() { return signatureTimeOutPeriod; };

         double __stdcall SetZoomFactor(double zf) { return E_NOTIMPL; };

         void __stdcall PrepareForPage(RECT *pRect);

         void __stdcall DisplaySignatureBitmapFile(char *pszImageFile,BOOL populatePad = FALSE,BOOL isLastPage = TRUE);
         void __stdcall DisplaySignatureBitmapHandle(UINT_PTR hBitmap,HDC hdc,long x,long y,long cx,long cy);
         void __stdcall OverlaySignatureBitmapHandle(UINT_PTR hBitmap,HDC hdc,long x,long y,long cx,long cy);

         void __stdcall DisplayHotSpotFile(char *pszFile,long eventID,long x,long y,long cx,long cy);
         void __stdcall DisplayHotSpotHandle(UINT_PTR hBitmap,HDC hdc,long eventID,long x,long y,long cx,long cy);
         void __stdcall DisplayOk(long eventID);
         void __stdcall DisplayClear(long eventID);
         void __stdcall DisplayCancel(long eventID);

         short __stdcall KeyPadQueryHotSpot(short keyCode);

         void __stdcall SetBackgroundFile(BSTR bstrFileName);
         void __stdcall SetBackgroundHandle(OLE_HANDLE hBitmap);

         char * __stdcall DeviceName() { return "Phablet Signature Pad"; };
         char * __stdcall DeviceModel() { return "InnoVisioNate"; };
         char * __stdcall DeviceProductName() { return STR_PHABLET_MODEL; };

         long __stdcall IsLCD() { return true; };
         long __stdcall IsFullPage() { return false; };
         long __stdcall HasScaling() { return true; };
         long __stdcall IsTabletPC() { return false; };

         void __stdcall GetRect(RECT *);
         void __stdcall GetHotSpot(long hotSpotNumber,RECT *);

         long __stdcall ClearInk();

         long __stdcall SignatureData();
         double * __stdcall SignatureDataX() { return pSignatureDataX; };
         double * __stdcall SignatureDataY() { return pSignatureDataY; };
         void __stdcall ClearSignatureData();
         void __stdcall DeleteSignatureData();

         void __stdcall ClearTablet();
         void __stdcall TabletState(long ts);
         BOOL __stdcall EnableTablet();
         BOOL __stdcall DisableTablet();

         void __stdcall SetSigWindow() { return; };
         void __stdcall SetInkArea() { return; };

         long __stdcall GetPixelBits(HDC hdc);

         long __stdcall MaximumX();
      
         long __stdcall MaximumY();

         long __stdcall MinimumX();
      
         long __stdcall MinimumY();

         long __stdcall MaximumSignableY();

         long __stdcall FireOption(long optionNumber);

         BOOL __stdcall CanSaveSignatureValidationData() { return FALSE; };

         BOOL __stdcall CaptureSignatureValidationData(BOOL) { return E_NOTIMPL; };
         long __stdcall AddKeyData(BSTR) { return E_NOTIMPL; };
         long __stdcall SaveSignatureValidationData(BSTR) { return E_NOTIMPL; };
         long __stdcall ImportSignature(BSTR) { return E_NOTIMPL; };
         long __stdcall ExportSignature(BSTR) { return E_NOTIMPL; };
         long __stdcall GetSignature(BSTR *pBstrClientFree) { return E_NOTIMPL; };
         long __stdcall SetSignature(BSTR) { return E_NOTIMPL; };

         long __stdcall OriginPointX() { return originPoint.x; };

         long __stdcall OriginPointY() { return originPoint.y; };

         long __stdcall BaselinePointX() { return baselinePoint.x; };

         long __stdcall BaselinePointY() { return baselinePoint.y; };

         DWORD __stdcall InkColor() { return pParent -> inkColor; };

         long __stdcall InkWeight() { return pParent -> inkWeight; };

         long __stdcall ShowProperties();

         void __stdcall PutLCDCaptureMode(long mode) { return; };

         void __stdcall LCDRefresh(long Mode,long XPos,long YPos,long XSize,long YSize);
   
         //      IConnectionPointContainer

         struct _IConnectionPointContainer : public IConnectionPointContainer {

         public:

            _IConnectionPointContainer(_ICursiVisionSignaturePad *pp);
		    ~_IConnectionPointContainer();

            STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
            STDMETHOD_ (ULONG, AddRef)();
            STDMETHOD_ (ULONG, Release)();

            STDMETHOD(FindConnectionPoint)(REFIID riid,IConnectionPoint **);
            STDMETHOD(EnumConnectionPoints)(IEnumConnectionPoints **);

            void fire_PenUp(long x,long y);
            void fire_PenDown(long x,long y);
            void fire_PenPoint(long x,long y,float inkWeight);
            void fire_OptionSelected(long optionNumber);

         private:

		    _ICursiVisionSignaturePad *pParent;

         } *pIConnectionPointContainer;

         ICursiVisionServices *pICursiVisionServices;

         HWND hwndCursiVisionFrameWindow;
         bool isActive;

         double signatureTimeOutPeriod;

         POINTL originPoint,baselinePoint;

         double *pSignatureDataX;
         double *pSignatureDataY;

         pkAPI *pParent;

         friend class pkAPI;

      } *pICursiVisionSignaturePad;


      class _IVisioLoggerSignaturePad : public IVisioLoggerSignaturePad {

      public:

         _IVisioLoggerSignaturePad(pkAPI *pp);
         ~_IVisioLoggerSignaturePad();

         //   IUnknown

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

      private:

#include "IVisioLoggerSignaturePad_declarations.h"

         //      IConnectionPointContainer

         struct _IConnectionPointContainer : public IConnectionPointContainer {

         public:

            _IConnectionPointContainer(_IVisioLoggerSignaturePad *pp);
		    ~_IConnectionPointContainer();

            STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
            STDMETHOD_ (ULONG, AddRef)();
            STDMETHOD_ (ULONG, Release)();

            STDMETHOD(FindConnectionPoint)(REFIID riid,IConnectionPoint **);
            STDMETHOD(EnumConnectionPoints)(IEnumConnectionPoints **);

            void fire_PenUp(long x,long y);
            void fire_PenDown(long x,long y);
            void fire_PenPoint(long x,long y,float inkWeight);
            void fire_OptionSelected(long optionNumber);

         private:

		    _IVisioLoggerSignaturePad *pParent;

         } *pIConnectionPointContainer;

      private:

         pkAPI *pParent;
         bool menuMode;

         friend class pkAPI;

      } *pIVisioLoggerSignaturePad;

      class _IVisioLoggerKioskPad : public IVisioLoggerKioskPad {

      public:

         _IVisioLoggerKioskPad(pkAPI *pp);
         ~_IVisioLoggerKioskPad();

         //   IUnknown

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

      private:

         // IVisioLoggerKioskPad

         STDMETHOD(Connect)(BSTR networkNameOrIPAddress);
         STDMETHOD(HostWidth)(long);
         STDMETHOD(HostHeight)(long);

         STDMETHOD(put_Background)(UINT_PTR);

         //      IConnectionPointContainer

         struct _IConnectionPointContainer : public IConnectionPointContainer {

         public:

            _IConnectionPointContainer(_IVisioLoggerKioskPad *pp);
		    ~_IConnectionPointContainer();

            STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
            STDMETHOD_ (ULONG, AddRef)();
            STDMETHOD_ (ULONG, Release)();

            STDMETHOD(FindConnectionPoint)(REFIID riid,IConnectionPoint **);
            STDMETHOD(EnumConnectionPoints)(IEnumConnectionPoints **);

         private:

		    _IVisioLoggerKioskPad *pParent;

         } *pIConnectionPointContainer;

         pkAPI *pParent;

         friend class pkAPI;

      } *pIVisioLoggerKioskPad;

      //   IPropertiesClient

      class _IGPropertiesClient : public IGPropertiesClient {

      public:
   
         _IGPropertiesClient(pkAPI *pp) : pParent(pp), refCount(0) {};
         ~_IGPropertiesClient() {};

         //   IUnknown
   
         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

         STDMETHOD(SavePrep)();
         STDMETHOD(InitNew)();
         STDMETHOD(Loaded)();
         STDMETHOD(Saved)();
         STDMETHOD(IsDirty)();
         STDMETHOD(GetClassID)(BYTE *pCLSID);

      private:

         pkAPI *pParent;
         long refCount;

      } *pIGPropertiesClient;


      struct _IConnectionPoint : IConnectionPoint {

	   public:
         
         _IConnectionPoint(pkAPI *pp,REFIID myEvents);
		 ~_IConnectionPoint();

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

         STDMETHOD (GetConnectionInterface)(IID *);
         STDMETHOD (GetConnectionPointContainer)(IConnectionPointContainer **ppCPC);
         STDMETHOD (Advise)(IUnknown *pUnk,DWORD *pdwCookie);
         STDMETHOD (Unadvise)(DWORD);
         STDMETHOD (EnumConnections)(IEnumConnections **ppEnum);

         IUnknown *AdviseSink() { return adviseSink; };

      private:

         int getSlot();
         int findSlot(DWORD dwCookie);

         IUnknown *adviseSink;
         pkAPI *pParent;
         DWORD nextCookie;
         IID myEventsInterface;

		 int countConnections,countLiveConnections;

         CONNECTDATA *connections;

      } *pConnectionPointExtended,*pConnectionPointVisioLogger,*pConnectionPointCursiVision,*pConnectionPointVisioLoggerKiosk;


	  struct _IEnumConnectionPoints : IEnumConnectionPoints {

	  public:

        STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
        STDMETHOD_ (ULONG, AddRef)();
        STDMETHOD_ (ULONG, Release)();

 	     STDMETHOD (Next)(ULONG cConnections,IConnectionPoint **rgpcn,ULONG *pcFetched);
        STDMETHOD (Skip)(ULONG cConnections);
        STDMETHOD (Reset)();
        STDMETHOD (Clone)(IEnumConnectionPoints **);

	     _IEnumConnectionPoints(pkAPI *pp,_IConnectionPoint **cp,int connectionPointCount);
       ~_IEnumConnectionPoints();

     private:

        int cpCount,enumeratorIndex;
		  pkAPI *pParent;
		  _IConnectionPoint **connectionPoints;

     } *pEnumConnectionPoints;

     struct _IEnumerateConnections : public IEnumConnections {

     public:

        _IEnumerateConnections(IUnknown* pParentUnknown,ULONG cConnections,CONNECTDATA* paConnections,ULONG initialIndex);
        ~_IEnumerateConnections();

         STDMETHODIMP QueryInterface(REFIID, void **);
         STDMETHODIMP_(ULONG) AddRef();
         STDMETHODIMP_(ULONG) Release();
         STDMETHODIMP Next(ULONG, CONNECTDATA*, ULONG*);
         STDMETHODIMP Skip(ULONG);
         STDMETHODIMP Reset();
         STDMETHODIMP Clone(IEnumConnections**);

      private:
        ULONG refCount;
        IUnknown *pParentUnknown;
        ULONG enumeratorIndex;
        ULONG countConnections;
        CONNECTDATA *connections;

      };

      //      IPropertyPageClient

      class _IGPropertyPageClient : public IGPropertyPageClient {

      public:

         _IGPropertyPageClient(pkAPI *p) : pParent(p),refCount(0) {};
         ~_IGPropertyPageClient() {};

         //   IUnknown

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

      private:

         STDMETHOD(BeforeAllPropertyPages)();
         STDMETHOD(GetPropertyPagesInfo)(long* countPages,SAFEARRAY** stringDescriptions,SAFEARRAY** stringHelpDirs,SAFEARRAY** pSize);
         STDMETHOD(CreatePropertyPage)(long,HWND,RECT*,BOOL,HWND *pHwndPropertyPage);
         STDMETHOD(Apply)();
         STDMETHOD(IsPageDirty)(long,BOOL*);
         STDMETHOD(Help)(BSTR bstrHelpDir);
         STDMETHOD(TranslateAccelerator)(long,long*);
         STDMETHOD(AfterAllPropertyPages)(BOOL);
         STDMETHOD(DestroyPropertyPage)(long);

         STDMETHOD(GetPropertySheetHeader)(void *pHeader);
         STDMETHOD(get_PropertyPageCount)(long *pCount);
         STDMETHOD(GetPropertySheets)(void *pSheets);

         pkAPI *pParent;
         long refCount;

      } *pIGPropertyPageClient;

#include "ActiveX Control\OleObject.h"

      IOleObject *pIOleObject_HTML;
      IOleInPlaceActiveObject *pIOleInPlaceActiveObject_HTML;
      IOleInPlaceObject *pIOleInPlaceObject_HTML;
      IWebBrowser2 *pIWebBrowser;

#define EMBEDDED_OBJECT_EMBEDDER_CLASS pkAPI

#include "interfacesToSupportAnEmbeddedObject.h"

   public:

      _IOleObject *GetIOleObject() { return pIOleObject; };

   private:

      long acquireMutex();
      long releaseMutex();

      HRESULT pingServer();
      HRESULT connectServer();
      HRESULT disconnectServer();

      HRESULT postConnectDetermineScale();

      HRESULT sendImage(char *pszImageName,long x,long y,long cx,long cy);
      HRESULT recieveImage(char *pszImageFile);

      HRESULT sendPDF(char *pszPdfFileName);
      HRESULT recievePDF(char *pszPdfFileName);

      HRESULT sendFile(char *pszFileName,long countBytes,WCHAR *pszwCommandPreamble);
      HRESULT recieveFile(char *pszFileName,WCHAR *pszwCommandPreamble);

      HRESULT sendStart();
      HRESULT sendStop();
      HRESULT sendCommand(enum pkCommands);
      HRESULT sendCommand(WCHAR *pszwCommand);
      //HRESULT sendCommand() { return sendCommand(szwCommand); };
      HRESULT writeCommand(WCHAR *pszwCommand);

      HRESULT queryValue(pkParameters parameter,WCHAR *pszwResult,long resultSize,long identifier = -1L,WCHAR *pszArgument = NULL);

      HRESULT setValue(pkParameters parameter,WCHAR *pszValue,long identifier = -1L);
      HRESULT setValue(pkParameters parameter,long value,long identifier = -1L);
      HRESULT setValue(pkParameters parameter,float value,long identifier = -1L);

      SOCKET connectPort(char *pszServer,WCHAR *pszPort);

      void openEventSink();
      void closeEventSink();

      void startHeartbeat();
      void stopHeartbeat(bool waitForCompletion);

      bool IsConnected();

      void penPoint(long x,long y,float inkWeight);
      void penDown(long x,long y);
      void penUp(long x,long y);
      void optionSelected(long optionNumber);
      void optionUnSelected(long optionNumber);
      void deviceReady();
      void configurationChanged();
      void itemSelected(long controlId,WCHAR *pszItem);
      void textChanged(long controlId,WCHAR *pszOldText,WCHAR *pszNewText);

      void setupGDIPlus(HWND hwndDrawing);
      void teardownGDIPlus();

      long getPadWidth();
      long getPadHeight();

      void setPadWidth(long v);
      void setPadHeight(long v);

      double getPadWidthInInches();

      float getPhysicalWidthInches();
      float getPhysicalHeightInches();

      long getPhysicalWidthPixels();
      long getPhysicalHeightPixels();

      void initializeMyWindow(bool isTransparent,bool isSunkenBorder,HWND hwndParent);

      void startIE();

      BYTE startProperties;

      long inkWeight;
      DWORD inkColor;
      long pixelWidth,pixelHeight;
      double widthInInches;
      double heightInInches;
      float fontSize;
      WCHAR szwFontFamily[64];
      char szDeviceIpOrNetworkName[MAX_PATH];
      long connectAttempts;

      char szPDFFileName[MAX_PATH];

      BYTE endProperties;

      BYTE *pProperties;
      long propertiesSize;

      HWND hwndSettings;
      HWND hwndSite;
      HWND hwndClientProvidedSite;

      SIZEL sizelDisplay;

      WCHAR szwCommandsPort[32];
      WCHAR szwEventSourcePort[32];
      WCHAR szwControlPort[32];

      long refCount;

      bool isExtended;
      bool isCursiVision;

      long cachedPadWidth;
      long cachedPadHeight;

      IGProperties *pIGProperties;

      ULONG_PTR gdiplusToken;
      HDC hdcGDIPlus;
      Gdiplus::Graphics *pGDIPlusGraphic;
      Gdiplus::Pen *pGDIPlusPen;
      Gdiplus::GdiplusStartupInput gdiplusStartupInput;

      Gdiplus::ImageCodecInfo *pTheEncoder;
      Gdiplus::EncoderParameters *pTheEncoderParameters;

      HWND hwndBackground;

      float scaleInchesWidthToPixels {0};
      float scaleInchesHeightToPixels {0};

      static unsigned int __stdcall _eventSink(void *);
      static unsigned int __stdcall _heartbeat(void *);

      static LRESULT CALLBACK siteHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);
      static LRESULT CALLBACK settingsHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK runtimeSettingsHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK documentationSettingsHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK redTextHandler(HWND,UINT,WPARAM,LPARAM);

      static LRESULT CALLBACK inkHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK swatchHandler(HWND,UINT,WPARAM,LPARAM);

      static WNDPROC defaultSwatchHandler;
      static WNDPROC defaultTextHandler;

      IOleClientSite *pIOleClientSite_MySite;
      IOleInPlaceSite *pIOleInPlaceSite_MySite;

      friend class _IOleObject;

};

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

MIDL_DEFINE_GUID(CLSID,CLSID_InnoVisioNateVisioLoggerProperties,0x9CAEFD87,0x55E7,0x11d3,0x83,0x65,0x00,0x60,0x08,0xBD,0x5B,0xC3);

MIDL_DEFINE_GUID(CLSID,CLSID_PhabletSignaturePadPropertyPage,0x2FAAB00E,0x3D9F,0x4b4d,0xB5,0xFE,0xB7,0x1B,0x6B,0x7B,0xDD,0xFF);
MIDL_DEFINE_GUID(CLSID,CLSID_PhabletSignaturePadRuntimePropertyPage,0x2FAAB00E,0x3D9F,0x4b4d,0xB5,0xFE,0xB7,0x1B,0x6B,0x7B,0xDE,0x00);
MIDL_DEFINE_GUID(CLSID,CLSID_PhabletSignaturePadDocumentationPropertyPage,0x2FAAB00E,0x3D9F,0x4b4d,0xB5,0xFE,0xB7,0x1B,0x6B,0x7B,0xDE,0x01);

#define PUT_BOOL(v,id)  SendDlgItemMessage(hwnd,id,BM_SETCHECK,v ? BST_CHECKED : BST_UNCHECKED,0L);
#define PUT_LONG(v,id)  { char szX[32]; sprintf(szX,"%ld",v); SetDlgItemText(hwnd,id,szX); }
#define PUT_DOUBLE(v,id) { char szX[64]; sprintf(szX,"%5.2lf",v); SetDlgItemText(hwnd,id,szX); }
#define PUT_STRING(v,id) SetDlgItemText(hwnd,id,v);

#define GET_BOOL(v,id)  v = ( BST_CHECKED == SendDlgItemMessage(hwnd,id,BM_GETCHECK,0L,0L) ? true : false );
#define GET_LONG(v,id) {char szX[32]; GetDlgItemText(hwnd,id,szX,32); v = atol(szX); }
#define GET_DOUBLE(v,id) {char szX[64]; GetDlgItemText(hwnd,id,szX,64); v = atof(szX); }
#define GET_STRING(v,id) GetDlgItemText(hwnd,id,v,MAX_PATH);

   extern "C" int GetCommonAppDataLocation(HWND hwnd,char *);
   extern "C" int GetDocumentsLocation(HWND hwnd,char *);

   int GetLocation(HWND hwnd,long key,char *szFolderLocation);

   void SaveBitmapFile(HDC hdcSource,HBITMAP hBitmap,char *pszFileName);

#define PIXELS_TO_HIMETRIC(x,ppli)  ( (2540*(x) + ((ppli) >> 1)) / (ppli) )
#define HIMETRIC_TO_PIXELS(x,ppli)  ( ((ppli)*(x) + 1270) / 2540 )

   int pixelsToHiMetric(SIZEL *pPixels,SIZEL *phiMetric);
   int hiMetricToPixel(SIZEL *phiMetric,SIZEL *pPixels);

#ifdef DEFINE_DATA

   pkAPI *pStaticObject = NULL;

   char szModuleName[MAX_PATH];
   OLECHAR wstrModuleName[256];

   char szApplicationDataDirectory[MAX_PATH];
   HINSTANCE hModule;

   HANDLE hMutexConnection = INVALID_HANDLE_VALUE;

   SOCKET commandSocket = INVALID_SOCKET;
   SOCKET controlSocket = INVALID_SOCKET;

   long eventSinkStopRequested = 0L;
   SOCKET eventSinkSocket = INVALID_SOCKET;
   HANDLE hEventSinkThread = NULL;

   long heartbeatStopRequested = 0L;
   HANDLE hHeartbeatThread = NULL;

   HBITMAP hBitmapBackground = NULL;
   long cxBitmapBackground = 0L;
   long cyBitmapBackground = 0L;

   long lastSignatureX = 0L;
   long lastSignatureY = 0L;

   char szResponse[RESPONSE_SIZE];

   ITypeInfo *pITypeInfo_IBasicSignaturePad = NULL;

   bool fromOleObject = false;

   HANDLE hProcessingDone = NULL;

   long maxStringConsumptionIndex = 127;
   WCHAR szwStringsForConsumption[128][MAX_STRING_SIZE];
   long stringConsumptionIndex = -1L;

#else

   extern pkAPI *pStaticObject;

   extern char szModuleName[];
   extern OLECHAR wstrModuleName[];

   extern char szApplicationDataDirectory[];
   extern HINSTANCE hModule;

   extern HANDLE hMutexConnection;

   extern SOCKET commandSocket;
   extern SOCKET controlSocket;

   extern long eventSinkStopRequested;
   extern SOCKET eventSinkSocket;
   extern HANDLE hEventSinkThread;

   extern long heartbeatStopRequested;
   extern HANDLE hHeartbeatThread;

   extern HBITMAP hBitmapBackground;
   extern long cxBitmapBackground;
   extern long cyBitmapBackground;

   extern long lastSignatureX;
   extern long lastSignatureY;

   extern char szResponse[];

   extern ITypeInfo *pITypeInfo_IBasicSignaturePad;

   extern bool fromOleObject;

   extern HANDLE hProcessingDone;

   extern long maxStringConsumptionIndex;
   extern WCHAR szwStringsForConsumption[][MAX_STRING_SIZE];
   extern long stringConsumptionIndex;

#endif
