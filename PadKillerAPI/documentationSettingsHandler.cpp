// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

#define MSHTML_SUPPORTING_CLASS pkAPI

#include "interfacesToSupportMSHTML.h"

#include <Urlmon.h>

   static HWND hwndHTMLHost = NULL;
   static RECT rcHTMLHost;

   LRESULT CALLBACK pkAPI::documentationSettingsHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   pkAPI *p = (pkAPI *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   switch ( msg ) {

   case WM_INITDIALOG: {

      PROPSHEETPAGE *pPage = reinterpret_cast<PROPSHEETPAGE *>(lParam);

      p = (pkAPI *)pPage -> lParam;

      SetWindowLongPtr(hwnd,GWLP_USERDATA,(ULONG_PTR)p);

      GetClientRect(hwnd,&rcHTMLHost);

      hwndHTMLHost = hwnd;

      GetClientRect(hwndHTMLHost,&rcHTMLHost);

      p -> startIE();

      }
      return LRESULT(FALSE);

   case WM_SIZE: 
      if ( ! p -> pIOleInPlaceObject_HTML )
         break;
      rcHTMLHost.right = LOWORD(lParam);
      rcHTMLHost.bottom = HIWORD(lParam);
      p -> pIOleInPlaceObject_HTML -> SetObjectRects(&rcHTMLHost,&rcHTMLHost);
      break;

   case WM_DESTROY: {

      if ( ! p -> pIOleObject_HTML )
         break;

      p -> pIOleObject_HTML -> Close(OLECLOSE_NOSAVE);

      p -> pIOleObject_HTML -> SetClientSite(NULL);

      p -> pIWebBrowser -> Release();

      p -> pIOleInPlaceObject_HTML -> Release();

//TODO: Need to release MSHTML appropriately

#if 0
      delete p -> pIOleClientSite_HTML_Host;
      delete p -> pIOleInPlaceSite_HTML_Host;
      delete p -> pIOleInPlaceObject_HTML;
      delete p -> pIOleInPlaceActiveObject_HTML;
#endif

      p -> pIOleClientSite_HTML_Host = NULL;
      p -> pIOleInPlaceSite_HTML_Host = NULL;
      p -> pIOleInPlaceObject_HTML = NULL;
      p -> pIOleInPlaceActiveObject_HTML = NULL;

      }
      break;

   default:
      break;

   }

   return LRESULT(FALSE);
   }


   void pkAPI::startIE() {

   HRESULT rc = CoCreateInstance(CLSID_WebBrowser,NULL,CLSCTX_INPROC_SERVER,IID_IWebBrowser2,reinterpret_cast<void **>(&pIWebBrowser));

   CoInternetSetFeatureEnabled(FEATURE_LOCALMACHINE_LOCKDOWN,SET_FEATURE_ON_PROCESS,FALSE);

   pIOleInPlaceFrame_HTML_Host = new _IOleInPlaceFrame(this,hwndHTMLHost);
   pIOleInPlaceSite_HTML_Host = new _IOleInPlaceSite(this,pIOleInPlaceFrame_HTML_Host);
   pIOleClientSite_HTML_Host = new _IOleClientSite(this,pIOleInPlaceSite_HTML_Host,pIOleInPlaceFrame_HTML_Host);
   pIOleDocumentSite_HTML_Host = new _IOleDocumentSite(this,pIOleClientSite_HTML_Host);

   pIWebBrowser -> QueryInterface(IID_IOleObject,reinterpret_cast<void **>(&pIOleObject_HTML));
   
   pIOleObject_HTML -> QueryInterface(IID_IOleInPlaceObject,reinterpret_cast<void **>(&pIOleInPlaceObject_HTML));

   pIOleObject_HTML -> SetClientSite(pIOleClientSite_HTML_Host);

   VARIANT target,vEmpty;

   VariantInit(&vEmpty);
   VariantInit(&target);

   target.vt = VT_BSTR;
   target.bstrVal = L"_self";

   rc = pIWebBrowser -> put_Resizable(TRUE);

   rc = pIWebBrowser -> put_AddressBar(VARIANT_FALSE);

   rc = pIWebBrowser -> put_FullScreen(VARIANT_FALSE);

   wchar_t applicationDataDirectory[MAX_PATH];

   wchar_t homePage[MAX_PATH];

   MultiByteToWideChar(CP_ACP,0,szApplicationDataDirectory,-1,applicationDataDirectory,MAX_PATH);

   if ( isCursiVision )
      swprintf(homePage,L"%s\\SetupPhabletSignaturePad.htm",applicationDataDirectory);
   else
      swprintf(homePage,L"%s\\Documentation\\index.htm",applicationDataDirectory);

   rc = pIWebBrowser -> Navigate(homePage,&vEmpty,&target,&vEmpty,&vEmpty);

   pIOleInPlaceObject_HTML -> SetObjectRects(&rcHTMLHost,&rcHTMLHost);

   pIOleObject_HTML -> DoVerb(OLEIVERB_SHOW,NULL,pIOleClientSite_HTML_Host,0,hwndHTMLHost,&rcHTMLHost);

   return;
   }

#include "interfacesToSupportAnEmbeddedObject_IOleClientSite.cpp"
#include "interfacesToSupportAnEmbeddedObject_IOleDocumentSite.cpp"
#include "interfacesToSupportAnEmbeddedObject_IOleInPlaceFrame.cpp"
#include "interfacesToSupportAnEmbeddedObject_IOleInPlaceSite.cpp"

