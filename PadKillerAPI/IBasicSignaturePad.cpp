// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   extern WCHAR szwCommand[];

   long pkAPI::Connect(BSTR serverIPorNetworkName) {

   if ( IsConnected() ) 
      return postConnectDetermineScale();

   if ( serverIPorNetworkName && serverIPorNetworkName[0] )
      WideCharToMultiByte(CP_ACP,0,serverIPorNetworkName,-1,szDeviceIpOrNetworkName,MAX_PATH,0,0);

   if ( ! szDeviceIpOrNetworkName[0] )
      strcpy(szDeviceIpOrNetworkName,"localhost");

   HRESULT rc = connectServer();

   connectAttempts++;

   if ( 1 < connectAttempts && ! ( S_OK == rc ) ) 
      return E_FAIL;

   if ( ! ( S_OK == rc ) ) {
      if ( ! pIOleObject || ( pIOleObject -> isRunning ) ) {
         char szMessage[1024];
         sprintf(szMessage,"There was an error connecting to the server %s.\n\nWindows Tablets: Is the computer turned on and the pkListener running on it ?"
                              "\n\nAndroid Tablets: Please be sure to start the Phablet Signature Pad application.",szDeviceIpOrNetworkName);
         MessageBox(NULL,szMessage,"Error",MB_ICONEXCLAMATION | MB_TOPMOST);
      }
      return E_FAIL;
   }

   return postConnectDetermineScale();
   }


   long pkAPI::Disconnect() {

   if ( INVALID_SOCKET == commandSocket ) 
      return E_UNEXPECTED;

   if ( gdiplusToken ) {

      if ( hdcGDIPlus )
         ReleaseDC(hwndBackground,hdcGDIPlus);

      hwndBackground = NULL;

      if ( pGDIPlusPen )
         delete pGDIPlusPen;

      if ( pGDIPlusGraphic )
         delete pGDIPlusGraphic;

      pGDIPlusPen = NULL;
      pGDIPlusGraphic = NULL;

      Gdiplus::GdiplusShutdown(gdiplusToken);

      gdiplusToken = 0L;

   }

   ClearEverything();

   disconnectServer();

   return S_OK;
   }


   HRESULT pkAPI::get_IsConnected(VARIANT_BOOL *pResult) {

   if ( ! pResult)
      return E_POINTER;
   
   *pResult = VARIANT_TRUE;

   if ( IsConnected() )
      return S_OK;

   *pResult = VARIANT_FALSE;

   return S_OK;
   }


   HRESULT pkAPI::Start() {
   sendStart();
   if ( hwndClientProvidedSite )
      setupGDIPlus(hwndClientProvidedSite);
   return S_OK;
   }


   HRESULT pkAPI::Stop() {
   sendStop();
   return S_OK;
   }

   HRESULT pkAPI::put_IPOrNetworkName(BSTR newName) {
   WideCharToMultiByte(CP_ACP,0,newName,-1,szDeviceIpOrNetworkName,64,0,0);
   return S_OK;
   }

   HRESULT pkAPI::get_IPOrNetworkName(BSTR *pBstrName) {
   if ( ! pBstrName )
      return E_POINTER;
   *pBstrName = SysAllocStringLen(NULL,(DWORD)strlen(szDeviceIpOrNetworkName));
   MultiByteToWideChar(CP_ACP,0,szDeviceIpOrNetworkName,-1,*pBstrName,(DWORD)strlen(szDeviceIpOrNetworkName));
   return S_OK;
   }


   HRESULT pkAPI::put_Width(long v) { 
   setPadWidth(v); 
   return S_OK; 
   }


   HRESULT pkAPI::get_Width(long *pV) {
   if ( ! pV ) 
      return E_POINTER; 
   *pV = getPadWidth();
   return S_OK;
   }

   HRESULT pkAPI::put_DeviceWidth(long v) {
   return put_Width(v);
   }

   HRESULT pkAPI::get_DeviceWidth(long *pV) {
   return get_Width(pV);
   }

   HRESULT pkAPI::put_WidthInches(float v) { 
   setPadWidth((int)((float)v * scaleInchesWidthToPixels)); 
   long deviceHeight,deviceWidth;
   get_DeviceHeight(&deviceHeight);
   get_DeviceWidth(&deviceWidth);
   double aspect = (double)deviceWidth / (double)deviceHeight;
   put_HeightInches((float)((double)v / aspect));
   return S_OK; 
   }

   HRESULT pkAPI::get_WidthInches(float *pV) {
   if ( ! pV ) 
      return E_POINTER; 

   *pV = (float)widthInInches;

   return S_OK;
   }

   HRESULT pkAPI::put_Height(long v) {
   setPadHeight(v);
   return S_OK;
   }

   HRESULT pkAPI::get_Height(long *pV) {
   if ( ! pV ) 
      return E_POINTER; 
   *pV = getPadHeight(); 
   return S_OK; 
   }

   HRESULT pkAPI::put_DeviceHeight(long v) {
   return put_Height(v);
   }

   HRESULT pkAPI::get_DeviceHeight(long *pV) {
   return get_Height(pV);
   }

   HRESULT pkAPI::put_HeightInches(float v) {
   setPadHeight((int)((float)v * scaleInchesHeightToPixels));
   long deviceHeight,deviceWidth;
   get_DeviceHeight(&deviceHeight);
   get_DeviceWidth(&deviceWidth);
   double aspect = (double)deviceWidth / (double)deviceHeight;
   put_WidthInches((float)((double)v * aspect));
   return S_OK;
   }

   HRESULT pkAPI::get_HeightInches(float *pV) {
   if ( ! pV ) 
      return E_POINTER; 

   *pV = (float)heightInInches;

   return S_OK; 
   }


   HRESULT pkAPI::put_Bounds(RECT *pResult) {
   if ( ! pResult )
      return E_POINTER;
   swprintf(szwCommand,L"%ld %ld %ld %ld %ld %ld",SETPARAMETER,PARM_BOUNDS_PIXELS,pResult -> left,pResult -> top,pResult -> right - pResult -> left,pResult -> bottom - pResult -> top);
   return sendCommand(szwCommand);
   }


   HRESULT pkAPI::put_BoundsInches(RECTF *pResult) {
   if ( ! pResult )
      return E_POINTER;
   swprintf(szwCommand,L"%ld %ld %lf %lf %lf %lf",SETPARAMETER,PARM_BOUNDS_INCHES,pResult -> left,pResult -> top,pResult -> right - pResult -> left,pResult -> bottom - pResult -> top);
   return sendCommand(szwCommand);
   }

   HRESULT pkAPI::get_Bounds(RECT *pResult) {
   if ( ! pResult )
      return E_POINTER;
   memset(pResult,0,sizeof(RECT));
   WCHAR szwResult[32];
   HRESULT rc = queryValue(PARM_BOUNDS_PIXELS,szwResult,32);
   if ( ! ( S_OK == rc ) )
      return rc;
   pResult -> left = _wtol(wcstok(szwResult,L" ,",NULL));
   pResult -> top = _wtol(wcstok(NULL,L" ,",NULL));
   pResult -> right = _wtol(wcstok(NULL,L" ,",NULL));
   pResult -> bottom = _wtol(wcstok(NULL,L" ,",NULL));
   return S_OK;
   }

   HRESULT pkAPI::get_BoundsInches(RECTF *pResult) {
   if ( ! pResult )
      return E_POINTER;
   memset(pResult,0,sizeof(RECT));
   WCHAR szwResult[128];
   HRESULT rc = queryValue(PARM_BOUNDS_INCHES,szwResult,128);
   if ( ! ( S_OK == rc ) )
      return rc;
   pResult -> left = (float)_wtof(wcstok(szwResult,L" ,",NULL));
   pResult -> top = (float)_wtof(wcstok(NULL,L" ,",NULL));
   pResult -> right = (float)_wtof(wcstok(NULL,L" ,",NULL));
   pResult -> bottom = (float)_wtof(wcstok(NULL,L" ,",NULL));
   return S_OK;
   }

   HRESULT pkAPI::put_InkColor(long value) {
   inkColor = value; 
   return S_OK;
   }


   HRESULT pkAPI::get_InkColor(long *pValue) {
   if ( ! pValue ) 
      return E_POINTER;
   *pValue = inkColor; 
   return S_OK;
   }


   HRESULT pkAPI::put_InkWeight(long value) {
   inkWeight = value;
   return S_OK;
   }


   HRESULT pkAPI::get_InkWeight(long *pValue) {
   if ( ! pValue )
      return E_POINTER;
   *pValue = inkWeight;
   return S_OK;
   }


   HRESULT pkAPI::put_FontSize(float value) {
   fontSize = value;
   if ( IsConnected() )
      setValue(PARM_FONT_SIZE,value);
   if ( pIOleObject )
      pIOleObject -> setControlsFontSizeValue(value);
   return S_OK;
   }


   HRESULT pkAPI::get_FontSize(float *pValue) {
   if ( ! pValue )
      return E_POINTER;
   *pValue = fontSize;
   if ( IsConnected() ) {
      WCHAR szwResult[128];
      if ( S_OK == queryValue(PARM_FONT_SIZE,szwResult,128) ) {
         swscanf(szwResult,L"%f",pValue);
         fontSize = *pValue;
      }
   }      
   return S_OK;
   }


   HRESULT pkAPI::put_FontFamily(BSTR fontFamily) {
   if ( IsConnected() )
      setValue(PARM_FONT_FAMILY,fontFamily);
   if ( pIOleObject ) {
      wcscpy(szwFontFamily,fontFamily);
      pIOleObject -> setControlsFontFace(szwFontFamily);
   }
   return S_OK;
   }


   HRESULT pkAPI::get_FontFamily(BSTR *pFontFamily) {
   if ( ! pFontFamily )
      return E_POINTER;
   if ( IsConnected() )
      queryValue(PARM_FONT_FAMILY,szwFontFamily,64);
   *pFontFamily = SysAllocString(szwFontFamily);
   return S_OK;
   }




   long pkAPI::FireOption(long optionNumber) {
   connectionPointContainer.fire_OptionSelected(optionNumber);
   return S_OK;
   }


   long pkAPI::FireSelection(long controlId,BSTR selectedItem) {
   connectionPointContainer.fire_ItemSelected(controlId,selectedItem);
   return S_OK;
   }


   long pkAPI::FireKeyStroke(long controlId,WPARAM keyStroke) {
   connectionPointContainer.fire_KeyStroke(controlId,keyStroke);
   return S_OK;
   }


   long pkAPI::ShowProperties() {
   if ( ! pIGProperties )
      return E_UNEXPECTED;
   IUnknown *pIUnknown;
   QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));
   pIGProperties -> ShowProperties(hwndSite,pIUnknown);
   pIUnknown -> Release();
   return S_OK;
   }


   HRESULT pkAPI::ClearInk() {
   if ( hwndBackground ) {
      InvalidateRect(hwndBackground,NULL,TRUE);
      RedrawWindow(hwndBackground,NULL,NULL,RDW_UPDATENOW | RDW_ERASENOW);
   }
   return sendCommand(CLEARINK);
   }

 
   HRESULT pkAPI::ClearEverything() {

   if ( hBitmapBackground )
      DeleteObject(hBitmapBackground);

   hBitmapBackground = NULL;

   if ( hwndBackground ) {
      InvalidateRect(hwndBackground,NULL,TRUE);
      RedrawWindow(hwndBackground,NULL,NULL,RDW_UPDATENOW | RDW_ERASENOW);
   }

   if ( pIOleObject ) {
      for ( HWND hwnd : pIOleObject -> runtimeControls )
         DestroyWindow(hwnd);
      pIOleObject -> runtimeControls.clear();
   }

   /*return*/ sendCommand(CLEAREVERYTHING);
   return S_OK;
   }


   HRESULT pkAPI::ClearSettings() {
   ClearEverything();
   return sendCommand(CLEARSETTINGS);
   }

   HRESULT pkAPI::ClearBackground() {

   if ( hBitmapBackground )
      DeleteObject(hBitmapBackground);

   hBitmapBackground = NULL;

   if ( hwndBackground ) {
      InvalidateRect(hwndBackground,NULL,TRUE);
      RedrawWindow(hwndBackground,NULL,NULL,RDW_UPDATENOW | RDW_ERASENOW);
   }

   return sendCommand(CLEARBACKGROUND);
   }


   HRESULT pkAPI::ShowControl(long eventId) {

   WCHAR szwTemp[128];
   swprintf(szwTemp,L"%ld %ld",SHOWCONTROL,eventId);
   HRESULT rc = sendCommand(szwTemp);

   if ( ! pIOleObject )
      return rc;

   if ( ! pIOleObject -> renderControls )
      return rc;

   pIOleObject -> showControl(eventId);

   return rc;
   }


   HRESULT pkAPI::ShowControlList(BSTR controlIds) {

   WCHAR szwIds[256];

   wcscpy(szwIds,controlIds);//WideCharToMultiByte(CP_ACP,0,controlIds,-1,szIds,256,0,0);

   WCHAR szwIdsToPass[256];

   memset(szwIdsToPass,0,sizeof(szwIdsToPass));

   WCHAR *p = wcstok(szwIds,L",",NULL);

   while ( p ) {
      if ( pIOleObject && pIOleObject -> renderControls )
         pIOleObject -> showControl(_wtol(p));
      swprintf(szwIdsToPass + wcslen(szwIdsToPass),L"%s:",p);
      p = wcstok(NULL,L",",NULL);
   }

   szwIdsToPass[wcslen(szwIdsToPass)] = L'\0';

   WCHAR szwTemp[256];

   swprintf(szwTemp,L"%ld %s",SHOWCONTROLLIST,szwIdsToPass);

   return sendCommand(szwTemp);
   }

   HRESULT pkAPI::HideControl(long eventId) {

   WCHAR szwTemp[128];
   swprintf(szwTemp,L"%ld %ld",HIDECONTROL,eventId);
   HRESULT rc = sendCommand(szwTemp);

   if ( ! pIOleObject )
      return rc;

   if ( ! pIOleObject -> renderControls )
      return rc;

   pIOleObject -> hideControl(eventId);

   return rc;
   }

   HRESULT pkAPI::HideControlList(BSTR controlIds) {

   WCHAR szwIds[256];

   wcscpy(szwIds,controlIds);

   WCHAR szwIdsToPass[256];

   memset(szwIdsToPass,0,sizeof(szwIdsToPass));

   WCHAR *p = wcstok(szwIds,L",",NULL);

   while ( p ) {
      if ( pIOleObject && pIOleObject -> renderControls )
         pIOleObject -> hideControl(_wtol(p));
      swprintf(szwIdsToPass + wcslen(szwIdsToPass),L"%ls:",p);
      p = wcstok(NULL,L",",NULL);
   }

   szwIdsToPass[wcslen(szwIdsToPass)] = '\0';

   WCHAR szwTemp[256];

   swprintf(szwTemp,L"%ld %ls",HIDECONTROLLIST,szwIdsToPass);

   return sendCommand(szwTemp);
   }

   HRESULT pkAPI::RemoveControl(long eventId) {
   WCHAR szwTemp[128];
   swprintf(szwTemp,L"%ld %ld",DESTROYCONTROL,eventId);
   return sendCommand(szwTemp);
   }

   HRESULT pkAPI::get_ControlBounds(long eventId,RECT *pBounds) {
   if ( ! pBounds )
      return E_POINTER;
   if ( ! IsConnected() )
      return E_UNEXPECTED;
   WCHAR szwResult[128];
   if ( ! ( S_OK == queryValue(PARM_CONTROL_BOUNDS,szwResult,128,eventId) ) )
      return E_FAIL;
   swscanf(szwResult,L"%ld %ld %ld %ld",&pBounds -> left,&pBounds -> top,&pBounds -> right,&pBounds -> bottom);
   return S_OK;
   }

   HRESULT pkAPI::put_ControlPosition(long eventId,POINT point) {
   if ( ! IsConnected() )
      return E_UNEXPECTED;
   WCHAR szwValue[128];
   swprintf(szwValue,L"%ld %ld %ld %ld %ld",SETPARAMETER,PARM_CONTROL_POSITION,eventId,point.x,point.y);
   if ( ! ( S_OK == sendCommand(szwValue) ) )
      return E_FAIL;
   if ( pIOleObject )
      pIOleObject -> positionControl(eventId,point.x,point.y);
   return S_OK;
   }

   HRESULT pkAPI::get_ControlPosition(long eventId,POINT *pPoint) {
   if ( ! pPoint )
      return E_POINTER;
   if ( ! IsConnected() )
      return E_UNEXPECTED;
   WCHAR szwResult[128];
   if ( ! ( S_OK == queryValue(PARM_CONTROL_POSITION,szwResult,128,eventId) ) )
      return E_FAIL;
   swscanf(szwResult,L"%ld %ld",&pPoint -> x,&pPoint -> y);
   return S_OK;
   }


   HRESULT pkAPI::put_ControlText(long eventId,BSTR text) {
   if ( ! IsConnected() )
      return E_UNEXPECTED;
   WCHAR szwValue[128];
   swprintf(szwValue,L"%ld %ld %ld �%ls�",SETPARAMETER,PARM_CONTROL_TEXT,eventId,text);
   if ( ! ( S_OK == sendCommand(szwValue) ) )
      return E_FAIL;
   if ( pIOleObject ) 
      pIOleObject -> setControlText(eventId,text);
   return S_OK;
   }


   HRESULT pkAPI::get_ControlText(long eventId,BSTR *pResult) {
   if ( ! pResult )
      return E_POINTER;
   if ( ! IsConnected() )
      return E_UNEXPECTED;
   WCHAR szwResult[128];
   if ( ! ( S_OK == queryValue(PARM_CONTROL_TEXT,szwResult,128,eventId) ) )
      return E_FAIL;
   *pResult = SysAllocString(szwResult);
   return S_OK;
   }

   HRESULT pkAPI::get_ControlTextBounds(long eventId,RECT *pBounds) {
   if ( ! pBounds )
      return E_POINTER;
   if ( ! IsConnected() )
      return E_UNEXPECTED;
   WCHAR szwResult[128];
   if ( ! ( S_OK == queryValue(PARM_CONTROL_TEXT_BOUNDS,szwResult,128,eventId) ) )
      return E_FAIL;
   swscanf(szwResult,L"%ld %ld %ld %ld",&pBounds -> left,&pBounds -> top,&pBounds -> right,&pBounds -> bottom);
   return S_OK;
   }

   HRESULT pkAPI::get_ControlFontHeightPixels(long eventId,float *pResult) {
   if ( ! pResult )
      return E_POINTER;
   if ( ! IsConnected() )
      return E_UNEXPECTED;
   WCHAR szwResult[128];
   if ( ! ( S_OK == queryValue(PARM_CONTROL_TEXT_FONT_HEIGHT,szwResult,128,eventId) ) )
      return E_FAIL;
   swscanf(szwResult,L"%f",pResult);
   return S_OK;
   }

   HRESULT pkAPI::put_BackgroundBitmapFile(BSTR imageFile) {

   if ( pIOleObject && pIOleObject -> renderBackground ) {

      Gdiplus::Bitmap *pBitmap = new Gdiplus::Bitmap(imageFile);

      if ( hBitmapBackground )
         DeleteObject(hBitmapBackground);

      pBitmap -> GetHBITMAP(NULL,&hBitmapBackground);

      cxBitmapBackground = pBitmap -> GetWidth();
      cyBitmapBackground = pBitmap -> GetHeight();
   
      delete pBitmap;

      InvalidateRect(hwndSite,NULL,TRUE);

   }

   if ( ! imageFile || ! imageFile[0] ) {
      return 0L;
   }

   char szImageFile[MAX_PATH];

   WideCharToMultiByte(CP_ACP,0,imageFile,-1,szImageFile,MAX_PATH,0,0);

   return sendImage(szImageFile,-1,-1,-1,-1);
   }


   HRESULT pkAPI::put_BackgroundBitmapHandle(UINT_PTR hBitmap) {

   char szTempFile[MAX_PATH];
   sprintf(szTempFile,_tempnam(NULL,NULL));

   HDC hdc = CreateCompatibleDC(NULL);

   SaveBitmapFile(hdc,(HBITMAP)hBitmap,szTempFile);

   DeleteDC(hdc);

   HRESULT rc = sendImage(szTempFile,-1,-1,-1,-1);

   if ( ! pIOleObject ) {
      DeleteFile(szTempFile);
      return rc;
   }

   wchar_t fms[MAX_PATH];

   MultiByteToWideChar(CP_ACP,0,szTempFile,-1,fms,MAX_PATH);

   Gdiplus::Bitmap *pBitmap = new Gdiplus::Bitmap(fms);

   if ( hBitmapBackground )
      DeleteObject(hBitmapBackground);

   pBitmap -> GetHBITMAP(NULL,&hBitmapBackground);

   cxBitmapBackground = pBitmap -> GetWidth();
   cyBitmapBackground = pBitmap -> GetHeight();
   
   delete pBitmap;

   DeleteFile(szTempFile);

   InvalidateRect(hwndSite,NULL,TRUE);

   return rc;
   }


   HRESULT pkAPI::AreaBitmapHandle(UINT_PTR hBitmap,long x,long y,long cx,long cy) {
   char szTempFile[MAX_PATH];
   sprintf(szTempFile,_tempnam(NULL,NULL));
   HDC hdc = CreateCompatibleDC(NULL);
   SaveBitmapFile(hdc,(HBITMAP)hBitmap,szTempFile);
   DeleteDC(hdc);
   HRESULT rc = sendImage(szTempFile,x,y,cx,cy);
   DeleteFile(szTempFile);
   return rc;
   }


   HRESULT pkAPI::get_Image(UINT_PTR *pResult) {

   if ( ! pResult )
      return E_POINTER;

   BSTR bstrNative = NULL;

   HRESULT rc = get_NativeSizeImageFile(&bstrNative);

   if ( ! ( S_OK == rc ) )
      return rc;

   Gdiplus::Bitmap *pBitmap = Gdiplus::Bitmap::FromFile(bstrNative);

   HDC hdcTarget = CreateCompatibleDC(NULL);

   HBITMAP hbmTarget = CreateBitmap(sizelDisplay.cx,sizelDisplay.cy,1,GetDeviceCaps(hdcTarget,BITSPIXEL),NULL);

   HGDIOBJ oldObj = SelectObject(hdcTarget,hbmTarget);

   HDC hdcSource = CreateCompatibleDC(NULL);
   HBITMAP hbmSource = NULL;

   pBitmap -> GetHBITMAP(NULL,&hbmSource);

   HGDIOBJ oldObj2 = SelectObject(hdcSource,hbmSource);

   StretchBlt(hdcTarget,0,0,sizelDisplay.cx,sizelDisplay.cy,hdcSource,0,0,pBitmap -> GetWidth(),pBitmap -> GetHeight(),SRCCOPY);

   SelectObject(hdcTarget,oldObj);

   delete pBitmap;

   DeleteFileW(bstrNative);

   DeleteDC(hdcTarget);
   DeleteDC(hdcSource);

   *pResult = (UINT_PTR)hbmTarget;

   return S_OK;
   }

   HRESULT pkAPI::get_NativeSizeImage(UINT_PTR *pResult) {

   if ( ! pResult )
      return E_POINTER;

   BSTR resultFile = NULL;

   HRESULT rc = get_NativeSizeImageFile(&resultFile);

   if ( ! ( S_OK == rc ) )
      return rc;

   Gdiplus::Bitmap *pBitmap = Gdiplus::Bitmap::FromFile(resultFile);

   HDC hdcTarget = CreateCompatibleDC(NULL);

   HBITMAP hbmTarget = CreateBitmap(pBitmap -> GetWidth(),pBitmap -> GetHeight(),1,GetDeviceCaps(hdcTarget,BITSPIXEL),NULL);

   HGDIOBJ oldObj = SelectObject(hdcTarget,hbmTarget);

   HDC hdcSource = CreateCompatibleDC(NULL);
   HBITMAP hbmSource = NULL;

   pBitmap -> GetHBITMAP(NULL,&hbmSource);

   HGDIOBJ oldObj2 = SelectObject(hdcSource,hbmSource);

   BitBlt(hdcTarget,0,0,pBitmap -> GetWidth(),pBitmap -> GetHeight(),hdcSource,0,0,SRCCOPY);

   SelectObject(hdcTarget,oldObj);

   delete pBitmap;

   DeleteFileW(resultFile);

   DeleteDC(hdcTarget);
   DeleteDC(hdcSource);

   *pResult = (UINT_PTR)hbmTarget;

   SysFreeString(resultFile);

   return S_OK;
   }

   HRESULT pkAPI::get_ImageFile(BSTR *pResult) {

   if ( ! pResult )
      return E_POINTER;

   UINT_PTR hbm = NULL;

   HRESULT rc = get_Image(&hbm);

   if ( ! ( S_OK == rc ) )
      return rc;

   Gdiplus::Bitmap *pBitmap = Gdiplus::Bitmap::FromHBITMAP((HBITMAP)hbm,NULL);

   BSTR bstrTemp = _wtempnam(NULL,NULL);

   pBitmap -> Save(bstrTemp,&pTheEncoder -> Clsid,NULL);

   delete pBitmap;

   *pResult = SysAllocString(bstrTemp);

   return S_OK;
   }


   HRESULT pkAPI::get_NativeSizeImageFile(BSTR *pResult) {
   if ( ! pResult )
      return E_POINTER;
   char szImageFile[MAX_PATH];
   HRESULT rc = recieveImage(szImageFile);
   if ( ! ( S_OK == rc ) )
      return rc;
   BSTR fms = SysAllocStringLen(NULL,(DWORD)strlen(szImageFile));
   MultiByteToWideChar(CP_ACP,0,szImageFile,-1,fms,MAX_PATH);
   *pResult = SysAllocString(fms);
   SysFreeString(fms);
   return S_OK;
   }

   HRESULT pkAPI::put_PDFFile(BSTR pdfFile) {
   if ( ! pdfFile || ! pdfFile[0] ) 
      return 0L;
   WideCharToMultiByte(CP_ACP,0,pdfFile,-1,szPDFFileName,MAX_PATH,0,0);
   if ( ! IsConnected() )
      return S_OK;
   return sendPDF(szPDFFileName);
   }


   HRESULT pkAPI::get_PDFFile(BSTR *pResult) {
   if ( ! pResult )
      return E_POINTER;
   *pResult = SysAllocStringLen(NULL,(DWORD)strlen(szPDFFileName));
   MultiByteToWideChar(CP_ACP,0,szPDFFileName,-1,*pResult,(DWORD)strlen(szPDFFileName));
   return S_OK;
   }


   HRESULT pkAPI::put_ShowUpdates(VARIANT_BOOL showUpdates) {
   setValue(PARM_UPDATES_SHOWING,(long)(VARIANT_TRUE == showUpdates ? 1 : 0));
   return S_OK;
   }

   HRESULT pkAPI::get_ShowUpdates(VARIANT_BOOL *pShowUpdates) {
   if ( ! pShowUpdates )
      return E_POINTER;
   WCHAR szwResult[32];
   queryValue(PARM_UPDATES_SHOWING,szwResult,32);
   if ( _wtol(szwResult) )
      *pShowUpdates = VARIANT_TRUE;
   else
      *pShowUpdates = VARIANT_FALSE;
   return S_OK;
   }


   HRESULT pkAPI::put_DrawingWindowHandle(HWND hwndDrawingWindow) {

   if ( ! IsConnected() )
      return E_UNEXPECTED;

   hwndClientProvidedSite = hwndDrawingWindow;

   setupGDIPlus(hwndClientProvidedSite);

   return S_OK;
   }