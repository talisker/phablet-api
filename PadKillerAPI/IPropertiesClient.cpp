// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   long __stdcall pkAPI::_IGPropertiesClient::QueryInterface(REFIID riid,void **ppv) {

   *ppv = NULL; 
 
   if ( riid == IID_IGPropertiesClient )
      *ppv = static_cast<IGPropertiesClient *>(this);
   else
 
      return pParent -> QueryInterface(riid,ppv);
 
   static_cast<IUnknown*>(*ppv) -> AddRef();
  
   return S_OK; 
   }
 
   unsigned long __stdcall pkAPI::_IGPropertiesClient::AddRef() {
   return 1;
   }
 
   unsigned long __stdcall pkAPI::_IGPropertiesClient::Release() {
   return 1;
   }


   HRESULT pkAPI::_IGPropertiesClient::SavePrep() {
   return S_OK;
   }


   HRESULT pkAPI::_IGPropertiesClient::InitNew() {
   pParent -> widthInInches = 4.0;
   return Loaded();
   }


   HRESULT pkAPI::_IGPropertiesClient::Loaded() {
   return S_OK;
   }


   HRESULT pkAPI::_IGPropertiesClient::Saved() {
   return S_OK;
   }


   HRESULT pkAPI::_IGPropertiesClient::IsDirty() {
   return S_FALSE;
   }

   HRESULT pkAPI::_IGPropertiesClient::GetClassID(BYTE *pCLSID) {
   return E_NOTIMPL;
   }
