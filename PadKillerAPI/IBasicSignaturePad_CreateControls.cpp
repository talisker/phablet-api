// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   HRESULT pkAPI::CreateButton(BSTR buttonText,long controlId,long xPad,long yPad,BOOL isVisible) {

   RemoveControl(controlId);

   WCHAR szwTemp[256];
   swprintf(szwTemp,L"%ld %ld %ld %ld %ld �%ls�",CREATEBUTTON,xPad,yPad,controlId,isVisible ? 1L : 0L,buttonText);

   HRESULT rc = sendCommand(szwTemp);

   if ( ! pIOleObject )
      return rc;

   if ( ! pIOleObject -> renderControls )
      return rc;

   RECT rcControl = {0};

   get_ControlBounds(controlId,&rcControl);

   long x = (long)((double)rcControl.left * pIOleObject -> cxScale);

   long y = (long)((double)rcControl.top * pIOleObject -> cyScale);

   HWND hwndControl = pIOleObject -> addControl(L"button",0L,BS_PUSHBUTTON,x,y,controlId,buttonText,isVisible ? true : false);

   HFONT hFont = pIOleObject -> setControlFont(controlId);

   SetWindowPos(hwndControl,NULL,0,0,(long)((double)(rcControl.right - rcControl.left) * pIOleObject -> cxScale),(long)((double)(rcControl.bottom - rcControl.top) * pIOleObject -> cyScale),SWP_NOMOVE);

   return rc;
   }


   HRESULT pkAPI::CreateDropDownBox(BSTR commaDelimitedItems,long eventId,long xPad,long yPad,BOOL isVisible,VARIANT vSelectedItem) {

   RemoveControl(eventId);

   WCHAR szwItems[1024];
   wcscpy(szwItems,commaDelimitedItems);

   WCHAR szwSelectedItem[128];
   szwSelectedItem[0] = L'\0';

   if ( vSelectedItem.vt == VT_BSTR ) {
      if ( vSelectedItem.bstrVal && 0 < wcslen(vSelectedItem.bstrVal) )
         wcscpy(szwSelectedItem,vSelectedItem.bstrVal);
   } 

   WCHAR szwTemp[256];
   swprintf(szwTemp,L"%ld %ld %ld %ld %ld �%ls� �%ls�",CREATEDROPDOWNBOX,xPad,yPad,eventId,isVisible ? 1L : 0L,szwItems,szwSelectedItem);
   HRESULT rc = sendCommand(szwTemp);

   if ( ! pIOleObject )
      return rc;

   if ( ! pIOleObject -> renderControls )
      return rc;

   long x = (long)((double)xPad * pIOleObject -> cxScale);

   long y = (long)((double)yPad * pIOleObject -> cyScale);

   HWND hwndControl = pIOleObject -> addControl(L"combobox",0L,CBS_DROPDOWNLIST,x,y,eventId,NULL,isVisible ? true : false);

   WCHAR *p = wcstok(szwItems,L",",NULL);

   WCHAR *pszwMaxLength = NULL;
   DWORD maxLength = 0L;

   WCHAR *pSelected = NULL;

   while ( p ) {
      if ( maxLength < (DWORD)wcslen(p) ) {
         maxLength = (DWORD)wcslen(p);
         pszwMaxLength = p;
      }
      SendMessageW(hwndControl,CB_ADDSTRING,0L,(LPARAM)p);
      if ( szwSelectedItem[0] && 0 == wcscmp(szwSelectedItem,p) )
         pSelected = p;
      p = wcstok(NULL,L",",NULL);
   }

   if ( pszwMaxLength ) {

      HFONT hFont = pIOleObject -> setControlFont(eventId);

      RECT rcText = {0};

      rcText.right = LONG_MAX;
      rcText.bottom = LONG_MAX;

      HDC hdc = GetDC(hwndControl);
      HGDIOBJ oldFont = SelectObject(hdc,hFont);
      long textHeight = DrawTextExW(hdc,pszwMaxLength,-1,&rcText,DT_CALCRECT | DT_WORDBREAK | DT_NOCLIP | DT_EDITCONTROL,NULL);
      SelectObject(hdc,oldFont);
      ReleaseDC(hwndControl,hdc);

      COMBOBOXINFO comboBoxInfo = {0};

      comboBoxInfo.cbSize = sizeof(comboBoxInfo);

      SendMessage(hwndControl,CB_GETCOMBOBOXINFO,0L,(LPARAM)&comboBoxInfo);

      SetWindowPos(hwndControl,HWND_TOP,0,0,rcText.right - rcText.left + 2 * (comboBoxInfo.rcButton.right - comboBoxInfo.rcButton.left),(long)SendMessage(hwndControl,CB_GETCOUNT,0L,0L) * (comboBoxInfo.rcItem.bottom - comboBoxInfo.rcItem.top),SWP_NOMOVE);

   }

   if ( pSelected )
      SendMessage(hwndControl,CB_SELECTSTRING,-1L,(LPARAM)pSelected);

   return rc;
   }


   HRESULT pkAPI::CreateLabelInches(BSTR labelText,long controlId,float x,float y,BOOL isVisible,VARIANT vFontFace,VARIANT vFontSize) {
   return CreateLabel(labelText,controlId,(long)(x * scaleInchesWidthToPixels),(long)(y * scaleInchesHeightToPixels),isVisible,vFontFace,vFontSize);
   }


   HRESULT pkAPI::CreateLabel(BSTR labelText,long controlId,long xPad,long yPad,BOOL isVisible,VARIANT vFontFace,VARIANT vFontSize) {

   RemoveControl(controlId);

   WCHAR szwFontFace[64];
   
   szwFontFace[0] = L'\0';

   if ( vFontFace.vt == VT_BSTR )
      wcscpy(szwFontFace,vFontFace.bstrVal);

   float fontSize = 0.0f;

   if ( vFontSize.vt == VT_R4 )
      fontSize = vFontSize.fltVal;

   else if ( vFontSize.vt == VT_R8 )
      fontSize = (float)vFontSize.dblVal;

   else if ( vFontSize.vt == VT_I4 )
      fontSize = (float)vFontSize.lVal;

   WCHAR szwTemp[65536];

   swprintf(szwTemp,L"%ld %ld %ld %ld %ld �%ls� \"%ls\" %lf",CREATELABEL,xPad,yPad,controlId,isVisible ? 1L : 0L,labelText,szwFontFace,fontSize);

   HRESULT rc = sendCommand(szwTemp);

   if ( ! pIOleObject )
      return rc;

   if ( ! pIOleObject -> renderControls )
      return rc;

   long x = (long)((double)xPad * pIOleObject -> cxScale);

   long y = (long)((double)yPad * pIOleObject -> cyScale);

   HWND hwndControl = pIOleObject -> addControl(L"static",0L,SS_LEFT,x,y,controlId,labelText,isVisible ? true : false);

   if ( ! labelText[0] ) 
      return rc;

   HFONT hFont = pIOleObject -> setControlFont(controlId,vFontFace.bstrVal);

   RECT rcText = {0};

   rcText.right = LONG_MAX;
   rcText.bottom = LONG_MAX;

   HDC hdc = GetDC(hwndSite);

   HGDIOBJ oldFont = SelectObject(hdc,hFont);

   long textHeight = DrawTextExW(hdc,labelText,-1,&rcText,DT_CALCRECT | DT_WORDBREAK | DT_NOCLIP | DT_EDITCONTROL,NULL);

   SelectObject(hdc,oldFont);

   ReleaseDC(HWND_DESKTOP,hdc);

   SetWindowPos(hwndControl,HWND_TOP,0,0,rcText.right - rcText.left,textHeight,SWP_NOMOVE);

   return S_OK;
   }


   HRESULT pkAPI::CreateCheckBox(BSTR labelText,long controlId,long xPad,long yPad,BOOL isVisible,BOOL isChecked) {

   RemoveControl(controlId);

   WCHAR szwTemp[256];

   swprintf(szwTemp,L"%ld %ld %ld %ld %ld %ld �%s�",CREATECHECKBOX,xPad,yPad,controlId,isVisible ? 1L : 0L,isChecked ? 1L : 0L,labelText);

   HRESULT rc = sendCommand(szwTemp);

   if ( ! pIOleObject )
      return rc;

   if ( ! pIOleObject -> renderControls )
      return rc;

   long x = (long)((double)xPad * pIOleObject -> cxScale);

   long y = (long)((double)yPad * pIOleObject -> cyScale);

   HWND hwndControl = pIOleObject -> addCheckBox(x,y,controlId,labelText,isVisible ? true : false,isChecked ? true : false);

   HFONT hFont = pIOleObject -> setControlFont(controlId);

   RECT rcControl = {0};

   get_ControlBounds(controlId,&rcControl);

   SetWindowPos(hwndControl,NULL,0,0,(long)((double)(rcControl.right - rcControl.left) * pIOleObject -> cxScale),(long)((double)(rcControl.bottom - rcControl.top) * pIOleObject -> cyScale),SWP_NOMOVE);

   return rc;
   }


   HRESULT pkAPI::CreateRadioButton(BSTR labelText,long controlId,long xPad,long yPad,BOOL isVisible,long groupNumber,BOOL isChecked) {

   RemoveControl(controlId);

   WCHAR szwTemp[256];
   swprintf(szwTemp,L"%ld %ld %ld %ld %ld %ld %ld �%ls�",CREATERADIOBUTTON,xPad,yPad,controlId,isVisible ? 1L : 0L,isChecked ? 1L : 0L,groupNumber,labelText);

   HRESULT rc = sendCommand(szwTemp);

   if ( ! pIOleObject )
      return rc;

   if ( ! pIOleObject -> renderControls )
      return rc;

   RECT rcControl = {0};

   get_ControlBounds(controlId,&rcControl);

   long x = (long)((double)rcControl.left * pIOleObject -> cxScale);

   long y = (long)((double)rcControl.top * pIOleObject -> cyScale);

   HWND hwndControl = pIOleObject -> addRadioButton(x,y,controlId,labelText,isVisible ? true : false,isChecked ? true : false,groupNumber);

   HFONT hFont = pIOleObject -> setControlFont(controlId);

   SetWindowPos(hwndControl,NULL,0,0,(long)((double)(rcControl.right - rcControl.left) * pIOleObject -> cxScale),(long)((double)(rcControl.bottom - rcControl.top) * pIOleObject -> cyScale),SWP_NOMOVE);

   return rc;
   }


   HRESULT pkAPI::CreateTextBox(BSTR theText,long controlId,long xPad,long yPad,long widthPad,long maxHeight,BOOL isVisible,VARIANT vFontFace,VARIANT vFontSize) {

   RemoveControl(controlId);

   WCHAR szwFontFace[64];
   
   szwFontFace[0] = '\0';

   if ( vFontFace.vt == VT_BSTR )
      wcscpy(szwFontFace,vFontFace.bstrVal);

   float fontSize = 0.0f;

   if ( vFontSize.vt == VT_R4 )
      fontSize = vFontSize.fltVal;
   else if ( vFontSize.vt == VT_R8 )
      fontSize = (float)vFontSize.dblVal;
   else if ( vFontSize.vt == VT_I4 )
      fontSize = (float)vFontSize.lVal;

   WCHAR *pszwText = new WCHAR[wcslen(theText) + 32];

   memset(pszwText,0,wcslen(theText) + 32);

   wcscpy(pszwText,theText);

   WCHAR *p = wcschr(pszwText,0x0d);

   while ( p ) {
      *p = '\\';
      *(p + 1) = 'n';
      p = wcschr(pszwText,0x0d);
   }

   WCHAR szwTemp[32768];

   swprintf(szwTemp,L"%ld %ld %ld %ld %ld %ld %ld �%ls� \"%ls\" %lf",CREATETEXTBOX,xPad,yPad,widthPad,maxHeight,controlId,isVisible ? 1L : 0L,pszwText,szwFontFace,fontSize);

   delete [] pszwText;

   HRESULT rc = sendCommand(szwTemp);

   if ( ! pIOleObject )
      return rc;

   if ( ! pIOleObject -> renderControls )
      return rc;

   long x = (long)((double)xPad * pIOleObject -> cxScale);

   long y = (long)((double)yPad * pIOleObject -> cyScale);

   long width = (long)((double)widthPad * pIOleObject -> cxScale);

   HWND hwndControl = pIOleObject -> addControl(L"edit",0L,ES_MULTILINE | ES_AUTOVSCROLL | 0*ES_READONLY,x,y,controlId,theText,isVisible ? true : false);

   HFONT hFont = pIOleObject -> setControlFont(controlId,szwFontFace);

   RECT rcControl = {0};

   get_ControlBounds(controlId,&rcControl);

   RECT rcText = {0};

   rcText.right = width;

   rcText.bottom = 0;

   HDC hdc = GetDC(HWND_DESKTOP);

   HGDIOBJ oldFont = SelectObject(hdc,hFont);

   long textHeight = DrawTextExW(hdc,theText,-1,&rcText,DT_CALCRECT | DT_WORDBREAK | DT_NOCLIP | DT_EDITCONTROL,NULL);

   long currentHeight = (long)((double)(rcControl.bottom - rcControl.top) * pIOleObject -> cyScale);

   if ( textHeight > currentHeight ) {

      RECT rcOneLine = {0};
      rcOneLine.right = width;
      rcOneLine.bottom = 0;

      long oneLineTextHeight = DrawTextExW(hdc,L"MMMM",-1,&rcOneLine,DT_CALCRECT | DT_WORDBREAK | DT_NOCLIP | DT_EDITCONTROL,NULL);

      if ( textHeight - currentHeight < oneLineTextHeight / 3 )
         textHeight = currentHeight + oneLineTextHeight / 3;
      else {
         SetWindowLongPtr(hwndControl,GWL_STYLE,(DWORD)GetWindowLongPtr(hwndControl,GWL_STYLE) | WS_VSCROLL);
         textHeight = currentHeight;
      }

   } else

      textHeight = currentHeight;

   SetWindowPos(hwndControl,NULL,0,0,(long)((double)(rcControl.right - rcControl.left) * pIOleObject -> cxScale),textHeight,SWP_NOMOVE);

   SelectObject(hdc,oldFont);

   ReleaseDC(HWND_DESKTOP,hdc);

   return S_OK;
   }


   HRESULT pkAPI::CreateEntryField(BSTR theText,long controlId,long xPad,long yPad,long widthPad,BOOL isVisible,VARIANT vFontFace,VARIANT vFontSize) {

   RemoveControl(controlId);

   WCHAR szwFontFace[64];
   
   szwFontFace[0] = '\0';

   if ( vFontFace.vt == VT_BSTR )
      wcscpy(szwFontFace,vFontFace.bstrVal);

   float fontSize = 0.0f;

   if ( vFontSize.vt == VT_R4 )
      fontSize = vFontSize.fltVal;
   else if ( vFontSize.vt == VT_R8 )
      fontSize = (float)vFontSize.dblVal;
   else if ( vFontSize.vt == VT_I4 )
      fontSize = (float)vFontSize.lVal;

   WCHAR *pszwText = new WCHAR[wcslen(theText) + 32];

   memset(pszwText,0,(wcslen(theText) + 32) * sizeof(WCHAR));

   wcscpy(pszwText,theText);

   WCHAR *p = wcschr(pszwText,0x0d);

   while ( p ) {
      *p = L'\\';
      *(p + 1) = L'n';
      p = wcschr(pszwText,0x0d);
   }

   WCHAR szwTemp[32768];

   swprintf(szwTemp,L"%ld %ld %ld %ld %ld %ld �%ls� \"%ls\" %lf",CREATEENTRYFIELD,xPad,yPad,widthPad,controlId,isVisible ? 1L : 0L,pszwText,szwFontFace,fontSize);

   HRESULT rc = sendCommand(szwTemp);

   delete [] pszwText;

   if ( ! pIOleObject )
      return rc;

   if ( ! pIOleObject -> renderControls )
      return rc;

   long x = (long)((double)xPad * pIOleObject -> cxScale);

   long y = (long)((double)yPad * pIOleObject -> cyScale);

   HWND hwndControl = pIOleObject -> addControl(L"Edit",WS_EX_CLIENTEDGE,ES_AUTOHSCROLL,x,y,controlId,theText,isVisible ? true : false);

   HFONT hFont = pIOleObject -> setControlFont(controlId,vFontFace.bstrVal);

   HDC hdc = GetDC(HWND_DESKTOP);

   HGDIOBJ oldFont = SelectObject(hdc,hFont);

   RECT rcControl = {0};
   RECT rcText = {0};

   rcText.right = LONG_MAX;
   rcText.bottom = LONG_MAX;

   long textHeight = DrawTextExW(hdc,L"MMM",-1,&rcText,DT_CALCRECT | DT_WORDBREAK | DT_NOCLIP | DT_EDITCONTROL,NULL);

   get_ControlBounds(controlId,&rcControl);

   SetWindowPos(hwndControl,NULL,0,0,(long)((double)(rcControl.right - rcControl.left) * pIOleObject -> cxScale),(long)((double)(textHeight) * 1.10 * pIOleObject -> cyScale),SWP_NOMOVE);

   SelectObject(hdc,oldFont);

   ReleaseDC(HWND_DESKTOP,hdc);

   return rc;
   }