// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   WCHAR szwCommand[COMMAND_SIZE];

   HRESULT pkAPI::sendStart() {

   lastSignatureX = 0L;
   lastSignatureY = 0L;

   BYTE rgb[] = { GetRValue(inkColor), GetGValue(inkColor), GetBValue(inkColor) };

   swprintf(szwCommand,L"%ld %ld %ld %ld %ld",SETPARAMETER,PARM_INK_COLOR,(long)rgb[0],(long)rgb[1],(long)rgb[2]);
   sendCommand(szwCommand);

   swprintf(szwCommand,L"%ld %ld %ld",SETPARAMETER,PARM_INK_WEIGHT,inkWeight);
   sendCommand(szwCommand);

   swprintf(szwCommand,L"%ld %ld %ld",SETPARAMETER,PARM_SHOW_INK,1L);
   sendCommand(szwCommand);

   swprintf(szwCommand,L"%ld %ld %ld",SETPARAMETER,PARM_SEND_POINTS,1L);
   sendCommand(szwCommand);

   return sendCommand(START);
   }


   HRESULT pkAPI::sendStop() {
   return sendCommand(STOP);
   }


   HRESULT pkAPI::sendCommand(enum pkCommands pkId) {
   swprintf(szwCommand,L"%ld",(long)pkId);
   return sendCommand(szwCommand);
   }


   HRESULT pkAPI::sendCommand(WCHAR *pszwCommand) {

   if ( ! IsConnected() )
      return E_FAIL;

   if ( ! acquireMutex() )
      return E_FAIL;

   HRESULT rc = writeCommand(pszwCommand);

   releaseMutex();

   return rc;
   }


   HRESULT pkAPI::writeCommand(WCHAR *pszwCommand) {

   WCHAR szwBytes[5];

   long n = 2 * (DWORD)wcslen(pszwCommand);

   swprintf(szwBytes,L"%04ld",n);

   if ( ! send(commandSocket,(char *)szwBytes,8,0) ) {

   }

   if ( ! send(commandSocket,(char *)pszwCommand,n,0) )
{
#ifdef SHOW_COMM_ERROR_NOTIFICATION
char szX[64];
sprintf(szX,"%s %ld",__FUNCTION__,__LINE__);
MessageBox(NULL,szX,"",MB_OK);
      Beep(2000,100);
#endif
}

   long rc = recv(commandSocket,(char *)szwCommand,COMMAND_SIZE,0);

   if ( 0 > rc )
{
#ifdef SHOW_COMM_ERROR_NOTIFICATION
char szX[64];
sprintf(szX,"%s %s %ld %ld",__FUNCTION__,szCommand,__LINE__,WSAGetLastError());
MessageBox(NULL,szX,"",MB_OK);
      Beep(2000,100);
#endif
}
   szwCommand[rc / 2] = L'\0';

   return S_OK;
   }


   HRESULT pkAPI::queryValue(pkParameters theParameter,WCHAR *pszwResult,long resultSize,long identifier,WCHAR *pszArgument) {

   if ( ! IsConnected() )
      return E_FAIL;

   if ( ! acquireMutex() )
      return E_FAIL;

   long n = 0;

   if ( -1L == identifier ) 
      n = swprintf(szwCommand,L"%ld %ld",QUERYPARAMETER,theParameter);
   else
      n = swprintf(szwCommand,L"%ld %ld %ld",QUERYPARAMETER,theParameter,identifier);

   if ( ! ( NULL == pszArgument ) )
      n += swprintf(&szwCommand[n],L" %ls",pszArgument);

   HRESULT rc = writeCommand(szwCommand);

   if ( ! ( S_OK ==  rc ) ) {
      releaseMutex();
      return rc;
   }

   memset(pszwResult,0,resultSize * sizeof(WCHAR));

   wcsncpy(pszwResult,szwCommand,resultSize);

   releaseMutex();

   return S_OK;
   }


   HRESULT pkAPI::setValue(pkParameters theParameter,WCHAR *pszValue,long identifier) {
   if ( -1L == identifier )
      swprintf(szwCommand,L"%ld %ld %ls",SETPARAMETER,theParameter,pszValue);
   else  
      swprintf(szwCommand,L"%ld %ld %ld %ls",SETPARAMETER,theParameter,identifier,pszValue);
   return sendCommand(szwCommand);
   }

   HRESULT pkAPI::setValue(pkParameters theParameter,long value,long identifier) {
   if ( -1L == identifier )
      swprintf(szwCommand,L"%ld %ld %ld",SETPARAMETER,theParameter,value);
   else
      swprintf(szwCommand,L"%ld %ld %ld %ld",SETPARAMETER,theParameter,identifier,value);
   return sendCommand(szwCommand);
   }

   HRESULT pkAPI::setValue(pkParameters theParameter,float value,long identifier) {
   if ( -1L == identifier )
      swprintf(szwCommand,L"%ld %ld %f",SETPARAMETER,theParameter,value);
   else  
      swprintf(szwCommand,L"%ld %ld %ld %f",SETPARAMETER,theParameter,identifier,value);
   return sendCommand(szwCommand);
   }


   HRESULT pkAPI::sendImage(char *szImageFile,long x,long y,long cx,long cy) {

   if ( INVALID_SOCKET == commandSocket )
      return E_UNEXPECTED;

   FILE *fImage = fopen(szImageFile,"rb");
   fseek(fImage,0,SEEK_END);
   
   long countBytes = ftell(fImage);

   fclose(fImage);

   WCHAR szwCommandPreamble[256];

   long n = 0;
   if ( -1L == cx && -1L == cy )
      n = swprintf(szwCommandPreamble,L"%ld %ld",BACKGROUND,countBytes);
   else
      n = swprintf(szwCommandPreamble,L"%ld %ld %ld %ld %ld %ld",RECEIVEIMAGE,countBytes,x,y,cx,cy);

   return sendFile(szImageFile,countBytes,szwCommandPreamble);
   }


   HRESULT pkAPI::sendFile(char *pszFile,long countBytes,WCHAR *pszwCommandPreamble) {

   if ( ! IsConnected() ) 
      return E_FAIL;

   WCHAR szwBytes[5];

   swprintf(szwBytes,L"%04ld",2 * (DWORD)wcslen(pszwCommandPreamble));

   if ( ! acquireMutex() )
      return E_FAIL;

   long rc = send(commandSocket,(char *)szwBytes,8,0);

   if ( 0 > rc ) {
      releaseMutex();
      return E_FAIL;
   }

   rc = send(commandSocket,(char *)pszwCommandPreamble,2 * (DWORD)wcslen(pszwCommandPreamble),0);

   if ( 0 > rc ) {
      releaseMutex();
      return E_FAIL;
   }

   memset(szwCommand,0,COMMAND_SIZE * sizeof(WCHAR));

   rc = recv(commandSocket,(char *)szwCommand,1024,0);

   if ( 0 > rc ) {
      releaseMutex();
      return E_FAIL;
   }

   if ( _wcsnicmp(szwCommand,L"send",4) ) {
      releaseMutex();
      return E_FAIL;
   }

   FILE *fImage = fopen(pszFile,"rb");

   BYTE *pBinary = new BYTE[countBytes];
   fread(pBinary,countBytes,1,fImage);
   fclose(fImage);  

   long totalBytes = 0;

   for ( totalBytes = 0; totalBytes + 512 < countBytes; totalBytes += 512 ) 
      send(commandSocket,(char *)pBinary + totalBytes,512,0L);

   if ( totalBytes < countBytes )
      send(commandSocket,(char *)pBinary + totalBytes,countBytes - totalBytes,0L);

   memset(szwCommand,0,COMMAND_SIZE * sizeof(WCHAR));

   rc = recv(commandSocket,(char *)szwCommand,MAX_PATH,0);

   delete [] pBinary;

   releaseMutex();

   return S_OK;
   }


   HRESULT pkAPI::recieveImage(char *pszImageFile) {

   if ( ! pszImageFile )
      return E_POINTER;

   if ( ! IsConnected() )
      return E_FAIL;

   if ( ! acquireMutex() ) 
      return E_FAIL;

   WCHAR szwImageCommand[64];

   memset(szwImageCommand,0,sizeof(szwImageCommand));

   long n = swprintf(szwImageCommand,L"%ld %ld",QUERYPARAMETER,PARM_IMAGE);

   HRESULT rc = recieveFile(pszImageFile,szwImageCommand);

#if 0
   char *p = strrchr(pszImageFile,'.');
   if ( p )
      sprintf(p,".jpg");
   else
      sprintf(pszImageFile + strlen(pszImageFile),".jpg");
#endif

   return rc;
   }


   HRESULT pkAPI::recieveFile(char *pszFile,WCHAR *pszwCommandPreamble) {

   WCHAR szwBytes[5];

   long n = 2 * (DWORD)wcslen(pszwCommandPreamble);

   swprintf(szwBytes,L"%04ld",n);

   if ( ! send(commandSocket,(char *)szwBytes,8,0) ) {

   }

   if ( ! send(commandSocket,(char *)pszwCommandPreamble,n,0) )
      Beep(2000,100);

   WCHAR szwResponse[MAX_PATH];
   memset(szwResponse,0,sizeof(szwResponse));

   long rc = recv(commandSocket,(char *)szwResponse,2 * MAX_PATH,0);

   if ( 0 > rc )
{
      Beep(2000,100);
}

//TODO: Need to "transactionalize" recieveImage - do not let any other communication occur (the Mutex)
// until the entire image is recieved.
//

   long countBytes;

   swscanf(szwResponse,L"recieve %ld",&countBytes);

   send(commandSocket,(char *)L"ok",4,0);

   BYTE *pBinary = new BYTE[countBytes];

   memset(pBinary,0,countBytes * sizeof(BYTE));

   long totalBytes = 0;

   while ( totalBytes + 512 < countBytes ) {
      long rc = recv(commandSocket,(char *)pBinary + totalBytes,512,MSG_WAITALL);
      if ( 0 > rc ) {
         delete [] pBinary;
         releaseMutex();
         return E_FAIL;
      }
      send(commandSocket,(char *)L"ok",4,0L);
      totalBytes += rc;
   }

   if ( totalBytes < countBytes ) {
      long rc = recv(commandSocket,(char *)pBinary + totalBytes,countBytes - totalBytes,MSG_WAITALL);
      if ( 0 > rc ) {
         delete [] pBinary;
         releaseMutex();
         return E_FAIL;
      }
      send(commandSocket,(char *)L"ok",4,0L);
   }

   strcpy(pszFile,_tempnam(NULL,NULL));

   FILE *fResult = fopen(pszFile,"wb");
   fwrite(pBinary,1,countBytes,fResult);
   fclose(fResult);

   send(commandSocket,(char *)L"ok",4,0L);

   delete [] pBinary;

   releaseMutex();

   return S_OK;
   }


   HRESULT pkAPI::sendPDF(char *szFile) {

   if ( INVALID_SOCKET == commandSocket )
      return E_UNEXPECTED;

   FILE *fFile = fopen(szFile,"rb");

   fseek(fFile,0,SEEK_END);
   
   long countBytes = ftell(fFile);

   fclose(fFile);

   WCHAR szwCommandPreamble[256];

   swprintf(szwCommandPreamble,L"%ld %ld",DISPLAYPDF,countBytes);

   return sendFile(szFile,countBytes,szwCommandPreamble);
   }


   HRESULT pkAPI::recievePDF(char *pszFile) {

   if ( ! pszFile )
      return E_POINTER;

   if ( ! IsConnected() )
      return E_FAIL;

   WCHAR szwCommandPreamble[64];

   memset(szwCommandPreamble,0,64 * sizeof(WCHAR));

   swprintf(szwCommandPreamble,L"%ld %ld",QUERYPARAMETER,PARM_PDF);

   return recieveFile(pszFile,szwCommandPreamble);
   }


   HRESULT pkAPI::pingServer() {

   if ( IsConnected() ) 
      return S_OK;

   commandSocket = connectPort(szDeviceIpOrNetworkName,SERVICE_PORT_W);

   if ( INVALID_SOCKET == commandSocket )
      return E_FAIL;

   WCHAR szwInstructions[256];
   memset(szwInstructions,0,sizeof(szwInstructions));

   long rc = recv(commandSocket,(char *)szwInstructions,256 * sizeof(WCHAR),0L);

   if ( 0 > rc ) 
      return E_FAIL;

   WCHAR *p = wcsstr(szwInstructions,L"commands:");

   if ( p ) {

      p += 9;

      WCHAR *pszwCommandsPort = wcstok(p,L" ",NULL);

      if ( ! ( _wtol(pszwCommandsPort) == _wtol(SERVICE_PORT_W) ) ) {
      
         SOCKET redirectedSocket = connectPort(szDeviceIpOrNetworkName,pszwCommandsPort);

         shutdown(commandSocket,SD_BOTH);

         closesocket(commandSocket);

         commandSocket = INVALID_SOCKET;

         if ( INVALID_SOCKET == redirectedSocket )
            return E_FAIL;

         commandSocket = redirectedSocket;

      }

   }

   writeCommand(L"disconnect");

   shutdown(commandSocket,SD_BOTH);

   closesocket(commandSocket);

   commandSocket = INVALID_SOCKET;

   return S_OK;
   }


   HRESULT pkAPI::connectServer() {

   cachedPadWidth = -1L;
   cachedPadHeight = -1L;

   commandSocket = connectPort(szDeviceIpOrNetworkName,SERVICE_PORT_W);
   
   if ( INVALID_SOCKET == commandSocket )
      return E_FAIL;

   WCHAR szwInstructions[MAX_PATH];

   memset(szwInstructions,0,sizeof(szwInstructions));

   long rc = recv(commandSocket,(char *)szwInstructions,2 * MAX_PATH,0L);

   if ( 0 > rc ) 
      return E_FAIL;

   wcscpy(szwCommandsPort,SERVICE_PORT_W);
   wcscpy(szwEventSourcePort,EVENT_SERVER_PORT_W);
   wcscpy(szwControlPort,CONTROL_LISTENER_PORT_W);

   WCHAR *p = wcsstr(szwInstructions,L"commands:");

   if ( p ) {

      WCHAR szwKeep[MAX_PATH];

      wcscpy(szwKeep,szwInstructions);

      p += 9;

      WCHAR *pszwCommandsPort = wcstok(p,L" ",NULL);

      SOCKET redirectedSocket = connectPort(szDeviceIpOrNetworkName,pszwCommandsPort);

      shutdown(commandSocket,SD_BOTH);

      closesocket(commandSocket);

      commandSocket = INVALID_SOCKET;

      if ( INVALID_SOCKET == redirectedSocket )
         return E_FAIL;

      commandSocket = redirectedSocket;

      wcscpy(szwCommandsPort,pszwCommandsPort);

      WCHAR szwKeep2[64];

      wcscpy(szwKeep2,szwKeep);

      p = wcsstr(szwKeep,L"events:");

      p += 7;

      pszwCommandsPort = wcstok(p,L" ",NULL);

      wcscpy(szwEventSourcePort,pszwCommandsPort);

      p = wcsstr(szwKeep2,L"control:");

      p += 9;

      pszwCommandsPort = wcstok(p,L" ",NULL);

      wcscpy(szwControlPort,pszwCommandsPort);
      
   }

   startHeartbeat();
   openEventSink();

   hMutexConnection = CreateMutex(NULL,FALSE,NULL);

   cachedPadWidth = getPadWidth();
   cachedPadHeight = getPadHeight();

   return S_OK;
   }


   SOCKET pkAPI::connectPort(char *pszServerName,WCHAR *pszwPort) {

   addrinfo addressInfo = {0};
   addrinfo *pResolvedAddressInfo = NULL;

   addressInfo.ai_flags = 0L;
   addressInfo.ai_family = AF_INET;
   addressInfo.ai_socktype = SOCK_STREAM;
   addressInfo.ai_protocol = IPPROTO_TCP;
   addressInfo.ai_addrlen = 0;
   addressInfo.ai_addr = NULL;
   addressInfo.ai_canonname = NULL;
   addressInfo.ai_next = NULL;

   SOCKET theSocket = INVALID_SOCKET;

   char szPort[32];

   WideCharToMultiByte(CP_ACP,0,pszwPort,-1,szPort,32,0,0);

   if ( ! ( 0 == getaddrinfo(pszServerName,szPort,&addressInfo,&pResolvedAddressInfo) ) )
      return theSocket;

   for ( addrinfo *p = pResolvedAddressInfo; p; p = p -> ai_next ) {

      if ( ! ( SOCK_STREAM == p -> ai_socktype ) )
         continue;

      theSocket = socket(p -> ai_family,p -> ai_socktype,p -> ai_protocol);

      if ( ! ( INVALID_SOCKET == theSocket ) ) {

         if ( ! ( SOCKET_ERROR == connect(theSocket,p -> ai_addr,(int)p -> ai_addrlen) ) )
            break;

      }

      int k = WSAGetLastError();

      theSocket = INVALID_SOCKET;

   }

   freeaddrinfo(pResolvedAddressInfo);

   return theSocket;
   }


   HRESULT pkAPI::disconnectServer() {

   sendCommand(L"disconnect");
   closeEventSink();
   stopHeartbeat(false);
   if ( ! ( INVALID_SOCKET == commandSocket ) ) {
      shutdown(commandSocket,SD_BOTH);
      closesocket(commandSocket);
      commandSocket = INVALID_SOCKET;
   }

   if ( ! ( INVALID_HANDLE_VALUE == hMutexConnection ) )
      CloseHandle(hMutexConnection);

   hMutexConnection = INVALID_HANDLE_VALUE;

   cachedPadWidth = -1L;
   cachedPadHeight = -1L;

   return S_OK;
   }


   bool pkAPI::IsConnected() {

   if ( INVALID_HANDLE_VALUE == hMutexConnection )
      return false;

   if ( INVALID_SOCKET == commandSocket ) {
      CloseHandle(hMutexConnection);
      hMutexConnection = INVALID_HANDLE_VALUE;
      return false;
   }

   if ( ! hHeartbeatThread ) {
      CloseHandle(hMutexConnection);
      hMutexConnection = INVALID_HANDLE_VALUE;
      shutdown(commandSocket,SD_BOTH);
      closesocket(commandSocket);
      commandSocket = INVALID_SOCKET;
      return false;
   }

   return true;
   }


   long pkAPI::acquireMutex() {

   if ( INVALID_HANDLE_VALUE == hMutexConnection )
      return 0L;

   long trialCount = 0;
   while ( 1 ) {
      DWORD rc = WaitForSingleObject(hMutexConnection,1000);
      if ( WAIT_ABANDONED == rc )
         return 1L;
      if ( WAIT_OBJECT_0 == rc )
         return 1L;
      if ( WAIT_TIMEOUT ) {
         ++trialCount;
         if ( 3 < trialCount ) 
            continue;
         rc = MessageBox(NULL,"There is a problem attempting to connect to the Signature Capture device.\n\nIs CursiVision already processing another document ?"
                              "\n\nChoose Retry or Cancel","Error",MB_ICONEXCLAMATION | MB_RETRYCANCEL | MB_DEFBUTTON1);
         if ( IDCANCEL == rc )
            return 0L;
         trialCount = 0;
         continue;
      }
   }

   return 1L;
   }


   long pkAPI::releaseMutex() {
   BOOL rc = ReleaseMutex(hMutexConnection);
   return 0L;
   }