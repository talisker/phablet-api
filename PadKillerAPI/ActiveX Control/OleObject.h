// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

   // IOleObject 

   class _IOleObject : public IOleObject {

   public:

      _IOleObject(pkAPI *pp);
      ~_IOleObject();

      IPropertyPage *PropertyPage(long index) { return static_cast<IPropertyPage *>(pIPropertyPage[index]); };

      STDMETHOD(QueryInterface)(REFIID riid,void **ppv);
      STDMETHOD_ (ULONG, AddRef)();
      STDMETHOD_ (ULONG, Release)();

      BOOL IsRunning() { return isRunning; };

      HWND addControl(WCHAR *pszwClass,long exStyle,long style,long x,long y,long eventId,WCHAR *pszwText,bool isVisible);
      HWND addRadioButton(long x,long y,long eventId,WCHAR *pszwText,bool isVisible,bool isChecked,long groupNumber);
      HWND addCheckBox(long x,long y,long eventId,WCHAR *pszwText,bool isVisible,bool isChecked);

      void showControl(long eventId);

      void hideControl(long eventId);

      void setControlsFontSizeValue(float value);

      void setControlsFontFace(WCHAR *pszwFace);

      void positionControl(long eventId,long x,long y);

      void setControlText(long eventId,WCHAR *pszwText);

      void fire_PropertyChanged() { connectionPointContainer.fire_PropertyChanged(); };

   private:

      STDMETHOD(SetClientSite)(IOleClientSite *pClientSite);
      STDMETHOD(GetClientSite)(IOleClientSite **ppCLientSite);
      STDMETHOD(SetHostNames)(LPCOLESTR szContainerApp,LPCOLESTR szContainerObj);
      STDMETHOD(Close)(DWORD dwSaveOption);
      STDMETHOD(SetMoniker)(DWORD dwWhichMoniker, IMoniker *pmk);
      STDMETHOD(GetMoniker)(DWORD dwAssign, DWORD dwWhichMoniker,IMoniker **ppmk);
      STDMETHOD(InitFromData)(IDataObject *pDataObject,BOOL fCreation,DWORD dwReserved);
      STDMETHOD(GetClipboardData)(DWORD dwReserved,IDataObject **ppDataObject);
      STDMETHOD(DoVerb)(LONG iVerb,LPMSG lpmsg,IOleClientSite *pActiveSite,LONG lindex,HWND hwndParent,LPCRECT lprcPosRect);
      STDMETHOD(EnumVerbs)(IEnumOLEVERB **ppenumOleVerb);
      STDMETHOD(Update)();
      STDMETHOD(IsUpToDate)();
      STDMETHOD(GetUserClassID)(CLSID * pClsid);
      STDMETHOD(GetUserType)(DWORD dwFormOfType, LPOLESTR *pszUserType);
      STDMETHOD(SetExtent)(DWORD dwDrawAspect, LPSIZEL lpsizel);
      STDMETHOD(GetExtent)(DWORD dwDrawAspect, LPSIZEL lpsizel);
     
      STDMETHOD(Advise)(IAdviseSink *pAdvSink, DWORD * pdwConnection);
      STDMETHOD(Unadvise)(DWORD dwConnection);
      STDMETHOD(EnumAdvise)(IEnumSTATDATA **ppenumAdvise);
      STDMETHOD(GetMiscStatus)(DWORD dwAspect, DWORD * pdwStatus);        
     
      STDMETHOD(SetColorScheme)(LPLOGPALETTE lpLogpal);

      pkAPI *pParent;

      long refCount;

      // IDataObject

      class _IDataObject : public IDataObject {

      public:

         _IDataObject(_IOleObject *pp) : pParent(pp) {};
         ~_IDataObject() {};

         STDMETHOD(QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

      private:

         // IDataObject

         STDMETHOD(GetData)(FORMATETC *,STGMEDIUM *);
         STDMETHOD(GetDataHere)(FORMATETC *,STGMEDIUM *);
         STDMETHOD(QueryGetData)(FORMATETC *);
         STDMETHOD(GetCanonicalFormatEtc)(FORMATETC *,FORMATETC *);
         STDMETHOD(SetData)(FORMATETC *,STGMEDIUM *,BOOL);
         STDMETHOD(EnumFormatEtc)(DWORD,IEnumFORMATETC **);
         STDMETHOD(DAdvise)(FORMATETC *,DWORD,IAdviseSink *,DWORD *);
         STDMETHOD(DUnadvise)(DWORD);
         STDMETHOD(EnumDAdvise)(IEnumSTATDATA **);

         _IOleObject *pParent;

      } *pIDataObject;

      // IOleControl

      class _IOleControl : public IOleControl {

      public:

         _IOleControl(_IOleObject *pp) : pParent(pp) {};
         ~_IOleControl() {};

         STDMETHOD(QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

      private:
     
         STDMETHOD(GetControlInfo)(CONTROLINFO *);
         STDMETHOD(OnMnemonic)(MSG *);
         STDMETHOD(OnAmbientPropertyChange)(long);
         STDMETHOD(FreezeEvents)(int);

         _IOleObject *pParent;

      } *pIOleControl;

      // IOleInPlaceObject

      class _IOleInPlaceObject : public IOleInPlaceObject {

      public:

         _IOleInPlaceObject(_IOleObject *pp) : pParent(pp) {};
         ~_IOleInPlaceObject() {};

         STDMETHOD(QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

         // IOleWindow
   
         STDMETHOD (GetWindow)(HWND *);
         STDMETHOD (ContextSensitiveHelp)(BOOL);

         // IOleInPlaceObject

         STDMETHOD (InPlaceActivate)();
         STDMETHOD (InPlaceDeactivate)();
         STDMETHOD (UIDeactivate)();
         STDMETHOD (SetObjectRects)(LPCRECT,LPCRECT);
         STDMETHOD (ReactivateAndUndo)();

      private:

         _IOleObject *pParent;

      } *pIOleInPlaceObject;

      // IOleInPlaceActiveObject

      class _IOleInPlaceActiveObject : public IOleInPlaceActiveObject {

      public:

         _IOleInPlaceActiveObject(_IOleObject *pp) : pParent(pp) {};
         ~_IOleInPlaceActiveObject() {};

         STDMETHOD(QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

      private:

         // IOleWindow
   
         STDMETHOD (GetWindow)(HWND *);
         STDMETHOD (ContextSensitiveHelp)(BOOL);

         // IOleInPlaceActiveObject

         STDMETHOD(TranslateAccelerator)(LPMSG);
         STDMETHOD(OnFrameWindowActivate)(BOOL);
         STDMETHOD(OnDocWindowActivate)(BOOL);
         STDMETHOD(ResizeBorder)(LPCRECT ,IOleInPlaceUIWindow *,BOOL);
         STDMETHOD(EnableModeless)(BOOL);

      private:

         _IOleObject *pParent;

      } *pIOleInPlaceActiveObject;


      class _IProvideClassInfo2 : public IProvideClassInfo2 {
      public:

         _IProvideClassInfo2(_IOleObject *pp) : pParent(pp) {};
         ~_IProvideClassInfo2() {};

         STDMETHOD(QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

         // IProvideClassInfo

         STDMETHOD(GetClassInfo)(ITypeInfo **);

         // IProvideClassInfo2

         STDMETHOD(GetGUID)(DWORD,GUID *);

      private:

         _IOleObject *pParent;

      } *pIProvideClassInfo2;

      class _IQuickActivate : public IQuickActivate {
      public:

         _IQuickActivate(_IOleObject *pp) : pParent(pp) {};
         ~_IQuickActivate() {};

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

        //  IQuickActivate

        STDMETHOD(QuickActivate)(QACONTAINER* pQAContainer,QACONTROL* pQAControl);
        STDMETHOD(SetContentExtent)(SIZEL* pSizel);
        STDMETHOD(GetContentExtent)(SIZEL* pSizel);

      private:
   
         _IOleObject* pParent;

      } *pIQuickActivate;

      class _IViewObjectEx : public IViewObjectEx {
      public:

         _IViewObjectEx(_IOleObject *pp) : pParent(pp) {};
         ~_IViewObjectEx() {};

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

         // IViewObject

         STDMETHOD (Draw)(unsigned long,long,void *,DVTARGETDEVICE *,HDC,HDC,const struct _RECTL *,const struct _RECTL *,int (__stdcall *)(ULONG_PTR),ULONG_PTR);
         STDMETHOD (GetColorSet)(DWORD,long,void *,DVTARGETDEVICE *,HDC,LOGPALETTE **);
         STDMETHOD (Freeze)(DWORD,long,void *,DWORD *);
         STDMETHOD (Unfreeze)(DWORD);
         STDMETHOD (SetAdvise)(DWORD,DWORD,IAdviseSink *);
         STDMETHOD (GetAdvise)(DWORD *,DWORD *,IAdviseSink **);

         // IViewObject2

         STDMETHOD (GetExtent)(unsigned long,long,DVTARGETDEVICE *,struct tagSIZE *);

         // IViewObjectEx

         STDMETHOD (GetRect)(DWORD dwAspect,RECTL *);
         STDMETHOD (GetViewStatus)(DWORD *);
         STDMETHOD (QueryHitPoint)(DWORD dwAspect,const struct tagRECT *pRectBounds,POINT ptlHit,long lCloseHint,DWORD *dwHitResult);
         STDMETHOD (QueryHitRect)(DWORD dwAspect,const struct tagRECT *pRectBounds,const struct tagRECT *rctHit,long lCloseHint,DWORD *dwHitResult);
         STDMETHOD (GetNaturalExtent)(DWORD dwExtent,LONG lIndex,DVTARGETDEVICE *ptd,HDC hicTargetDev,DVEXTENTINFO *extentInfo,SIZEL *);

      private:
   
         _IOleObject *pParent;

      } *pIViewObjectEx;

      class _IRunnableObject : public IRunnableObject {

      public:

         _IRunnableObject(_IOleObject *pp) : pParent(pp) {};
         ~_IRunnableObject() {};

         // IUnknown

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

         // IRunnableObject

         STDMETHOD (GetRunningClass)(CLSID *);
         STDMETHOD (Run)(LPBC);
         int __stdcall IsRunning(void);
         STDMETHOD (LockRunning)(BOOL,BOOL);
         STDMETHOD (SetContainedObject)(BOOL);

      private:
   
         _IOleObject *pParent;

      } *pIRunnableObject;


      class _ISpecifyPropertyPages : public ISpecifyPropertyPages {

      public:

         _ISpecifyPropertyPages(_IOleObject *p) : pParent(p) {};
         ~_ISpecifyPropertyPages() {};

         // IUnknown

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

         // ISpecifyPropertyPages

         STDMETHOD(GetPages)(CAUUID *pPages);

      private:

         _IOleObject *pParent;

      } *pISpecifyPropertyPages;

      class _IPropertyPage : public IPropertyPage {
      public:

         _IPropertyPage(_IOleObject *p,CLSID clsid) : pParent(p),theCLSID(clsid) {};
         ~_IPropertyPage() {};

         // IUnknown

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

         // IPropertyPage

      private:

         STDMETHOD(SetPageSite)(IPropertyPageSite *pPageSite);
         STDMETHOD(Activate)(HWND hWndParent, LPCRECT prc, BOOL fModal);
         STDMETHOD(Deactivate)();
         STDMETHOD(GetPageInfo)(PROPPAGEINFO *pPageInfo);
         STDMETHOD(SetObjects)(ULONG cObjects, IUnknown **ppUnk);
         STDMETHOD(Show)(UINT nCmdShow);
         STDMETHOD(Move)(LPCRECT prc);
         STDMETHOD(IsPageDirty)();
         STDMETHOD(Apply)();
         STDMETHOD(Help)(LPCOLESTR pszHelpDir);
         STDMETHOD(TranslateAccelerator)(LPMSG pMsg);

         _IOleObject *pParent;
         CLSID theCLSID;

      } *pIPropertyPage[3];

      class _IPersistStreamInit : public IPersistStreamInit {

      public:
 
         _IPersistStreamInit(_IOleObject *pp) : pParent(pp) {};
         ~_IPersistStreamInit() {};
 
         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();
 
         STDMETHOD(GetClassID)(CLSID *);
 
         // IPersistStream
 
         STDMETHOD(GetSizeMax)(ULARGE_INTEGER *);
         STDMETHOD(IsDirty)();
         STDMETHOD(Load)(IStream *);
         STDMETHOD(Save)(IStream *,int);
 
         // IPersistStreamInit
 
         STDMETHOD(InitNew)();

     private:
 
         _IOleObject *pParent;
 
      } * pIPersistStreamInit;
 
      class _IPersistStorage : public IPersistStorage {

      public:
 
         _IPersistStorage(_IOleObject *pp) : pParent(pp), noScribble(true) {};
         ~_IPersistStorage() { };
 
         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();
 
         STDMETHOD(GetClassID)(CLSID *);
         STDMETHOD(IsDirty)();
         STDMETHOD(InitNew)(IStorage *);
         STDMETHOD(Load)(IStorage *);
         STDMETHOD(Save)(IStorage *,BOOL);
         STDMETHOD(SaveCompleted)(IStorage *);
         STDMETHOD(HandsOffStorage)();
 
      private:
 
         _IOleObject *pParent;

         boolean noScribble;
 
      } *pIPersistStorage;

      // IConnectionPointContainer

      struct _IConnectionPointContainer : public IConnectionPointContainer {

      public:

        STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
        STDMETHOD_ (ULONG, AddRef)();
        STDMETHOD_ (ULONG, Release)();

        STDMETHOD(FindConnectionPoint)(REFIID riid,IConnectionPoint **);
        STDMETHOD(EnumConnectionPoints)(IEnumConnectionPoints **);

        _IConnectionPointContainer(_IOleObject *pp);
        ~_IConnectionPointContainer();

        void fire_PropertyChanged();

        void fire_PenDown(long x,long y);
        void fire_PenUp(long x,long y);
        void fire_PenPoint(long x,long y,float inkWeight);
        void fire_DeviceReady();
        void fire_ConfigurationChanged();

     private:

        _IOleObject *pParent;

     } connectionPointContainer;

     struct _IConnectionPoint : IConnectionPoint {

      public:

        STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
        STDMETHOD_ (ULONG, AddRef)();
        STDMETHOD_ (ULONG, Release)();

        STDMETHOD (GetConnectionInterface)(IID *);
        STDMETHOD (GetConnectionPointContainer)(IConnectionPointContainer **ppCPC);
        STDMETHOD (Advise)(IUnknown *pUnk,DWORD *pdwCookie);
        STDMETHOD (Unadvise)(DWORD);
        STDMETHOD (EnumConnections)(IEnumConnections **ppEnum);

        _IConnectionPoint(_IOleObject *pp,const IID &theInterface);
        ~_IConnectionPoint();
        IUnknown *AdviseSink() { return adviseSink; };

     private:

        int getSlot();
        int findSlot(DWORD dwCookie);

        IID eventInterface;

        IUnknown *adviseSink;
        _IOleObject *pParent;
        DWORD nextCookie;
        int countConnections,countLiveConnections;

        long refCount;

        CONNECTDATA *connections;

     } connectionPoint_PropertyNotifySink,connectionPoint_pkAPIEvents;


     struct _IEnumConnectionPoints : IEnumConnectionPoints {

     public:

        STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
        STDMETHOD_ (ULONG, AddRef)();
        STDMETHOD_ (ULONG, Release)();

         STDMETHOD (Next)(ULONG cConnections,IConnectionPoint **rgpcn,ULONG *pcFetched);
        STDMETHOD (Skip)(ULONG cConnections);
        STDMETHOD (Reset)();
        STDMETHOD (Clone)(IEnumConnectionPoints **);

        _IEnumConnectionPoints(_IOleObject *pp,_IConnectionPoint **cp,int connectionPointCount);
       ~_IEnumConnectionPoints();

     private:

        int cpCount,enumeratorIndex;
        _IOleObject *pParent;
        _IConnectionPoint **connectionPoints;

     } *enumConnectionPoints;


     struct _IEnumerateConnections : public IEnumConnections {

     public:

        _IEnumerateConnections(IUnknown* pParentUnknown,ULONG cConnections,CONNECTDATA* paConnections,ULONG initialIndex);
        ~_IEnumerateConnections();

         STDMETHOD(QueryInterface)(REFIID, void **);
         STDMETHODIMP_(ULONG) AddRef();
         STDMETHODIMP_(ULONG) Release();
         STDMETHOD(Next)(ULONG, CONNECTDATA*, ULONG*);
         STDMETHOD(Skip)(ULONG);
         STDMETHOD(Reset)();
         STDMETHOD(Clone)(IEnumConnections**);

      private:

        ULONG refCount;
        IUnknown *pParentUnknown;
        ULONG enumeratorIndex;
        ULONG countConnections;
        CONNECTDATA *connections;

      } *enumConnections;

   private:

      void initialize();

      HFONT createControlsFont(long eventId);

      HFONT setControlFont(long controlId,WCHAR *pszwFontFace = NULL);

      IOleAdviseHolder *pOleAdviseHolder;
      IDataAdviseHolder *pDataAdviseHolder;

      IAdviseSink *pViewAdviseSink;
      DWORD adviseSink_dwAspect,adviseSink_advf;
      DWORD dwViewAdviseSinkConnection;

      BYTE *pDeviceProperties;
      long devicePropertiesSize;

      IOleClientSite *pIOleClientSite_MySite;
      IOleInPlaceSite *pIOleInPlaceSite_MySite;

      SIZEL containerSize;

      bool isRunning;

      bool renderSignature;
      bool runTransparent;
      bool renderBackground;
      bool renderControls;
      bool renderSunkenBorder;

      BYTE *pRuntimeProperties;
      long runtimePropertiesSize;

      HWND hwndContainer;

      HWND hwndPropertyPage;
      HWND hwndRuntimePropertyPage;
      HWND hwndDocumentationPropertyPage;

      std::list<HWND> runtimeControls;
      std::map<HWND,long> radioButtonGroups;

      long maxControlId;

      float controlsFontSize;
      WCHAR controlsFontFamily[64];

      double cxScale;
      double cyScale;

      static LRESULT CALLBACK settingsHandlerISpecifyPropertyPageImplementation(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);
      static LRESULT CALLBACK runtimeSettingsHandlerISpecifyPropertyPageImplementation(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);
      static LRESULT CALLBACK documentationSettingsHandlerISpecifyPropertyPageImplementation(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

      static WNDPROC nativePropertySheetFrameHandler;
      static long nativePropertySheetFrameHandlerRefCount;
      static HWND hwndPropertySheetFrame;

      static LRESULT CALLBACK propertySheetFrameHandler(HWND,UINT,WPARAM,LPARAM);

      friend class pkAPI;

   } *pIOleObject;