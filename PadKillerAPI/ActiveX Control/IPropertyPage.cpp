// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   static bool okayToSave = false;

   long __stdcall pkAPI::_IOleObject::_IPropertyPage::QueryInterface(REFIID riid,void **ppv) {
   *ppv = NULL;
   if ( riid == IID_IPropertyPage )
      *ppv = static_cast<IPropertyPage *>(this);
   else
      return pParent -> QueryInterface(riid,ppv);
   AddRef(); 
   return S_OK; 
   }
   unsigned long __stdcall pkAPI::_IOleObject::_IPropertyPage::AddRef() {
   return pParent -> AddRef();
   }
   unsigned long __stdcall pkAPI::_IOleObject::_IPropertyPage::Release() {
   return pParent -> Release();
   }
 
 
   HRESULT pkAPI::_IOleObject::_IPropertyPage::SetPageSite(IPropertyPageSite *pPageSite) {
   return S_OK;
   }

   
   HRESULT pkAPI::_IOleObject::_IPropertyPage::Activate(HWND hwndParent,const RECT *pRect,BOOL doModal) {

   if ( 0 == nativePropertySheetFrameHandlerRefCount ) {
      hwndPropertySheetFrame = GetParent(hwndParent);
      nativePropertySheetFrameHandler = (WNDPROC)SetWindowLongPtr(hwndPropertySheetFrame,GWLP_WNDPROC,(ULONG_PTR)propertySheetFrameHandler);
   }

   nativePropertySheetFrameHandlerRefCount++;

   if ( CLSID_PhabletSignaturePadPropertyPage == theCLSID ) {
      if ( pParent -> hwndPropertyPage )
         DestroyWindow(pParent -> hwndPropertyPage);
      pParent -> hwndPropertyPage = CreateDialogParam(hModule,MAKEINTRESOURCE(IDD_PAD_PROPERTIES),hwndParent,(DLGPROC)pkAPI::_IOleObject::settingsHandlerISpecifyPropertyPageImplementation,(ULONG_PTR)pParent -> pParent);
      SetWindowPos(pParent -> hwndPropertyPage,HWND_TOP,pRect -> left,pRect -> top,pRect -> right - pRect -> left,pRect -> bottom - pRect -> top,SWP_SHOWWINDOW);
      return S_OK;
   }

   if ( CLSID_PhabletSignaturePadRuntimePropertyPage == theCLSID ) {
      if ( pParent -> hwndRuntimePropertyPage )
         DestroyWindow(pParent -> hwndRuntimePropertyPage);
      pParent -> hwndRuntimePropertyPage = CreateDialogParam(hModule,MAKEINTRESOURCE(IDD_ACTIVEX_CONTROL_PROPERTIES),hwndParent,(DLGPROC)pkAPI::_IOleObject::runtimeSettingsHandlerISpecifyPropertyPageImplementation,(ULONG_PTR)pParent -> pParent);
      SetWindowPos(pParent -> hwndRuntimePropertyPage,HWND_TOP,pRect -> left,pRect -> top,pRect -> right - pRect -> left,pRect -> bottom - pRect -> top,SWP_SHOWWINDOW);
      return S_OK;
   }

   if ( CLSID_PhabletSignaturePadDocumentationPropertyPage == theCLSID ) {
      if ( pParent -> hwndDocumentationPropertyPage )
         DestroyWindow(pParent -> hwndDocumentationPropertyPage);
      pParent -> hwndDocumentationPropertyPage = CreateDialogParam(hModule,MAKEINTRESOURCE(IDD_DOCUMENTATION),hwndParent,(DLGPROC)pkAPI::_IOleObject::documentationSettingsHandlerISpecifyPropertyPageImplementation,(ULONG_PTR)pParent -> pParent);
      SetWindowPos(pParent -> hwndDocumentationPropertyPage,HWND_TOP,pRect -> left,pRect -> top,pRect -> right - pRect -> left,pRect -> bottom - pRect -> top,SWP_SHOWWINDOW);
      return S_OK;
   }

   return S_OK;
   }


   HRESULT pkAPI::_IOleObject::_IPropertyPage::Deactivate() {

   if ( CLSID_PhabletSignaturePadPropertyPage == theCLSID ) {
      if ( pParent -> hwndPropertyPage ) {
         DestroyWindow(pParent -> hwndPropertyPage);
         pParent -> hwndPropertyPage = NULL;
      }
   }

   if ( CLSID_PhabletSignaturePadRuntimePropertyPage == theCLSID ) {
      if ( pParent -> hwndRuntimePropertyPage ) {
         DestroyWindow(pParent -> hwndRuntimePropertyPage);
         pParent -> hwndRuntimePropertyPage = NULL;
      }
   }

   if ( CLSID_PhabletSignaturePadDocumentationPropertyPage == theCLSID ) {
      if ( pParent -> hwndDocumentationPropertyPage ) {
         DestroyWindow(pParent -> hwndDocumentationPropertyPage);
         pParent -> hwndDocumentationPropertyPage = NULL;
      }
   }

   if ( 0 == --nativePropertySheetFrameHandlerRefCount )
      SetWindowLongPtr(hwndPropertySheetFrame,GWLP_WNDPROC,(ULONG_PTR)nativePropertySheetFrameHandler);

   return S_OK;
   }



   HRESULT pkAPI::_IOleObject::_IPropertyPage::GetPageInfo(PROPPAGEINFO *pPageInfo) {

   memset(pPageInfo,0,sizeof(PROPPAGEINFO));

   pPageInfo -> cb = sizeof(PROPPAGEINFO);

   pPageInfo -> pszTitle = (BSTR)CoTaskMemAlloc(128);

   pPageInfo -> pszDocString = NULL;

   memset(pPageInfo -> pszTitle,0,128);

   if ( CLSID_PhabletSignaturePadPropertyPage == theCLSID ) {
      MultiByteToWideChar(CP_ACP,0,"Device Properties",-1,(BSTR)pPageInfo -> pszTitle,128);
      pPageInfo -> size.cx = 256 + 128 + 64;
      pPageInfo -> size.cy = 256 + 128;
   }

   if ( CLSID_PhabletSignaturePadRuntimePropertyPage == theCLSID ) {
      MultiByteToWideChar(CP_ACP,0,"Runtime Properties",-1,(BSTR)pPageInfo -> pszTitle,128);
      pPageInfo -> size.cx = 256 + 128 + 64;
      pPageInfo -> size.cy = 256 + 128;
   }

   if ( CLSID_PhabletSignaturePadDocumentationPropertyPage == theCLSID ) {
      MultiByteToWideChar(CP_ACP,0,"Documentation",-1,(BSTR)pPageInfo -> pszTitle,128);
      pPageInfo -> size.cx = 256 + 128 + 128 + 64;
      pPageInfo -> size.cy = 256 + 128 + 128;
   }


   return S_OK;
   }


   HRESULT pkAPI::_IOleObject::_IPropertyPage::SetObjects(ULONG cObjects,IUnknown** pUnk) {
   return S_OK;
   }


   HRESULT pkAPI::_IOleObject::_IPropertyPage::Show(UINT cmdShow) {

   if ( CLSID_PhabletSignaturePadPropertyPage == theCLSID )
      ShowWindow(pParent -> hwndPropertyPage,cmdShow);

   if ( CLSID_PhabletSignaturePadRuntimePropertyPage == theCLSID )
      ShowWindow(pParent -> hwndRuntimePropertyPage,cmdShow);

   if ( CLSID_PhabletSignaturePadDocumentationPropertyPage == theCLSID )
      ShowWindow(pParent -> hwndDocumentationPropertyPage,cmdShow);

   return S_OK;
   }


   HRESULT pkAPI::_IOleObject::_IPropertyPage::Move(const RECT *prc) {

   if ( CLSID_PhabletSignaturePadPropertyPage == theCLSID )
      SetWindowPos(pParent -> hwndPropertyPage,HWND_TOP,prc -> left,prc -> top, prc -> right - prc -> left,prc -> bottom - prc -> top,0L);

   if ( CLSID_PhabletSignaturePadRuntimePropertyPage == theCLSID )
      SetWindowPos(pParent -> hwndRuntimePropertyPage,HWND_TOP,prc -> left,prc -> top, prc -> right - prc -> left,prc -> bottom - prc -> top,0L);

   if ( CLSID_PhabletSignaturePadDocumentationPropertyPage == theCLSID )
      SetWindowPos(pParent -> hwndDocumentationPropertyPage,HWND_TOP,prc -> left,prc -> top, prc -> right - prc -> left,prc -> bottom - prc -> top,0L);

   return S_OK;
   }


   HRESULT pkAPI::_IOleObject::_IPropertyPage::IsPageDirty() {
   return S_OK;
   }


   HRESULT pkAPI::_IOleObject::_IPropertyPage::Apply() {

   pParent -> fire_PropertyChanged();

   if ( ! okayToSave )
      return S_OK;

   okayToSave = false;

   PSHNOTIFY notify = {0};

   notify.hdr.code = PSN_APPLY;
   notify.lParam = 1L;

//   if ( CLSID_PhabletSignaturePadPropertyPage == theCLSID ) 
      SendMessage(pParent -> hwndPropertyPage,WM_NOTIFY,0L,(LPARAM)&notify);

//   if ( CLSID_PhabletSignaturePadRuntimePropertyPage == theCLSID ) 
      SendMessage(pParent -> hwndRuntimePropertyPage,WM_NOTIFY,0L,(LPARAM)&notify);

//   if ( CLSID_PhabletSignaturePadDocumentationPropertyPage == theCLSID ) 
      SendMessage(pParent -> hwndDocumentationPropertyPage,WM_NOTIFY,0L,(LPARAM)&notify);

   return S_OK;
   }


   HRESULT pkAPI::_IOleObject::_IPropertyPage::Help(LPCOLESTR pszHelpDir) {
   return E_FAIL;
   }


   HRESULT pkAPI::_IOleObject::_IPropertyPage::TranslateAccelerator(MSG* pMsg) {
   return S_FALSE;
   }

//
//NTC: 02-08-2012. This is a total bullshit hack that is necessary because the MS documentation
// cannot bother to say anywhere how on earth to tell if the user pressed OK, or Cancel - as if
// nobody would ever want to know this absolutely obvious thing to need to know !?!
//
   LRESULT CALLBACK pkAPI::_IOleObject::propertySheetFrameHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {
   if ( msg == WM_COMMAND ) {
      switch ( LOWORD(wParam ) ) {
      case IDOK:
         okayToSave = true;
         break;
      case IDCANCEL:
         okayToSave = false;
         break;
      }
   }
   return CallWindowProc(nativePropertySheetFrameHandler,hwnd,msg,wParam,lParam);
   }
   