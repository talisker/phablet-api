// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

#define ALLOC_CONNECTIONS  16

   pkAPI::_IOleObject::_IConnectionPoint::_IConnectionPoint(_IOleObject *pp,const IID &theInterface) : 
      pParent(pp), 
      adviseSink(0),
      nextCookie(400),
      countLiveConnections(0),
      eventInterface(theInterface),
      refCount(0),
      countConnections(ALLOC_CONNECTIONS)
   { 
   connections = new CONNECTDATA[countConnections];
   memset(connections, 0, countConnections * sizeof(CONNECTDATA));
   return;
   };


   pkAPI::_IOleObject::_IConnectionPoint::~_IConnectionPoint() {
   for ( int k = 0; k < countConnections; k++ ) 
      if ( connections[k].pUnk )
         connections[k].pUnk -> Release();
   delete [] connections;
   return;
   }


   HRESULT pkAPI::_IOleObject::_IConnectionPoint::QueryInterface(REFIID riid,void **ppv) {
   if ( IID_IUnknown == riid ) 
      *ppv = static_cast<IUnknown *>(this);
   else 
      if ( ! ( IID_IConnectionPoint == riid ) )   
         return pParent -> QueryInterface(riid,ppv);
      else
         *ppv = static_cast<IConnectionPoint *>(this);
   AddRef();
   return S_OK;
   }


   STDMETHODIMP_(ULONG) pkAPI::_IOleObject::_IConnectionPoint::AddRef() {
   return ++refCount;
   }

   STDMETHODIMP_(ULONG) pkAPI::_IOleObject::_IConnectionPoint::Release() {
   return --refCount;
   }


   STDMETHODIMP pkAPI::_IOleObject::_IConnectionPoint::GetConnectionInterface(IID *pIID) {
   if ( NULL == pIID ) 
      return E_POINTER;
   *pIID = eventInterface;
   return S_OK;
   }


   STDMETHODIMP pkAPI::_IOleObject::_IConnectionPoint::GetConnectionPointContainer(IConnectionPointContainer **ppCPC) {
   return pParent -> QueryInterface(IID_IConnectionPointContainer,(void **)ppCPC);
   }


   STDMETHODIMP pkAPI::_IOleObject::_IConnectionPoint::Advise(IUnknown *pUnkSink,DWORD *pdwCookie) {

   HRESULT hr;
   IUnknown* pISink = 0;

   hr = pUnkSink -> QueryInterface(eventInterface,(void **)&pISink);

   if ( hr == E_NOINTERFACE ) 
      return CONNECT_E_NOCONNECTION;

   if ( ! SUCCEEDED(hr) ) 
      return hr;

   if ( ! pISink ) 
      return CONNECT_E_CANNOTCONNECT;

   int freeSlot = getSlot();

   *pdwCookie = 0;

   pISink -> AddRef();

   connections[freeSlot].pUnk = pISink;

   connections[freeSlot].dwCookie = nextCookie;

   *pdwCookie = nextCookie++;

   countLiveConnections++;

   return S_OK;
   }


   STDMETHODIMP pkAPI::_IOleObject::_IConnectionPoint::Unadvise(DWORD dwCookie) {

   if ( 0 == dwCookie )
      return E_INVALIDARG;

   int slot = findSlot(dwCookie);

   if ( -1 == slot )
      return CONNECT_E_NOCONNECTION;

   if ( connections[slot].pUnk )
      connections[slot].pUnk -> Release();

   connections[slot].dwCookie = 0;

   countLiveConnections--;

   return S_OK;
   }

   STDMETHODIMP pkAPI::_IOleObject::_IConnectionPoint::EnumConnections(IEnumConnections **ppEnum) {

   CONNECTDATA *tempConnections;
   int i,j;

   *ppEnum = NULL;

   if ( countLiveConnections == 0 ) 
      return OLE_E_NOCONNECTION;

   tempConnections = new CONNECTDATA[countLiveConnections];

   for ( i = 0, j = 0; i < countConnections && j < countLiveConnections; i++) {

     if ( 0 != connections[i].dwCookie ) {
       tempConnections[j].pUnk = (IUnknown *)connections[i].pUnk;
       tempConnections[j].dwCookie = connections[i].dwCookie;
       j++;
     }
   }

   _IEnumerateConnections *p = new _IEnumerateConnections(this,countLiveConnections,tempConnections,0);

   p -> QueryInterface(IID_IEnumConnections,(void **)ppEnum);

   delete [] tempConnections;

   return S_OK;
   }


   int pkAPI::_IOleObject::_IConnectionPoint::getSlot() {
   CONNECTDATA* moreConnections;
   int i;
   i = findSlot(0);
   if ( i > -1 ) return i;
   moreConnections = new CONNECTDATA[countConnections + ALLOC_CONNECTIONS];
   memset( moreConnections, 0, sizeof(CONNECTDATA) * (countConnections + ALLOC_CONNECTIONS));
   memcpy( moreConnections, connections, sizeof(CONNECTDATA) * countConnections);
   delete [] connections;
   connections = moreConnections;
   countConnections += ALLOC_CONNECTIONS;
   return countConnections - ALLOC_CONNECTIONS;
   }


   int pkAPI::_IOleObject::_IConnectionPoint::findSlot(DWORD dwCookie) {
   for ( int i = 0; i < countConnections; i++ )
      if ( dwCookie == connections[i].dwCookie ) return i;
   return -1;
   }