// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   pkAPI::_IOleObject::_IConnectionPointContainer::_IConnectionPointContainer(_IOleObject *pp) : pParent(pp) { 
   return;
   }

   pkAPI::_IOleObject::_IConnectionPointContainer::~_IConnectionPointContainer() {
   return;
   }

   HRESULT pkAPI::_IOleObject::_IConnectionPointContainer::QueryInterface(REFIID riid,void **ppv) {
   return pParent -> QueryInterface(riid,ppv);
   }

   STDMETHODIMP_(ULONG) pkAPI::_IOleObject::_IConnectionPointContainer::AddRef() {
   return pParent -> AddRef();
   }

   STDMETHODIMP_(ULONG) pkAPI::_IOleObject::_IConnectionPointContainer::Release() {
   return pParent -> Release();
   }


   STDMETHODIMP pkAPI::_IOleObject::_IConnectionPointContainer::EnumConnectionPoints(IEnumConnectionPoints **ppEnum) {

   _IConnectionPoint *connectionPoints[2];

   *ppEnum = NULL;
 
   if ( pParent -> enumConnectionPoints ) 
      delete pParent -> enumConnectionPoints;
 
   connectionPoints[1] = &pParent -> connectionPoint_PropertyNotifySink;
   connectionPoints[0] = &pParent -> connectionPoint_pkAPIEvents;

   pParent -> enumConnectionPoints = new _IEnumConnectionPoints(pParent,connectionPoints,2);
 
   return pParent -> enumConnectionPoints -> QueryInterface(IID_IEnumConnectionPoints,(void **)ppEnum);
   }
 
 
   STDMETHODIMP pkAPI::_IOleObject::_IConnectionPointContainer::FindConnectionPoint(REFIID riid,IConnectionPoint **ppCP) {

   *ppCP = NULL;

   if ( IID_IPropertyNotifySink == riid ) 
      return pParent -> connectionPoint_PropertyNotifySink.QueryInterface(IID_IConnectionPoint,(void **)ppCP);

   if ( IID_IPhabletSignaturePadEvents == riid ) 
      return pParent -> connectionPoint_pkAPIEvents.QueryInterface(IID_IConnectionPoint,(void **)ppCP);

   return CONNECT_E_NOCONNECTION;
   }


   void pkAPI::_IOleObject::_IConnectionPointContainer::fire_PropertyChanged() {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> connectionPoint_PropertyNotifySink.EnumConnections(&pIEnum);
   if ( ! pIEnum ) 
      return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPropertyNotifySink *pClient = reinterpret_cast<IPropertyNotifySink *>(connectData.pUnk);
      pClient -> OnChanged(DISPID_UNKNOWN);
   }
   pIEnum -> Release();
   return;
   }

   void pkAPI::_IOleObject::_IConnectionPointContainer::fire_PenDown(long x,long y) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> connectionPoint_pkAPIEvents.EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents *pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> PenDown(x,y);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }

   void pkAPI::_IOleObject::_IConnectionPointContainer::fire_PenUp(long x,long y) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> connectionPoint_pkAPIEvents.EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents *pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> PenUp(x,y);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }

   void pkAPI::_IOleObject::_IConnectionPointContainer::fire_PenPoint(long x,long y,float inkWeight) {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> connectionPoint_pkAPIEvents.EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents *pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> PenPoint(x,y,inkWeight);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }

   void pkAPI::_IOleObject::_IConnectionPointContainer::fire_DeviceReady() {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> connectionPoint_pkAPIEvents.EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents *pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> DeviceReady();
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }

   void pkAPI::_IOleObject::_IConnectionPointContainer::fire_ConfigurationChanged() {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> connectionPoint_pkAPIEvents.EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPhabletSignaturePadEvents *pClient = reinterpret_cast<IPhabletSignaturePadEvents *>(connectData.pUnk);
      pClient -> ConfigurationChanged();
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }