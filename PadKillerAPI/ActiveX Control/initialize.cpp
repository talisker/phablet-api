// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   void pkAPI::_IOleObject::initialize() {

   if ( pIOleInPlaceSite_MySite )
      pIOleInPlaceSite_MySite -> Release();
 
   pIOleInPlaceSite_MySite = NULL;
 
   pIOleClientSite_MySite -> QueryInterface(IID_IOleInPlaceSite,(void **)&pIOleInPlaceSite_MySite);

   IDispatch* pIDispatch;

   HRESULT rc = pIOleClientSite_MySite -> QueryInterface(IID_IDispatch,reinterpret_cast<void **>(&pIDispatch));

   if ( S_OK == rc ) {
      DISPPARAMS dispparamsNoArgs = {NULL, NULL, 0, 0};
      VARIANT var = {VT_EMPTY};
      var.vt = VT_BOOL;
      pIDispatch -> Invoke(DISPID_AMBIENT_USERMODE,IID_NULL,LOCALE_USER_DEFAULT,DISPATCH_PROPERTYGET,&dispparamsNoArgs,&var,NULL,NULL);
      pIDispatch -> Release();
      isRunning = (var.bVal == 0 ? false : true);
   } else
      isRunning = true;

   hwndContainer = NULL;

   pIOleInPlaceSite_MySite -> GetWindow(&hwndContainer);

   if ( pParent -> hwndSite ) {
      SetParent(pParent -> hwndSite,hwndContainer);
      return;
   }

   UINT arraySize;
   
   UINT countEncoders = 0L;

   Gdiplus::GetImageEncodersSize(&countEncoders,&arraySize);

   Gdiplus::ImageCodecInfo *pEncoders = (Gdiplus::ImageCodecInfo *)new BYTE[arraySize];

   Gdiplus::GetImageEncoders(countEncoders,arraySize,pEncoders);
   
   for ( DWORD k = 0; k < countEncoders; k++ ) {
      if ( 0 == _wcsicmp(pEncoders[k].MimeType,L"image/jpeg") ) {
         pParent -> pTheEncoder = &pEncoders[k];
#if 0
         if ( 0 == wcsicmp(preferredImageMimeType,L"image/jpeg") ) {
         Bitmap *fms = new Bitmap(1,1);
         UINT encoderParameterSize = fms -> GetEncoderParameterListSize(&pTheEncoder -> Clsid);
         if ( encoderParameterSize ) {
            pEncoderParameters = (EncoderParameters *)new BYTE[encoderParameterSize];
            fms -> GetEncoderParameterList(&pTheEncoder -> Clsid,encoderParameterSize,pEncoderParameters);
            for ( long j = 0; j < pEncoderParameters -> Count; j++ ) {
               if ( EncoderQuality /*EncoderColorDepth*/ == pEncoderParameters -> Parameter[j].Guid ) {
                  encoderQuality = 50L;
                  //pEncoderParameters -> Parameter[j].Value = &encoderQuality;
               }
            }
         }
         delete fms;
         }
#endif
         break;
      }

   }

   pParent -> initializeMyWindow(runTransparent,renderSunkenBorder,hwndContainer);

   return;
   }

