// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   long __stdcall pkAPI::_IOleObject::_IRunnableObject::QueryInterface(REFIID riid,void **ppv) {

   if ( IID_IRunnableObject == riid )
      *ppv = static_cast<IRunnableObject *>(this); 
   else
      return pParent -> QueryInterface(riid,ppv);
   
   AddRef();
   return S_OK;
   }
   unsigned long __stdcall pkAPI::_IOleObject::_IRunnableObject::AddRef() {
   return pParent -> AddRef();
   }
   unsigned long __stdcall pkAPI::_IOleObject::_IRunnableObject::Release() {
   return pParent -> Release();
   }


   STDMETHODIMP pkAPI::_IOleObject::_IRunnableObject::GetRunningClass(CLSID *pcid) {
   *pcid = CLSID_PhabletSignaturePad;
   return E_UNEXPECTED;
   }

   STDMETHODIMP pkAPI::_IOleObject::_IRunnableObject::Run(LPBC pBindingContext) { 
   return S_OK; 
   }

   int __stdcall pkAPI::_IOleObject::_IRunnableObject::IsRunning() { 
   return 1; 
   }

   STDMETHODIMP pkAPI::_IOleObject::_IRunnableObject::LockRunning(BOOL,BOOL) { 
   return S_OK; 
   }

   STDMETHODIMP pkAPI::_IOleObject::_IRunnableObject::SetContainedObject(BOOL) { 
   return S_OK; 
   }

