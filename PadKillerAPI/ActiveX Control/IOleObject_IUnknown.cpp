// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   long __stdcall pkAPI::_IOleObject::QueryInterface(REFIID riid,void **ppv) {
 
   if ( IID_IUnknown == riid )
      *ppv = static_cast<IUnknown *>(this);
   else

   if ( IID_IOleObject == riid )
      *ppv = static_cast<IOleObject *>(this); 
   else

   if ( IID_IQuickActivate == riid )
      return pIQuickActivate -> QueryInterface(riid,ppv);
   else

   if ( IID_IDataObject == riid )
      return pIDataObject -> QueryInterface(riid,ppv);
   else

   if ( IID_IOleControl == riid ) 
      return pIOleControl -> QueryInterface(riid,ppv);
   else

   if ( IID_IOleInPlaceObject == riid )
      return pIOleInPlaceObject -> QueryInterface(riid,ppv);
   else

   if ( IID_IOleInPlaceActiveObject == riid )
      return pIOleInPlaceActiveObject -> QueryInterface(riid,ppv);
   else

   if ( IID_IProvideClassInfo == riid )
      return pIProvideClassInfo2 -> QueryInterface(riid,ppv);
   else

   if ( IID_IProvideClassInfo2 == riid )
      return pIProvideClassInfo2 -> QueryInterface(riid,ppv);
   else

   if ( IID_IViewObject == riid )
      return pIViewObjectEx -> QueryInterface(riid,ppv);
   else

   if ( IID_IViewObject2 == riid )
      return pIViewObjectEx -> QueryInterface(riid,ppv);
   else

   if ( IID_IViewObjectEx == riid )
      return pIViewObjectEx -> QueryInterface(riid,ppv);
   else

   if ( IID_IRunnableObject == riid )
      return pIRunnableObject -> QueryInterface(riid,ppv);
   else

   if ( IID_ISpecifyPropertyPages == riid )
      return pISpecifyPropertyPages -> QueryInterface(riid,ppv);
   else

#if 0
   if ( IID_IGPropertyPageClient == riid ) 
      return pIGPropertyPageClient -> QueryInterface(riid,ppv);
   else
#endif

   if ( IID_IPersistStreamInit == riid )
      return pIPersistStreamInit -> QueryInterface(riid,ppv);
   else

#if 0
   if ( IID_IPersistStorage == riid )
      return pIPersistStorage -> QueryInterface(riid,ppv);
   else
#endif

   if ( IID_IConnectionPointContainer == riid ) 

      *ppv = static_cast<IConnectionPointContainer *>(&connectionPointContainer);

   else {

      fromOleObject = true;
      HRESULT rc = pParent -> QueryInterface(riid,ppv);
      fromOleObject = false;
      return rc;

   }

   AddRef();
  
   return S_OK; 
   }

   unsigned long __stdcall pkAPI::_IOleObject::AddRef() {
   return ++refCount;
   }
 
   unsigned long __stdcall pkAPI::_IOleObject::Release() {
   if ( 1 == refCount ) {
      delete this;
      return 0;
   }
   return --refCount;
   }
