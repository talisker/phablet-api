// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   LRESULT CALLBACK pkAPI::_IOleObject::settingsHandlerISpecifyPropertyPageImplementation(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {
   if ( WM_INITDIALOG == msg ) {
      PROPSHEETPAGE propSheetPage = {0};
      propSheetPage.lParam = lParam;
      return settingsHandler(hwnd,msg,wParam,(ULONG_PTR)&propSheetPage);
   }
   return settingsHandler(hwnd,msg,wParam,lParam);
   }


   LRESULT CALLBACK pkAPI::_IOleObject::runtimeSettingsHandlerISpecifyPropertyPageImplementation(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {
   if ( WM_INITDIALOG == msg ) {
      PROPSHEETPAGE propSheetPage = {0};
      propSheetPage.lParam = lParam;
      return runtimeSettingsHandler(hwnd,msg,wParam,(ULONG_PTR)&propSheetPage);
   }
   return runtimeSettingsHandler(hwnd,msg,wParam,lParam);
   }


   LRESULT CALLBACK pkAPI::_IOleObject::documentationSettingsHandlerISpecifyPropertyPageImplementation(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {
   if ( WM_INITDIALOG == msg ) {
      PROPSHEETPAGE propSheetPage = {0};
      propSheetPage.lParam = lParam;
      return documentationSettingsHandler(hwnd,msg,wParam,(ULONG_PTR)&propSheetPage);
   }
   return documentationSettingsHandler(hwnd,msg,wParam,lParam);
   }
