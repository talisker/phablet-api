// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"
#include <uxtheme.h>

   WNDPROC pkAPI::_IOleObject::nativePropertySheetFrameHandler = NULL;
   long pkAPI::_IOleObject::nativePropertySheetFrameHandlerRefCount = 0L;
   HWND pkAPI::_IOleObject::hwndPropertySheetFrame = NULL;

   pkAPI::_IOleObject::_IOleObject(pkAPI *pp) :

      pParent(pp),

      pIOleControl(NULL),
      pIDataObject(NULL),
      pIOleInPlaceObject(NULL),
      pIOleInPlaceActiveObject(NULL),
      pIViewObjectEx(NULL),
      pIProvideClassInfo2(NULL),
      pIPersistStreamInit(NULL),
      pIPersistStorage(NULL),
      pIQuickActivate(NULL),
      pISpecifyPropertyPages(NULL),
      pIRunnableObject(NULL),

      pOleAdviseHolder(NULL),
      pDataAdviseHolder(NULL),

      pViewAdviseSink(NULL),
      dwViewAdviseSinkConnection(0L),
      adviseSink_dwAspect(0L),
      adviseSink_advf(0L),

      pIOleClientSite_MySite(NULL),
      pIOleInPlaceSite_MySite(NULL),
   
      connectionPointContainer(this),
      connectionPoint_PropertyNotifySink(this,IID_IPropertyNotifySink),
      connectionPoint_pkAPIEvents(this,IID_IPhabletSignaturePadEvents),
      enumConnectionPoints(0),
      enumConnections(0),

      isRunning(false),
      renderSignature(true),
      runTransparent(false),
      renderSunkenBorder(false),

      hwndContainer(NULL),

      hwndPropertyPage(NULL),
      hwndRuntimePropertyPage(NULL),
      hwndDocumentationPropertyPage(NULL),

      controlsFontSize(12.0f),

      maxControlId(0L),

      cxScale(1.0),
      cyScale(1.0),

      refCount(1)

   {

   memset(&containerSize,0,sizeof(containerSize));
   memset(&controlsFontFamily,0,sizeof(controlsFontFamily));

   pIOleControl = new _IOleControl(this);
   pIDataObject = new _IDataObject(this);
   pIOleInPlaceObject = new _IOleInPlaceObject(this);
   pIOleInPlaceActiveObject = new _IOleInPlaceActiveObject(this);
   pIProvideClassInfo2 = new _IProvideClassInfo2(this);
   pIViewObjectEx = new _IViewObjectEx(this);
   pIRunnableObject = new _IRunnableObject(this);
   pIPersistStreamInit = new _IPersistStreamInit(this);
   pIPersistStorage = new _IPersistStorage(this);
   pIQuickActivate = new _IQuickActivate(this);
   pISpecifyPropertyPages = new _ISpecifyPropertyPages(this);

   pIPropertyPage[0] = new _IPropertyPage(this,CLSID_PhabletSignaturePadPropertyPage);
   pIPropertyPage[1] = new _IPropertyPage(this,CLSID_PhabletSignaturePadRuntimePropertyPage);
   pIPropertyPage[2] = new _IPropertyPage(this,CLSID_PhabletSignaturePadDocumentationPropertyPage);

   pDeviceProperties = pParent -> pProperties;
   devicePropertiesSize = pParent -> propertiesSize;

   pRuntimeProperties = (BYTE *)&renderSignature;
   runtimePropertiesSize = offsetof(_IOleObject,pRuntimeProperties) - offsetof(_IOleObject,renderSignature);

   return;
   }


   pkAPI::_IOleObject::~_IOleObject() {

   delete pIOleControl;
   delete pIDataObject;
   delete pIOleInPlaceObject;
   delete pIOleInPlaceActiveObject;
   delete pIProvideClassInfo2;
   delete pIViewObjectEx;
   delete pIRunnableObject;
   delete pIPersistStreamInit;
   delete pIPersistStorage;
   delete pIQuickActivate;
   delete pISpecifyPropertyPages;

   delete pIPropertyPage[0];
   delete pIPropertyPage[1];
   delete pIPropertyPage[2];

   for ( std::list<HWND>::iterator it = runtimeControls.begin(); it != runtimeControls.end(); it++ )
      DestroyWindow((*it));

   runtimeControls.clear();

   return;
   }


   HWND pkAPI::_IOleObject::addControl(WCHAR *pszwClass,long exStyle,long style,long x,long y,long eventId,WCHAR *pszwText,bool isVisible) {

   HWND hwndOldControl = GetDlgItem(pParent -> hwndSite,IDDI_ACTIVEX_RUNTIME_COMMAND_BASE + eventId);

   if ( hwndOldControl )
      DestroyWindow(hwndOldControl);

   if ( maxControlId < eventId )
      maxControlId = eventId;

   HWND hwndControl = CreateWindowExW(exStyle,pszwClass,pszwText,(( isVisible ? 1 : 0 ) * WS_VISIBLE ) | style | WS_CHILD,x,y,0,0,pParent -> hwndSite,(HMENU)(UINT_PTR)(IDDI_ACTIVEX_RUNTIME_COMMAND_BASE + eventId),NULL,NULL);

   runtimeControls.insert(runtimeControls.end(),hwndControl);

   return hwndControl;
   }


   HWND pkAPI::_IOleObject::addRadioButton(long x,long y,long eventId,WCHAR *pszwText,bool isVisible,bool isChecked,long groupNumber) {
   HWND hwndControl = addControl(L"button",0L,BS_RADIOBUTTON,x,y,eventId,pszwText,isVisible);
   radioButtonGroups[hwndControl] = groupNumber;
   SendMessage(hwndControl,BM_SETCHECK,isChecked ? BST_CHECKED : BST_UNCHECKED,0L);
   return hwndControl;
   }

   HWND pkAPI::_IOleObject::addCheckBox(long x,long y,long eventId,WCHAR *pszwText,bool isVisible,bool isChecked) {
   HWND hwndControl = addControl(L"button",0L,BS_AUTOCHECKBOX,x,y,eventId,pszwText,isVisible);
   SendMessage(hwndControl,BM_SETCHECK,isChecked ? BST_CHECKED : BST_UNCHECKED,0L);
   return hwndControl;
   }

   void pkAPI::_IOleObject::showControl(long controlId) {
   ShowWindow(GetDlgItem(pParent -> hwndSite,(IDDI_ACTIVEX_RUNTIME_COMMAND_BASE + controlId)),SW_SHOW);
   return;
   }

   void pkAPI::_IOleObject::hideControl(long controlId) {
   ShowWindow(GetDlgItem(pParent -> hwndSite,(IDDI_ACTIVEX_RUNTIME_COMMAND_BASE + controlId)),SW_HIDE);
   return;
   }

   void pkAPI::_IOleObject::setControlsFontSizeValue(float value) {
   controlsFontSize = value;
   return;
   }

   void pkAPI::_IOleObject::setControlsFontFace(WCHAR *pszwFace) {
   wcscpy(controlsFontFamily,pszwFace);
   return;
   }


   HFONT pkAPI::_IOleObject::createControlsFont(long controlId) {

   RECT rcHost = {0};

   GetWindowRect(pParent -> hwndSite,&rcHost);

   RECT rcControl = {0};

   pParent -> get_ControlBounds(controlId,&rcControl);

   double pctButtonHeight = (double)(rcControl.bottom - rcControl.top) / (double)pParent -> getPadHeight();

   HDC hdc = GetDC(pParent -> hwndSite);

   LOGFONTW baseSystemFont = {0};

   GetObjectW(GetStockObject(DEFAULT_GUI_FONT),sizeof(LOGFONTW),&baseSystemFont);

   long pointSize = 8;

   baseSystemFont.lfHeight = -MulDiv(pointSize,GetDeviceCaps(hdc,LOGPIXELSY),72);

   if ( controlsFontFamily[0] )
      wcscpy(baseSystemFont.lfFaceName,controlsFontFamily);

   HFONT hFontControls = CreateFontIndirectW(&baseSystemFont);

   RECT rcText = {0};

   HGDIOBJ oldFont = SelectObject(hdc,hFontControls);

   rcText.right = LONG_MAX;
   rcText.bottom = LONG_MAX;

   long textHeight = DrawTextExW(hdc,L"M",-1,&rcText,DT_CALCRECT | DT_WORDBREAK | DT_NOCLIP | DT_EDITCONTROL,NULL);

   SelectObject(hdc,oldFont);

   double currentPctHeight = (double)(rcText.bottom - rcText.top) / (double)(rcHost.bottom - rcHost.top);

   while ( currentPctHeight < pctButtonHeight ) {

      SelectObject(hdc,oldFont);

      DeleteObject(hFontControls);

      pointSize++;

      baseSystemFont.lfHeight = -MulDiv(pointSize,GetDeviceCaps(hdc,LOGPIXELSY),72);

      hFontControls = CreateFontIndirectW(&baseSystemFont);

      oldFont = SelectObject(hdc,hFontControls);

      rcText.right = LONG_MAX;
      rcText.bottom = LONG_MAX;

      textHeight = DrawTextExW(hdc,L"M",-1,&rcText,DT_CALCRECT | DT_WORDBREAK | DT_NOCLIP | DT_EDITCONTROL,NULL);

      currentPctHeight = (double)(rcText.bottom - rcText.top) / (double)(rcHost.bottom - rcHost.top);

   }

   ReleaseDC(pParent -> hwndSite,hdc);

   return hFontControls;
   }


   HFONT pkAPI::_IOleObject::setControlFont(long controlId,WCHAR *pszwFontFace) {

   HWND hwndControl = GetDlgItem(pParent -> hwndSite,(IDDI_ACTIVEX_RUNTIME_COMMAND_BASE + controlId));

   if ( ! hwndControl )
      return NULL;

   float fontHeightPixels;

   pParent -> get_ControlFontHeightPixels(controlId,&fontHeightPixels);

   fontHeightPixels = (float)((double)fontHeightPixels * cyScale);

   LOGFONTW baseSystemFont = {0};

   GetObjectW(GetStockObject(DEFAULT_GUI_FONT),sizeof(LOGFONTW),&baseSystemFont);

   baseSystemFont.lfHeight = -(long)fontHeightPixels;

   if ( fontHeightPixels + baseSystemFont.lfHeight > 0.5 )
      baseSystemFont.lfHeight -= 1;

   if ( pszwFontFace && pszwFontFace[0] )
      wcscpy(baseSystemFont.lfFaceName,pszwFontFace);
   else
      wcscpy(baseSystemFont.lfFaceName,controlsFontFamily);

   HFONT hFont = CreateFontIndirectW(&baseSystemFont);

   SendMessage(hwndControl,WM_SETFONT,(WPARAM)hFont,0L);

   return hFont;
   }


   void pkAPI::_IOleObject::positionControl(long eventId,long x,long y) {

   HWND hwnd = GetDlgItem(pParent -> hwndSite,IDDI_ACTIVEX_RUNTIME_COMMAND_BASE + eventId);

   if ( ! hwnd )
      return;

   SetWindowPos(hwnd,HWND_TOP,(int)((double)x * cxScale),(int)((double)y * cyScale),0,0,SWP_SHOWWINDOW | SWP_NOSIZE);

   return;
   }

   void pkAPI::_IOleObject::setControlText(long controlId,WCHAR *pszwText) {
   SendDlgItemMessageW(pParent -> hwndSite,IDDI_ACTIVEX_RUNTIME_COMMAND_BASE + controlId,WM_SETTEXT,0L,(LPARAM)pszwText);
   return;
   }

