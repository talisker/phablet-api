// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   static long isProcessingPhabletEvent = 0L;

   LRESULT CALLBACK pkAPI::siteHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   pkAPI *p = (pkAPI *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   switch ( msg ) {

   case WM_CREATE: {
      CREATESTRUCT *pcs = (CREATESTRUCT *)lParam;
      SetWindowLongPtr(hwnd,GWLP_USERDATA,(ULONG_PTR)pcs -> lpCreateParams);
      }
      break;

   case WM_PAINT: {

      PAINTSTRUCT ps = {0};

      BeginPaint(hwnd,&ps);

      if ( ! p -> pIOleObject -> runTransparent || ! p -> pIOleObject -> isRunning )
         FillRect(ps.hdc,&ps.rcPaint,(HBRUSH)(COLOR_WINDOW + 1));

      if ( ! p -> pIOleObject -> isRunning || p -> pIOleObject -> runTransparent ) {
         EndPaint(hwnd,&ps);
         break;
      }

      if ( hBitmapBackground ) {

         HDC hdcSource = CreateCompatibleDC(NULL);

         HGDIOBJ oldObj = SelectObject(hdcSource,hBitmapBackground);

         BOOL rc = StretchBlt(ps.hdc,0,0,p -> sizelDisplay.cx,p -> sizelDisplay.cy,hdcSource,0,0,cxBitmapBackground,cyBitmapBackground,SRCCOPY);

         SelectObject(hdcSource,oldObj);

         DeleteDC(hdcSource);

      } else

         BitBlt(ps.hdc,ps.rcPaint.left,ps.rcPaint.top,ps.rcPaint.right - ps.rcPaint.left,ps.rcPaint.bottom - ps.rcPaint.top,NULL,0,0,WHITENESS);

      EndPaint(hwnd,&ps);

      break;
      }

   case WM_COMMAND: {

      if ( ! p )
         break;

      if ( ! p -> pIOleObject )
         break;

      if ( IDDI_ACTIVEX_RUNTIME_COMMAND_BASE > LOWORD(wParam) || LOWORD(wParam) > IDDI_ACTIVEX_RUNTIME_COMMAND_BASE + p -> pIOleObject -> maxControlId ) 
         break;

      long controlStyle = (long)GetWindowLongPtr((HWND)lParam,GWL_STYLE);

      switch ( HIWORD(wParam) ) {

      case BN_CLICKED: {

         if ( controlStyle & BS_AUTOCHECKBOX ) {

            if ( ! isProcessingPhabletEvent ) {
               WCHAR szwCommand[128];
               memset(szwCommand,0,sizeof(szwCommand));
               swprintf(szwCommand,L"%ld %ld %ld",SETOPTIONSTATE,LOWORD(wParam) - IDDI_ACTIVEX_RUNTIME_COMMAND_BASE,SendMessage((HWND)lParam,BM_GETCHECK,0L,0L) == BST_CHECKED ? 1L : 0L);
               p -> sendCommand(szwCommand);
            }
               
            break;

         }

         if ( controlStyle & BS_RADIOBUTTON ) {

            SendMessage((HWND)lParam,BM_SETCHECK,BST_CHECKED,0L);   

            WCHAR szwCommand[128];

            long myGroup = p -> pIOleObject -> radioButtonGroups[(HWND)lParam];

            for ( std::pair<HWND,long>pair : p -> pIOleObject -> radioButtonGroups ) {
               if ( (HWND)lParam == pair.first ) 
                  continue;
               if ( pair.second == myGroup ) {
                  SendMessage(pair.first,BM_SETCHECK,BST_UNCHECKED,0L);
                  if ( ! isProcessingPhabletEvent ) {
                     memset(szwCommand,0,sizeof(szwCommand));
                     long idOther = (long)GetWindowLongPtr(pair.first,GWL_ID) - IDDI_ACTIVEX_RUNTIME_COMMAND_BASE;
                     swprintf(szwCommand,L"%ld %ld %ld",SETOPTIONSTATE,idOther,0L);
                     p -> sendCommand(szwCommand);
                  }
               }
            }

            if ( ! isProcessingPhabletEvent ) {
               memset(szwCommand,0,sizeof(szwCommand));
               swprintf(szwCommand,L"%ld %ld %ld",SETOPTIONSTATE,LOWORD(wParam) - IDDI_ACTIVEX_RUNTIME_COMMAND_BASE,1L);
               p -> sendCommand(szwCommand);
            }

            break;

         }

         char szClass[64];

         GetClassName(GetDlgItem(p -> hwndSite,LOWORD(wParam)),szClass,64);

         if ( 0 == _stricmp("button",szClass) )
            p -> FireOption(LOWORD(wParam) - IDDI_ACTIVEX_RUNTIME_COMMAND_BASE);

         }

         break;

      case CBN_SELCHANGE: {

         WCHAR szwCommand[128];
         memset(szwCommand,0,sizeof(szwCommand));
         swprintf(szwCommand,L"%ld %ld �",SELECTITEM,LOWORD(wParam) - IDDI_ACTIVEX_RUNTIME_COMMAND_BASE);
         SendMessageW((HWND)lParam,CB_GETLBTEXT,SendMessage((HWND)lParam,CB_GETCURSEL,0L,0L),(LPARAM)&szwCommand[wcslen(szwCommand)]);
         szwCommand[wcslen(szwCommand)] = L'�';
         p -> sendCommand(szwCommand);

         }
         break;

      case EN_CHANGE: {

         char szClass[64];

         GetClassName((HWND)lParam,szClass,64);

         if ( 0 == _stricmp("edit",szClass) ) {

            WCHAR szwCommand[1024];
            memset(szwCommand,0,sizeof(szwCommand));
            swprintf(szwCommand,L"%ld %ld %ld �",SETPARAMETER,PARM_CONTROL_TEXT,LOWORD(wParam) - IDDI_ACTIVEX_RUNTIME_COMMAND_BASE);
            GetWindowTextW((HWND)lParam,&szwCommand[(DWORD)wcslen(szwCommand)],1024 - (DWORD)wcslen(szwCommand));
            szwCommand[wcslen(szwCommand)] = L'�';
            p -> sendCommand(szwCommand);

         }

         }
         break;

      }

      }

      break;

   case WM_CTLCOLORBTN:
   case WM_CTLCOLORSTATIC: {
      HDC hdc = (HDC)wParam;
      SetBkMode(hdc,TRANSPARENT);
      }
      return (LRESULT)GetStockObject(HOLLOW_BRUSH);

   case WM_NOTIFY: { 

      NMHDR *pNotificationHeader = (NMHDR *)lParam;

      switch ( pNotificationHeader -> code ) {

      case NM_CUSTOMDRAW: {

         NMCUSTOMDRAW *pCustomDraw = (NMCUSTOMDRAW *)lParam;

         switch ( pCustomDraw -> dwDrawStage ) {

         case CDDS_PREERASE: {

            long style = (long)GetWindowLongPtr(pNotificationHeader -> hwndFrom,GWL_STYLE);

            if ( style & BS_AUTOCHECKBOX || style & BS_RADIOBUTTON )
               FillRect(pCustomDraw -> hdc,&pCustomDraw -> rc,(HBRUSH)GetStockObject(WHITE_BRUSH)); // <----- WTF ??? HOLLOW_BRUSH doesn't work, NULL_BRUSH doesn't work WHERE is the documentation ?

            }
            break;

         default:
            break;
         }

         }
	     return CDRF_DODEFAULT;

      default:
         break;
      }

      }
      break;

   case WM_SIZE: 
      p -> sizelDisplay.cx = LOWORD(lParam);
      p -> sizelDisplay.cy = HIWORD(lParam);
      p -> setupGDIPlus(hwnd);
      break;

   case WM_PEN_UP_EVENT:
      if ( p -> pICursiVisionSignaturePad )
         p -> pICursiVisionSignaturePad -> pIConnectionPointContainer -> fire_PenUp((long)wParam,(long)lParam);
      else if ( p -> pIVisioLoggerSignaturePad )
         p -> pIVisioLoggerSignaturePad -> pIConnectionPointContainer -> fire_PenUp((long)wParam,(long)lParam);
      else
         p -> connectionPointContainer.fire_PenUp((long)wParam,(long)lParam);
      break;

   case WM_PEN_DOWN_EVENT:
      if ( p -> pICursiVisionSignaturePad )
         p -> pICursiVisionSignaturePad -> pIConnectionPointContainer -> fire_PenDown((long)wParam,(long)lParam);
      else if ( p -> pIVisioLoggerSignaturePad )
         p -> pIVisioLoggerSignaturePad -> pIConnectionPointContainer -> fire_PenDown((long)wParam,(long)lParam);
      else
         p -> connectionPointContainer.fire_PenDown((long)wParam,(long)lParam);
      break;

   case WM_PEN_POINT_EVENT: {
      long x = LOWORD(wParam);
      long y = HIWORD(wParam);
      float inkWeight = (float)lParam / 10000.0f;
      if ( p -> pICursiVisionSignaturePad )
         p -> pICursiVisionSignaturePad -> pIConnectionPointContainer -> fire_PenPoint(x,y,inkWeight);
      else if ( p -> pIVisioLoggerSignaturePad )
         p -> pIVisioLoggerSignaturePad -> pIConnectionPointContainer -> fire_PenPoint(x,y,inkWeight);
      else 
         p -> connectionPointContainer.fire_PenPoint(x,y,inkWeight);
      }
      break;

   case WM_OPTION_EVENT: {

      HWND hwndControl = GetDlgItem(p -> hwndSite,IDDI_ACTIVEX_RUNTIME_COMMAND_BASE + (long)wParam);

      long style = (long)GetWindowLongPtr(hwndControl,GWL_STYLE);

      isProcessingPhabletEvent = 1L;

      if ( style & BS_RADIOBUTTON )
         siteHandler(hwnd,WM_COMMAND,MAKEWPARAM(IDDI_ACTIVEX_RUNTIME_COMMAND_BASE + (long)wParam,BN_CLICKED),(LPARAM)hwndControl);

      if ( style & BS_AUTOCHECKBOX )
         SendMessage(hwndControl,BM_SETCHECK,(WPARAM)BST_CHECKED,0L);

      isProcessingPhabletEvent = 0L;

      if ( p -> pICursiVisionSignaturePad )
         p -> pICursiVisionSignaturePad -> pIConnectionPointContainer -> fire_OptionSelected((long)wParam);
      else if ( p -> pIVisioLoggerSignaturePad )
         p -> pIVisioLoggerSignaturePad -> pIConnectionPointContainer -> fire_OptionSelected((long)wParam);
      else
         p -> connectionPointContainer.fire_OptionSelected((long)wParam);

      }
      break;

   case WM_OPTION_UNSELECTED_EVENT: {

      HWND hwndControl = GetDlgItem(p -> hwndSite,IDDI_ACTIVEX_RUNTIME_COMMAND_BASE + (long)wParam);

      long style = (long)GetWindowLongPtr(hwndControl,GWL_STYLE);

      isProcessingPhabletEvent = 1L;

      if ( style & BS_AUTOCHECKBOX )
         SendMessage(hwndControl,BM_SETCHECK,(WPARAM)BST_UNCHECKED,0L);

      isProcessingPhabletEvent = 0L;

      p -> connectionPointContainer.fire_OptionUnSelected((long)wParam);

      }

      break;

   case WM_DEVICE_READY_EVENT:
      p -> connectionPointContainer.fire_DeviceReady();
      break;

   case WM_CONFIGURATION_CHANGED_EVENT:
      p -> connectionPointContainer.fire_ConfigurationChanged();
      break;

   case WM_ITEM_SELECTED_EVENT: {
      WCHAR *pszwItem = szwStringsForConsumption[lParam];
      HWND hwndControl = GetDlgItem(p -> hwndSite,IDDI_ACTIVEX_RUNTIME_COMMAND_BASE + (long)wParam);
      SendMessageW(hwndControl,CB_SELECTSTRING,-1L,(LPARAM)pszwItem);
      BSTR bstrItem = SysAllocString(pszwItem);
      p -> connectionPointContainer.fire_ItemSelected((long)wParam,bstrItem);
      SysFreeString(bstrItem);
      }
      break;

   case WM_TEXT_CHANGED_EVENT: {
      WCHAR *pszwIndexes = szwStringsForConsumption[lParam];
      HWND hwndControl = GetDlgItem(p -> hwndSite,IDDI_ACTIVEX_RUNTIME_COMMAND_BASE + (long)wParam);
      long stringIndexOld = _wtol(wcstok(pszwIndexes,L" ",NULL));
      long stringIndexNew = _wtol(wcstok(NULL,L" ",NULL));
      SetWindowTextW(hwndControl,szwStringsForConsumption[stringIndexNew]);
      SendMessage(hwndControl,EM_SETSEL,(WPARAM)wcslen(szwStringsForConsumption[stringIndexNew]),(LPARAM)wcslen(szwStringsForConsumption[stringIndexNew]));
      }
      break;

   default:
      break;

   }

   return DefWindowProc(hwnd,msg,wParam,lParam);
   }
