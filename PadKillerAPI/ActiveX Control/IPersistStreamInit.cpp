// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   long __stdcall pkAPI::_IOleObject::_IPersistStreamInit::QueryInterface(REFIID riid,void **ppv) {

   *ppv = NULL;

   if ( IID_IPersistStreamInit == riid )
      *ppv = static_cast<IPersistStreamInit *>(this);
   else

      return pParent -> QueryInterface(riid,ppv);

   AddRef();

   return S_OK;
   }
   unsigned long __stdcall pkAPI::_IOleObject::_IPersistStreamInit::AddRef() {
   return pParent -> AddRef();
   }
   unsigned long __stdcall pkAPI::_IOleObject::_IPersistStreamInit::Release() {
   return pParent -> Release();
   }


   STDMETHODIMP pkAPI::_IOleObject::_IPersistStreamInit::GetClassID(CLSID *pcid) {
   memcpy(pcid,&CLSID_PhabletSignaturePad,sizeof(GUID));
   return S_OK;
   }


   STDMETHODIMP pkAPI::_IOleObject::_IPersistStreamInit::IsDirty() {
   return S_OK;
   }
 
 
   STDMETHODIMP pkAPI::_IOleObject::_IPersistStreamInit::Load(IStream *pIStream) {

   ULONG cbRead = pParent -> devicePropertiesSize;
   HRESULT rc = pIStream -> Read(pParent -> pDeviceProperties,cbRead,&cbRead);

   cbRead = pParent -> runtimePropertiesSize;
   rc = pIStream -> Read(pParent -> pRuntimeProperties,cbRead,&cbRead);

   if ( pParent -> pParent -> hwndSite ) {
      if ( pParent -> renderSunkenBorder )
         SetWindowLongPtr(pParent -> pParent -> hwndSite,GWL_EXSTYLE,GetWindowLongPtr(pParent -> pParent -> hwndSite,GWL_EXSTYLE) | WS_EX_CLIENTEDGE);
      else
         SetWindowLongPtr(pParent -> pParent -> hwndSite,GWL_EXSTYLE,GetWindowLongPtr(pParent -> pParent -> hwndSite,GWL_EXSTYLE) & ~WS_EX_CLIENTEDGE);
   }

   return rc;
   }
 
 
   STDMETHODIMP pkAPI::_IOleObject::_IPersistStreamInit::Save(IStream *pIStream,BOOL clearDirty) {

   ULONG cbWritten = pParent -> devicePropertiesSize;
   pIStream -> Write(pParent -> pDeviceProperties,cbWritten,&cbWritten);

   cbWritten = pParent -> runtimePropertiesSize;
   return pIStream -> Write(pParent -> pRuntimeProperties,cbWritten,&cbWritten);
   }


   STDMETHODIMP pkAPI::_IOleObject::_IPersistStreamInit::GetSizeMax(ULARGE_INTEGER *pb) {
   pb -> QuadPart = pParent -> devicePropertiesSize + pParent -> runtimePropertiesSize;
   return S_OK;
   }

 
   STDMETHODIMP pkAPI::_IOleObject::_IPersistStreamInit::InitNew() {
   memset(pParent -> pDeviceProperties,0,pParent -> devicePropertiesSize);
   memset(pParent -> pRuntimeProperties,0,pParent -> runtimePropertiesSize);
   pParent -> renderSignature = true;
   pParent -> renderBackground = true;
   pParent -> renderControls = true;
   pParent -> runTransparent = false;
   return S_OK;
   }