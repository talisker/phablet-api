// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   long __stdcall pkAPI::_IOleObject::_IOleInPlaceActiveObject::QueryInterface(REFIID riid,void **ppv) {
 
   if ( IID_IOleInPlaceActiveObject == riid )
      *ppv = static_cast<IOleInPlaceActiveObject *>(this); 
   else
      return pParent -> QueryInterface(riid,ppv);

   AddRef();
  
   return S_OK; 
   }
   unsigned long __stdcall pkAPI::_IOleObject::_IOleInPlaceActiveObject::AddRef() {
   return pParent -> AddRef();
   }
   unsigned long __stdcall pkAPI::_IOleObject::_IOleInPlaceActiveObject::Release() {
   return pParent -> Release();
   }

   STDMETHODIMP pkAPI::_IOleObject::_IOleInPlaceActiveObject::GetWindow(HWND *pHwnd) {
   *pHwnd = pParent -> pParent -> hwndSite;
   return S_OK;
   }

   STDMETHODIMP pkAPI::_IOleObject::_IOleInPlaceActiveObject::ContextSensitiveHelp(BOOL) {
   return E_NOTIMPL;
   }

   STDMETHODIMP pkAPI::_IOleObject::_IOleInPlaceActiveObject::TranslateAccelerator(MSG *pMsg) {

   if ( ! pParent -> pParent -> hwndSite )
      return S_OK;

   HWND hwndFocus = GetFocus();

   if ( NULL == hwndFocus )
      return S_OK;

   for ( HWND hwndControl : pParent -> runtimeControls ) 
      if ( hwndControl == hwndFocus ) 
         return S_FALSE;

   return S_OK;

   //
   //NTC: 10-28-2017: I attempted (briefly) to use "per keystroke events" to the phablet device, but it's
   // an overcomplication. Instead, by returning S_FALSE for a control that has focus, I will receive the appropriate
   // EN_CHANGE and EN_CHANGED events through WM_COMMAND in the site handler.
   //
#if 0
   long controlId = (long)GetWindowLongPtr(hwndFocus,GWLP_ID) - IDDI_ACTIVEX_RUNTIME_COMMAND_BASE;

   char szCommand[128];

   memset(szCommand,0,sizeof(szCommand));

   sprintf(szCommand,"%ld %ld %ld",TAKEKEYSTROKE,controlId,pMsg -> wParam);

   pParent -> pParent -> sendCommand(szCommand);

   return S_FALSE;
#endif
   }

   STDMETHODIMP pkAPI::_IOleObject::_IOleInPlaceActiveObject::OnFrameWindowActivate(BOOL) {
   return S_OK;   
   }

   STDMETHODIMP pkAPI::_IOleObject::_IOleInPlaceActiveObject::OnDocWindowActivate(BOOL) { 
   return S_OK;   
   }

   STDMETHODIMP pkAPI::_IOleObject::_IOleInPlaceActiveObject::ResizeBorder(LPCRECT,IOleInPlaceUIWindow *,BOOL) {   
   return S_OK;   
   }

   STDMETHODIMP pkAPI::_IOleObject::_IOleInPlaceActiveObject::EnableModeless(BOOL) {   
   return S_OK;   
   }