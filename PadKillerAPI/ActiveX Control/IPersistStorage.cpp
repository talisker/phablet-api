// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   long __stdcall pkAPI::_IOleObject::_IPersistStorage::QueryInterface(REFIID riid,void **ppv) {

   *ppv = NULL;

   if ( IID_IPersistStorage == riid )
      *ppv = static_cast<IPersistStorage *>(this);
   else

      return pParent -> QueryInterface(riid,ppv);

   AddRef();

   return S_OK;
   }
   unsigned long __stdcall pkAPI::_IOleObject::_IPersistStorage::AddRef() {
   return pParent -> AddRef();
   }
   unsigned long __stdcall pkAPI::_IOleObject::_IPersistStorage::Release() {
   return pParent -> Release();
   }

   STDMETHODIMP pkAPI::_IOleObject::_IPersistStorage::GetClassID(CLSID *pcid) {
   memcpy(pcid,&CLSID_PhabletSignaturePad,sizeof(GUID));
   return S_OK;
   }
 
 
   STDMETHODIMP pkAPI::_IOleObject::_IPersistStorage::IsDirty() {
   return S_OK;
   }
 
 
   STDMETHODIMP pkAPI::_IOleObject::_IPersistStorage::InitNew(IStorage *pIStorage) {
   memset(pParent -> pDeviceProperties,0,pParent -> devicePropertiesSize);
   memset(pParent -> pRuntimeProperties,0,pParent -> runtimePropertiesSize);
   pParent -> renderSignature = true;
   pParent -> runTransparent = false;
   return S_OK;
   }
 
 
   STDMETHODIMP pkAPI::_IOleObject::_IPersistStorage::Load(IStorage *pIStorage) {

   IStream *pIStream;

   HRESULT hr = pIStorage -> OpenStream(L"PhabletSignaturePad",0,STGM_READ | STGM_SHARE_EXCLUSIVE,0,&pIStream);

   if ( S_OK != hr )
      return E_FAIL;

   ULONG cbRead = pParent -> devicePropertiesSize;
   hr = pIStream -> Read((BYTE *)&pParent -> pDeviceProperties,cbRead,&cbRead);

   cbRead = pParent -> runtimePropertiesSize;
   hr = pIStream -> Read((BYTE *)&pParent -> pRuntimeProperties,cbRead,&cbRead);

   pIStream -> Release();

   if ( pParent -> pParent -> hwndSite ) {
      if ( pParent -> renderSunkenBorder )
         SetWindowLongPtr(pParent -> pParent -> hwndSite,GWL_EXSTYLE,GetWindowLongPtr(pParent -> pParent -> hwndSite,GWL_EXSTYLE) | WS_EX_CLIENTEDGE);
      else
         SetWindowLongPtr(pParent -> pParent -> hwndSite,GWL_EXSTYLE,GetWindowLongPtr(pParent -> pParent -> hwndSite,GWL_EXSTYLE) & ~WS_EX_CLIENTEDGE);
   }

   return S_OK;
   }
 
 
   STDMETHODIMP pkAPI::_IOleObject::_IPersistStorage::Save(IStorage *pIStorage,BOOL sameAsLoad) {

   IStream *pIStream;

   HRESULT hr = pIStorage -> DestroyElement(L"PhabletSignaturePad");

   hr = pIStorage -> CreateStream(L"PhabletSignaturePad",STGM_READWRITE | STGM_SHARE_EXCLUSIVE | STGM_CREATE,0,0,&pIStream);

   noScribble = true;

   ULONG cbWritten = pParent -> devicePropertiesSize;
   hr = pIStream -> Write((BYTE *)&pParent -> pDeviceProperties,cbWritten,&cbWritten);

   cbWritten = pParent -> runtimePropertiesSize;
   hr = pIStream -> Write((BYTE *)&pParent -> pRuntimeProperties,cbWritten,&cbWritten);

   pIStream -> Release();

   return S_OK;
   }
 
 
   STDMETHODIMP pkAPI::_IOleObject::_IPersistStorage::SaveCompleted(IStorage *) {
   noScribble = false;
   return S_OK;
   }
 
 
   STDMETHODIMP pkAPI::_IOleObject::_IPersistStorage::HandsOffStorage() {
   return S_OK;
   }
