// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

#define LOAD_CONTROLS                                                         \
{                                                                             \
PUT_BOOL(p -> pIOleObject -> renderSignature,IDDI_ACTIVEX_RENDER_SIGNATURE)   \
PUT_BOOL(p -> pIOleObject -> runTransparent,IDDI_ACTIVEX_TRANSPARENT)         \
PUT_BOOL(p -> pIOleObject -> renderBackground,IDDI_ACTIVEX_RENDER_BACKGROUND) \
PUT_BOOL(p -> pIOleObject -> renderControls,IDDI_ACTIVEX_RENDER_CONTROLS)     \
PUT_BOOL(p -> pIOleObject -> renderSunkenBorder,IDDI_ACTIVEX_SUNKEN_BORDER)   \
}

#define UNLOAD_CONTROLS                                                       \
{                                                                             \
GET_BOOL(p -> pIOleObject -> renderSignature,IDDI_ACTIVEX_RENDER_SIGNATURE)   \
GET_BOOL(p -> pIOleObject -> runTransparent,IDDI_ACTIVEX_TRANSPARENT)         \
GET_BOOL(p -> pIOleObject -> renderBackground,IDDI_ACTIVEX_RENDER_BACKGROUND) \
GET_BOOL(p -> pIOleObject -> renderControls,IDDI_ACTIVEX_RENDER_CONTROLS)     \
GET_BOOL(p -> pIOleObject -> renderSunkenBorder,IDDI_ACTIVEX_SUNKEN_BORDER)   \
}


   LRESULT CALLBACK pkAPI::runtimeSettingsHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   pkAPI *p = (pkAPI *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   switch ( msg ) {

   case WM_INITDIALOG: {
      PROPSHEETPAGE *pPage = reinterpret_cast<PROPSHEETPAGE *>(lParam);
      p = (pkAPI *)pPage -> lParam;
      SetWindowLongPtr(hwnd,GWLP_USERDATA,(ULONG_PTR)p);
      LOAD_CONTROLS;
      }
      return LRESULT(FALSE);

   case WM_COMMAND: {

      switch ( LOWORD(wParam) ) {

      case IDDI_PAD_PROPERTIES_ACCEPT:
         UNLOAD_CONTROLS;
         break;

      default:
         break;
      }

      }
      break;


   case WM_NOTIFY: {

      NMHDR *pNotifyHeader = (NMHDR *)lParam;

      switch ( pNotifyHeader -> code ) {

      case PSN_SETACTIVE: {
         LOAD_CONTROLS
         }
         break;

      case PSN_KILLACTIVE:
         SetWindowLongPtr(pNotifyHeader -> hwndFrom,DWLP_MSGRESULT,FALSE);
         break;

      case PSN_APPLY: {

         PSHNOTIFY *pNotify = (PSHNOTIFY *)lParam;

         UNLOAD_CONTROLS;

         SetWindowLongPtr(pNotifyHeader -> hwndFrom,DWLP_MSGRESULT,PSNRET_NOERROR);

         }
         break;

      case PSN_RESET: {
         if ( p -> pIGProperties ) {
            p -> pIGProperties -> Pop();
            p -> pIGProperties -> Pop();
         }
         }
         break;

      case TTN_NEEDTEXT: {
         NMTTDISPINFO *pNotify = (NMTTDISPINFO *)lParam;
         LONG_PTR k = 0L;
         if ( pNotify -> uFlags & TTF_IDISHWND )
            k = SendMessage((HWND)pNotify -> hdr.idFrom,TBM_GETPOS,0L,0L);
         else
            k = SendDlgItemMessage(hwnd,(int)pNotify -> hdr.idFrom,TBM_GETPOS,0L,0L);
         }
         return (LRESULT)0;

      default:
         break;
      }

      }
      break;

   default:
      break;
   }

   return LRESULT(FALSE);
   }
