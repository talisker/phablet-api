// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "pkAPI.h"

   void pkAPI::optionSelected(long optionNumber ) {
   if ( pIOleObject && hwndSite )
      PostMessage(hwndSite,WM_OPTION_EVENT,(WPARAM)optionNumber,0L);
   else {
      if ( pICursiVisionSignaturePad )
         pICursiVisionSignaturePad -> pIConnectionPointContainer -> fire_OptionSelected(optionNumber);
      else
         connectionPointContainer.fire_OptionSelected(optionNumber);
   }
   return;
   }


   void pkAPI::optionUnSelected(long optionNumber ) {

   if ( pIOleObject && hwndSite )
      PostMessage(hwndSite,WM_OPTION_UNSELECTED_EVENT,(WPARAM)optionNumber,0L);
   else
      connectionPointContainer.fire_OptionUnSelected(optionNumber);
  
   return;
   }


   void pkAPI::penUp(long x,long y) {
   lastSignatureX = 0;
   lastSignatureY = 0;
   if ( pIVisioLoggerSignaturePad )
      pIVisioLoggerSignaturePad -> pIConnectionPointContainer -> fire_PenUp(x,y);
   else {
      if ( pICursiVisionSignaturePad )
         pICursiVisionSignaturePad -> pIConnectionPointContainer -> fire_PenUp(x,y);
      else {
         if ( pIOleObject && hwndSite )
            PostMessage(hwndSite,WM_PEN_UP_EVENT,(WPARAM)x,(LPARAM)y);
         else
            connectionPointContainer.fire_PenUp(x,y);
      }
   }
   return;
   }

   void pkAPI::penDown(long x,long y) {
   lastSignatureX = 0;
   lastSignatureY = 0;
   if ( pIVisioLoggerSignaturePad )
      pIVisioLoggerSignaturePad -> pIConnectionPointContainer -> fire_PenDown(x,y);
   else {
      if ( pICursiVisionSignaturePad )
         pICursiVisionSignaturePad -> pIConnectionPointContainer -> fire_PenDown(x,y);
      else {
         if ( pIOleObject && hwndSite )
            PostMessage(hwndSite,WM_PEN_DOWN_EVENT,(WPARAM)x,(LPARAM)y);
         else
            connectionPointContainer.fire_PenDown(x,y);
      }
   }
   return;
   }

   void pkAPI::penPoint(long x,long y,float inkWeight) {

   if ( pIVisioLoggerSignaturePad )
      pIVisioLoggerSignaturePad -> pIConnectionPointContainer -> fire_PenPoint(x,y,inkWeight);
   else {
      if ( pICursiVisionSignaturePad )
         pICursiVisionSignaturePad -> pIConnectionPointContainer -> fire_PenPoint(x,y,inkWeight);
      else {
         if ( pIOleObject && hwndSite ) {
            PostMessage(hwndSite,WM_PEN_POINT_EVENT,MAKEWPARAM(x,y),(LPARAM)(long)(inkWeight * 10000.0));
         } else
            connectionPointContainer.fire_PenPoint(x,y,inkWeight);
      }
   }

   if ( 0 == lastSignatureX ) {
      lastSignatureX = x;
      lastSignatureY = y;
      return;
   }

   if ( pGDIPlusGraphic ) {
      if ( ! ( 0.0 == inkWeight ) ) 
         pGDIPlusPen -> SetWidth(inkWeight);
      pGDIPlusGraphic -> DrawLine(pGDIPlusPen,lastSignatureX,lastSignatureY,x,y);
   }

   lastSignatureX = x;
   lastSignatureY = y;

   return;
   }


   void pkAPI::deviceReady() {

   if ( pIOleObject && pIOleObject -> renderBackground && hwndSite ) {

      BSTR bstrFile;

      if ( S_OK == get_NativeSizeImageFile(&bstrFile) ) {

         Gdiplus::Bitmap *pBitmap = new Gdiplus::Bitmap(bstrFile);
      
         if ( hBitmapBackground )
            DeleteObject(hBitmapBackground);
      
         pBitmap -> GetHBITMAP(NULL,&hBitmapBackground);
      
         cxBitmapBackground = pBitmap -> GetWidth();
         cyBitmapBackground = pBitmap -> GetHeight();
      
         delete pBitmap;
      
         InvalidateRect(hwndSite,NULL,TRUE);
      
         DeleteFileW(bstrFile);
      
      }

      SysFreeString(bstrFile);

   }

   if ( pIOleObject && hwndSite )
      PostMessage(hwndSite,WM_DEVICE_READY_EVENT,0L,0L);
   else
      connectionPointContainer.fire_DeviceReady();

   return;
   }


   void pkAPI::configurationChanged() {

   if ( pIOleObject && ( hwndSite || hwndClientProvidedSite ) ) {
      if ( pIOleObject -> renderSignature )
         setupGDIPlus(hwndSite ? hwndSite : hwndClientProvidedSite);
      PostMessage(hwndSite,WM_CONFIGURATION_CHANGED_EVENT,0L,0L);
   } else
      connectionPointContainer.fire_ConfigurationChanged();

   return;
   }


   void pkAPI::itemSelected(long controlId,WCHAR *pszwItem) {

   if ( ! pszwItem )
      return;

   if ( ! pszwItem[0] )
      return;

   if ( pIOleObject && hwndSite ) {

      stringConsumptionIndex++;
      if ( maxStringConsumptionIndex < stringConsumptionIndex )
         stringConsumptionIndex = 0L;

      wcscpy(szwStringsForConsumption[stringConsumptionIndex],pszwItem);

      PostMessage(hwndSite,WM_ITEM_SELECTED_EVENT,(WPARAM)controlId,(LPARAM)stringConsumptionIndex);

   } else {

      BSTR bstrItem = SysAllocString(pszwItem);

      connectionPointContainer.fire_ItemSelected(controlId,bstrItem);

      SysFreeString(bstrItem);
   }

   return;
   }


   void pkAPI::textChanged(long controlId,WCHAR *pszwOldText,WCHAR *pszwNewText) {

   if ( pIOleObject && hwndSite ) {
      long index1,index2;

      stringConsumptionIndex++;
      if ( maxStringConsumptionIndex < stringConsumptionIndex )
         stringConsumptionIndex = 0L;

      wcscpy(szwStringsForConsumption[stringConsumptionIndex],pszwOldText);

      index1 = stringConsumptionIndex;

      stringConsumptionIndex++;
      if ( maxStringConsumptionIndex < stringConsumptionIndex )
         stringConsumptionIndex = 0L;

      wcscpy(szwStringsForConsumption[stringConsumptionIndex],pszwNewText);

      index2 = stringConsumptionIndex;

      stringConsumptionIndex++;
      if ( maxStringConsumptionIndex < stringConsumptionIndex )
         stringConsumptionIndex = 0L;

      swprintf_s(szwStringsForConsumption[stringConsumptionIndex],MAX_STRING_SIZE,L"%ld %ld",index1,index2);

      PostMessage(hwndSite,WM_TEXT_CHANGED_EVENT,(WPARAM)controlId,(LPARAM)stringConsumptionIndex);

   } else {

      //NTC: 10-27-2017: The if condition here handles the Phablet Signature Pad as a terminal on Android, that is, 
      // deploying windows UI components on Android and communicating back to windows.
      // I have not decided if either the CursiVision signature pad, or the VisioLogger pad use case needs this
      // event.


   }

   delete [] pszwOldText;
   delete [] pszwNewText;

   return;
   }