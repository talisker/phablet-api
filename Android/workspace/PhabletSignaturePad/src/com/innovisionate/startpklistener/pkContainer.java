package com.innovisionate.startpklistener;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

public class pkContainer extends ViewGroup {

   private Context theContext;
   
   public pkContainer(Context context) {
   super(context);
   theContext = context;
   return;
   }

   @Override
   protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
   
//   signaturePad sp = (signaturePad)getChildAt(0);
   signaturePanel sp = (signaturePanel)getChildAt(0);

   sp.layout(left,top,right,bottom);

   sp.setVisibility(VISIBLE);
   
   for ( int k = 1; k < getChildCount(); k++ ) {
      pkButton b = (pkButton)getChildAt(k);
      b.layout(b.x, b.y, b.x + b.width,b.y + b.height);
   }
   
   return;
   }
   
   @Override
   public boolean shouldDelayChildPressedState() {
   return false;
   }
   
   @Override
   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
   final DisplayMetrics displayMetrics = new DisplayMetrics();
   ((WindowManager)theContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);   
   setMeasuredDimension(displayMetrics.widthPixels,displayMetrics.heightPixels);
   return;
   }
   
}
