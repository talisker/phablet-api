package com.innovisionate.startpklistener;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

@SuppressLint("WrongCall")
public class signaturePad extends SurfaceView implements SurfaceHolder.Callback {

   public Paint paint = new Paint();
   public int backgroundColor = Color.WHITE;
   private Bitmap theBitmap = null;
   private boolean isInitialBitmap = true;
   private boolean isValid = false;
   private Rect rect = null;
   private Path path = null;
   public int lastX = 0;
   public int lastY = 0;
   private boolean isHidden = false;
   
   public signaturePad(Context context) {
   super(context);
   getHolder().addCallback(this);
   setFocusable(true);
   paint.setColor(Color.BLACK);
   paint.setStyle(Paint.Style.STROKE);
   paint.setStrokeWidth(2.0f);
   path = new Path();
   return;
   }
   
   public boolean IsValid() {
   return isValid;
   }
   
   public boolean IsHidden() {
   return isHidden;
   }
   
   public Bitmap TheBitmap() {
   return theBitmap;
   }
   
   public void TheBitmap(Bitmap v) {
   theBitmap = v;
   if ( ! ( null == theBitmap ) ) 
      rect = new Rect(0,0,theBitmap.getWidth(),theBitmap.getHeight());
   isInitialBitmap = false;
   return;
   }
      
   public Path ThePath() {
   return path;
   }
   
   public Paint ThePaint() {
   return paint;
   }
   
   public int Width() {
   return getWidth();
   }
   
   public int Height() {
   return getHeight();
   }

   public void Hide() {
   isHidden = true;
   PhabletSignaturePad.theHandler.post(PhabletSignaturePad.thePainter);
   return;
   }
   
   public void Show() {
   isHidden = false;
   PhabletSignaturePad.theHandler.post(PhabletSignaturePad.thePainter);
   return;
   }
   
   @Override
   public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
   rect = new Rect(0,0,width,height);
   return;
   }   

   @Override
   public void surfaceCreated(SurfaceHolder holder) {
   isValid = true;
   Canvas c = getHolder().lockCanvas();
   onDraw(c);
   getHolder().unlockCanvasAndPost(c);
   return;
   }

   @Override
   public void surfaceDestroyed(SurfaceHolder holder) {
   isValid = false;
   return;
   }

   public void clearPath() {
   path = new Path();
   lastX = 0;
   lastY = 0;
   return;
   }
   
   
   @Override
   public void onDraw(Canvas canvas) {
   super.onDraw(canvas);
   if ( ! ( null == theBitmap ) )
      canvas.drawBitmap(theBitmap,null,rect,null);
   else
      canvas.drawColor(backgroundColor);
   canvas.drawPath(path, paint);
   return;
   }

   public void updateSignature(int x,int y) {
   if ( 0 == lastX )
      path.moveTo(x, y);
   else if ( ! ( 0 == x ) )
      path.lineTo(x, y);
   lastX = x;
   lastY = y;
   if ( IsValid() ) {
      Canvas c = getHolder().lockCanvas();
      onDraw(c);
      getHolder().unlockCanvasAndPost(c);
   }
   return;
   }
   
   @Override
   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
   final DisplayMetrics displayMetrics = new DisplayMetrics();
   ((WindowManager)PhabletSignaturePad.theMainActivity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);   
   setMeasuredDimension(displayMetrics.widthPixels,displayMetrics.heightPixels);
   return;
   }
   
}
