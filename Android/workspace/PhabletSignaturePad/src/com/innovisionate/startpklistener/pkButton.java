package com.innovisionate.startpklistener;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.TypedValue;
import android.widget.Button;

public class pkButton extends Button {

   public int optionId = 0;
   public int x,y,width,height;
   
   pkButton(Context ct,String theText,int oid,int px,int py,int visibility) {

   super(ct);

   optionId = oid;
   
   x = (int)(PhabletSignaturePad.scaleObjectsX * (double)px);
   
   y = (int)(PhabletSignaturePad.scaleObjectsY * (double)py);
   
   Paint paint = getPaint();

   Rect textSize = new Rect();

   float v = 2.0f * TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PT,PhabletSignaturePad.properties.fontSize,PhabletSignaturePad.theMainActivity.getBaseContext().getResources().getDisplayMetrics());
   
   paint.setTypeface(Typeface.create(PhabletSignaturePad.properties.fontFamily,Typeface.NORMAL));

   paint.setTextSize(v);
   
   paint.getTextBounds(theText,0,theText.length(),textSize);
   
   width = (int)((double)(textSize.right - textSize.left) / 0.5);

   height = (int)((double)(textSize.bottom - textSize.top) / 0.5);

   setIncludeFontPadding(false);
   
   setText(theText);

   if ( 0 == visibility )
      setVisibility(INVISIBLE);
   
   return;
   }
   
}
