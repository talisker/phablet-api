package com.innovisionate.startpklistener;

import java.net.InetAddress;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings.Secure;
import android.support.v4.util.ArrayMap;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.ServerManagedPolicy;

public class PhabletSignaturePad extends Activity {

   private static final String BASE64_PUBLIC_KEY = 
         "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAi06FXPK/odmH5w338sAJ13s" +
         "0WBQwneETfMRMHurdYULibcbx8oFGgowQrqG0/5x9taIKKYN2xZAoeI3hS1+mHvTy8F" +
         "9voNqQtgJuWMJ2kBVn7ej2zmsZraOGaDi3GdkhFfUqUpJ5i8XMMP0r8QabzeqF0bSyQ" +
         "DnbDU9FkHXj1zt0p8s1ngOY9fqnDXq1T5zbHHvBcE0Q87kZX5O3l/Yq22sVr5E8dFJL" +
         "bxHOPtpNENgZv9osQzztOfPC99ZXfU1IzEgBm9SX6xF8hYFXR+TvkgDh+EmIdTnTd35" +
         "sCGEeXBCMf/CtnoFDAMFK656YB+9REf7gvvq9T0RqwbX+iV4utwIDAQAB";

   private static final byte[] SALT = new byte[] {
      -46, 65, 30, -128, -103, -57, 74, -64, 51, 88, -95, -45, 77, -117, -36, -113, -11, 32, -64, 89 };
   
   private static final int EVENT_PENDOWN = 1;
   private static final int EVENT_PENUP = 2;
   private static final int EVENT_PENPOINT = 3;
   private static final int EVENT_OPTIONSELECTED = 4;
   public static final int EVENT_DEVICEREADY = 5;
   public static final int EVENT_CONFIGURATIONCHANGED = 6;

   public static PhabletSignaturePad theMainActivity = null;
   public static pkContainer theContainer = null;

   public static signaturePanel theSignaturePadView = null;

   public static Bitmap theSignaturePadBitmap = null;
   public static String[] controlDetails = null;
   public static String toastMessage = null;
   public static String myIPAddress = null;
   
   public static Handler theHandler = new Handler();
   
   public static Runnable theBitmapRunnable = new Runnable() { public void run() { setViewBitmap(); } };

   public static Runnable theStartPenRunnable = new Runnable() { public void run() { startPenEvents(); } };
   public static Runnable theStopPenRunnable = new Runnable() { public void run() { stopPenEvents(); } };

   public static Runnable theStartEventDispatcherRunnable = new Runnable() { public void run() { startEventDispatcher(); } };
   public static Runnable theStopEventDispatcherRunnable = new Runnable() { public void run() { stopEventDispatcher(); } };

   public static Runnable theStartControlListenerRunnable = new Runnable() { public void run() { startControlListener(); } };
   public static Runnable theStopControlListenerRunnable = new Runnable() { public void run() { stopControlListener(); } };

   public static Runnable theCreateControlRunnable = new Runnable() { public void run() { createButton(); } };
   public static Runnable theRemoveControlRunnable = new Runnable() { public void run() { removeControl(); } };
   
   public static Runnable theCycleRunnable = new Runnable() { public void run() { cycle(); } } ;
   
   public static Runnable thePainter = new Runnable() { public void run() { repaint(); } };
   public static Runnable theRefreshControls = new Runnable() { public void run() { refreshControls(); } };
   public static Runnable theToastRunnable = new Runnable() { public void run() { doToast(); } };
   public static Runnable theFinishRunnable = new Runnable() { public void run() { theMainActivity.finish(); } };

   public static CountDownLatch actionComplete = new CountDownLatch(1);
   
   public static InetAddress clientAddress = null;
   
   public static LinkedBlockingQueue<String> eventsQueue = null;
   
   public static double scaleObjectsX = 1.0;
   public static double scaleObjectsY = 1.0;
   
   public static final int ASSUME_CLIENT_DISCONNECTED_DURATION = 2000;
   
   public class properties {
   public int inkColor;
   public float inkWeight = 2.0f;
   public int showInk = 1;
   public int sendPoints = 1;
   public String fontFamily = "Arial";
   public float fontSize = 12.0f;
   public String backgroundBitmapFileName;
   public ArrayMap<Integer,String> buttonCreationStrings;
   public ArrayMap<Integer,String> controlMotionStrings;
   public int orientation;
   };
   
   public static SharedPreferences preferences = null;
   
   public static properties properties = null;
   
   @Override
   protected void onCreate(Bundle savedInstanceState) {
   
   super.onCreate(savedInstanceState);
   
   preferences = getPreferences(MODE_PRIVATE);
   
   requestWindowFeature(Window.FEATURE_NO_TITLE);
   
   getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

   //initiateLicenseCheck();
   
   theMainActivity = this;

   properties = new properties();
   
   theContainer = new pkContainer(theMainActivity);
   
   theSignaturePadView = new signaturePanel(theMainActivity);
   
   theContainer.addView(theSignaturePadView,0);

   theMainActivity.setContentView(theContainer);

   RestorePreferences();
   
   cycle();

   return;
   }
   
   @Override
   protected void onStop(){
   super.onStop();
   SavePreferences();
   return;
   }
   
   @Override
   public void onConfigurationChanged(Configuration newConfiguration) {

   super.onConfigurationChanged(newConfiguration);

   final DisplayMetrics displayMetrics = new DisplayMetrics();
   ((WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);   

   theSignaturePadView.setBottom(displayMetrics.heightPixels);
   theSignaturePadView.setRight(displayMetrics.widthPixels);

   for ( int k = 1; k < theContainer.getChildCount(); k++ ) {
      pkButton b = (pkButton)theContainer.getChildAt(k);
      b.x = displayMetrics.widthPixels * b.x / displayMetrics.heightPixels;
      b.y = displayMetrics.widthPixels * b.y / displayMetrics.heightPixels;      
   }

   theContainer.layout(0, 0, displayMetrics.heightPixels, displayMetrics.widthPixels);

   theHandler.post(thePainter);
   
   sendEvent(String.valueOf(EVENT_CONFIGURATIONCHANGED));
   
   return;
   }
   
   @Override
   protected void onDestroy() {
   super.onDestroy();
   if ( isFinishing() )
      stopService(new Intent(getBaseContext(),com.innovisionate.startpklistener.pkListener.class));
   return;
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
   getMenuInflater().inflate(R.menu.main, menu);
   return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
   switch (item.getItemId()) {
   case R.id.action_exit:
      finish();
      return true;
   case R.id.action_information:
      startActivity(new Intent(this,Information.class));
      return true;
   case R.id.action_settings:
      startActivity(new Intent(this,Settings.class));
      return true;
   default:
      return super.onOptionsItemSelected(item);
   }
   }   
   
   public static void SavePreferences() {
   
   SharedPreferences.Editor editor = PhabletSignaturePad.preferences.edit();
   
   editor.putInt("ink color", properties.inkColor);
   editor.putFloat("ink weight", properties.inkWeight);
   editor.putInt("show ink", properties.showInk);
   editor.putInt("send points",properties.sendPoints);
   editor.putString("font family",properties.fontFamily);
   editor.putFloat("font size", properties.fontSize);
   editor.putString("background bitmap file name", PhabletSignaturePad.properties.backgroundBitmapFileName);
   
   editor.putInt("orientation",theMainActivity.getRequestedOrientation());

 //
 //NTC: 7-27-2014: For the time being, buttons are not recreated on the pad as if they are in non-volatile memory. There is no
 // event mechanism to do anything with the buttons.
 //
/*
   if ( ! ( null == properties.buttonCreationStrings ) ) {
      String s = "";
      for ( Entry<Integer,String> cb : properties.buttonCreationStrings.entrySet() )
         s += cb.getValue() + ";";
      editor.putString("button creation strings", s);
   } else
*/
      editor.putString("button creation strings",null);

/*
   if ( ! ( null == properties.controlMotionStrings ) ) {
      String s = "";
      for ( Entry<Integer,String> cb : properties.controlMotionStrings.entrySet() )
         s += cb.getValue() + ";";
      editor.putString("control motion strings", s);
   } else
*/
      editor.putString("control motion strings",null);

   editor.commit();
   
   return;
   }
   
   public static void RestorePreferences() {
   
   properties.inkColor = preferences.getInt("ink color", Color.BLACK);
   properties.inkWeight = preferences.getFloat("ink weight", 2.0f);
   properties.showInk = preferences.getInt("show ink", 1);
   properties.sendPoints = preferences.getInt("send points",1);
   properties.fontFamily = preferences.getString("font family","Arial");
   properties.fontSize = preferences.getFloat("font size", 12.0f);
   properties.backgroundBitmapFileName = preferences.getString("background bitmap file name", null);
   
   properties.orientation = preferences.getInt("orientation",ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

//   theMainActivity.setRequestedOrientation(properties.orientation);
   if ( ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE == properties.orientation ) {
   
   }
   
   if ( ! ( null == properties.backgroundBitmapFileName ) )
      theSignaturePadBitmap = BitmapFactory.decodeFile(properties.backgroundBitmapFileName,null);
   else
      theSignaturePadBitmap = null;
   
   theHandler.post(PhabletSignaturePad.theBitmapRunnable);

   controlDetails = new String[] { "0", "-1"};
   
   if ( Looper.getMainLooper().getThread() == Thread.currentThread() )
      removeControl();
   else {
      actionComplete = new CountDownLatch(1);
      theHandler.post(PhabletSignaturePad.theRemoveControlRunnable);
      try {
      actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }
   }
   
   properties.buttonCreationStrings = new ArrayMap<Integer,String>();
   String s = preferences.getString("button creation strings", null);
   if ( ! ( null == s ) && 0 < s.length() ) {
      String cbStrings[] = s.split(";");
      for ( int k = 0; k < cbStrings.length; k++ ) {
         controlDetails = cbStrings[k].split(" ");
         properties.buttonCreationStrings.put(Integer.valueOf(controlDetails[3]),cbStrings[k]);
         if ( Looper.getMainLooper().getThread() == Thread.currentThread() ) {
            createButton();
         } else {
            PhabletSignaturePad.actionComplete = new CountDownLatch(1);
            PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theCreateControlRunnable);
            try {
            PhabletSignaturePad.actionComplete.await();
            } catch ( InterruptedException e ) {
               e.printStackTrace();
            }
         }
      }
   }
   
   properties.controlMotionStrings = new ArrayMap<Integer,String>();
   
   String cms = preferences.getString("control motion strings", null);
   
   if ( ! ( null == cms ) && 0 < cms.length() ) {
   
      String cbStrings[] = cms.split(";");
      
      for ( int k = 0; k < cbStrings.length; k++ ) {

         controlDetails = cbStrings[k].split(" ");
         
         properties.controlMotionStrings.put(Integer.valueOf(controlDetails[3]),cbStrings[k]);

         int eventId = Integer.valueOf(controlDetails[2]);

         for ( int j = 1; j < PhabletSignaturePad.theContainer.getChildCount(); j++ ) {
         
            pkButton b = (pkButton)PhabletSignaturePad.theContainer.getChildAt(j);
            
            if ( b.optionId == eventId ) {
               b.x = Integer.valueOf(controlDetails[3]);
               b.y = Integer.valueOf(controlDetails[4]);
               b.width = Integer.valueOf(controlDetails[5]) - b.x;
               b.height = Integer.valueOf(controlDetails[6]) - b.y;
               PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theRefreshControls);
               break;
            }
         } 
      }
   }
   
   return;
   }
   
   public static void ResetPreferences() {
   SharedPreferences.Editor editor = PhabletSignaturePad.preferences.edit();
   editor.clear();
   editor.commit();
   RestorePreferences();
   return;
   }
   
   private static void setViewBitmap() {
   theSignaturePadView.TheBitmap(theSignaturePadBitmap);
   theSignaturePadView.clearPath();
   theSignaturePadView.invalidate();
   return;
   }

   private static void createButton() {
   
   pkButton button = new pkButton(theMainActivity,controlDetails[5],Integer.valueOf(controlDetails[3]),
                                    Integer.valueOf(controlDetails[1]),Integer.valueOf(controlDetails[2]),Integer.valueOf(controlDetails[4]));

   theContainer.addView(button,theContainer.getChildCount());
   
   button.bringToFront();
   
   button.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
         sendEvent(String.valueOf(EVENT_OPTIONSELECTED) + " " + ((pkButton)v).optionId);
         return;
      }
      });
   
   actionComplete.countDown();
   
   repaint();

   return;
   }
   
   private static void removeControl() {
   if ( -1 == Integer.valueOf(controlDetails[1]) ) {
      int n = theContainer.getChildCount() - 1;
      for ( int k = n; k > 0; k-- )
         theContainer.removeViewAt(k);
      actionComplete.countDown();
      repaint();
      return;
   }
   for ( int k = 1; k < theContainer.getChildCount(); k++ ) {
      pkButton b = (pkButton)theContainer.getChildAt(k);
      if ( b.optionId == Integer.valueOf(controlDetails[1]) ) {
         theContainer.removeViewAt(k);
         actionComplete.countDown();
         repaint();
         return;
      }
   }
   actionComplete.countDown();
   return;
   }
   
   private static void cycle() {
   theMainActivity.stopService(new Intent(theMainActivity.getBaseContext(),com.innovisionate.startpklistener.pkListener.class));
   theMainActivity.startService(new Intent(theMainActivity.getBaseContext(),com.innovisionate.startpklistener.pkListener.class));
   return;
   }
   
   private static void startEventDispatcher() {
   eventsQueue = new LinkedBlockingQueue<String>();
   theMainActivity.startService(new Intent(theMainActivity.getBaseContext(),com.innovisionate.startpklistener.pkEventsDispatcher.class));
   return;
   }
   
   private static void stopEventDispatcher() {
   theMainActivity.stopService(new Intent(theMainActivity.getBaseContext(),com.innovisionate.startpklistener.pkEventsDispatcher.class));
   return;
   }

   private static void startControlListener() {
   theMainActivity.startService(new Intent(theMainActivity.getBaseContext(),com.innovisionate.startpklistener.pkControlListener.class));
   WindowManager.LayoutParams layoutParams = theMainActivity.getWindow().getAttributes();
   layoutParams.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
   theMainActivity.getWindow().setAttributes(layoutParams);
   return;
   }
   
   private static void stopControlListener() {
   theMainActivity.stopService(new Intent(theMainActivity.getBaseContext(),com.innovisionate.startpklistener.pkControlListener.class));
   return;
   }
   
   private static void startPenEvents() {
   
   theSignaturePadView.setOnTouchListener(new OnTouchListener() {
   
      @Override
      public boolean onTouch(View v, android.view.MotionEvent event) {
         
         if ( android.view.MotionEvent.ACTION_DOWN == event.getAction() ) {
            sendEvent(EVENT_PENDOWN + " " + event.getX() + " " + event.getY());
            return true;
         }
         if ( android.view.MotionEvent.ACTION_UP == event.getAction() ) {
            sendEvent(EVENT_PENUP + " " + event.getX() + " " + event.getY());
            theSignaturePadView.lastX = 0;
            theSignaturePadView.lastY = 0;
            return true;
         }
         if ( android.view.MotionEvent.ACTION_MOVE == event.getAction() ) {
            int historySize = event.getHistorySize();
            for ( int k = 0; k < historySize; k++ )
               sendEvent("" + EVENT_PENPOINT + " " + (int)event.getHistoricalX(0,k) + " " + (int)event.getHistoricalY(0,k));
            sendEvent("" + EVENT_PENPOINT + " " + (int)event.getX() + " " + (int)event.getY());
            theSignaturePadView.updateSignature((int)event.getX(),(int)event.getY());
            return true;
         }
         
         return true;
      }
      });   

   actionComplete.countDown();   

   return;
   }

   private static void stopPenEvents() {
   theSignaturePadView.setOnTouchListener(null);
   eventsQueue.clear();
   actionComplete.countDown();
   return;
   }
 
   public static void sendEvent(String event) {
   if ( null == eventsQueue )
      return;
   try {
   //System.out.println("event: " + event);
   eventsQueue.put(event);
   } catch ( InterruptedException e ) {
      e.printStackTrace();
      //stopPenEvents();
   }
   return;
   }

   private static void doToast() {
   Toast.makeText(theMainActivity,toastMessage,Toast.LENGTH_LONG).show();
   return;
   }
   
   private static void repaint() {
   theSignaturePadView.invalidate();
   for ( int k = 1; k < theContainer.getChildCount(); k++ ) 
      theContainer.getChildAt(k).invalidate();
   return;
   }

   private static void refreshControls() {
   theSignaturePadView.invalidate();
   for ( int k = 1; k < theContainer.getChildCount(); k++ ) {
      pkButton b = (pkButton)theContainer.getChildAt(k);
      theContainer.removeViewAt(k);
      theContainer.addView(b,k);
      b.bringToFront();
      b.invalidate();
   }   
   return;
   }

   private void initiateLicenseCheck() {
   
   String deviceId = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
   String fms1 = "InnoVisioNateRocks14";
   String fms2 = "VisioLogger Is Great";

   int j = 0;
   for ( int k = fms1.length() - 1; k > 0; k--, j++ )
      SALT[fms1.length() - k] = (byte) ((byte)fms1.charAt(k) | (byte)fms2.charAt(j));
   
   licenseCheckerCallback licenseCheckerCallback = new licenseCheckerCallback();
   
   LicenseChecker licenseChecker = new LicenseChecker(this, new ServerManagedPolicy(this,new AESObfuscator(SALT, getPackageName(), deviceId)),BASE64_PUBLIC_KEY);

   licenseChecker.checkAccess(licenseCheckerCallback);   
   
   return;
   }
   
   private class licenseCheckerCallback implements LicenseCheckerCallback {

   public void allow(int policyReason) {
   if ( isFinishing() )
      return;
   toastMessage = "You are sufficiently licensed to use the PhabletSignaturePad device";
   theHandler.post(theToastRunnable);
   return;
   }

   public void dontAllow(int policyReason) {
   if ( isFinishing() )
      return;
   toastMessage = "You are not licensed to use the PhabletSignaturePad device";
   theHandler.post(theToastRunnable);
   return;
   }

   public void applicationError(int errorCode) {
   if ( isFinishing() )
      return;
   toastMessage = "There was an error (" + String.valueOf(errorCode) + ") while checking your license for PhabletSignaturePad";
   theHandler.post(theToastRunnable);
   return;
   }
   
   }   
}
