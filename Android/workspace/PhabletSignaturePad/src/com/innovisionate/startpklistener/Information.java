package com.innovisionate.startpklistener;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

public class Information extends Activity {

   @Override
   protected void onCreate(Bundle savedInstanceState) {
   super.onCreate(savedInstanceState);
   setContentView(R.layout.information_main);
   WebView theView = (WebView)findViewById(R.id.informationView);
   theView.setVerticalScrollBarEnabled(true);
   theView.getSettings().setJavaScriptEnabled(true);   
   theView.loadUrl("file:///android_asset/documentation/index.htm");
   return;
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
   getMenuInflater().inflate(R.menu.information, menu);
   return true;
   }
   
   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
   switch (item.getItemId()) {
   case R.id.action_exit:
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theFinishRunnable);
      finish();
      return true;
   case R.id.action_signature_capture:
      Intent intent = new Intent(this,PhabletSignaturePad.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(intent);
      finish();
      return true;
   case R.id.action_settings:
      startActivity(new Intent(this,Settings.class));
      finish();
      return true;
   default:
      return super.onOptionsItemSelected(item);
   }
   } 
   
   @Override
   protected void onDestroy() {
   super.onDestroy();
   return;
   }   

}