package com.innovisionate.startpklistener;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Timer;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class pkControlListener extends Service {

   private ServerSocket serverSocket = null;
   private Socket clientSocket = null;
   private boolean shutdownRequested = false;
   private ScheduledThreadPoolExecutor killTimerExecutor = null;   
   private ScheduledFuture killTimerAction = null;
   
   @Override
   public IBinder onBind(Intent intent) {
   return null;
   }

   @Override
   public int onStartCommand(Intent intent,int flags,int startId) {
   new Thread(new Runnable() { public void run() { serviceLoop(); } }).start();
   System.out.println("The Control Listener service has started");
   return START_NOT_STICKY;
   }

   @Override
   public void onDestroy() {
   super.onDestroy();
   shutdownRequested = true;
   try {
   if ( ! ( null == clientSocket ) ) 
      clientSocket.close();
   if ( ! ( null == serverSocket ) )
      serverSocket.close();
   clientSocket = null;
   serverSocket = null;
   } catch ( IOException e ) {
      e.printStackTrace();
   }
   System.out.println("The Control Listener service has stopped. 1");
   return;
   }
   
   private void abandonClient() {
   System.out.println("The client is abandoned");
// MainActivity.theHandler.post(MainActivity.theCycleRunnable);   
//   MainActivity.theHandler.post(MainActivity.theFinishRunnable);   
   return;
   }
   
   public void serviceLoop() {
   
   killTimerExecutor = new ScheduledThreadPoolExecutor(1);
   
   killTimerAction = killTimerExecutor.schedule(new Runnable() { public  void run() { abandonClient(); }}, PhabletSignaturePad.ASSUME_CLIENT_DISCONNECTED_DURATION,TimeUnit.MILLISECONDS);
   
   try {

   serverSocket = new ServerSocket(17643);

   } catch ( IOException e ) {
      System.out.println("The pkControlListener failed while trying to make the server socket.");
      serverSocket = null;
      e.printStackTrace(); 
      if ( ! ( null == PhabletSignaturePad.actionComplete ) ) 
         PhabletSignaturePad.actionComplete.countDown();
      return;
   }

   shutdownRequested = false;
      
   while ( ! shutdownRequested ) {
      
      DataInputStream dataInputStream = null;

      DataOutputStream dataOutputStream = null;
   
      System.out.println("Waiting on " + PhabletSignaturePad.myIPAddress + " for a control connection to port 17643");
      
      if ( ! ( null == PhabletSignaturePad.actionComplete ) ) 
         PhabletSignaturePad.actionComplete.countDown();

      try {
      
      clientSocket = serverSocket.accept();

      } catch ( IOException e ) {
//         e.printStackTrace();
         break;
      }

      killTimerAction.cancel(false);
      
      killTimerAction = killTimerExecutor.schedule(new Runnable() { public  void run() { abandonClient(); }}, PhabletSignaturePad.ASSUME_CLIENT_DISCONNECTED_DURATION,TimeUnit.MILLISECONDS);
      
      try {

      dataInputStream = new DataInputStream(clientSocket.getInputStream());
      
      dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());

      dataOutputStream.writeBytes("heartbeats: " + String.valueOf(PhabletSignaturePad.ASSUME_CLIENT_DISCONNECTED_DURATION));
      
      } catch ( IOException e ) {
         System.out.println("The pkControlListener failed while writing the initial string to the connection.");
         e.printStackTrace();
         break;
      }

      byte[] inBuffer = new byte[512];
      
      while ( true ) {

         System.out.println("Waiting for the next control command");
      
         try {
   
         dataInputStream.readFully(inBuffer,0,4);

         killTimerAction.cancel(false);
         
         killTimerAction = killTimerExecutor.schedule(new Runnable() { public  void run() { abandonClient(); }}, PhabletSignaturePad.ASSUME_CLIENT_DISCONNECTED_DURATION,TimeUnit.MILLISECONDS);
         
         } catch ( Exception e ) {
//            e.printStackTrace();
            break;
         }
   
         int bytesToRead = 0;
         
         try {
         
         bytesToRead = Integer.valueOf(new String(inBuffer,0,4));
         
         } catch ( NumberFormatException e ) {
   
            try {
   
            dataOutputStream.write(new String("Invalid size value:" + new String(inBuffer,0,4)).getBytes());
   
            } catch ( IOException e1 ) {
               e1.printStackTrace();
               break;
            }
            
            continue;
            
         }

         try {
         
         dataInputStream.readFully(inBuffer,0,bytesToRead);

         } catch ( IOException e ) {
            e.printStackTrace();
            break;
         }

         String inputString = new String(inBuffer,0,bytesToRead);

         System.out.println("control command: " + inputString );

         try {

         dataOutputStream.writeBytes("ok");

         } catch (IOException e) {
            e.printStackTrace();
            break;
         }

      }
      
      killTimerAction.cancel(false);
      
      try {

      if ( ! ( null == clientSocket ) ) 
         clientSocket.close();

      clientSocket = null;

      } catch (IOException e) {
         e.printStackTrace();
      }
      
      if ( shutdownRequested )
         break;
      
   }
   
   killTimerAction.cancel(false);
   
   try {

   if ( ! ( null == clientSocket ) ) 
      clientSocket.close();
   
   if ( ! ( null == serverSocket ) )
      serverSocket.close();
   
   } catch (IOException e) {
      e.printStackTrace();
   }

   clientSocket = null;
   serverSocket = null;

   if ( ! ( null == PhabletSignaturePad.actionComplete ) )
      PhabletSignaturePad.actionComplete.countDown();
   
   System.out.println("The Control Listener has stopped. 2");
   
   return;
   }
   
}
