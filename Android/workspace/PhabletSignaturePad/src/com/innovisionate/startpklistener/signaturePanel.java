package com.innovisionate.startpklistener;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.widget.ImageView;

public class signaturePanel extends ImageView {

   public Paint paint = new Paint();
   public int backgroundColor = Color.WHITE;
   private Bitmap theBitmap = null;
   private boolean isHidden = false;   
   private Rect rectSource = null;
   private Path path = null;
   private Rect myRect = null;
   public int lastX = 0;
   public int lastY = 0;
   
   public signaturePanel(Context context) {
   super(context);
   paint.setColor(Color.BLACK);
   paint.setStyle(Paint.Style.STROKE);
   paint.setStrokeWidth(2.0f);
   path = new Path();
   return;
   }
   
   @Override
   public void onDraw(Canvas canvas) {
   super.onDraw(canvas);
   if ( ! ( null == theBitmap ) )
      canvas.drawBitmap(theBitmap,rectSource,myRect,null);
   else
      canvas.drawColor(backgroundColor);
   canvas.drawPath(path, paint);
   return;
   }
   
   @Override
   protected void onSizeChanged(int w,int h,int oldWidth,int oldHeight) {
   myRect = new Rect(0,0,w,h);
   return;
   }
   
   public Bitmap TheBitmap() {
   return theBitmap;
   }
   
   public void TheBitmap(Bitmap v) {
   theBitmap = v;
   if ( ! ( null == theBitmap ) ) 
      rectSource = new Rect(0,0,theBitmap.getWidth(),theBitmap.getHeight());
   //isInitialBitmap = false;
   return;
   }

   public Rect RectSource() {
   return rectSource;
   }
   
   public Path ThePath() {
   return path;
   }

   public Paint ThePaint() {
   return paint;
   }
   
   public int Width() {
   return getWidth();
   }
   
   public int Height() {
   return getHeight();
   }
   

   public void Hide() {
   isHidden = true;
   PhabletSignaturePad.theHandler.post(PhabletSignaturePad.thePainter);
   return;
   }
   
   public void Show() {
   isHidden = false;
   PhabletSignaturePad.theHandler.post(PhabletSignaturePad.thePainter);
   return;
   }
      
   public void clearPath() {
   path = new Path();
   lastX = 0;
   lastY = 0;
   PhabletSignaturePad.theHandler.post(PhabletSignaturePad.thePainter);
   return;
   }
   

   public void updateSignature(int x,int y) {
   if ( 0 == lastX )
      path.moveTo(x, y);
   else if ( ! ( 0 == x ) )
      path.lineTo(x, y);
   lastX = x;
   lastY = y;
   invalidate();
/*
   if ( IsValid() ) {
      Canvas c = getHolder().lockCanvas();
      onDraw(c);
      getHolder().unlockCanvasAndPost(c);
   }
*/
   return;
   }
}
