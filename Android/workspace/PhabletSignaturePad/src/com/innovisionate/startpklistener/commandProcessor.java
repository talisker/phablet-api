package com.innovisionate.startpklistener;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.util.concurrent.CountDownLatch;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public class commandProcessor {

   private static final int RECEIVEIMAGE = 1;
   private static final int START = 2;
   private static final int STOP = 3;
   private static final int CLEARHOTSPOTS = 4;
   private static final int HOTSPOT = 5;
   private static final int CLEAREVERYTHING = 6;
   private static final int CREATEBUTTON = 7; 
   private static final int POSITION = 8;
   private static final int RESIZE = 9;
   private static final int QUERYPARAMETER = 10;
   private static final int HIDE = 11;
   private static final int SHOW = 12;
   private static final int DESTROYBUTTON = 13;
   private static final int SETPARAMETER = 14;
   private static final int BACKGROUND = 15;
   private static final int CLEARINK = 16;
   private static final int CLEARSETTINGS = 17;
   
   private static final int PARM_BOUNDS = 1;
   private static final int PARM_CLIENT_ADDRESS = 2;
   private static final int PARM_INK_COLOR = 3;
   private static final int PARM_INK_WEIGHT = 4;
   private static final int PARM_SHOW_INK = 5;
   private static final int PARM_SEND_POINTS = 6;
   private static final int PARM_WIDTH = 7;
   private static final int PARM_HEIGHT = 8;
   private static final int PARM_IMAGE = 9;
   private static final int PARM_FONT_FAMILY = 10;
   private static final int PARM_FONT_SIZE = 11;
   private static final int PARM_CONTROL_BOUNDS = 12;

   private int imageLocationX,imageLocationY,imageWidth,imageHeight;

   private DataInputStream inputStream = null;
   private DataOutputStream outputStream = null;
   
   private Context theContext = null;

   private String rawCommand = null;
   
   public commandProcessor(Context c,DataInputStream is,DataOutputStream os) {
   theContext = c;
   inputStream = is;
   outputStream = os;
   return;
   }
   
   public void process(String command) throws IOException {
   
   rawCommand = command;
   
   String[] values = command.split(" ");
   
   int theCommand = -1;
   
   try {

   theCommand = Integer.valueOf(values[0]);

   } catch ( Exception e ) {
      outputStream.writeBytes(command + " is invalid");
      return;
   }

   String response = null;
   
   switch ( theCommand ) {
   
   case BACKGROUND:
   case RECEIVEIMAGE:
      response = recieveImage(theCommand,values);
      break;

   default:
      response = processCommand(theCommand,values);
      break;
   }

   if ( ! ( null == response ) ) 
      outputStream.writeBytes(response);

   PhabletSignaturePad.SavePreferences();
   
   return;
   }
  
   
   private String processCommand(int theCommand,String [] values) {

   switch ( theCommand ) {
   
   case START:
      startPen();
      break;

   case STOP:
      stopPen();
      break;

   case HIDE:
      PhabletSignaturePad.theSignaturePadView.Hide();
      break;
      
   case SHOW:
      PhabletSignaturePad.theSignaturePadView.Show();
      break;
      
   case CLEAREVERYTHING:
      PhabletSignaturePad.theSignaturePadBitmap = null;
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theBitmapRunnable);
      PhabletSignaturePad.controlDetails = new String[] {"-1","-1"};
      PhabletSignaturePad.actionComplete = new CountDownLatch(1);
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theRemoveControlRunnable);
      try {
      PhabletSignaturePad.actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }
      break;

   case CREATEBUTTON:
   
      int key = Integer.valueOf(values[3]);

      if ( PhabletSignaturePad.properties.buttonCreationStrings.containsKey(key) ) 
         PhabletSignaturePad.properties.buttonCreationStrings.remove(key);
      
      PhabletSignaturePad.properties.buttonCreationStrings.put(key,rawCommand);
      
      PhabletSignaturePad.controlDetails = values;

      PhabletSignaturePad.controlDetails[1] = values[3];
      
      PhabletSignaturePad.actionComplete = new CountDownLatch(1);
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theRemoveControlRunnable);
      try {
      PhabletSignaturePad.actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }

      PhabletSignaturePad.controlDetails = values;
      
      PhabletSignaturePad.actionComplete = new CountDownLatch(1);
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theCreateControlRunnable);
      try {
      PhabletSignaturePad.actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }
      
      break;

   case DESTROYBUTTON:
   
      int key2 = Integer.valueOf(values[1]);
      
      if ( PhabletSignaturePad.properties.buttonCreationStrings.containsKey(key2) ) 
         PhabletSignaturePad.properties.buttonCreationStrings.remove(key2);   
      
      PhabletSignaturePad.controlDetails = values;
      PhabletSignaturePad.actionComplete = new CountDownLatch(1);
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theRemoveControlRunnable);
      try {
      PhabletSignaturePad.actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }
      System.out.println("control destroyed");
      break;
   
   case SETPARAMETER: 
   
      switch ( Integer.valueOf(values[1]) ) {

      case PARM_BOUNDS:
/*
         MainActivity.theSignaturePadView.setAdjustViewBounds(true);
         MainActivity.theSignaturePadView.setX((float)Integer.valueOf(values[2]));
         MainActivity.theSignaturePadView.setY((float)Integer.valueOf(values[3]));
         MainActivity.theSignaturePadView.setMaxWidth(Integer.valueOf(values[4]));
         MainActivity.theSignaturePadView.setMaxHeight(Integer.valueOf(values[5]));
*/
         break;

      case PARM_CLIENT_ADDRESS:
         //MainActivity.clientAddress = new InetAddress(values[2]);
         //MainActivity.clientAddress.
         break;
         
      case PARM_INK_COLOR:
         PhabletSignaturePad.properties.inkColor = Color.rgb(Integer.valueOf(values[2]),Integer.valueOf(values[3]),Integer.valueOf(values[4]));
         PhabletSignaturePad.theSignaturePadView.paint.setColor(PhabletSignaturePad.properties.inkColor);
         break;
         
      case PARM_INK_WEIGHT:
         PhabletSignaturePad.properties.inkWeight = Float.valueOf(values[2]);
         PhabletSignaturePad.theSignaturePadView.paint.setStrokeWidth(PhabletSignaturePad.properties.inkWeight);
         break;
         
      case PARM_SHOW_INK:
         PhabletSignaturePad.properties.showInk = Integer.valueOf(values[2]);
         break;

      case PARM_SEND_POINTS:
         PhabletSignaturePad.properties.sendPoints = Integer.valueOf(values[2]);
         break;
         
      case PARM_FONT_FAMILY:
         PhabletSignaturePad.properties.fontFamily = values[2];
         break;
         
      case PARM_FONT_SIZE:
         PhabletSignaturePad.properties.fontSize = (float)Float.valueOf(values[2]);
         break;

      case PARM_CONTROL_BOUNDS:

         int eventId = Integer.valueOf(values[2]);

         if ( PhabletSignaturePad.properties.controlMotionStrings.containsKey(eventId) ) 
            PhabletSignaturePad.properties.controlMotionStrings.remove(eventId);

         PhabletSignaturePad.properties.controlMotionStrings.put(eventId, rawCommand);
         
         for ( int k = 1; k < PhabletSignaturePad.theContainer.getChildCount(); k++ ) {
            pkButton b = (pkButton)PhabletSignaturePad.theContainer.getChildAt(k);
            if ( b.optionId == eventId ) {
               b.x = Integer.valueOf(values[3]);
               b.y = Integer.valueOf(values[4]);
               b.width = Integer.valueOf(values[5]) - b.x;
               b.height = Integer.valueOf(values[6]) - b.y;
               PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theRefreshControls);
               break;
            }
         }
         break;
         
      default:
         return "unknown parameter to set";

      }

      break;
      
   case QUERYPARAMETER: 
   
      switch ( Integer.valueOf(values[1]) ) {
      
      case PARM_WIDTH:
      case PARM_HEIGHT:
      case PARM_BOUNDS:
         final DisplayMetrics displayMetrics = new DisplayMetrics();
         ((WindowManager)theContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
         int cx = displayMetrics.widthPixels - (int)PhabletSignaturePad.theContainer.getX();
         int cy = displayMetrics.heightPixels - (int)PhabletSignaturePad.theContainer.getY();
         if ( PARM_WIDTH == Integer.valueOf(values[1]) )
            return String.valueOf(cx);
         else if ( PARM_HEIGHT == Integer.valueOf(values[1]) )
            return String.valueOf(cy);
         return (new StringBuilder(" 0 0 ").append(cx).append(" ").append(cy)).toString();

      case PARM_CLIENT_ADDRESS:
         return PhabletSignaturePad.clientAddress.toString();

      case PARM_INK_COLOR:
         return String.valueOf(PhabletSignaturePad.properties.inkColor);
      
      case PARM_INK_WEIGHT:
         return String.valueOf(PhabletSignaturePad.properties.inkWeight);
         
      case PARM_SHOW_INK:
         return String.valueOf(PhabletSignaturePad.properties.showInk);

      case PARM_SEND_POINTS:
         return String.valueOf(PhabletSignaturePad.properties.sendPoints);
         
      case PARM_IMAGE:
         return sendImage();
         
      case PARM_FONT_FAMILY:
         return PhabletSignaturePad.properties.fontFamily;
         
      case PARM_FONT_SIZE:
         return String.valueOf(PhabletSignaturePad.properties.fontSize);

      case PARM_CONTROL_BOUNDS:
         int eventId = Integer.valueOf(values[2]);
         for ( int k = 1; k < PhabletSignaturePad.theContainer.getChildCount(); k++ ) {
            pkButton b = (pkButton)PhabletSignaturePad.theContainer.getChildAt(k);
            if ( b.optionId == eventId ) 
               return String.valueOf(b.x) + " " + String.valueOf(b.y) + " " + String.valueOf(b.x + b.width) + " " + String.valueOf(b.y + b.height);
         }
         return "The control with id = " + String.valueOf(eventId) + " is not found";
         
      default: 
         return "unknown parameter to query";

      }

   case CLEARINK:
      PhabletSignaturePad.theSignaturePadView.clearPath();
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.thePainter);
      break;
            
   case CLEARSETTINGS:
      PhabletSignaturePad.ResetPreferences();
      break;
      
   default:
      return values[0] + " is an unknown command";
   }
   
   return "ok";
   }


   public void startPen() {
   PhabletSignaturePad.actionComplete = new CountDownLatch(1);
   PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theStartPenRunnable);
   try {
   PhabletSignaturePad.actionComplete.await();
   } catch ( InterruptedException e ) {
      e.printStackTrace();
   }
   return;
   }

   
   public void stopPen() {
   PhabletSignaturePad.actionComplete = new CountDownLatch(1);
   PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theStopPenRunnable);
   try {
   PhabletSignaturePad.actionComplete.await();
   } catch ( InterruptedException e ) {
      e.printStackTrace();
   }
   return;
   }

   
   private String recieveImage(int theCommand, String[] values) throws IOException {
   
   if ( 2 > values.length )
      return "the image command is invalid";
   
   int expectedFileSize = Integer.valueOf(values[1]);

   if ( RECEIVEIMAGE == theCommand ) {
      imageLocationX = Integer.valueOf(values[2]);
      imageLocationY = Integer.valueOf(values[3]);
      imageWidth = Integer.valueOf(values[4]);
      imageHeight = Integer.valueOf(values[5]);
   } else {
      imageLocationX = -1;
      imageLocationY = -1;
      imageWidth = -1;
      imageHeight = -1;
   }

   StringBuilder response = new StringBuilder("send ").append(expectedFileSize).append(" bytes");
   
   try {
   
   outputStream.writeBytes(response.toString());
   
   int totalBytes = 0;
   
   byte[] imageBytes = new byte[512];

   PhabletSignaturePad.properties.backgroundBitmapFileName = PhabletSignaturePad.theMainActivity.getFilesDir().getAbsolutePath() + "/backgroundBitmapFile";   

   File imageFile = new File(PhabletSignaturePad.properties.backgroundBitmapFileName);    
   
   FileOutputStream imageStream = new FileOutputStream(imageFile);
   
   while ( totalBytes < expectedFileSize ) {
      try {
      int bytesRead = inputStream.read(imageBytes);
      imageStream.write(imageBytes,0,bytesRead);
      totalBytes += bytesRead;
      } catch ( IOException e ) {
         imageStream.close();
         throw new IOException(e);
      }
   }
   
   imageStream.close();
   
   if ( totalBytes < expectedFileSize ) {
      throw new IOException("Insufficient image bytes sent");
   }

   BitmapFactory.Options options = new BitmapFactory.Options();

   options.inJustDecodeBounds = true;
   
   BitmapFactory.decodeFile(imageFile.getAbsolutePath(),options);
   
   final DisplayMetrics displayMetrics = new DisplayMetrics();
   ((WindowManager)theContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);

   PhabletSignaturePad.scaleObjectsX = (double)displayMetrics.widthPixels / (double)options.outWidth;
   PhabletSignaturePad.scaleObjectsY = (double)displayMetrics.heightPixels / (double)options.outHeight;
   
   options.inJustDecodeBounds = false;
   
   options.inSampleSize = calculateInSampleSize(options,displayMetrics.widthPixels,displayMetrics.heightPixels);

   options.outWidth = displayMetrics.widthPixels;
   options.outHeight = displayMetrics.heightPixels; 

   PhabletSignaturePad.theSignaturePadBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(),options);
   
   PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theBitmapRunnable);

   response = new StringBuilder().append(totalBytes).append(" bytes recieved ");

   } catch ( IOException e ) {
      throw new IOException("IO Probem saving sent bitmap");
   }
   
   return response.toString();
   }

   
   private String sendImage() {
   
   Bitmap theBitmap = Bitmap.createBitmap(PhabletSignaturePad.theSignaturePadView.getWidth(),PhabletSignaturePad.theSignaturePadView.getHeight(),Bitmap.Config.ARGB_8888); 
   
   Canvas canvas = new Canvas(theBitmap);

   PhabletSignaturePad.theSignaturePadView.draw(canvas);
   
   ByteArrayOutputStream bytes = new ByteArrayOutputStream();

   theBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
   
   try {

   outputStream.writeBytes("recieve " + bytes.size() + " bytes");
   
   byte[] imageBytes = bytes.toByteArray();
   
   byte[] response = new byte[3];
   
   inputStream.readFully(response, 0, 2);

   int totalBytes = 0;

   while ( totalBytes + 512 < bytes.size() ) {
      outputStream.write(imageBytes,totalBytes,512);
      totalBytes += 512;
      inputStream.readFully(response,0,2);
   }
   
   if ( totalBytes < bytes.size() ) {
      outputStream.write(imageBytes,totalBytes,bytes.size() - totalBytes);
      inputStream.readFully(response,0,2);
   }
   
   inputStream.readFully(response,0,2);
   
   } catch (IOException e) {
      e.printStackTrace();
   }
   
   return null;
   }
   
   public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

   final int height = options.outHeight;
   final int width = options.outWidth;

   if ( height < reqHeight && width < reqWidth ) 
      return 1;

   int inSampleSize = 1;
   final int halfHeight = height / 2;
   final int halfWidth = width / 2;

// Calculate the largest inSampleSize value that is a power of 2 and keeps both
// height and width larger than the requested height and width.

   while ( (halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) 
      inSampleSize *= 2;

   return inSampleSize;
   }   

 
}
