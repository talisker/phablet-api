package com.innovisionate.startpklistener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class Settings extends Activity {

   @Override
   protected void onCreate(Bundle savedInstanceState) {
   super.onCreate(savedInstanceState);
   setContentView(R.layout.settings_main);
   ((TextView)findViewById(R.id.ipAddress)).setText(PhabletSignaturePad.myIPAddress);
   ((TextView)findViewById(R.id.widthValue)).setText(String.valueOf(PhabletSignaturePad.theSignaturePadView.Width()));
   ((TextView)findViewById(R.id.heightValue)).setText(String.valueOf(PhabletSignaturePad.theSignaturePadView.Height()));
   return;
   }
   
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
   getMenuInflater().inflate(R.menu.settings, menu);
   return true;
   }
   
   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
   switch (item.getItemId()) {
   case R.id.action_exit:
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theFinishRunnable);
      finish();
      return true;
   case R.id.action_signature_capture:
      Intent intent = new Intent(this,PhabletSignaturePad.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(intent);
      finish();
      return true;
   case R.id.action_information:
      startActivity(new Intent(this,Information.class));
      finish();
      return true;
   default:
      return super.onOptionsItemSelected(item);
   }
   }   
   
}
