/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.android.vending.licensing;

import com.google.android.vending.licensing.util.Base64;
import com.google.android.vending.licensing.util.Base64DecoderException;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.Settings.Secure;
import android.util.Log;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * Client library for Android Market license verifications.
 * <p>
 * The LicenseChecker is configured via a {@link Policy} which contains the
 * logic to determine whether a user should have access to the application. For
 * example, the Policy can define a threshold for allowable number of server or
 * client failures before the library reports the user as not having access.
 * <p>
 * Must also provide the Base64-encoded RSA public key associated with your
 * developer account. The public key is obtainable from the publisher site.
 */
public class LicenseChecker implements ServiceConnection {

   private static final String TAG = "LicenseChecker";

   private static final String KEY_FACTORY_ALGORITHM = "RSA";

   // Timeout value (in milliseconds) for calls to service.
    
   private static final int TIMEOUT_MS = 10 * 1000;

   private static final SecureRandom RANDOM = new SecureRandom();
    
   private static final boolean DEBUG_LICENSE_ERROR = true;

   private ILicensingService mService;

   private PublicKey mPublicKey;
   private final Context mContext;
   private final Policy mPolicy;

   private Handler mHandler;
   private final String mPackageName;
   private final String mVersionCode;
   private final Set<LicenseValidator> mChecksInProgress = new HashSet<LicenseValidator>();
   private final Queue<LicenseValidator> mPendingChecks = new LinkedList<LicenseValidator>();

    /**
     * @param context a Context
     * @param policy implementation of Policy
     * @param encodedPublicKey Base64-encoded RSA public key
     * @throws IllegalArgumentException if encodedPublicKey is invalid
     */
   
   public LicenseChecker(Context context, Policy policy, String encodedPublicKey) {
   mContext = context;
   mPolicy = policy;
   mPublicKey = generatePublicKey(encodedPublicKey);
   mPackageName = mContext.getPackageName();
   mVersionCode = getVersionCode(context, mPackageName);
   HandlerThread handlerThread = new HandlerThread("background thread");
   handlerThread.start();
   mHandler = new Handler(handlerThread.getLooper());
   return;
   }

    /**
     * Generates a PublicKey instance from a string containing the
     * Base64-encoded public key.
     * 
     * @param encodedPublicKey Base64-encoded public key
     * @throws IllegalArgumentException if encodedPublicKey is invalid
     */
   private static PublicKey generatePublicKey(String encodedPublicKey) {
   
   try {
  
   return KeyFactory.getInstance(KEY_FACTORY_ALGORITHM).generatePublic(new X509EncodedKeySpec(Base64.decode(encodedPublicKey)));
  
   } catch ( NoSuchAlgorithmException e ) {
   
      throw new RuntimeException(e);

   } catch ( Base64DecoderException e ) {

      Log.e(TAG, "Could not decode from Base64.");
      throw new IllegalArgumentException(e);

   } catch (InvalidKeySpecException e) {
      
      Log.e(TAG, "Invalid key specification.");
      throw new IllegalArgumentException(e);
   }

   }

   public synchronized void checkAccess(LicenseCheckerCallback callback) {

   if ( mPolicy.allowAccess() ) {
      Log.i(TAG, "Using cached license response");
      callback.allow(Policy.LICENSED);
      return;
   } 
    
   LicenseValidator validator = new LicenseValidator(mPolicy,new NullDeviceLimiter(),callback,RANDOM.nextInt(),mPackageName,mVersionCode);

   if ( ! ( null == mService ) ) {
      mPendingChecks.offer(validator);
      runChecks();
      return;
   }
    
   Log.i(TAG, "Binding to licensing service.");

   try {

//   if ( mContext.bindService(new Intent("com.android.vending.licensing.ILicensingService"),this,Context.BIND_AUTO_CREATE) ) {
    
   if ( mContext.bindService(new Intent(new String(Base64.decode("Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U="))),this,Context.BIND_AUTO_CREATE) )

      mPendingChecks.offer(validator);
       
   else {
      Log.e(TAG, "Could not bind to service.");
      handleServiceConnectionError(validator);
   }

   } catch ( SecurityException e ) {
    
      callback.applicationError(LicenseCheckerCallback.ERROR_MISSING_PERMISSION);

   } catch ( Base64DecoderException e ) {
    
      e.printStackTrace();
       
   }

   return;
   }

   
   private void runChecks() {
    
   LicenseValidator validator;
    
   while ( ! ( null == ( validator = mPendingChecks.poll() ) ) ) {

      try {

      Log.i(TAG, "Calling checkLicense on service for " + validator.getPackageName());
       
      mService.checkLicense(validator.getNonce(),validator.getPackageName(),new ResultListener(validator));
       
      mChecksInProgress.add(validator);
       
      } catch ( RemoteException e ) {
         Log.w(TAG, "RemoteException in checkLicense call.", e);
         handleServiceConnectionError(validator);
      }
       
   }
   
   return;
   }

   private synchronized void finishCheck(LicenseValidator validator) {
   mChecksInProgress.remove(validator);
   if ( mChecksInProgress.isEmpty() )
      cleanupService();
   return;
   }
   

   private class ResultListener extends ILicenseResultListener.Stub {

      private static final int ERROR_CONTACTING_SERVER = 0x101;
      private static final int ERROR_INVALID_PACKAGE_NAME = 0x102;
      private static final int ERROR_NON_MATCHING_UID = 0x103;

      private final LicenseValidator validator;
      private Runnable onTimeout;

      public ResultListener(LicenseValidator pValidator) {
      validator = pValidator;
      onTimeout = new Runnable() { public void run() { Log.i(TAG, "the License Check timed out.");  handleServiceConnectionError(validator); finishCheck(validator); } };
      startTimeout();
      return;
      }

      // Runs in IPC thread pool. Post it to the Handler, so we can guarantee
      // either this or the timeout runs.

      public void verifyLicense(final int responseCode,final String signedData,final String signature) {
      mHandler.post(new Runnable() { public void run() { recievedResponse(responseCode,signedData,signature); } });
      return;
      }

      private void startTimeout() {
      Log.i(TAG, "Start monitoring timeout.");
      mHandler.postDelayed(onTimeout, TIMEOUT_MS);
      return;
      }

      private void clearTimeout() {
      Log.i(TAG, "Clearing timeout.");
      mHandler.removeCallbacks(onTimeout);
      return;
      }
      
      private void recievedResponse(int responseCode,String signedData,String signature) {
        
      Log.i(TAG,"Received response " + String.valueOf(responseCode));
      Log.i(TAG,"Signed Data: " + signedData);
      Log.i(TAG,"Signature: " + signature);
        
      if ( mChecksInProgress.contains(validator) ) {
         clearTimeout();
         validator.verify(mPublicKey,responseCode,signedData,signature);
         finishCheck(validator);
      }
        
      if ( ! DEBUG_LICENSE_ERROR ) 
         return;
      
      String stringError = null;
      
      switch ( responseCode ) {
      
      case ERROR_CONTACTING_SERVER:
         stringError = "ERROR_CONTACTING_SERVER";
         break;
         
      case ERROR_INVALID_PACKAGE_NAME:
         stringError = "ERROR_INVALID_PACKAGE_NAME";
         break;
         
      case ERROR_NON_MATCHING_UID:
         stringError = "ERROR_NON_MATCHING_UID";
         break;
         
      }

      if ( ! ( null == stringError ) ) {
         Log.d(TAG, "Server Failure: " + stringError);
         Log.d(TAG, "Android ID: " + Secure.getString(mContext.getContentResolver(),Secure.ANDROID_ID));
         Log.d(TAG, "Time: " + (new Date()).toGMTString());
      }

      return;
      }        
        
   }

   public synchronized void onServiceConnected(ComponentName name, IBinder service) {
   mService = ILicensingService.Stub.asInterface(service);
   runChecks();
   return;
   }

   public synchronized void onServiceDisconnected(ComponentName name) {
   Log.w(TAG, "Service unexpectedly disconnected.");
   mService = null;
   return;
   }


   private synchronized void handleServiceConnectionError(LicenseValidator validator) {
   mPolicy.processServerResponse(Policy.RETRY, null);
   if ( mPolicy.allowAccess() )
      validator.getCallback().allow(Policy.RETRY);
   else
      validator.getCallback().dontAllow(Policy.RETRY);
   return;
   }

   
   private void cleanupService() {

   if ( null == mService ) 
      return;

   try {
   
   mContext.unbindService(this);
   
   } catch ( IllegalArgumentException e ) {
      Log.e(TAG, "Unable to unbind from licensing service (already unbound)");
   }
   
   mService = null;
   
   return;
   }

    /**
     * Inform the library that the context is about to be destroyed, so that any
     * open connections can be cleaned up.
     * <p>
     * Failure to call this method can result in a crash under certain
     * circumstances, such as during screen rotation if an Activity requests the
     * license check or when the user exits the application.
     */
   public synchronized void onDestroy() {
   cleanupService();
   mHandler.getLooper().quit();
   return;
   }

   private static String getVersionCode(Context context, String packageName) {   
   try {   
   return String.valueOf(context.getPackageManager().getPackageInfo(packageName, 0).versionCode);
   } catch ( NameNotFoundException e ) {
      Log.e(TAG, "Package not found. could not get version code.");
      return "";
   }

   }
}
