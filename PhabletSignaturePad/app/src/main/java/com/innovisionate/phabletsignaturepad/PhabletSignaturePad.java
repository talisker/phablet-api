package com.innovisionate.phabletsignaturepad;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

//import android.support.v7.app.ActionBar;
import android.app.ActionBar;
import android.app.Activity;
//import android.support.v7.app.AppCompatActivity;
import android.app.DownloadManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.provider.Settings.Secure;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.android.vending.billing.IInAppBillingService;

import com.innovisionate.phabletsignaturepad.licensing.AESObfuscator;
import com.innovisionate.phabletsignaturepad.licensing.LicenseChecker;
import com.innovisionate.phabletsignaturepad.licensing.LicenseCheckerCallback;
import com.innovisionate.phabletsignaturepad.licensing.ServerManagedPolicy;

//public class PhabletSignaturePad extends AppCompatActivity {
public class PhabletSignaturePad extends Activity {

   private static final String BASE64_PUBLIC_KEY = 
         "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAi06FXPK/odmH5w338sAJ13s" +
         "0WBQwneETfMRMHurdYULibcbx8oFGgowQrqG0/5x9taIKKYN2xZAoeI3hS1+mHvTy8F" +
         "9voNqQtgJuWMJ2kBVn7ej2zmsZraOGaDi3GdkhFfUqUpJ5i8XMMP0r8QabzeqF0bSyQ" +
         "DnbDU9FkHXj1zt0p8s1ngOY9fqnDXq1T5zbHHvBcE0Q87kZX5O3l/Yq22sVr5E8dFJL" +
         "bxHOPtpNENgZv9osQzztOfPC99ZXfU1IzEgBm9SX6xF8hYFXR+TvkgDh+EmIdTnTd35" +
         "sCGEeXBCMf/CtnoFDAMFK656YB+9REf7gvvq9T0RqwbX+iV4utwIDAQAB";

   private static final byte[] SALT = new byte[] {
      -46, 65, 30, -128, -103, -57, 74, -64, 51, 88, -95, -45, 77, -117, -36, -113, -11, 32, -64, 89 };
   
   public static final int EVENT_PENDOWN = 1;
   public static final int EVENT_PENUP = 2;
   public static final int EVENT_PENPOINT = 3;
   public static final int EVENT_OPTIONSELECTED = 4;
   public static final int EVENT_OPTIONUNSELECTED = 5;
   public static final int EVENT_DEVICEREADY = 6;
   public static final int EVENT_CONFIGURATIONCHANGED = 7;
   public static final int EVENT_ITEMSELECTED = 8;
   public static final int EVENT_TEXTCHANGED = 9;

   public static PhabletSignaturePad theMainActivity = null;
   public static pkContainer theContainer = null;

   public static signaturePanel theSignaturePadView = null;

   public static String [] controlDetails = new String[32];

   public static String toastMessage = null;
   public static String myIPAddress = null;
   
   public static Handler theHandler = new Handler();
   
   public static Runnable theBitmapRunnable = new Runnable() { public void run() { setViewBitmap(); } };
   public static Runnable thePDFRunnable = new Runnable() { public void run() { showPDF(); } };

   public static Runnable theAddBitmapRunnable = new Runnable() { public void run() { addViewBitmap(); } };

   public static Runnable theStartPenRunnable = new Runnable() { public void run() { startPenEvents(); } };
   public static Runnable theStopPenRunnable = new Runnable() { public void run() { stopPenEvents(); } };

   public static Runnable theStartEventDispatcherRunnable = new Runnable() { public void run() { startEventDispatcher(); } };
   public static Runnable theStopEventDispatcherRunnable = new Runnable() { public void run() { stopEventDispatcher(); } };

   public static Runnable theStartControlListenerRunnable = new Runnable() { public void run() { startControlListener(); } };
   public static Runnable theStopControlListenerRunnable = new Runnable() { public void run() { stopControlListener(); } };

   public static Runnable theCreateControlRunnable = new Runnable() { public void run() { createControl(); } };
   public static Runnable theRemoveControlRunnable = new Runnable() { public void run() { removeControl(); } };
   
   public static Runnable theCycleRunnable = new Runnable() { public void run() { cycle(); } } ;
   
   public static Runnable thePainter = new Runnable() { public void run() { repaint(); } };
   public static Runnable theReSizeControl = new Runnable() { public void run() { reSizeControl(); } };
   public static Runnable theRePositionControl = new Runnable() { public void run() { rePositionControl(); } };
   public static Runnable theToastRunnable = new Runnable() { public void run() { doToast(); } };
   public static Runnable theFinishRunnable = new Runnable() { public void run() { theMainActivity.finish(); } };

   public static Runnable theControlVisibilityRunnable = new Runnable() { public void run() { setControlVisibility(); } };
   public static Runnable theControlValueSetter = new Runnable() { public void run() { setControlValue(); } } ;
   public static Runnable theControlStateSetter = new Runnable() { public void run() { setControlState(); } } ;
   //public static Runnable theControlTextSetter = new Runnable() { public void run() { setControlText(); } } ;
   public static Runnable theDispatchKeyEventRunnable = new Runnable() { public void run() { dispatchKeyEvent(); } } ;

   public static Runnable theContainerVisibilityRunnable = new Runnable() { public void run() { setContainerVisibility(); } } ;
   
   public static CountDownLatch actionComplete = new CountDownLatch(1);
   
   public static InetAddress clientAddress = null;
   
   public static LinkedBlockingQueue<String> eventsQueue = null;
   
   public static double scaleObjectsX = 1.0;
   public static double scaleObjectsY = 1.0;
   
   public static int hostUIWidthInPixels = 0;
   public static int hostUIHeightInPixels = 0;

   public static int controlUpdatesShowing = 1;

   private static boolean holdEvents = false;

   public static final int ASSUME_CLIENT_DISCONNECTED_DURATION = 2000;

   private static boolean inAppBillingAvailable = false;

   public static BillingClient billingClient = null;

   public static String cursiVisionPrice = "";
   public static final String cursiVisionSku = /*"android.test.purchased"*/"cursivision" /**/;
   public static final String cursiVisionPurchaseSite = "http://www.cursivision.com/purchase.php";
   public static final String cursiVisionEvaluationSite = "http://www.cursivision.com/evaluate.php";

   public class properties {
      public int inkColor;
      public float inkWeight = 2.0f;
      public int showInk = 1;
      public int sendPoints = 1;
      public String fontFamily = "Arial";
      public float fontSize = 12.0f;
      public String backgroundBitmapFileName;
      public int hideActionBarTimeout = 0;//10000;
      public String pdfFileName;
      public boolean useFullScreen = false;
      public int purchaseCount = 0;
      public String uuid = "";
   /*
      public ArrayMap<Integer,String> buttonCreationStrings;
      public ArrayMap<Integer,String> controlMotionStrings;
   */
      public int orientation;
   };
   
   public static SharedPreferences preferences = null;
   
   public static properties properties = null;
   
   @Override
   protected void onCreate(Bundle savedInstanceState) {
   
      super.onCreate(savedInstanceState);

      getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

      preferences = getPreferences(MODE_PRIVATE);

      /*
      requestWindowFeature(Window.FEATURE_NO_TITLE);
      */

      /*
      initiateLicenseCheck();
      */

      theMainActivity = this;

      properties = new properties();

      theContainer = new pkContainer(theMainActivity);

      theSignaturePadView = new signaturePanel(theMainActivity);

      theContainer.addView(theSignaturePadView,0);

      theMainActivity.setContentView(theContainer);

      RestorePreferences();

      cycle();

      if ( properties.useFullScreen )
         getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

      if ( ! ( 0 == properties.hideActionBarTimeout ) ) {

         new CountDownTimer(properties.hideActionBarTimeout, properties.hideActionBarTimeout) {

            public void onTick(long t) {
               return;
            }

            public void onFinish() {
               if ( 0 == properties.hideActionBarTimeout )
                  return;
               ActionBar actionBar = getActionBar();
               actionBar.hide();
               getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
               return;
            }
         }.start();

      }

      inAppBillingAvailable = false;

      BillingClientStateListener bcListener = new BillingClientStateListener() {

         @Override
         public void onBillingSetupFinished(int responseCode) {

            if ( ! ( BillingClient.BillingResponse.OK == responseCode ) )
               return;

            Purchase.PurchasesResult theResult = PhabletSignaturePad.billingClient.queryPurchases("inapp");

            if ( 0 < theResult.getPurchasesList().size() ) {
               Purchase thePurchase = (Purchase)(theResult.getPurchasesList().get(0));
               billingClient.consumeAsync(thePurchase.getPurchaseToken(), new ConsumeResponseListener() {
                  @Override
                  public void onConsumeResponse(int responseCode, String purchaseToken) {
                     return;
                  }
               });
            }

            List<String> skus = Arrays.asList(cursiVisionSku);

            SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder().setSkusList(skus).setType(BillingClient.SkuType.INAPP);

            billingClient.querySkuDetailsAsync(params.build(),new SkuDetailsResponseListener() {

               @Override
               public void onSkuDetailsResponse(int responseCode, List<SkuDetails> skuDetailsList) {

                  if ( ! ( BillingClient.BillingResponse.OK == responseCode ) )
                     return;

                  cursiVisionPrice = skuDetailsList.get(0).getPrice();

                  return;
               }
            });

            inAppBillingAvailable = true;

            return;
         }

         @Override
         public void onBillingServiceDisconnected() {
            inAppBillingAvailable = true;
            return;
         }
      };

      PurchasesUpdatedListener puListener = new PurchasesUpdatedListener() {

         @Override
         public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {

            if ( ! ( BillingClient.BillingResponse.OK == responseCode ) )
               return;

            Purchase.PurchasesResult theResult = PhabletSignaturePad.billingClient.queryPurchases("inapp");
            final Purchase thePurchase = (Purchase)(theResult.getPurchasesList().get(0));

            Runnable doPurchase = new Runnable() {

               public void run() {

                  try {

                  String data = URLEncoder.encode("purchaseToken", "UTF-8") + "=" + URLEncoder.encode(thePurchase.getPurchaseToken(), "UTF-8");
                  data += "&" + URLEncoder.encode("orderId", "UTF-8") + "=" + URLEncoder.encode(thePurchase.getOrderId(), "UTF-8");
                  data += "&" + URLEncoder.encode("productId", "UTF-8") + "=" + URLEncoder.encode(thePurchase.getSku(), "UTF-8");
                  data += "&" + URLEncoder.encode("signature", "UTF-8") + "=" + URLEncoder.encode(thePurchase.getSignature(), "UTF-8");
                  data += "&" + URLEncoder.encode("uuid","UTF-8") + "=" + URLEncoder.encode(properties.uuid,"UTF-8");

                  URL url = new URL(cursiVisionPurchaseSite);
                  URLConnection conn = url.openConnection();
                  conn.setDoOutput(true);
                  OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                  wr.write(data);
                  wr.flush();

                  BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                  String line;
                  while( (line = rd.readLine()) != null ) {
                     System.out.println(line);
                  }

                  wr.close();
                  rd.close();

                  } catch ( Exception e ) {
                     System.out.println(e.getMessage());
                  }
               }
            };

            new Thread(doPurchase).start();

            properties.purchaseCount += 1;

            SavePreferences();

            Uri theFileUri = Uri.parse("http://www.cursiVision.com/dmdocuments/CursiVisionSetup.exe");

            DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

            DownloadManager.Request request = new DownloadManager.Request(theFileUri);

            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
            request.setAllowedOverRoaming(true);
            //request.setTitle("");
            request.setDescription("Please move this file to your windows computer ");
            request.setVisibleInDownloadsUi(true);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/CursiVisionSetup.exe");

            long refid = downloadManager.enqueue(request);

            PhabletSignaturePad.toastMessage = "The CursiVision Installer is downloading. When it completes, please run this installer on your Windows computer.";

            PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theToastRunnable);

            if ( 0 < theResult.getPurchasesList().size() ) {
               billingClient.consumeAsync(thePurchase.getPurchaseToken(), new ConsumeResponseListener() {
                  @Override
                  public void onConsumeResponse(int responseCode, String purchaseToken) {
                     return;
                  }
               });
            }

            return;
         }
      };

      billingClient = BillingClient.newBuilder(this).setListener(puListener).build();

      billingClient.startConnection(bcListener);

      return;
   }
   
   @Override
   protected void onStop(){
      super.onStop();
      SavePreferences();
   return;
   }
   
   @Override
   public void onConfigurationChanged(Configuration newConfiguration) {

      super.onConfigurationChanged(newConfiguration);

      final DisplayMetrics displayMetrics = new DisplayMetrics();

      ((WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);

      theSignaturePadView.setBottom(displayMetrics.heightPixels);
      theSignaturePadView.setRight(displayMetrics.widthPixels);

      for ( int k = 1; k < theContainer.getChildCount(); k++ ) {
         pkUIObject b = (pkUIObject)theContainer.getChildAt(k);
         b.setX(displayMetrics.widthPixels * b.X() / displayMetrics.heightPixels);
         b.setY(displayMetrics.widthPixels * b.Y() / displayMetrics.heightPixels);
      }

      theContainer.layout(0, 0, displayMetrics.heightPixels, displayMetrics.widthPixels);

      theHandler.post(thePainter);

      sendEvent(String.valueOf(EVENT_CONFIGURATIONCHANGED));

      return;
   }
   
   @Override
   protected void onDestroy() {
      super.onDestroy();
      if ( isFinishing() )
         stopService(new Intent(getBaseContext(),com.innovisionate.phabletsignaturepad.pkListener.class));
      return;
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      return doOptionsMenu(R.id.action_signature_capture,menu);
   }

   public boolean doOptionsMenu(int hiddenId,Menu menu) {
      getMenuInflater().inflate(R.menu.common, menu);
      MenuItem item = menu.findItem(hiddenId);
      item.setVisible(false);
      item = menu.findItem(R.id.action_purchase);
      item.setVisible(inAppBillingAvailable);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      switch ( item.getItemId() ) {
      case R.id.action_exit:
         finish();
         return true;
      case R.id.action_signature_capture:
         return true;
      case R.id.action_information:
         startActivity(new Intent(this,Information.class));
         return true;
      case R.id.action_evaluate:
         startActivity(new Intent(this,EvaluateCursiVision.class));
         return true;
      case R.id.action_purchase:
         startActivity(new Intent(this,PurchaseCursiVision.class));
         return true;
      case R.id.action_settings:
         startActivity(new Intent(this,Settings.class));
         return true;
      default:
         return super.onOptionsItemSelected(item);
      }
   }   
   
   public static void SavePreferences() {
   
      SharedPreferences.Editor editor = PhabletSignaturePad.preferences.edit();

      if ( "" == properties.uuid )
         properties.uuid = UUID.randomUUID().toString();

      editor.putInt("ink color", properties.inkColor);
      editor.putFloat("ink weight", properties.inkWeight);
      editor.putInt("show ink", properties.showInk);
      editor.putInt("send points",properties.sendPoints);
      editor.putString("font family",properties.fontFamily);
      editor.putFloat("font size", properties.fontSize);
      editor.putInt("hide action bar timeout",properties.hideActionBarTimeout);

   //   editor.putString("background bitmap file name", PhabletSignaturePad.properties.backgroundBitmapFileName);

      editor.putInt("orientation",theMainActivity.getRequestedOrientation());

    //
    //NTC: 7-27-2014: For the time being, buttons are not recreated on the pad as if they are in non-volatile memory. There is no
    // event mechanism to do anything with the buttons.
    //
   /*
      if ( ! ( null == properties.buttonCreationStrings ) ) {
         String s = "";
         for ( Entry<Integer,String> cb : properties.buttonCreationStrings.entrySet() )
            s += cb.getValue() + ";";
         editor.putString("button creation strings", s);
      } else
   */
         editor.putString("button creation strings",null);

   /*
      if ( ! ( null == properties.controlMotionStrings ) ) {
         String s = "";
         for ( Entry<Integer,String> cb : properties.controlMotionStrings.entrySet() )
            s += cb.getValue() + ";";
         editor.putString("control motion strings", s);
      } else
   */
         editor.putString("control motion strings",null);

      editor.commit();

      return;
   }
   
   public static void RestorePreferences() {
   
      properties.inkColor = preferences.getInt("ink color", Color.BLACK);
      properties.inkWeight = preferences.getFloat("ink weight", 2.0f);
      properties.showInk = preferences.getInt("show ink", 1);
      properties.sendPoints = preferences.getInt("send points",1);
      properties.fontFamily = preferences.getString("font family","Arial");
      properties.fontSize = preferences.getFloat("font size", 12.0f);
      properties.hideActionBarTimeout = preferences.getInt("hide action bar timeout",10000);

   //   properties.backgroundBitmapFileName = preferences.getString("background bitmap file name", null);

      properties.orientation = preferences.getInt("orientation",ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

   //   theMainActivity.setRequestedOrientation(properties.orientation);
      if ( ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE == properties.orientation ) {

      }

   //   if ( ! ( null == properties.backgroundBitmapFileName ) )
   //      theSignaturePadBitmap = BitmapFactory.decodeFile(properties.backgroundBitmapFileName,null);
   //   else
   //      theSignaturePadBitmap = null;

   //   theHandler.post(PhabletSignaturePad.theBitmapRunnable);

      controlDetails[0] = "0";
      controlDetails[1] = "-1";

      if ( Looper.getMainLooper().getThread() == Thread.currentThread() )
         removeControl();
      else {
         actionComplete = new CountDownLatch(1);
         theHandler.post(PhabletSignaturePad.theRemoveControlRunnable);
         try {
         actionComplete.await();
         } catch ( InterruptedException e ) {
            e.printStackTrace();
         }
      }

      return;
   }

   public static void ResetPreferences() {
   SharedPreferences.Editor editor = PhabletSignaturePad.preferences.edit();
   editor.clear();
   editor.commit();
   RestorePreferences();
   return;
   }
   
   private static void setViewBitmap() {
   theSignaturePadView.clearPath();
   theSignaturePadView.TheBitmap(commandProcessor.newBitmap);
   theSignaturePadView.invalidate();
   actionComplete.countDown();
   return;
   }

   private static void showPDF() {
   return;
   }

   private static void addViewBitmap() {
   Boolean didExist = true;
   if ( null == theSignaturePadView.TheBitmap() ) {
      theSignaturePadView.TheBitmap(Bitmap.createBitmap(theSignaturePadView.Width(), theSignaturePadView.Height(), Bitmap.Config.ARGB_8888));
      didExist = false;
   }
   Canvas cv = new Canvas(theSignaturePadView.TheBitmap());
   if ( ! didExist )
      cv.drawColor(Color.WHITE);
   cv.drawBitmap(commandProcessor.newBitmap,null,
                 new Rect(commandProcessor.imageLocationX,commandProcessor.imageLocationY,
                              commandProcessor.imageLocationX + commandProcessor.imageWidth,commandProcessor.imageLocationY + commandProcessor.imageHeight),theSignaturePadView.ThePaint());
   theSignaturePadView.invalidate();
   System.out.println("added bitmap at:" + commandProcessor.imageLocationX + " : " + commandProcessor.imageLocationY + " (" + commandProcessor.imageWidth + " , " + commandProcessor.imageHeight + ")" +
                           "Pad is width: " + theSignaturePadView.Width() + " height:" + theSignaturePadView.Height());
   return;
   }

   
   private static void createControl() {
      
   if ( Integer.valueOf(controlDetails[0]) == commandProcessor.CREATECHECKBOX )
      theContainer.addView(new pkCheckBox(),theContainer.getChildCount());

   if ( Integer.valueOf(controlDetails[0]) == commandProcessor.CREATEBUTTON )
      theContainer.addView(new pkButton(),theContainer.getChildCount());

   if ( Integer.valueOf(controlDetails[0]) == commandProcessor.CREATEDROPDOWNBOX ) 
      theContainer.addView(new pkDropDownBox(),theContainer.getChildCount());

   if ( Integer.valueOf(controlDetails[0]) == commandProcessor.CREATELABEL ) 
      theContainer.addView(new pkTextView(),theContainer.getChildCount());

   if ( Integer.valueOf(controlDetails[0]) == commandProcessor.CREATERADIOBUTTON )
      theContainer.addView(new pkRadioButton(),theContainer.getChildCount());
   
   if ( Integer.valueOf(controlDetails[0]) == commandProcessor.CREATETEXTBOX )
      theContainer.addView(new pkTextBox(),theContainer.getChildCount());

   if ( Integer.valueOf(controlDetails[0]) == commandProcessor.CREATEENTRYFIELD )
      theContainer.addView(new pkEntryField(),theContainer.getChildCount());
   
   actionComplete.countDown();
   
   repaint();

   return;
   }
   
   private static void removeControl() {
   if ( -1 == Integer.valueOf(controlDetails[1]) ) {
      int n = theContainer.getChildCount() - 1;
      for ( int k = n; k > 0; k-- )
         theContainer.removeViewAt(k);
      actionComplete.countDown();
      repaint();
      return;
   }
   for ( int k = 1; k < theContainer.getChildCount(); k++ ) {
      View v = (View)theContainer.getChildAt(k);
      if ( v.getId() == Integer.valueOf(controlDetails[1]) ) {
         theContainer.removeViewAt(k);
         actionComplete.countDown();
         repaint();
         return;
      }
   }
   actionComplete.countDown();
   theContainer.requestLayout();
   return;
   }
   
   private static void reSizeControl() {

   for ( int k = 1; k < theContainer.getChildCount(); k++ ) {
      View v = (View)theContainer.getChildAt(k);
      if ( v.getId() == Integer.valueOf(controlDetails[0]) ) {
         if ( v instanceof pkButton || v instanceof pkDropDownBox || v instanceof pkTextView || v instanceof pkCheckBox || v instanceof pkRadioButton ) {
            pkUIObject p = (pkUIObject)v;
            p.setX(Integer.valueOf(controlDetails[1]));
            p.setY(Integer.valueOf(controlDetails[2]));
            p.setWidth(Integer.valueOf(controlDetails[3]) - p.X());
            p.setHeight(Integer.valueOf(controlDetails[4]) - p.Y());
         }
         break;
      }
   }

   actionComplete.countDown();
   
   theContainer.requestLayout();
   
   return;
   }

   private static void rePositionControl() {

   for ( int k = 1; k < theContainer.getChildCount(); k++ ) {
      View v = (View)theContainer.getChildAt(k);
      if ( v.getId() == Integer.valueOf(controlDetails[0]) ) {
         if ( v instanceof pkButton || v instanceof pkDropDownBox || v instanceof pkTextView || v instanceof pkCheckBox || v instanceof pkRadioButton ) {
            pkUIObject p = (pkUIObject)v;
            p.setX(Integer.valueOf(controlDetails[1]));
            p.setY(Integer.valueOf(controlDetails[2]));
         }
         break;
      }
   }

   actionComplete.countDown();
   
   theContainer.requestLayout();
   
   return;
   }

   private static void setControlVisibility() {
   for ( int k = 1; k < theContainer.getChildCount(); k++ ) {
      View b = (View) theContainer.getChildAt(k);
      if ( b.getId() == Integer.valueOf(controlDetails[0]) ) {
         pkUIObject control = (pkUIObject)b;
         if ( "0".equals(controlDetails[1]) )
            control.setRequestedVisibility(View.INVISIBLE);
         else
            control.setRequestedVisibility(View.VISIBLE);
         if ( 0 == PhabletSignaturePad.controlUpdatesShowing )
            break;
         control.restoreVisibility();
      }
   }
   actionComplete.countDown();
   return;
   }  
   
   private static void setControlValue() {
   for ( int k = 1; k < theContainer.getChildCount(); k++ ) {
      View b = (View) theContainer.getChildAt(k);
      if ( b.getId() == Integer.valueOf(controlDetails[1]) ) {
         if ( b instanceof pkDropDownBox ) {
            pkDropDownBox v = (pkDropDownBox)b;
            v.setSelection(v.getPosition(PhabletSignaturePad.controlDetails[2].replace('"', ' ').trim()));
            break;
         }
         if ( b instanceof pkTextView ) {
            pkTextView v = (pkTextView)b;
            v.setText(PhabletSignaturePad.controlDetails[2].replace('"', ' ').trim());
            break;
         }
         if ( b instanceof pkEntryField ) {
            pkEntryField v = (pkEntryField)b;
            holdEvents = true;
            v.setText(PhabletSignaturePad.controlDetails[2].replace("\"", "").trim());
            v.setSelection(v.getText().toString().length());
            holdEvents = false;
            break;
         }
         break;
      }
   }
   actionComplete.countDown();
   return;
   }  
   
   private static void setControlState() {
   for ( int k = 1; k < theContainer.getChildCount(); k++ ) {
      View b = (View) theContainer.getChildAt(k);
      if ( b.getId() == Integer.valueOf(controlDetails[1]) ) {
         if ( b instanceof pkCheckBox ) {
            pkCheckBox v = (pkCheckBox)b;
            v.setChecked("1".equals(PhabletSignaturePad.controlDetails[2]) ? true : false);
            v.onClick(null);
            break;
         }
         if ( b instanceof pkRadioButton ) {
            pkRadioButton v = (pkRadioButton)b;
            v.setChecked("1".equals(PhabletSignaturePad.controlDetails[2]) ? true : false);
            v.onClick(null);
            break;
         }
         break;
      }
   }
   actionComplete.countDown();
   return;
   }

   private static void dispatchKeyEvent() {
      for ( int k = 1; k < theContainer.getChildCount(); k++ ) {
         View b = (View) theContainer.getChildAt(k);
         if ( b.getId() == Integer.valueOf(controlDetails[1]) ) {
            pkUIObject p = (pkUIObject)b;
            //p.setText(PhabletSignaturePad.controlDetails[2].replaceAll("\"", "").trim());
            //
            //NTC: 10-28-2019: This method has not been completed, sending individual keystrokes
            // is an overcomplication.
            //
            b.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,Integer.valueOf(controlDetails[2])));
            b.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP,Integer.valueOf(controlDetails[2])));
            break;
         }
      }
      actionComplete.countDown();
      return;
   }

   private static void setContainerVisibility() {
   for ( int k = 1; k < theContainer.getChildCount(); k++ ) {
      pkUIObject  b = (pkUIObject)(View)theContainer.getChildAt(k);
      if ( 1 == PhabletSignaturePad.controlUpdatesShowing )
         b.restoreVisibility();
   }
   actionComplete.countDown();
   return;
   }

   private static void cycle() {
      
   theMainActivity.stopService(new Intent(theMainActivity.getBaseContext(),com.innovisionate.phabletsignaturepad.pkListener.class));

   scaleObjectsX = 1.0;
   scaleObjectsY = 1.0;
   hostUIWidthInPixels = 0;
   hostUIHeightInPixels = 0;
   
   theMainActivity.startService(new Intent(theMainActivity.getBaseContext(),com.innovisionate.phabletsignaturepad.pkListener.class));

   return;
   }
   
   private static void startEventDispatcher() {
   eventsQueue = new LinkedBlockingQueue<String>();
   theMainActivity.startService(new Intent(theMainActivity.getBaseContext(),com.innovisionate.phabletsignaturepad.pkEventsDispatcher.class));
   return;
   }
   
   private static void stopEventDispatcher() {
   theMainActivity.stopService(new Intent(theMainActivity.getBaseContext(),com.innovisionate.phabletsignaturepad.pkEventsDispatcher.class));
   return;
   }

   private static void startControlListener() {
   theMainActivity.startService(new Intent(theMainActivity.getBaseContext(),com.innovisionate.phabletsignaturepad.pkControlListener.class));
   WindowManager.LayoutParams layoutParams = theMainActivity.getWindow().getAttributes();
   layoutParams.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
   theMainActivity.getWindow().setAttributes(layoutParams);
   return;
   }
   
   private static void stopControlListener() {
   theMainActivity.stopService(new Intent(theMainActivity.getBaseContext(),com.innovisionate.phabletsignaturepad.pkControlListener.class));
   return;
   }
   
   private static void startPenEvents() {
   theSignaturePadView.startPenEvents();
   actionComplete.countDown();
   return;
   }

   private static void stopPenEvents() {
   theSignaturePadView.stopPenEvents();
   eventsQueue.clear();
   actionComplete.countDown();
   return;
   }
 
   public static void sendEvent(String event) {
   if ( null == eventsQueue )
      return;
   if ( holdEvents )
      return;
   try {
   eventsQueue.put(event);
   } catch ( InterruptedException e ) {
      e.printStackTrace();
   }
   return;
   }
   
   public static float ToInches(int fromPixels) {
   DisplayMetrics displayMetrics = PhabletSignaturePad.theMainActivity.getBaseContext().getResources().getDisplayMetrics();
   return (float)fromPixels / TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_IN, 1, displayMetrics);
   }

   public static float DeviceWidthInches() {
   return ToInches(DeviceWidthPixels());
   }

   public static float DeviceHeightInches() {
   return ToInches(DeviceHeightPixels());
   }

   public static int DeviceWidthPixels() {
   return theMainActivity.getBaseContext().getResources().getDisplayMetrics().widthPixels;
   }

   public static int DeviceHeightPixels() {
   return theMainActivity.getBaseContext().getResources().getDisplayMetrics().heightPixels;
   }
    
   private static void doToast() {
   Toast.makeText(theMainActivity,toastMessage,Toast.LENGTH_LONG).show();
   return;
   }
   
   private static void repaint() {
   theSignaturePadView.invalidate();
   for ( int k = 1; k < theContainer.getChildCount(); k++ ) 
      theContainer.getChildAt(k).invalidate();
   return;
   }
   
   private void initiateLicenseCheck() {
   
   String deviceId = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
   String fms1 = "InnoVisioNateRocks14";
   String fms2 = "VisioLogger Is Great";

   int j = 0;
   for ( int k = fms1.length() - 1; k > 0; k--, j++ )
      SALT[fms1.length() - k] = (byte) ((byte)fms1.charAt(k) | (byte)fms2.charAt(j));
   
   licenseCheckerCallback licenseCheckerCallback = new licenseCheckerCallback();
   
   LicenseChecker licenseChecker = new LicenseChecker(this, new ServerManagedPolicy(this,new AESObfuscator(SALT, getPackageName(), deviceId)),BASE64_PUBLIC_KEY);

   licenseChecker.checkAccess(licenseCheckerCallback);   
   
   return;
   }
   
   private class licenseCheckerCallback implements LicenseCheckerCallback {

   public void allow(int policyReason) {
   if ( isFinishing() )
      return;
   toastMessage = "You are sufficiently licensed to use the PhabletSignaturePad device";
   theHandler.post(theToastRunnable);
   return;
   }

   public void dontAllow(int policyReason) {
   if ( isFinishing() )
      return;
   toastMessage = "You are not licensed to use the PhabletSignaturePad device";
   theHandler.post(theToastRunnable);
   return;
   }

   public void applicationError(int errorCode) {
   if ( isFinishing() )
      return;
   toastMessage = "There was an error (" + String.valueOf(errorCode) + ") while checking your license for PhabletSignaturePad";
   theHandler.post(theToastRunnable);
   return;
   }
   
   }   
}
