package com.innovisionate.phabletsignaturepad;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CountDownLatch;
//import java.awt;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.ParcelFileDescriptor;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

/*
import android.graphics.pdf.PdfRenderer;
import android.graphics.pdf.PdfRenderer.Page;
*/

public class commandProcessor {

   private static final int RECEIVEIMAGE = 1;
   private static final int START = 2;
   private static final int STOP = 3;
   private static final int CLEARHOTSPOTS = 4;
   private static final int HOTSPOT = 5;
   private static final int CLEAREVERYTHING = 6;
   public static final int CREATEBUTTON = 7; 
   private static final int POSITION = 8;
   private static final int RESIZE = 9;
   private static final int QUERYPARAMETER = 10;
   private static final int HIDE = 11;
   private static final int SHOW = 12;
   private static final int DESTROYCONTROL = 13;
   private static final int SETPARAMETER = 14;
   private static final int BACKGROUND = 15;
   private static final int CLEARINK = 16;
   private static final int CLEARSETTINGS = 17;
   private static final int CLEARBACKGROUND = 18;
   private static final int HIDECONTROL = 19;
   private static final int SHOWCONTROL = 20;
   private static final int HIDECONTROLLIST = 21;
   private static final int SHOWCONTROLLIST = 22;
   private static final int DISPLAYPDF = 23;
   public static final int CREATEDROPDOWNBOX = 24;
   public static final int SELECTITEM = 25;
   public static final int CREATELABEL = 26;
   public static final int SETLABEL = 27;
   public static final int CREATECHECKBOX = 28;
   public static final int SETOPTIONSTATE = 29;
   public static final int CREATERADIOBUTTON = 30;
   public static final int CREATETEXTBOX = 31;
   public static final int CREATEENTRYFIELD = 32;
   public static final int TAKEKEYSTROKE = 33;

   private static final int CREATECONTROL = 101;
   
   private static final int PARM_BOUNDS_PIXELS = 1;
   private static final int PARM_BOUNDS_INCHES = 2;
   private static final int PARM_CLIENT_ADDRESS = 3;
   private static final int PARM_INK_COLOR = 4;
   private static final int PARM_INK_WEIGHT = 5;
   private static final int PARM_SHOW_INK = 6;
   private static final int PARM_SEND_POINTS = 7;
   private static final int PARM_WIDTH_PIXELS = 8;
   private static final int PARM_HEIGHT_PIXELS = 9;
   private static final int PARM_WIDTH_INCHES = 10;
   private static final int PARM_HEIGHT_INCHES = 11;
   private static final int PARM_DEVICE_WIDTH_INCHES = 12;
   private static final int PARM_DEVICE_HEIGHT_INCHES = 13;
   private static final int PARM_DEVICE_WIDTH_PIXELS = 14;
   private static final int PARM_DEVICE_HEIGHT_PIXELS = 15;
   private static final int PARM_IMAGE = 16;
   private static final int PARM_FONT_FAMILY = 17;
   private static final int PARM_FONT_SIZE = 18;
   private static final int PARM_CONTROL_BOUNDS = 19;
   private static final int PARM_PDF = 20;
   private static final int PARM_UI_HOST_WIDTH = 21;
   private static final int PARM_UI_HOST_HEIGHT = 22;
   private static final int PARM_CONTROL_TEXT = 23;
   private static final int PARM_CONTROL_TEXT_BOUNDS = 24;
   private static final int PARM_CONTROL_TEXT_LINE = 25;
   private static final int PARM_CONTROL_POSITION = 26;
   private static final int PARM_UPDATES_SHOWING = 27;
   private static final int PARM_CONTROL_TEXT_FONT_HEIGHT = 28;

   public static int imageLocationX,imageLocationY,imageWidth,imageHeight;
   public static Bitmap newBitmap = null;

   private /*Data*/InputStream inputStream = null;
   private DataOutputStream outputStream = null;
   
   private Context theContext = null;

   private String rawCommand = null;
   
   public commandProcessor(Context c,/*Data*/InputStream is,DataOutputStream os) {
   theContext = c;
   inputStream = is;
   outputStream = os;
   return;
   }
   
   public void process(String command) throws IOException {
   
   rawCommand = command.trim();

   for ( int k = 0; k < PhabletSignaturePad.controlDetails.length; k++ )
      PhabletSignaturePad.controlDetails[k] = "";

   int n = rawCommand.length();
   char [] p = new char[n];
   rawCommand.getChars(0,n,p,0);
   
   Boolean inQuotes = ('"' == p[0]);

   for ( int k = 1; k < n; k++ ) {
      if ( '"' == p[k] ) {
         inQuotes = ! inQuotes;
         continue;
      }
      if ( 'î' == p[k] ) {
         inQuotes = true;
         continue;
      }
      if ( 'ì' == p[k] ) {
         inQuotes = false;
         continue;
      }
      if ( inQuotes && ' ' == p[k] )
         p[k] = 'Ä';
   }
   
   String[] values = (new String(p)).split(" ");
   
   for ( int k = 0; k < values.length; k++ ) {
      values[k] = values[k].replaceAll("Ä", " ");
      if ( values[k].startsWith("\"") || values[k].startsWith("î") )
         values[k] = values[k].substring((1));
      if ( values[k].endsWith("\"") || values[k].endsWith("ì"))
         values[k] = values[k].substring(0, values[k].length() - 1);
   }
   
   int theCommand = -1;
   
   try {

   theCommand = Integer.valueOf(values[0]);

   } catch ( Exception e ) {
      outputStream.write((command + " is invalid").getBytes("UTF-16LE"));
      return;
   }

   String response = processCommand(theCommand,values);

   if ( ! ( null == response ) ) 
      outputStream.write(response.getBytes("UTF-16LE"));

   if ( SETPARAMETER == theCommand )
      PhabletSignaturePad.SavePreferences();
   
   return;
   }
  
   
   private String processCommand(int theCommand,String [] values) {

   switch ( theCommand ) {

   case BACKGROUND:
   case RECEIVEIMAGE:
      return recieveImage(theCommand,values);

   case START:
      startPen();
      break;

   case STOP:
      stopPen();
      break;

   case HIDE:
      PhabletSignaturePad.theSignaturePadView.Hide();
      break;
      
   case SHOW:
      PhabletSignaturePad.theSignaturePadView.Show();
      break;
      
   case CLEAREVERYTHING:
      newBitmap = null;
      PhabletSignaturePad.actionComplete = new CountDownLatch(1);
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theBitmapRunnable);
      try {
      PhabletSignaturePad.actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }
      PhabletSignaturePad.controlDetails[0] = "-1";
      PhabletSignaturePad.controlDetails[1] = "-1";
      PhabletSignaturePad.actionComplete = new CountDownLatch(1);
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theRemoveControlRunnable);
      try {
      PhabletSignaturePad.actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }
      break;

   // CREATEBUTTON x, y, id, isVisible, text
   // CREATEDROPDOWNBOX x, y, id, isVisible items selectedItem
   // CREATECHECKBOX x, y, id, isVisible, isChecked, text
   // CREATERADIOBUTTON x, y, id, isVisible, isChecked, groupNumber, text
   // CREATELABEL x, y, id, isVisible, text, fontFace, fontSize
   // CREATETEXTBOX x, y, width, maxHeight, id, isVisible, text, fontFace, fontSize
   
   case CREATEBUTTON:
   case CREATEDROPDOWNBOX:
   case CREATECHECKBOX:
   case CREATERADIOBUTTON:
   case CREATELABEL:
   case CREATETEXTBOX:
   case CREATEENTRYFIELD: {
      String [] tempValues = {String.valueOf(DESTROYCONTROL),values[3]};
      processCommand(DESTROYCONTROL,tempValues);
      return processCommand(CREATECONTROL,values);
      }   

   case SETOPTIONSTATE: {
      
      for ( int k = 0; k < values.length; k++ )
         PhabletSignaturePad.controlDetails[k] = values[k];
     
      for ( int k = values.length; k < PhabletSignaturePad.controlDetails.length; k++ )
         PhabletSignaturePad.controlDetails[k] = "";      

      PhabletSignaturePad.actionComplete = new CountDownLatch(1);
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theControlStateSetter);
      
      try {
      PhabletSignaturePad.actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }
      
      }
      break;
      
   case SELECTITEM: {

      for ( int k = 0; k < values.length; k++ )
         PhabletSignaturePad.controlDetails[k] = values[k];
     
      for ( int k = values.length; k < PhabletSignaturePad.controlDetails.length; k++ )
         PhabletSignaturePad.controlDetails[k] = "";

      PhabletSignaturePad.actionComplete = new CountDownLatch(1);

      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theControlValueSetter);

      try {
      PhabletSignaturePad.actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }
         
      }
      break;

   case TAKEKEYSTROKE: {
      //
      //NTC: 10-28-2019: This method has not been completed, sending individual keystrokes
      // is an overcomplication.
      //
      for ( int k = 0; k < values.length; k++ )
         PhabletSignaturePad.controlDetails[k] = values[k];

      for ( int k = values.length; k < PhabletSignaturePad.controlDetails.length; k++ )
         PhabletSignaturePad.controlDetails[k] = "";

      PhabletSignaturePad.actionComplete = new CountDownLatch(1);

      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theDispatchKeyEventRunnable);

      try {
         PhabletSignaturePad.actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }

      }
      break;

   case CREATECONTROL:
            
      for ( int k = 0; k < values.length; k++ )
         PhabletSignaturePad.controlDetails[k] = values[k];
      
      for ( int k = values.length; k < PhabletSignaturePad.controlDetails.length; k++ )
         PhabletSignaturePad.controlDetails[k] = "";
         
      PhabletSignaturePad.actionComplete = new CountDownLatch(1);

      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theCreateControlRunnable);

      try {
      PhabletSignaturePad.actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }
      
      break;

   case DESTROYCONTROL:

      PhabletSignaturePad.controlDetails[0] = values[0];
      PhabletSignaturePad.controlDetails[1] = values[1];
      PhabletSignaturePad.actionComplete = new CountDownLatch(1);
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theRemoveControlRunnable);
      try {
      PhabletSignaturePad.actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }
      System.out.println("control destroyed");
      break;

      
   case SETPARAMETER: 
   
      switch ( Integer.valueOf(values[1]) ) {

      case PARM_BOUNDS_PIXELS:
/*
         MainActivity.theSignaturePadView.setAdjustViewBounds(true);
         MainActivity.theSignaturePadView.setX((float)Integer.valueOf(values[2]));
         MainActivity.theSignaturePadView.setY((float)Integer.valueOf(values[3]));
         MainActivity.theSignaturePadView.setMaxWidth(Integer.valueOf(values[4]));
         MainActivity.theSignaturePadView.setMaxHeight(Integer.valueOf(values[5]));
*/
         break;

      case PARM_BOUNDS_INCHES:
/*
         MainActivity.theSignaturePadView.setAdjustViewBounds(true);
         MainActivity.theSignaturePadView.setX((float)Integer.valueOf(values[2]));
         MainActivity.theSignaturePadView.setY((float)Integer.valueOf(values[3]));
         MainActivity.theSignaturePadView.setMaxWidth(Integer.valueOf(values[4]));
         MainActivity.theSignaturePadView.setMaxHeight(Integer.valueOf(values[5]));
*/
         break;
         
      case PARM_CLIENT_ADDRESS:
         //MainActivity.clientAddress = new InetAddress(values[2]);
         //MainActivity.clientAddress.
         break;
         
      case PARM_INK_COLOR:
         PhabletSignaturePad.properties.inkColor = Color.rgb(Integer.valueOf(values[2]),Integer.valueOf(values[3]),Integer.valueOf(values[4]));
         PhabletSignaturePad.theSignaturePadView.paint.setColor(PhabletSignaturePad.properties.inkColor);
         break;
         
      case PARM_INK_WEIGHT:
         PhabletSignaturePad.properties.inkWeight = Float.valueOf(values[2]);
         PhabletSignaturePad.theSignaturePadView.paint.setStrokeWidth(PhabletSignaturePad.properties.inkWeight);
         break;
         
      case PARM_SHOW_INK:
         PhabletSignaturePad.properties.showInk = Integer.valueOf(values[2]);
         break;

      case PARM_SEND_POINTS:
         PhabletSignaturePad.properties.sendPoints = Integer.valueOf(values[2]);
         break;
         
      case PARM_FONT_FAMILY:
         PhabletSignaturePad.properties.fontFamily = values[2];
         break;
         
      case PARM_FONT_SIZE:
         PhabletSignaturePad.properties.fontSize = (float)Float.valueOf(values[2]);
         break;

      case PARM_CONTROL_TEXT:

         for ( int k = 1; k < values.length; k++ )
            PhabletSignaturePad.controlDetails[k - 1] = values[k];

         for ( int k = values.length; k < PhabletSignaturePad.controlDetails.length + 1; k++ )
            PhabletSignaturePad.controlDetails[k - 1] = "";

         PhabletSignaturePad.actionComplete = new CountDownLatch(1);

         PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theControlValueSetter);

         try {
            PhabletSignaturePad.actionComplete.await();
         } catch ( InterruptedException e ) {
            e.printStackTrace();
         }

         break;

      case PARM_CONTROL_POSITION:
         
         for ( int k = 2; k < values.length; k++ )
            PhabletSignaturePad.controlDetails[k - 2] = values[k];
         
         for ( int k = values.length; k < PhabletSignaturePad.controlDetails.length + 2; k++ )
            PhabletSignaturePad.controlDetails[k - 2] = "";
         
         PhabletSignaturePad.actionComplete = new CountDownLatch(1);
         
         PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theRePositionControl);

         try {
         PhabletSignaturePad.actionComplete.await();
         } catch ( InterruptedException e ) {
            e.printStackTrace();
         }
         
         break;
      
      case PARM_UI_HOST_WIDTH:
    	  PhabletSignaturePad.hostUIWidthInPixels = Integer.valueOf(values[2]);
    	  break;
    	  
      case PARM_UI_HOST_HEIGHT:
    	  PhabletSignaturePad.hostUIHeightInPixels = Integer.valueOf(values[2]);
    	  break;

      case PARM_UPDATES_SHOWING:

         PhabletSignaturePad.controlUpdatesShowing = Integer.valueOf(values[2]);
         PhabletSignaturePad.actionComplete = new CountDownLatch(1);
         PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theContainerVisibilityRunnable);

         try {

         PhabletSignaturePad.actionComplete.await();

         } catch ( InterruptedException e ) {
            e.printStackTrace();
         }

         break;

      default:
         return "unknown parameter to set";

      }

      break;
      
   case QUERYPARAMETER: 
   
      switch ( Integer.valueOf(values[1]) ) {
      
      case PARM_WIDTH_PIXELS:
      case PARM_HEIGHT_PIXELS:
      case PARM_WIDTH_INCHES:
      case PARM_HEIGHT_INCHES:
      case PARM_BOUNDS_PIXELS:
      case PARM_BOUNDS_INCHES:
         if ( PARM_WIDTH_PIXELS == Integer.valueOf(values[1]) )
            return String.valueOf(PhabletSignaturePad.theSignaturePadView.Width());
         else if ( PARM_HEIGHT_PIXELS == Integer.valueOf(values[1]) )
            return String.valueOf(PhabletSignaturePad.theSignaturePadView.Height());
         if ( PARM_WIDTH_INCHES == Integer.valueOf(values[1]) )
            return String.valueOf(PhabletSignaturePad.theSignaturePadView.InchesWidth());
         else if ( PARM_HEIGHT_INCHES == Integer.valueOf(values[1]) )
            return String.valueOf(PhabletSignaturePad.theSignaturePadView.InchesHeight());
         else if ( PARM_BOUNDS_PIXELS == Integer.valueOf(values[1]) )
            return (new StringBuilder(" 0 0 ").append(PhabletSignaturePad.theSignaturePadView.Width()).append(" ").append(PhabletSignaturePad.theSignaturePadView.Height())).toString();
        return (new StringBuilder(" 0 0 ").append(PhabletSignaturePad.theSignaturePadView.InchesWidth()).append(" ").append(PhabletSignaturePad.theSignaturePadView.InchesHeight())).toString();

      case PARM_DEVICE_WIDTH_PIXELS:
      case PARM_DEVICE_HEIGHT_PIXELS:
         if ( PARM_DEVICE_WIDTH_PIXELS == Integer.valueOf(values[1]) )
            return String.valueOf(PhabletSignaturePad.DeviceWidthPixels());
         return String.valueOf(PhabletSignaturePad.DeviceHeightPixels());

      case PARM_DEVICE_WIDTH_INCHES:
      case PARM_DEVICE_HEIGHT_INCHES:
         if ( PARM_DEVICE_WIDTH_INCHES == Integer.valueOf(values[1]) )
            return String.valueOf(PhabletSignaturePad.DeviceWidthInches());
         return String.valueOf(PhabletSignaturePad.DeviceHeightInches());

      case PARM_CLIENT_ADDRESS:
         return PhabletSignaturePad.clientAddress.toString();

      case PARM_INK_COLOR:
         return String.valueOf(PhabletSignaturePad.properties.inkColor);
      
      case PARM_INK_WEIGHT:
         return String.valueOf(PhabletSignaturePad.properties.inkWeight);
         
      case PARM_SHOW_INK:
         return String.valueOf(PhabletSignaturePad.properties.showInk);

      case PARM_SEND_POINTS:
         return String.valueOf(PhabletSignaturePad.properties.sendPoints);
         
      case PARM_IMAGE:
         return sendImage();
         
      case PARM_FONT_FAMILY:
         return PhabletSignaturePad.properties.fontFamily;
         
      case PARM_FONT_SIZE:
         return String.valueOf(PhabletSignaturePad.properties.fontSize);

      case PARM_CONTROL_TEXT:
      case PARM_CONTROL_BOUNDS:
      case PARM_CONTROL_POSITION:
      case PARM_CONTROL_TEXT_BOUNDS:
      case PARM_CONTROL_TEXT_LINE:
      case PARM_CONTROL_TEXT_FONT_HEIGHT:

         int controlId = Integer.valueOf(values[2]);
         
         for ( int k = 1; k < PhabletSignaturePad.theContainer.getChildCount(); k++ ) {

            View v = (View)PhabletSignaturePad.theContainer.getChildAt(k);
            
            if ( ! ( v.getId() == controlId ) )
               continue;
            
            pkUIObject p = (pkUIObject)v;

            if ( Integer.valueOf(values[1]) == PARM_CONTROL_TEXT )
               return p.Text();
               
            if ( Integer.valueOf(values[1]) == PARM_CONTROL_POSITION )
               return String.valueOf(p.X()) + " " + String.valueOf(p.Y());

            if ( Integer.valueOf(values[1]) == PARM_CONTROL_TEXT_BOUNDS )
               return String.valueOf(p.TextX()) + " " + String.valueOf(p.TextY()) + " " + String.valueOf(p.TextX() + p.TextWidth()) + " " + String.valueOf(p.TextY() + p.TextHeight());
            
            if ( Integer.valueOf(values[1]) == PARM_CONTROL_TEXT_LINE ) {
               if ( ! ( p instanceof pkTextBox ) )
                  return "<unknown>";
               String firstLine = ((pkTextBox)p).getLine(Integer.valueOf(values[3]));
               if ( 0 == firstLine.length() )
                  return "<unknown>";
               return firstLine;
            }

            if ( Integer.valueOf(values[1]) == PARM_CONTROL_TEXT_FONT_HEIGHT )
               return String.valueOf(p.TextFontHeightPixels());

            return String.valueOf(p.X()) + " " + String.valueOf(p.Y()) + " " + String.valueOf(p.X() + p.Width()) + " " + String.valueOf(p.Y() + p.Height());

         }
         
         return "The control with id = " + String.valueOf(controlId) + " is not found";

      case PARM_UPDATES_SHOWING:
         return String.valueOf(PhabletSignaturePad.controlUpdatesShowing);
         
      default: 
         
         return "unknown parameter to query";

      }

   case CLEARINK:
      PhabletSignaturePad.theSignaturePadView.clearPath();
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.thePainter);
      break;
            
   case CLEARSETTINGS:
      PhabletSignaturePad.ResetPreferences();
      break;

   case CLEARBACKGROUND:
      newBitmap = null;
      PhabletSignaturePad.actionComplete = new CountDownLatch(1);
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theBitmapRunnable);
      try {
      PhabletSignaturePad.actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }
      break;

   case HIDECONTROL:
   case SHOWCONTROL:
      PhabletSignaturePad.controlDetails[0] = values[1];
      PhabletSignaturePad.controlDetails[1] = "1";
      if ( theCommand == HIDECONTROL )
         PhabletSignaturePad.controlDetails[1] = "0";
      PhabletSignaturePad.actionComplete = new CountDownLatch(1);
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theControlVisibilityRunnable);
      try {
      PhabletSignaturePad.actionComplete.await();
      } catch ( InterruptedException e ) {
         e.printStackTrace();
      }

      break;

   case HIDECONTROLLIST:
   case SHOWCONTROLLIST:
      String [] theIds = values[1].split(":");
      for ( String s : theIds ) {
         String [] v = {s,s};
         if ( HIDECONTROLLIST == theCommand )
            processCommand(HIDECONTROL,v);
         else
            processCommand(SHOWCONTROL,v);
      }
      
      break;
      
   case DISPLAYPDF:
      return recievePDF(theCommand,values);

   default:
      return values[0] + " is an unknown command";
   }
   
   return "ok";
   }


   public void startPen() {
   PhabletSignaturePad.actionComplete = new CountDownLatch(1);
   PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theStartPenRunnable);
   try {
   PhabletSignaturePad.actionComplete.await();
   } catch ( InterruptedException e ) {
      e.printStackTrace();
   }
   return;
   }

   
   public void stopPen() {
   PhabletSignaturePad.actionComplete = new CountDownLatch(1);
   PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theStopPenRunnable);
   try {
   PhabletSignaturePad.actionComplete.await();
   } catch ( InterruptedException e ) {
      e.printStackTrace();
   }
   return;
   }

   private File recieveFile(int expectedFileSize,String fileName) {

   StringBuilder response = new StringBuilder("send ").append(expectedFileSize).append(" bytes");

   File imageFile = null;

   try {

   outputStream.write(response.toString().getBytes("UTF-16LE"));

   int totalBytes = 0;

   byte[] imageBytes = new byte[512];

   imageFile = new File(fileName);

   FileOutputStream fileStream = new FileOutputStream(imageFile);

   while ( totalBytes < expectedFileSize ) {
      try {
      int bytesRead = inputStream.read(imageBytes);
      fileStream.write(imageBytes,0,bytesRead);
      totalBytes += bytesRead;
      } catch ( IOException e ) {
         fileStream.close();
         throw new IOException(e);
      }
   }

   fileStream.close();

   if ( totalBytes < expectedFileSize ) {
      throw new IOException("Insufficient image bytes sent");
   }

   } catch ( IOException e ) {
      return null;
   }

   return imageFile;
   }

   private String recievePDF(int theCommand, String[] values) {

   if ( 2 > values.length )
      return "the image command is invalid";

   int expectedFileSize = Integer.valueOf(values[1]);

   PhabletSignaturePad.properties.pdfFileName = PhabletSignaturePad.theMainActivity.getFilesDir().getAbsolutePath() + "/pdfFile.pdf";

   File pdfFile = recieveFile(expectedFileSize,PhabletSignaturePad.properties.pdfFileName);

   if ( null == pdfFile )
      return null;

/*
   PdfRenderer renderer = null;
   
   try {
	   
   renderer = new PdfRenderer(ParcelFileDescriptor.open(pdfFile,ParcelFileDescriptor.MODE_READ_ONLY));

   } catch ( Exception e ) {
	   return null;
   }

   if ( null == PhabletSignaturePad.theSignaturePadView.TheBitmap() ) 
      PhabletSignaturePad.theSignaturePadView.CreateBitmap();
	   
   final int pageCount = renderer.getPageCount();

   for ( int k = 0; k < pageCount; k++) {
      Page page = renderer.openPage(k);
      page.render(PhabletSignaturePad.theSignaturePadView.TheBitmap(), null, null, Page.RENDER_MODE_FOR_DISPLAY);
      page.close();
   }

   renderer.close();
   
   */
   /*
   newBitmap = PhabletSignaturePad.theSignaturePadView.TheBitmap();
   
   PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theBitmapRunnable);
   */

   PhabletSignaturePad.toastMessage = "The display of PDF Files is currently under construction.";
   PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theToastRunnable);   
   
   StringBuilder response = new StringBuilder().append(expectedFileSize).append(" bytes recieved ");

   return response.toString();
   }


   private String recieveImage(int theCommand, String[] values) {

   if ( 2 > values.length)
      return "the image command is invalid";

   int expectedFileSize = Integer.valueOf(values[1]);

   if ( RECEIVEIMAGE == theCommand ) {
      imageLocationX = Integer.valueOf(values[2]);
      imageLocationY = Integer.valueOf(values[3]);
      imageWidth = Integer.valueOf(values[4]);
      imageHeight = Integer.valueOf(values[5]);
   } else {
      imageLocationX = -1;
      imageLocationY = -1;
      imageWidth = -1;
      imageHeight = -1;
   }

   PhabletSignaturePad.properties.backgroundBitmapFileName = PhabletSignaturePad.theMainActivity.getFilesDir().getAbsolutePath() + "/backgroundBitmapFile";

   File imageFile = recieveFile(expectedFileSize, PhabletSignaturePad.properties.backgroundBitmapFileName);

   if ( null == imageFile )
      return null;

   BitmapFactory.Options options = new BitmapFactory.Options();

   options.inJustDecodeBounds = true;

   BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);

   final DisplayMetrics displayMetrics = new DisplayMetrics();
   ((WindowManager) theContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);

   PhabletSignaturePad.scaleObjectsX = (double) displayMetrics.widthPixels / (double) options.outWidth;
   PhabletSignaturePad.scaleObjectsY = (double) displayMetrics.heightPixels / (double) options.outHeight;

   options.inJustDecodeBounds = false;

   options.inSampleSize = calculateInSampleSize(options, displayMetrics.widthPixels, displayMetrics.heightPixels);

   options.outWidth = displayMetrics.widthPixels;
   options.outHeight = displayMetrics.heightPixels;
   options.inMutable = true;

   newBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);

   if ( -1 == imageLocationX ) {
      PhabletSignaturePad.actionComplete = new CountDownLatch(1);
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theBitmapRunnable);
   } else
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theAddBitmapRunnable);

   StringBuilder response = new StringBuilder().append(expectedFileSize).append(" bytes recieved ");

   return response.toString();
   }

   
   private String sendImage() {
   
   Bitmap theBitmap = Bitmap.createBitmap(PhabletSignaturePad.theSignaturePadView.getWidth(),PhabletSignaturePad.theSignaturePadView.getHeight(),Bitmap.Config.ARGB_8888); 
   
   Canvas canvas = new Canvas(theBitmap);

   PhabletSignaturePad.theSignaturePadView.draw(canvas);
   
   ByteArrayOutputStream bytes = new ByteArrayOutputStream();

   theBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
   
   try {

   outputStream.write(("recieve " + bytes.size() + " bytes").getBytes("UTF-16LE"));
   
   byte [] imageBytes = bytes.toByteArray();
   
   byte [] response = new byte[9];
   
   inputStream.read/*Fully*/(response, 0, 8);

   int totalBytes = 0;

   while ( totalBytes + 512 < bytes.size() ) {
      outputStream.write(imageBytes,totalBytes,512);
      totalBytes += 512;
      inputStream.read/*Fully*/(response,0,8);
   }
   
   if ( totalBytes < bytes.size() ) {
      outputStream.write(imageBytes,totalBytes,bytes.size() - totalBytes);
      inputStream.read/*Fully*/(response,0,4);
   }
   
   inputStream.read/*Fully*/(response,0,4);
   
   } catch ( IOException e ) {
      e.printStackTrace();
   }
   
   return null;
   }


   public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

   final int height = options.outHeight;
   final int width = options.outWidth;

   if ( height < reqHeight && width < reqWidth ) 
      return 1;

   int inSampleSize = 1;
   final int halfHeight = height / 2;
   final int halfWidth = width / 2;

// Calculate the largest inSampleSize value that is a power of 2 and keeps both
// height and width larger than the requested height and width.

   while ( (halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) 
      inSampleSize *= 2;

   return inSampleSize;
   }   

 
}
