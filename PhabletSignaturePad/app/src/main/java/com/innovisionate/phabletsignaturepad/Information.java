package com.innovisionate.phabletsignaturepad;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Information extends Activity {

   @Override
   protected void onCreate(Bundle savedInstanceState) {

      super.onCreate(savedInstanceState);

      getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

      StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
      StrictMode.setVmPolicy(builder.build());

      setContentView(R.layout.information_main);

      WebView theView = (WebView)findViewById(R.id.informationView);

      theView.setWebViewClient(new WebViewClient() {

         public boolean shouldOverrideUrlLoading(WebView view, String url) {

         if ( url.contains("exitme") )
            finish();
         else {
            if ( url.contains("android_asset") )
               view.loadUrl(url);
            else {
               Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
               startActivity(intent);
               view.stopLoading();
               return false;
            }
         }

         return true;
         }});

      theView.setVerticalScrollBarEnabled(true);

      theView.getSettings().setJavaScriptEnabled(true);

      theView.loadUrl("file:///android_asset/documentation/index.htm");

      return;
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      return PhabletSignaturePad.theMainActivity.doOptionsMenu(R.id.action_information,menu);
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      boolean rv = PhabletSignaturePad.theMainActivity.onOptionsItemSelected(item);
      if ( rv )
         finish();
      return rv;
   } 
   
   @Override
   protected void onDestroy() {
      super.onDestroy();
      return;
   }   

}