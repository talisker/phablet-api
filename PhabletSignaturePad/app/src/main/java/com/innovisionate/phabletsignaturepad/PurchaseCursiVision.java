package com.innovisionate.phabletsignaturepad;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.SkuDetailsParams;

import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

public class PurchaseCursiVision extends Activity {

   @Override
   protected void onCreate(Bundle savedInstanceState) {

      super.onCreate(savedInstanceState);

      setContentView(R.layout.purchase_main);

      TextView tv = (TextView)findViewById(R.id.purchase_count);

      if ( 0 == PhabletSignaturePad.properties.purchaseCount ) {
         tv.setVisibility(View.INVISIBLE);
         tv.setHeight(0);
      } else
         tv.setText(tv.getText().toString().replace("___",String.valueOf(PhabletSignaturePad.properties.purchaseCount)));

      tv = (TextView)findViewById(R.id.purchase_price);
      tv.setText(tv.getText().toString().replace("___", PhabletSignaturePad.cursiVisionPrice));

      List<String> skus = Arrays.asList(PhabletSignaturePad.cursiVisionSku);

      SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder().setSkusList(skus).setType(BillingClient.SkuType.INAPP);

      ((Button)findViewById(R.id.btn_purchase)).setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            BillingFlowParams.Builder builder = BillingFlowParams.newBuilder().setSku(PhabletSignaturePad.cursiVisionSku).setType(BillingClient.SkuType.INAPP);
            PhabletSignaturePad.billingClient.launchBillingFlow(PhabletSignaturePad.theMainActivity,builder.build());
         }
      });

      return;
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      return PhabletSignaturePad.theMainActivity.doOptionsMenu(R.id.action_purchase,menu);
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      boolean rv = PhabletSignaturePad.theMainActivity.onOptionsItemSelected(item);
      if ( rv )
         finish();
      return rv;
   }
}
