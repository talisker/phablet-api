package com.innovisionate.phabletsignaturepad;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

@SuppressLint("WrongCall")
public class signaturePad extends SurfaceView implements SurfaceHolder.Callback {

    public Paint paint = new Paint();
    public int backgroundColor = Color.WHITE;
    private Bitmap theBitmap = null;
    private Rect rect = null;

    public signaturePad(Context context) {

        super(context);

        getHolder().addCallback(this);
        setFocusable(true);

        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2.0f);

        return;
    }

    public int Width() {
        return getWidth();
    }

    public int Height() {
        return getHeight();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        rect = new Rect(0,0,width,height);
        return;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Canvas c = getHolder().lockCanvas();
        onDraw(c);
        getHolder().unlockCanvasAndPost(c);
        return;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        return;
    }

    @Override
    public void onDraw(Canvas canvas) {

        super.onDraw(canvas);

        if ( ! ( null == theBitmap ) )
            canvas.drawBitmap(theBitmap,null,rect,null);
        else
            canvas.drawColor(backgroundColor);

        return;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager)PhabletSignaturePad.theMainActivity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
        setMeasuredDimension(displayMetrics.widthPixels,displayMetrics.heightPixels);
        return;
    }

}

