package com.innovisionate.phabletsignaturepad;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

public class pkContainer extends ViewGroup {
   
   public pkContainer(Context context) {
   super(context);
   return;
   }

   @Override
   protected void onLayout(boolean changed, int left, int top, int right, int bottom) {

   signaturePanel sp = (signaturePanel)getChildAt(0);

   sp.layout(left,top,right,bottom);

   sp.setVisibility(VISIBLE);
   
   for ( int k = 1; k < getChildCount(); k++ ) {
      View v = (View)getChildAt(k);
      pkUIObject p = (pkUIObject)v;
      v.layout(p.X(),p.Y(),p.X() + p.Width(),p.Y() + p.Height());
   }
   
   return;
   }
   
   @Override
   public boolean shouldDelayChildPressedState() {
   return false;
   }

   /* */
   @Override
   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

   for ( int k = 1; k < getChildCount(); k++ )
      measureChild(getChildAt(k),widthMeasureSpec,heightMeasureSpec);
   
   super.onMeasure(widthMeasureSpec,heightMeasureSpec);

   return;
   }
   /* */
}
