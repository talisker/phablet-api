package com.innovisionate.phabletsignaturepad;

//import android.support.v7.app.ActionBar;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Switch;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class Settings extends Activity {

   private boolean entrySwitchValue = false;

   @Override
   protected void onCreate(Bundle savedInstanceState) {

      super.onCreate(savedInstanceState);

      getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

      setContentView(R.layout.settings_main);

      ((TextView)findViewById(R.id.ipAddress)).setText(PhabletSignaturePad.myIPAddress);
      ((TextView)findViewById(R.id.widthValue)).setText(String.valueOf(PhabletSignaturePad.theSignaturePadView.Width()));
      ((TextView)findViewById(R.id.heightValue)).setText(String.valueOf(PhabletSignaturePad.theSignaturePadView.Height()));
      ((TextView)findViewById(R.id.versionValue)).setText(((TextView)findViewById(R.id.versionValue)).getText().toString() + " " + BuildConfig.VERSION_NAME);

      SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM. yyyy");

      ((TextView)findViewById(R.id.buildDateValue)).setText(((TextView)findViewById(R.id.buildDateValue)).getText().toString() + " " + formatter.format(BuildConfig.TIMESTAMP));


      if ( ! ( 0 == PhabletSignaturePad.properties.hideActionBarTimeout ) )
         ((Switch)findViewById(R.id.hideActionBar)).setChecked(true);
      else
         ((Switch)findViewById(R.id.hideActionBar)).setChecked(false);

      if ( PhabletSignaturePad.properties.useFullScreen )
         ((Switch)findViewById(R.id.useFullScreen)).setChecked(true);
      else
         ((Switch)findViewById(R.id.useFullScreen)).setChecked(false);

      entrySwitchValue = ((Switch)findViewById(R.id.hideActionBar)).isChecked();

      return;
   }

   @Override
   protected void onDestroy() {

      super.onDestroy();

      boolean doHideActionBar = false;

      if ( ((Switch)findViewById(R.id.hideActionBar)).isChecked() ) {
         PhabletSignaturePad.properties.hideActionBarTimeout = 10000;
         doHideActionBar = true;
      } else {
         PhabletSignaturePad.properties.hideActionBarTimeout = 0;
         doHideActionBar = false;
      }

      if ( ((Switch)findViewById(R.id.useFullScreen)).isChecked() )
         PhabletSignaturePad.properties.useFullScreen = true;
      else
         PhabletSignaturePad.properties.useFullScreen = false;

      PhabletSignaturePad.SavePreferences();

      if ( ( ! entrySwitchValue ) && doHideActionBar ) {
         new CountDownTimer( PhabletSignaturePad.properties.hideActionBarTimeout, PhabletSignaturePad.properties.hideActionBarTimeout) {

            public void onTick(long t) {
               return;
            }

            public void onFinish() {
               if ( 0 == PhabletSignaturePad.properties.hideActionBarTimeout )
                  return;
               PhabletSignaturePad.theMainActivity.getActionBar().hide();
               PhabletSignaturePad.theMainActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
               return;
            }
         }.start();
      }
      return;
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      return PhabletSignaturePad.theMainActivity.doOptionsMenu(R.id.action_settings,menu);
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      boolean rv = PhabletSignaturePad.theMainActivity.onOptionsItemSelected(item);
      if ( rv )
         finish();
      return rv;
   }
   
}
