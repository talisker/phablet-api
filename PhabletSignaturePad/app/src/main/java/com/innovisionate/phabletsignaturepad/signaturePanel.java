package com.innovisionate.phabletsignaturepad;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import static com.innovisionate.phabletsignaturepad.PhabletSignaturePad.sendEvent;

public class signaturePanel extends View {

   public Paint paint = new Paint();
   public int backgroundColor = Color.WHITE;
   
   private Bitmap theBitmap = null;
   private Bitmap theBitmapClone = null;
   private boolean isHidden = false;   
   private Path path = null;

   public int lastX = 0;
   public int lastY = 0;

   private List<signaturePoint> signaturePoints = new ArrayList<signaturePoint>();

   private int activePointerId = 0;

/*
   private static LinkedBlockingQueue<android.view.MotionEvent> motionEventsQueue = null;
*/
   Canvas signatureBitmapCanvas = null;

   private bezierCurves bezierCurves = new bezierCurves();
   private float lastPenWidth;
   private float lastSpeed;
   private float speedFilterWeight = 0.9f;

   public signaturePanel(Context context) {

      super(context);

      paint.setColor(Color.BLACK);
      paint.setStrokeWidth(2.0f);

      paint.setAntiAlias(true);
      paint.setStyle(Paint.Style.STROKE);
      paint.setStrokeCap(Paint.Cap.ROUND);
      paint.setStrokeJoin(Paint.Join.ROUND);

      path = new Path();

      return;
   }

   /*
   private void doTouchEvents() {

      while ( true ) {

         MotionEvent event = null;

         try {

            event = motionEventsQueue.take();

         } catch (InterruptedException e) {
            e.printStackTrace();
            break;
         }

         int pointerCount = event.getPointerCount();

         if ( android.view.MotionEvent.ACTION_DOWN == event.getAction() ) {
            if ( !(0 == activePointerId) )
               continue;
            sendEvent(PhabletSignaturePad.EVENT_PENDOWN + " " + event.getX() + " " + event.getY());
            continue;
         }

         if ( android.view.MotionEvent.ACTION_UP == event.getAction() ) {
            sendEvent(PhabletSignaturePad.EVENT_PENUP + " " + event.getX() + " " + event.getY());
            lastX = 0;
            lastY = 0;
            signaturePoints.clear();
            lastPenWidth = PhabletSignaturePad.properties.inkWeight;
            activePointerId = 0;
            continue;
         }

         if ( android.view.MotionEvent.ACTION_MOVE == event.getAction() ) {

            if ( ! ( 1 == PhabletSignaturePad.properties.sendPoints ) && ! ( 1 == PhabletSignaturePad.properties.showInk ) )
               continue;

            int historySize = event.getHistorySize();

            for (int k = 0; k < historySize; k++)
               for (int j = 0; j < pointerCount; j++)
                  addPoint(new signaturePoint(event.getHistoricalX(j, k), event.getHistoricalY(j, k), event.getHistoricalEventTime(k)));

            for (int k = 0; k < pointerCount; k++)
               addPoint(new signaturePoint(event.getX(k), event.getY(k), event.getEventTime()));

            continue;
         }

      }

   }
*/

   public void startPenEvents() {

      signaturePoints.clear();

      lastPenWidth = PhabletSignaturePad.properties.inkWeight;
/*
   //
   // 7-28-2018 - Putting the events out to a blocking queue does NOT seem to help in improving
   // "smoothness" - therefore I do not believe that non-smoothness is caused by motion events
   // getting dropped somehow - I believe that Android is not sending the events at a high
   // enough resolution when the user draws fast.
   //
      motionEventsQueue = new LinkedBlockingQueue<MotionEvent>();

      new Thread(new Runnable() { public void run() { doTouchEvents(); } }).start();
*/
      setOnTouchListener(new OnTouchListener() {

         @Override
         public boolean onTouch(View v, android.view.MotionEvent event) {
/*
            int pointerCount = event.getPointerCount();

            if ( 1 == pointerCount )
               activePointerId = event.getPointerId(0);
            else if ( ! ( event.getPointerId(1) == activePointerId ) )
               return true;

            try {
               motionEventsQueue.put(event);
            } catch ( InterruptedException e ) {
               e.printStackTrace();
            }
*/
/* */
            int pointerCount = event.getPointerCount();

            if ( 1 == pointerCount )
               activePointerId = event.getPointerId(0);
            else if ( ! ( event.getPointerId(1) == activePointerId ) )
               return true;

            if ( android.view.MotionEvent.ACTION_DOWN == event.getAction() ) {
               if ( ! ( 0 == activePointerId ) )
                  return true;
               sendEvent(PhabletSignaturePad.EVENT_PENDOWN + " " + event.getX() + " " + event.getY());
               return true;
            }

            if ( android.view.MotionEvent.ACTION_UP == event.getAction() ) {
               sendEvent(PhabletSignaturePad.EVENT_PENUP + " " + event.getX() + " " + event.getY());
               lastX = 0;
               lastY = 0;
               signaturePoints.clear();
               lastPenWidth = PhabletSignaturePad.properties.inkWeight;
               activePointerId = 0;
               return true;
            }

            if ( android.view.MotionEvent.ACTION_MOVE == event.getAction() ) {

               if ( ! ( 1 == PhabletSignaturePad.properties.sendPoints ) && ! ( 1 == PhabletSignaturePad.properties.showInk ) )
                  return true;

               int historySize = event.getHistorySize();

               for ( int k = 0; k < historySize; k++ )
                  for ( int j = 0; j < pointerCount; j++ )
                     addPoint(new signaturePoint(event.getHistoricalX(j, k), event.getHistoricalY(j, k), event.getHistoricalEventTime(k)));

               for ( int k = 0; k < pointerCount; k++ )
                  addPoint(new signaturePoint(event.getX(k), event.getY(k), event.getEventTime()));

               return true;

            }
/* */
            return true;
         }
      });

      return;
   }

   public void stopPenEvents() {
      setOnTouchListener(null);
      return;
   }

   @Override
   public void onDraw(Canvas canvas) {

      super.onDraw(canvas);

      if ( null == theBitmap )
         CreateBitmap();

      canvas.drawBitmap(theBitmap,null,new Rect(0,0,Width(),Height()),null);

      canvas.drawPath(path, paint);

      return;
   }


   public Bitmap TheBitmap() {
   return theBitmap;
   }
   
   public void TheBitmap(Bitmap v) {

      signatureBitmapCanvas = null;

      if ( null == theBitmap && ! ( null == v ) ) {
         if ( ! ( v.getWidth() == Width() ) || ! ( v.getHeight() == Height() ) ) {
            theBitmap = Bitmap.createBitmap(Width(), Height(), Bitmap.Config.ARGB_8888);
            signatureBitmapCanvas = new Canvas(theBitmap);
            signatureBitmapCanvas.drawColor(backgroundColor);
            int x = Width()/2 - v.getWidth()/2;
            int y = Height()/2 - v.getHeight()/2;
            signatureBitmapCanvas.drawBitmap(v,new Rect(0,0,v.getWidth(),v.getHeight()),new Rect(x, y,x + v.getWidth(),y + v.getHeight()),null);
            theBitmapClone = Bitmap.createBitmap(theBitmap);
            return;
         }
      }

      if ( ! ( null == v ) ) {

         if ( ! ( null == theBitmap ) && ( ! ( v.getWidth() == Width() ) || ! ( v.getHeight() == Height() ) ) ) {
            signatureBitmapCanvas = new Canvas(theBitmap);
            signatureBitmapCanvas.drawBitmap(v,new Rect(0,0,v.getWidth(),v.getHeight()),new Rect(0,0,Width(),Height()),null);
            theBitmapClone = Bitmap.createBitmap(theBitmap);
            return;
         }

      }

      theBitmap = v;

      if ( null == signatureBitmapCanvas && ! ( null == theBitmap ) ) {
         theBitmapClone = Bitmap.createBitmap(theBitmap);
         signatureBitmapCanvas = new Canvas(theBitmap);
      }

   return;
   }

   public void CreateBitmap() {

      theBitmap = Bitmap.createBitmap(Width(), Height(), Bitmap.Config.ARGB_8888);
      signatureBitmapCanvas = new Canvas(theBitmap);
      signatureBitmapCanvas.drawColor(backgroundColor);

      return;
   }

   public Paint ThePaint() {
   return paint;
   }
   
   public int Width() {
   return getWidth();
   }
   
   public int Height() {
   return getHeight();
   }
   
   public float InchesWidth() {
   return PhabletSignaturePad.ToInches(getWidth());
   }

   public float InchesHeight() {
      return PhabletSignaturePad.ToInches(getHeight());
   }
   
   public void Hide() {
      isHidden = true;
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.thePainter);
      return;
   }
   
   public void Show() {
      isHidden = false;
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.thePainter);
      return;
   }
      
   public void clearPath() {
      path = new Path();
      lastX = 0;
      lastY = 0;
      signaturePoints.clear();
      TheBitmap(theBitmapClone);
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.thePainter);
      return;
   }

   public void addPoint(signaturePoint p) {

      if ( true ) {

         if ( 0 == signaturePoints.size() ) {
            signaturePoints.add(p);
            sendEvent("" + PhabletSignaturePad.EVENT_PENPOINT + " " + p.X + " " + p.Y + " 0.0");
            return;
         }

         signaturePoint lastPoint = signaturePoints.get(signaturePoints.size() - 1);

         float speed = p.speedFrom(lastPoint);

         speed = Float.isNaN(speed) ? 0.0f : speed;

         speed = speedFilterWeight * speed + (1.0f - speedFilterWeight) * lastSpeed;

         float newWidth = strokeWidth(speed);

         signaturePoints.add(p);
/* */
         paint.setStrokeWidth(newWidth);

         signatureBitmapCanvas.drawLine(lastPoint.X,lastPoint.Y,p.X, p.Y, paint);
/* */
         sendEvent("" + PhabletSignaturePad.EVENT_PENPOINT + " " + p.X + " " + p.Y + " " + newWidth);
/* */
         invalidate();
/* */
         lastSpeed = speed;

         lastPenWidth = newWidth;

         return;
      }

      //      if ( 1 < signaturePoints.size() ) {
      //         signaturePoint lastPoint = signaturePoints.get(signaturePoints.size() - 1);
      //         if ( Math.abs(p.X - lastPoint.X) < 3 && Math.abs(p.Y - lastPoint.Y) < 3 )
      //            return;
      //      }

      signaturePoints.add(p);

      if ( 4 > signaturePoints.size() )
         return;

      int startIndex = signaturePoints.size() - 1;

      bezierCurves.controlPoints tmp = bezierCurves.getControlPoints(signaturePoints.get(startIndex - 3),signaturePoints.get(startIndex - 2),signaturePoints.get(startIndex - 1));

      signaturePoint save1 = tmp.point2;

      tmp = bezierCurves.getControlPoints(signaturePoints.get(startIndex - 2),signaturePoints.get(startIndex - 1),signaturePoints.get(startIndex));

      signaturePoint save2 = tmp.point1;

      bezierCurves.curve curve = bezierCurves.getCurve(signaturePoints.get(startIndex - 1), save1, save2, signaturePoints.get(startIndex));

      float speed = signaturePoints.get(startIndex).speedFrom(signaturePoints.get(startIndex - 1));

      speed = Float.isNaN(speed) ? 0.0f : speed;

      speed = speedFilterWeight * speed + (1.0f - speedFilterWeight) * lastSpeed;

      float newWidth = strokeWidth(speed);

      addCurve(curve,lastPenWidth,newWidth);

      lastSpeed = speed;

      lastPenWidth = newWidth;

      return;
   }

   private void addCurve(bezierCurves.curve curve, float startWidth, float endWidth) {

      float widthDelta = endWidth - startWidth;
      float drawSteps = (float) Math.floor(curve.length());

      for ( int k = 0; k < drawSteps; k++ ) {

         float t = ((float) k) / drawSteps;
         float tt = t * t;
         float ttt = tt * t;

         float u = 1.0f - t;
         float uu = u * u;
         float uuu = uu * u;

         float x = uuu * curve.startPoint.X;
         x += 3 * uu * t * curve.control1.X;
         x += 3 * u * tt * curve.control2.X;
         x += ttt * curve.endPoint.X;

         float y = uuu * curve.startPoint.Y;
         y += 3 * uu * t * curve.control1.Y;
         y += 3 * u * tt * curve.control2.Y;
         y += ttt * curve.endPoint.Y;

         float inkWeight = startWidth + ttt * widthDelta;

         paint.setStrokeWidth(inkWeight);

         signatureBitmapCanvas.drawPoint(x, y, paint);

         if ( 1 == PhabletSignaturePad.properties.sendPoints )
            sendEvent("" + PhabletSignaturePad.EVENT_PENPOINT + " " + x + " " + y + " " + inkWeight);

      }

      invalidate();

      return;
   }

   private float strokeWidth(float speed) {
      return Math.max(2.25f * PhabletSignaturePad.properties.inkWeight / (speed + 1.0f), PhabletSignaturePad.properties.inkWeight);
   }
}
