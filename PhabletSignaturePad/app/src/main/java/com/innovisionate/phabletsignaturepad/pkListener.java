package com.innovisionate.phabletsignaturepad;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.apache.http.conn.util.InetAddressUtils;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

public class pkListener extends Service {
   
   private ServerSocket serverSocket = null;
   private Socket clientSocket = null;
   private boolean shutdownRequested = false;
   private boolean disconnectRequested = false;
   private int recieveBufferSize = 0;

   private commandProcessor theCommandProcessor = null;
   
   @Override
   public IBinder onBind(Intent arg0) {
   return null;
   }

   @Override
   public int onStartCommand(Intent intent,int flags,int startId) {
   new Thread(new Runnable() { public void run() { serviceLoop(); } }).start();
   //Toast.makeText(this, "The Command Listener service has started", Toast.LENGTH_LONG).show();
   return START_NOT_STICKY;
   }

   @Override
   public void onDestroy() {
   super.onDestroy();
   shutdownRequested = true;
   try {
   if ( ! ( null == clientSocket ) )
      clientSocket.close();
   if ( ! ( null == serverSocket ) )
      serverSocket.close();
   } catch ( IOException e ) {
      e.printStackTrace();
   }
   System.out.println("The Command Listener service has stopped. 1");
   return;
   }

   
   public void serviceLoop() {

   try {

   List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
   
   for ( NetworkInterface intf : interfaces ) {
   
      List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
      
      for ( InetAddress addr : addrs ) {
         if ( ! addr.isLoopbackAddress() ) {
            String address = addr.getHostAddress().toUpperCase();
            boolean isIPv4 = InetAddressUtils.isIPv4Address(address);
            if ( ! isIPv4 )
               continue;
            PhabletSignaturePad.myIPAddress = address;
            break;
         }
      }
      
      if ( ! ( null == PhabletSignaturePad.myIPAddress ) )
         break;
      
   }

   } catch ( Exception ex ) { }

   shutdownRequested = false;
   disconnectRequested = false;

   while ( ! shutdownRequested ) {
   
      try {

      serverSocket = new ServerSocket(17639);

      recieveBufferSize = serverSocket.getReceiveBufferSize();

      serverSocket.setSoTimeout(0);

      } catch ( IOException e ) {
         PhabletSignaturePad.toastMessage = "The pkListener failed while trying to make the server socket.";
         PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theToastRunnable);
         serverSocket = null;
         e.printStackTrace();
         break;
      }

      DataInputStream dataInputStream = null;

      DataOutputStream dataOutputStream = null;
   
      System.out.println("Waiting on " + PhabletSignaturePad.myIPAddress + " for a connection to port 17639");

      clientSocket = null;
      
      try {
      
      clientSocket = serverSocket.accept();

      } catch ( IOException e ) {
         e.printStackTrace();
         break;
      }

      PhabletSignaturePad.RestorePreferences();
      
      PhabletSignaturePad.actionComplete = new CountDownLatch(2);
      
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theStartEventDispatcherRunnable);

      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theStartControlListenerRunnable);
      
      try {

      PhabletSignaturePad.actionComplete.await();

      } catch ( InterruptedException e ) {
         PhabletSignaturePad.toastMessage = "The pkListener failed while starting the support threads.";
         PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theToastRunnable);
         e.printStackTrace();
         break;
      }
      
      PhabletSignaturePad.clientAddress = clientSocket.getInetAddress();
      
      System.out.println("connection accepted: " + PhabletSignaturePad.clientAddress);

      try {

      //dataInputStream = clientSocket.getInputStream();//new DataInputStream(clientSocket.getInputStream());
      dataInputStream = new DataInputStream(clientSocket.getInputStream());

      dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());

      dataOutputStream.write("welcome to pkListener".getBytes("UTF-16LE"));//writeBytes("welcome to pkListener");
      
      } catch ( IOException e ) {
         PhabletSignaturePad.toastMessage = "The pkListener failed while writing the initial string to the connection.";
         PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theToastRunnable);
         e.printStackTrace();
         break;
      }

      if ( ! ( null == theCommandProcessor ) )
         theCommandProcessor.stopPen();
      
      theCommandProcessor = new commandProcessor(this,dataInputStream,dataOutputStream);

      byte [] inBuffer = new byte[65536];

      while ( true ) {

         System.out.println("Waiting for the next command");

         int r = 0;

         try {

            r = dataInputStream.read(inBuffer,0,8);

         } catch ( Exception e ) {
            e.printStackTrace();
            break;
         }

         int bytesToRead = 0;
         
         try {

         String s = new String(inBuffer,0,8,Charset.forName("UTF-16LE"));

         bytesToRead = Integer.valueOf(s);
         
         } catch ( NumberFormatException e ) {

            System.out.println("Bad number format: " + new String(inBuffer,0,8,Charset.forName("UTF-16LE")) + " read returned: " + r);

            try {

            dataOutputStream.write(new String("Invalid size value:" + new String(inBuffer,0,8,Charset.forName("UTF-16LE"))).getBytes());

            } catch ( IOException e1 ) {
               e1.printStackTrace();
               break;
            }
            
            continue;
            
         }

         int bytesRead = 0;

         try {

         dataInputStream.readFully(inBuffer,0,bytesToRead);

         } catch ( IOException e ) {
            e.printStackTrace();
            break;
         }

         String inputString = new String(inBuffer,0,bytesToRead,Charset.forName("UTF-16LE"));

//         System.out.println("command: " + inputString );

         if ( inputString.equals("disconnect") ) {
            disconnectRequested = true;
            break;
         }

         if ( inputString.equals("shutdown") ) {
            shutdownRequested = true;
            break;
         }

         try {

         theCommandProcessor.process(inputString);

         } catch ( IOException e ) {
            e.printStackTrace();
            break;
         }

      }
      
      theCommandProcessor.stopPen();
      
      System.out.println("Stopping the Event Dispatcher");
      System.out.println("Stopping the Control Listener");
      
      PhabletSignaturePad.actionComplete = new CountDownLatch(2);
      
      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theStopEventDispatcherRunnable);

      PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theStopControlListenerRunnable);

      try {

      PhabletSignaturePad.actionComplete.await();

      } catch ( InterruptedException e ) {
         PhabletSignaturePad.toastMessage = "The pkListener failed while stopping the support threads.";
         PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theToastRunnable);
         e.printStackTrace();
         break;
      }      

      System.out.println("The PK Listener is disconnecting from the client.");

      if ( disconnectRequested ) {

         try {

         dataOutputStream.write("ok".getBytes("UTF-16LE"));

         } catch(IOException e) {
            e.printStackTrace();
         }

      }

      try {

      if ( ! ( null == clientSocket ) )
         clientSocket.close();

      if ( ! ( null == serverSocket ) )
         serverSocket.close();

      } catch ( IOException e ) {
         e.printStackTrace();
      }

      clientSocket = null;

      serverSocket = null;

      if ( shutdownRequested )
         break;
   
   }

   System.out.println("The PK Listener is shutting down.");

   try {
   if ( ! ( null == clientSocket ) )
      clientSocket.close();
   if ( ! ( null == serverSocket ) )
      serverSocket.close();
   } catch ( IOException e ) {
      e.printStackTrace();
   }

   serverSocket = null;
   clientSocket = null;
   
   System.out.println("The Command Listener service has stopped. 2");
   
   return;
   }

}

