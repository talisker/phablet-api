/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.innovisionate.phabletsignaturepad.licensing;

import android.content.SharedPreferences;
import android.util.Log;

/**
 * An wrapper for SharedPreferences that transparently performs data obfuscation.
 */
public class PreferenceObfuscator {

    private static final String TAG = "PreferenceObfuscator";

    private final SharedPreferences preferences;
    private final Obfuscator obfuscator;
    private SharedPreferences.Editor editor;

    /**
     * Constructor.
     *
     * @param sp A SharedPreferences instance provided by the system.
     * @param pTHISFUCKINGPARAMETER_WAS_o The Obfuscator to use when reading or writing data. // ???? YOU HAVE GOT TO BE KIDDING !?!?!
     */
   public PreferenceObfuscator(SharedPreferences sp, Obfuscator pTHISFUCKINGPARAMETER_WAS_o) {
   preferences = sp;
   obfuscator = pTHISFUCKINGPARAMETER_WAS_o;
   editor = null;
   return;
   }

   public void putString(String key, String value) {
   if ( null == editor )
      editor = preferences.edit();
   String obfuscatedValue = obfuscator.obfuscate(value, key);
   editor.putString(key, obfuscatedValue);
   return;
   }

   public String getString(String key, String defValue) {

   String value = preferences.getString(key, null);

   if ( null == value )
         return defValue;

   try {
   return obfuscator.unobfuscate(value, key);
   } catch ( ValidationException e ) {
      Log.w(TAG, "Validation error while reading preference: " + key);
      return defValue;
   }

   }

   public void commit() {
   if ( null == editor )
      return;
   editor.commit();
   editor = null;
   return;
   }
}
