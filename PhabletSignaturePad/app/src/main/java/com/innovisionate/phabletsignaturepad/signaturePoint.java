package com.innovisionate.phabletsignaturepad;

import static android.os.SystemClock.uptimeMillis;

public class signaturePoint {

   public float X;
   public float Y;
   public long timeStamp;

   public signaturePoint(float x,float y) {
      X = x;
      Y = y;
      timeStamp = uptimeMillis();
      return;
   }

   public signaturePoint(float x,float y,long t) {
      X = x;
      Y = y;
      timeStamp = t;
      return;
   }

   //   public signaturePoint set(float x, float y) {
//      X = x;
//      Y = y;
//      timeStamp = System.currentTimeMillis();
//      return this;
//   }

   public float speedFrom(signaturePoint start) {
      long delta = timeStamp - start.timeStamp;
      if ( 0 > delta )
         delta = 1;
      float speed = distanceTo(start) / (float)delta;
      if ( Float.isInfinite(speed) || Float.isNaN(speed) )
         return 0.0f;
      return speed;
   }

   public float distanceTo(signaturePoint point) {
      return (float) Math.sqrt(Math.pow(point.X - X, 2) + Math.pow(point.Y - Y, 2));
   }

}
