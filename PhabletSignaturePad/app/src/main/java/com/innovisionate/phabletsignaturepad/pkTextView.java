package com.innovisionate.phabletsignaturepad;

import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.TypedValue;
import android.widget.TextView;
//import android.support.v7.widget.AppCompatTextView;

//public class pkTextView extends AppCompatTextView implements pkUIObject {
public class pkTextView extends TextView implements pkUIObject {

   private int x,y,width,height;
   private int currentRequestedVisibility;

   private Rect textSize;
   private Typeface typeFace;
   private float fontSize;
   private float textFontHeightPixels;
   
   pkTextView() {

   super(PhabletSignaturePad.theMainActivity);

   // CREATELABEL x, y, id, isVisible, text, fontFace, fontSize

   setId(Integer.valueOf(PhabletSignaturePad.controlDetails[3]));
   
   x = Integer.valueOf(PhabletSignaturePad.controlDetails[1]);
   
   y = Integer.valueOf(PhabletSignaturePad.controlDetails[2]);

   textSize = new Rect();
   
   setIncludeFontPadding(false);

   setText(PhabletSignaturePad.controlDetails[5].replaceAll("\"", "").trim());
   
   setLines(1);
   
   setSingleLine(true);

   currentRequestedVisibility = "1".equals(PhabletSignaturePad.controlDetails[4]) ? android.view.View.VISIBLE : android.view.ViewGroup.INVISIBLE;

   if ( 1 == PhabletSignaturePad.controlUpdatesShowing )
      setVisibility(currentRequestedVisibility);
   else
      setVisibility(android.view.ViewGroup.INVISIBLE);

   String face = PhabletSignaturePad.controlDetails[6].replaceAll("\"", "").trim();
   
   if ( 0 < face.length() )

      typeFace = Typeface.create(face,Typeface.NORMAL);
     
   else
      
      typeFace = Typeface.create(PhabletSignaturePad.properties.fontFamily,Typeface.NORMAL);

   fontSize = Float.valueOf(PhabletSignaturePad.controlDetails[7]);
   
   if ( 0 == fontSize )
      fontSize = PhabletSignaturePad.properties.fontSize;
   
   //fontSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PT,Float.valueOf(PhabletSignaturePad.controlDetails[7]),PhabletSignaturePad.theMainActivity.getBaseContext().getResources().getDisplayMetrics());

   setTypeface(typeFace);

   setTextSize(TypedValue.COMPLEX_UNIT_PT,fontSize);

   measure(0,0);

   return;
   }

   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

   TextPaint paint = getPaint();

   String text = (String)getText();

   paint.getTextBounds(text,0,text.length(),textSize);

   width = textSize.width();

   Paint.FontMetrics fm = paint.getFontMetrics();

   height = (int)(fm.bottom - fm.top + fm.leading);

   setMeasuredDimension(width,height);

   textFontHeightPixels = paint.getTextSize();
   
   return;
   }
   
   public int X() {
   return x;
   }

   public int Y() {
   return y;
   }

   public int Width() {
   return width;
   }

   public int Height() {
   return height;
   }
 
   public int TextX() {
   return 0;
   }

   public int TextY() {
   return 0;
   }
   
   public int TextWidth() {
   return width;
   }
   
   public int TextHeight() {
   return height;
   }
   
   public void setX(int x) {
   this.x = x;
   return;
   }

   public void setY(int y) {
   this.y = y;
   return;
   }

   public void setWidth(int width) {
   this.width = width;
   return;
   }

   public void setHeight(int height) {
   this.height = height;
   return;
   }

   public void setRequestedVisibility(int visibility) {
   currentRequestedVisibility = visibility;
   return;
   }

   public void restoreVisibility() {
   this.setVisibility(currentRequestedVisibility);
   return;
   }

   public String Text() {
   return (String)getText();
   }
   
   public void setText(String v) {
   super.setText(v);
   return;
   }
   
   public int ScaledX() {
   return (int)((double)x * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledY() {
   return (int)((double)y * PhabletSignaturePad.scaleObjectsY);
   }

   public int ScaledWidth() {
   return (int)((double)width * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledHeight() {
   return (int)((double)height * PhabletSignaturePad.scaleObjectsY);
   }
   
   public float InchesX() {
   return PhabletSignaturePad.ToInches(x);
   }

   public float InchesY() {
   return PhabletSignaturePad.ToInches(y);
   }
   
   public float InchesWidth() {
   return PhabletSignaturePad.ToInches(width);
   }

   public float InchesHeight() {
   return PhabletSignaturePad.ToInches(height);
   }

   public float TextFontHeightPixels() { return textFontHeightPixels; }

}
