package com.innovisionate.phabletsignaturepad;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class pkEventsDispatcher extends Service {

   private ServerSocket serverSocket = null;
   private Socket clientSocket = null;

   private boolean shutdownRequested = false;
   
   @Override
   public IBinder onBind(Intent intent) {
   return null;
   }

   @Override
   public int onStartCommand(Intent intent,int flags,int startId) {
   new Thread(new Runnable() { public void run() { serviceLoop(); } }).start();
   System.out.println("The EventsDispatcher service has started");
   return START_NOT_STICKY;
   }

   @Override
   public void onDestroy() {
   super.onDestroy();
   shutdownRequested = true;
   PhabletSignaturePad.eventsQueue.clear();
   PhabletSignaturePad.eventsQueue.add("disconnect");
   try {
   if ( ! ( null == clientSocket ) )
      clientSocket.close();
   if ( ! ( null == serverSocket ) )
      serverSocket.close();
   clientSocket = null;
   serverSocket = null;
   } catch ( IOException e ) {
      e.printStackTrace();
   }   
   System.out.println("The Events Dispatcher service has stopped. 1");
   return;
   }
   
   
   public void serviceLoop() {

   try {

   serverSocket = new ServerSocket(17641);

   } catch ( IOException e ) {
      System.out.println("The pkEventsDispatcher failed while trying to make the server socket.");
      serverSocket = null;
      e.printStackTrace(); 
      if ( ! ( null == PhabletSignaturePad.actionComplete ) ) 
         PhabletSignaturePad.actionComplete.countDown();
      return;
   }

   shutdownRequested = false;
      
   while ( ! shutdownRequested ) {
      
      DataInputStream eventsInputStream = null;
   
      DataOutputStream eventsOutputStream = null;

      if ( ! ( null == PhabletSignaturePad.actionComplete ) )
         PhabletSignaturePad.actionComplete.countDown();

      clientSocket = null;
      
      try {

      clientSocket = serverSocket.accept();

      eventsInputStream = new DataInputStream(clientSocket.getInputStream());
      
      eventsOutputStream = new DataOutputStream(clientSocket.getOutputStream());

      } catch ( IOException e ) {
         e.printStackTrace();
         break;
      } 

      PhabletSignaturePad.sendEvent(String.valueOf(PhabletSignaturePad.EVENT_DEVICEREADY));
      
      byte[] inBuffer = new byte[512];
      
      while ( true ) {
         
         String event = null;
         
         try {
         
         event = PhabletSignaturePad.eventsQueue.take();
   
//         System.out.println("Event recieved: " + event);

         if ( event.equals("disconnect") ) 
            break;

         } catch ( InterruptedException e ) {
            e.printStackTrace();
            break;
         }
   
         try {
   
         eventsOutputStream.write(event.getBytes("UTF-16LE"));
   
         eventsInputStream.read(inBuffer);
   
         } catch ( IOException e ) {
            e.printStackTrace();
            break;
         }

      }
      
      try {
      if ( ! ( null == clientSocket ) )
         clientSocket.close();
      clientSocket = null;
      } catch ( IOException e ) {
         e.printStackTrace();
         continue;
      }

      if ( shutdownRequested )
         break;
      
   }
   
   try {

   if ( ! ( null == serverSocket ) )
      serverSocket.close();
   
   if ( ! ( null == clientSocket ) )
      clientSocket.close();
   
   } catch ( IOException e ) {
      e.printStackTrace();
   }

   if ( ! ( null == PhabletSignaturePad.actionComplete ) )
      PhabletSignaturePad.actionComplete.countDown();
   
   serverSocket = null;

   clientSocket = null;

   System.out.println("The Events Dispatcher service has stopped. 2");
   
   return;
   }
   
}

