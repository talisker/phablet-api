package com.innovisionate.phabletsignaturepad;

import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
//import android.support.v7.widget.AppCompatButton;

//public class pkButton extends AppCompatButton implements pkUIObject, View.OnClickListener {
public class pkButton extends Button implements pkUIObject, View.OnClickListener {

   private int x,y,width,height;
   private int currentRequestedVisibility;
   
   private Rect textSize;
   private RectF textSizeF;
   private float textFontHeightPixels;

   private Typeface typeFace;

   pkButton() {

   super(PhabletSignaturePad.theMainActivity);

   // CREATEBUTTON x, y, id, isVisible, text

   setId(Integer.valueOf(PhabletSignaturePad.controlDetails[3]));
   
   x = Integer.valueOf(PhabletSignaturePad.controlDetails[1]);
   
   y = Integer.valueOf(PhabletSignaturePad.controlDetails[2]);

   currentRequestedVisibility = "1".equals(PhabletSignaturePad.controlDetails[4]) ? android.view.View.VISIBLE : android.view.ViewGroup.INVISIBLE;

   textSize = new Rect();
   textSizeF = new RectF();
   
   setIncludeFontPadding(false);

   if ( 1 == PhabletSignaturePad.controlUpdatesShowing )
      setVisibility(currentRequestedVisibility);
   else
      setVisibility(android.view.ViewGroup.INVISIBLE);

   setOnClickListener(this);

   typeFace = Typeface.create(PhabletSignaturePad.properties.fontFamily,Typeface.NORMAL);
   
   //fontSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PT,PhabletSignaturePad.properties.fontSize,PhabletSignaturePad.theMainActivity.getBaseContext().getResources().getDisplayMetrics());
   
   setTypeface(typeFace);

   setTextSize(TypedValue.COMPLEX_UNIT_PT,PhabletSignaturePad.properties.fontSize);
   
   setText(PhabletSignaturePad.controlDetails[5]);
   
   setTextScaleX(1.0f);

   getBackground().setAlpha(255);

   measure(0,0);

   return;
   }

   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

   Paint paint = getPaint();
   
   paint.setTypeface(typeFace);
   
   paint.setTextSize(getTextSize());
   
   String text = (String)getText();
   
   paint.getTextBounds(text,0,text.length(),textSize);
   
   width = Math.max((int)((double)(textSize.right - textSize.left) / 0.5),getSuggestedMinimumWidth());
   
   height = Math.max((int)((double)(textSize.bottom - textSize.top) / 0.4),getSuggestedMinimumHeight());
   
   setMeasuredDimension(width,height);
   
   Path textPath = new Path();

   paint.getTextPath(text, 0, text.length(), 0.0f, 0.0f, textPath);

   textPath.computeBounds(textSizeF, true);

   textFontHeightPixels = paint.getTextSize();

   return;
   }   
   
   public int X() {
   return x;
   }

   public int Y() {
   return y;
   }

   public int Width() {
   return width;
   }

   public int Height() {
   return height;
   }
   
   public int TextX() {
   int rv = (int)textSizeF.left;
   if ( 0.5 > textSizeF.left - rv )
      return rv;
   return ++rv;
   }

   public int TextY() {
   int rv = (int)textSizeF.top;
   if ( 0.5 > textSizeF.top - rv )
      return rv;
   return ++rv;
   }
   
   public int TextWidth() {
   int rv = (int)textSizeF.width();
   if ( 0.5 > textSizeF.width() - rv )
      return rv;
   return ++rv;
   }
   
   public int TextHeight() {
   int rv = (int)textSizeF.height();
   if ( 0.5 > textSizeF.height() - rv )
      return rv;
   return ++rv;
   }
   
   public void setX(int x) {
   this.x = x;
   return;
   }

   public void setY(int y) {
   this.y = y;
   return;
   }

   public void setWidth(int width) {
   this.width = width;
   return;
   }

   public void setHeight(int height) {
   this.height = height;
   return;
   }

   public void setRequestedVisibility(int visibility) {
   currentRequestedVisibility = visibility;
   }

   public void restoreVisibility() {
   this.setVisibility(currentRequestedVisibility);
   }

   public String Text() {
   return (String)getText();
   }
   
   public void setText(String v) {
   super.setText(v);
   return;
   }
      
   public int ScaledX() {
   return (int)((double)x * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledY() {
   return (int)((double)y * PhabletSignaturePad.scaleObjectsY);
   }

   public int ScaledWidth() {
   return (int)((double)width * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledHeight() {
   return (int)((double)height * PhabletSignaturePad.scaleObjectsY);
   }
   
   public float InchesX() {
   return PhabletSignaturePad.ToInches(x);
   }

   public float InchesY() {
   return PhabletSignaturePad.ToInches(y);
   }
   
   public float InchesWidth() {
   return PhabletSignaturePad.ToInches(width);
   }

   public float InchesHeight() {
   return PhabletSignaturePad.ToInches(height);
   }

   public float TextFontHeightPixels() { return textFontHeightPixels; }

   @Override
   public void onClick(View v) {
   PhabletSignaturePad.sendEvent(String.valueOf(PhabletSignaturePad.EVENT_OPTIONSELECTED) + " " + getId());
   return;
   }

   
}
