package com.innovisionate.phabletsignaturepad;

import java.util.AbstractMap;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.RadioButton;
//import android.support.v7.widget.AppCompatRadioButton;

//public class pkRadioButton extends AppCompatRadioButton implements pkUIObject, View.OnClickListener {
public class pkRadioButton extends RadioButton implements pkUIObject, View.OnClickListener {

   private int x,y,width,height;
   private int currentRequestedVisibility;

   private int radioButtonWidth,radioButtonHeight;
   private int radioButtonMinimumHeight;
   
   private Rect textSize;
   private RectF textSizeF;
   private Typeface typeFace;
   private float fontSize;
   private Path textPath;
   private float textFontHeightPixels;
   
   private static HashMap<pkRadioButton, Integer> radioButtonGroups = null;
   
   private int groupNumber;
   
   pkRadioButton() {
      
   super(PhabletSignaturePad.theMainActivity);

   // CREATERADIOBUTTON x, y, id, isVisible, isChecked, groupNumber, text
   
   setId(Integer.valueOf(PhabletSignaturePad.controlDetails[3]));
   
   x = Integer.valueOf(PhabletSignaturePad.controlDetails[1]);
   
   y = Integer.valueOf(PhabletSignaturePad.controlDetails[2]);

   textSize = new Rect();
   
   textSizeF = new RectF();
   
   textPath = new Path();
   
   setIncludeFontPadding(false);
   
   setText(PhabletSignaturePad.controlDetails[7].replaceAll("\"","").trim());

   currentRequestedVisibility = "1".equals(PhabletSignaturePad.controlDetails[4]) ? android.view.View.VISIBLE : android.view.ViewGroup.INVISIBLE;

   if ( 1 == PhabletSignaturePad.controlUpdatesShowing )
      setVisibility(currentRequestedVisibility);
   else
      setVisibility(android.view.ViewGroup.INVISIBLE);

   setChecked(("1".equals(PhabletSignaturePad.controlDetails[5])));
   
   groupNumber = Integer.valueOf(PhabletSignaturePad.controlDetails[6]);
   
   setOnClickListener(this);
   
   if ( null == radioButtonGroups )
      radioButtonGroups = new HashMap<pkRadioButton, Integer>();
   
   radioButtonGroups.put(this, groupNumber);

   fontSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PT,PhabletSignaturePad.properties.fontSize,PhabletSignaturePad.theMainActivity.getBaseContext().getResources().getDisplayMetrics());

   typeFace = Typeface.create(PhabletSignaturePad.properties.fontFamily,Typeface.NORMAL);
   
   setTextSize(TypedValue.COMPLEX_UNIT_PT,fontSize);
   
   setTypeface(typeFace);
   
   int resID = 0;

   if ( Build.VERSION.SDK_INT <= 10 ) {
     // pre-Honeycomb has a different way of setting the CheckBox button drawable
      resID = Resources.getSystem().getIdentifier("btn_check", "drawable", "android");
   } else {
     // starting with Honeycomb, retrieve the theme-based indicator as CheckBox button drawable
     TypedValue value = new TypedValue();
     PhabletSignaturePad.theMainActivity.getBaseContext().getTheme().resolveAttribute(android.R.attr.listChoiceIndicatorMultiple, value, true);
     resID = value.resourceId;
   }

   Drawable cbDrawable = getResources().getDrawable(resID);
   
   radioButtonHeight = cbDrawable.getIntrinsicHeight();
   radioButtonWidth = cbDrawable.getIntrinsicWidth();
   radioButtonMinimumHeight = cbDrawable.getMinimumHeight();   

   measure(0,0);

   return;
   }

   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

   Paint paint = getPaint();
   paint.setTypeface(typeFace);
   paint.setTextSize(fontSize);
   String text = (String)getText();

   Paint.FontMetrics fm = paint.getFontMetrics();

   height = (int)((fm.bottom - fm.top + fm.leading) / 0.9);
   
   //paint.getTextBounds("My",0,2,textSize);

   //height = Math.max(radioButtonMinimumHeight,(int)((double)(textSize.bottom - textSize.top) / 0.75));

   paint.getTextBounds(text + "MMMM",0,text.length() + 4,textSize);
   
   width = textSize.right - textSize.left + radioButtonWidth;

   setMeasuredDimension(width,height);
   
   paint.getTextPath(text, 0, text.length(), 0.0f, 0.0f, textPath);

   textPath.computeBounds(textSizeF, true);

   textFontHeightPixels = paint.getTextSize();
   
   return;
   }
   
   public int X() {
   return x;
   }

   public int Y() {
   return y;
   }

   public int Width() {
   return width;
   }

   public int Height() {
   return height;
   }
 
   public int TextX() {
   int rv = (int)textSizeF.left;
   if ( 0.5 > textSizeF.left - rv )
      return rv;
   return ++rv;
   }

   public int TextY() {
   int rv = (int)textSizeF.top;
   if ( 0.5 > textSizeF.top - rv )
      return rv;
   return ++rv;
   }
   
   public int TextWidth() {
   int rv = (int)textSizeF.width();
   if ( 0.5 > textSizeF.width() - rv )
      return rv;
   return ++rv;
   }
   
   public int TextHeight() {
   int rv = (int)textSizeF.height();
   if ( 0.5 > textSizeF.height() - rv )
      return rv;
   return ++rv;
   }
   
   public void setX(int x) {
   this.x = x;
   return;
   }

   public void setY(int y) {
   this.y = y;
   return;
   }

   public void setWidth(int width) {
   this.width = width;
   return;
   }

   public void setHeight(int height) {
   this.height = height;
   return;
   }

   public void setRequestedVisibility(int visibility) {
   currentRequestedVisibility = visibility;
   return;
   }

   public void restoreVisibility() {
   this.setVisibility(currentRequestedVisibility);
   return;
   }

   public String Text() {
   return (String)getText();
   }
   
   public void setText(String v) {
   super.setText(v);
   return;
   }
   
   public int ScaledX() {
   return (int)((double)x * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledY() {
   return (int)((double)y * PhabletSignaturePad.scaleObjectsY);
   }

   public int ScaledWidth() {
   return (int)((double)width * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledHeight() {
   return (int)((double)height * PhabletSignaturePad.scaleObjectsY);
   }
   
   public float InchesX() {
   return PhabletSignaturePad.ToInches(x);
   }

   public float InchesY() {
   return PhabletSignaturePad.ToInches(y);
   }
   
   public float InchesWidth() {
   return PhabletSignaturePad.ToInches(width);
   }

   public float InchesHeight() {
   return PhabletSignaturePad.ToInches(height);
   }

   public float TextFontHeightPixels() { return textFontHeightPixels; }

   @Override
   public void onClick(View v) {
   setChecked(true);
   for ( Map.Entry<pkRadioButton, Integer> entry : radioButtonGroups.entrySet() ) {
      if ( entry.getKey().getId() == getId() )
         continue;
      if ( entry.getValue() == groupNumber )
         entry.getKey().setChecked(false);
   }
   PhabletSignaturePad.sendEvent(String.valueOf(PhabletSignaturePad.EVENT_OPTIONSELECTED) + " " + getId());
   invalidate();
   return;
   }
   
}
