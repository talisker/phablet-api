package com.innovisionate.phabletsignaturepad;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
//import android.support.v7.widget.AppCompatTextView;

//public class pkTextBox extends AppCompatTextView implements pkUIObject {
public class pkTextBox extends TextView implements pkUIObject {

   private int x,y,width,height;
   private int currentRequestedVisibility;
   public int maxHeight;
   
   private Typeface typeFace;
   private float textFontHeightPixels;
   
   public pkTextBox() {
      
   super(PhabletSignaturePad.theMainActivity);

   // CREATETEXTBOX x, y, width, maxHeight, id, isVisible, text, fontFace, fontSize

   setId(Integer.valueOf(PhabletSignaturePad.controlDetails[5]));
   
   x = Integer.valueOf(PhabletSignaturePad.controlDetails[1]);
   
   y = Integer.valueOf(PhabletSignaturePad.controlDetails[2]);

   width = Integer.valueOf(PhabletSignaturePad.controlDetails[3]);
   
   maxHeight = Integer.valueOf(PhabletSignaturePad.controlDetails[4]);

   setIncludeFontPadding(true);
   
   setSingleLine(false);
   
   String text = PhabletSignaturePad.controlDetails[7].replaceAll("\"", "").trim().replaceAll("\\\\n",System.getProperty ("line.separator"));
   
   setText(text,TextView.BufferType.SPANNABLE);

   currentRequestedVisibility = "1".equals(PhabletSignaturePad.controlDetails[6]) ? android.view.View.VISIBLE : android.view.ViewGroup.INVISIBLE;

   if ( 1 == PhabletSignaturePad.controlUpdatesShowing )
      setVisibility(currentRequestedVisibility);
   else
      setVisibility(android.view.ViewGroup.INVISIBLE);

   String face = PhabletSignaturePad.controlDetails[8].replaceAll("\"", "").trim();
   
   if ( 0 < face.length() )

      typeFace = Typeface.create(face,Typeface.NORMAL);
     
   else
      
      typeFace = Typeface.create(PhabletSignaturePad.properties.fontFamily,Typeface.NORMAL);

   if ( 0 == Float.valueOf(PhabletSignaturePad.controlDetails[9]) )
      setTextSize(TypedValue.COMPLEX_UNIT_PT,PhabletSignaturePad.properties.fontSize);
   else
      setTextSize(TypedValue.COMPLEX_UNIT_PT,Float.valueOf(PhabletSignaturePad.controlDetails[9]));

   setTypeface(typeFace);

   TextPaint textPaint = new TextPaint();
   
   textPaint.setTypeface(typeFace);
   
   textPaint.setAntiAlias(true);

   textPaint.setTextSize(getTextSize());
   
   StaticLayout staticLayout = new StaticLayout(text, textPaint, width, Layout.Alignment.ALIGN_NORMAL, 1, 0, false);
   
   height = staticLayout.getHeight();

   if ( height > maxHeight ) {
      setVerticalScrollBarEnabled(true);
      setMovementMethod(new ScrollingMovementMethod());
      setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
      height = maxHeight;
   }

   textFontHeightPixels = getTextSize();

   return;
   }
   
   
   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
   setMeasuredDimension(width,height);
   return;
   }   

   
   public String getLine(int lineNumber) {
      
   if ( 0 == getLineCount() ) {
      TextPaint textPaint = new TextPaint();
      textPaint.setTypeface(typeFace);
      textPaint.setAntiAlias(true);
      textPaint.setTextSize(getTextSize());
      StaticLayout staticLayout = new StaticLayout(getText(), textPaint, width, Layout.Alignment.ALIGN_NORMAL, 1, 0, false);
      return getLineFromLayout(staticLayout,lineNumber);
   }
   
   if ( lineNumber > getLineCount() )
      return "<unknown>";

   return getLineFromLayout(getLayout(),lineNumber);
   /*
   Layout layout = getLayout();
   String text = getText().toString();
   int start = 0;
   int end = 0;
   for ( int k = 0; k < lineNumber; k++) {
      start = end;
      end = layout.getLineEnd(k);
   }    
   return text.substring(start,end);
   */
   }
   
   private String getLineFromLayout(Layout theLayout,int lineNumber) {
   String text = getText().toString();
   int start = 0;
   int end = 0;
   for ( int k = 0; k < lineNumber; k++) {
      start = end;
      end = theLayout.getLineEnd(k);
   }    
   return text.substring(start,end);
   }
   
   public int X() {
   return x;
   }

   public int Y() {
   return y;
   }

   public int Width() {
   return width;
   }

   public int Height() {
   return height;
   }
 
   public int TextX() {
   return 0;
   }

   public int TextY() {
   return 0;
   }
   
   public int TextWidth() {
   return width;
   }
   
   public int TextHeight() {
   return height;
   }
   
   public void setX(int x) {
   this.x = x;
   return;
   }

   public void setY(int y) {
   this.y = y;
   return;
   }

   public void setWidth(int width) {
   this.width = width;
   return;
   }

   public void setHeight(int height) {
   this.height = height;
   return;
   }

   public void setRequestedVisibility(int visibility) {
   currentRequestedVisibility = visibility;
   return;
   }

   public void restoreVisibility() {
   this.setVisibility(currentRequestedVisibility);
   return;
   }

   public String Text() {
   return (String)getText().toString();
   }
   
   public void setText(String v) {
   super.setText(v);
   return;
   }
   
   public int ScaledX() {
   return (int)((double)x * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledY() {
   return (int)((double)y * PhabletSignaturePad.scaleObjectsY);
   }

   public int ScaledWidth() {
   return (int)((double)width * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledHeight() {
   return (int)((double)height * PhabletSignaturePad.scaleObjectsY);
   }
   
   public float InchesX() {
   return PhabletSignaturePad.ToInches(x);
   }

   public float InchesY() {
   return PhabletSignaturePad.ToInches(y);
   }
   
   public float InchesWidth() {
   return PhabletSignaturePad.ToInches(width);
   }

   public float InchesHeight() {
   return PhabletSignaturePad.ToInches(height);
   }

   public float TextFontHeightPixels() { return textFontHeightPixels; }

}
