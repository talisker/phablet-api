package com.innovisionate.phabletsignaturepad;


public class bezierCurves {


   public class controlPoints {

      public signaturePoint point1;
      public signaturePoint point2;

      controlPoints(signaturePoint p1,signaturePoint p2) {
         point1 = p1;
         point2 = p2;
         return;
      }

   }

   public class curve {

      public signaturePoint startPoint;
      public signaturePoint control1;
      public signaturePoint control2;
      public signaturePoint endPoint;

      public curve(signaturePoint sp, signaturePoint c1, signaturePoint c2, signaturePoint ep) {
         startPoint = sp;
         control1 = c1;
         control2 = c2;
         endPoint = ep;
         return;
      }

      public float length() {

         int steps = 10;

         float length = 0;

         double cx, cy, px = 0, py = 0, xDiff, yDiff;

         for ( int k = 0; k <= steps; k++) {

            float t = (float)k / steps;

            cx = point(t, startPoint.X, control1.X, control2.X, endPoint.X);

            cy = point(t, startPoint.Y, control1.Y, control2.Y, endPoint.Y);

            if ( k > 0 ) {
               xDiff = cx - px;
               yDiff = cy - py;
               length += Math.sqrt(xDiff * xDiff + yDiff * yDiff);
            }

            px = cx;
            py = cy;
         }

         return length;

      }

      public double point(float t, float start, float c1, float c2, float end) {
         return start * (1.0 - t) * (1.0 - t) * (1.0 - t) + 3.0 * c1 * (1.0 - t) * (1.0 - t) * t + 3.0 * c2 * (1.0 - t) * t * t + end * t * t * t;
      }

   }

   public controlPoints getControlPoints(signaturePoint s1, signaturePoint s2, signaturePoint s3) {

      float dx1 = s1.X - s2.X;
      float dy1 = s1.Y - s2.Y;
      float dx2 = s2.X - s3.X;
      float dy2 = s2.Y - s3.Y;

      float m1X = (s1.X + s2.X) / 2.0f;
      float m1Y = (s1.Y + s2.Y) / 2.0f;
      float m2X = (s2.X + s3.X) / 2.0f;
      float m2Y = (s2.Y + s3.Y) / 2.0f;

      float l1 = (float) Math.sqrt(dx1 * dx1 + dy1 * dy1);
      float l2 = (float) Math.sqrt(dx2 * dx2 + dy2 * dy2);

      float dxm = (m1X - m2X);
      float dym = (m1Y - m2Y);

      float k = l2 / (l1 + l2);

      if ( Float.isNaN(k) )
         k = 0.0f;

      float cmX = m2X + dxm * k;
      float cmY = m2Y + dym * k;

      float tx = s2.X - cmX;
      float ty = s2.Y - cmY;

      return new controlPoints(new signaturePoint(m1X + tx, m1Y + ty), new signaturePoint(m2X + tx, m2Y + ty));
   }

   public curve getCurve(signaturePoint sp, signaturePoint c1, signaturePoint c2, signaturePoint ep) {
      return new curve(sp, c1, c2, ep);
   }

}
