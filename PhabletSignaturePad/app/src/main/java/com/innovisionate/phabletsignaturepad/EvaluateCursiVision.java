package com.innovisionate.phabletsignaturepad;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class EvaluateCursiVision extends Activity {

   private static Boolean canDownload = false;

   @Override
   protected void onCreate(Bundle savedInstanceState) {

      super.onCreate(savedInstanceState);

      setContentView(R.layout.evaluate_main);

      checkPermission();

      ((Button)findViewById(R.id.btn_evaluate)).setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {

            Uri theFileUri = Uri.parse("http://www.cursiVision.com/dmdocuments/CursiVisionEvaluation.exe");

            DownloadManager downloadManager = (DownloadManager)getSystemService(Context.DOWNLOAD_SERVICE);

            DownloadManager.Request request = new DownloadManager.Request(theFileUri);

            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
            request.setAllowedOverRoaming(true);
            //request.setTitle("");
            request.setDescription("The CursiVision Evaluation installer. Please install this on a Windows Computer.");
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
            request.setVisibleInDownloadsUi(true);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/CursiVisionEvaluation.exe");

            long refid = 0;

            if ( ! canDownload )
               PhabletSignaturePad.toastMessage = "Permission to download the file was not granted. Please allow the download in order to recieve the file.";
            else {
               downloadManager.enqueue(request);
               PhabletSignaturePad.toastMessage = "The CursiVision Evaluation Installer is downloading. When it completes, please run this installer on your Windows computer.";
            }

            PhabletSignaturePad.theHandler.post(PhabletSignaturePad.theToastRunnable);

            if ( canDownload ) {

               Runnable doEvaluate = new Runnable() {

                  public void run() {

                     try {

                        String data = URLEncoder.encode("uuid", "UTF-8") + "=" + URLEncoder.encode(PhabletSignaturePad.properties.uuid, "UTF-8");

                        URL url = new URL(PhabletSignaturePad.cursiVisionEvaluationSite);
                        URLConnection conn = url.openConnection();
                        conn.setDoOutput(true);
                        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                        wr.write(data);
                        wr.flush();

                        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        String line;
                        while ((line = rd.readLine()) != null) {
                           System.out.println(line);
                        }

                        wr.close();
                        rd.close();

                     } catch (Exception e) {
                        System.out.println(e.getMessage());
                     }
                  }
               };

               new Thread(doEvaluate).start();

            }

         }
      });

      return;
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      return PhabletSignaturePad.theMainActivity.doOptionsMenu(R.id.action_evaluate,menu);
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      boolean rv = PhabletSignaturePad.theMainActivity.onOptionsItemSelected(item);
      if ( rv )
         finish();
      return rv;
   }

   private void checkPermission() {

      if ( Build.VERSION.SDK_INT < Build.VERSION_CODES.M ) {
         canDownload = true;
         return;
      }

      if ( ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
               ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

         ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},1);

      } else {

      }
   }

   @Override
   public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
      switch ( requestCode ) {
      case 1: {
         if ( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ) {

            canDownload = true;

         } else {

            canDownload = false;

         }

         return;
      }
      }
   }

   @Override
   protected void onDestroy() {
      super.onDestroy();
      return;
   }

}