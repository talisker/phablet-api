
package com.innovisionate.phabletsignaturepad;

public interface pkUIObject {

   int X();
   int Y();
   int Width();
   int Height();

   String Text();
   void setText(String t);
   
   int TextX();
   int TextY();
   int TextWidth();
   int TextHeight();
   
   void setX(int x);
   void setY(int y);
   void setWidth(int width);
   void setHeight(int height);

   void setRequestedVisibility(int visibility);
   void restoreVisibility();
   
   int ScaledX();
   int ScaledY();
   int ScaledWidth();
   int ScaledHeight();
   
   float InchesX();
   float InchesY();
   float InchesWidth();
   float InchesHeight();

   float TextFontHeightPixels();

}
