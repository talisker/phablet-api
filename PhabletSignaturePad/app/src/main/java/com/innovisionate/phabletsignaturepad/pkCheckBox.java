package com.innovisionate.phabletsignaturepad;

import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.CheckBox;
//import android.support.v7.widget.AppCompatCheckBox;

//public class pkCheckBox extends AppCompatCheckBox implements pkUIObject, View.OnClickListener {
public class pkCheckBox extends CheckBox implements pkUIObject, View.OnClickListener {

   private int x,y,width,height;
   private int currentRequestedVisibility;

   private int checkBoxWidth,checkBoxHeight;
   private int checkBoxMinimumHeight;
   
   private Rect textSize;
   private RectF textSizeF;
   private Typeface typeFace;
   private float fontSize;
   private Path textPath;
   private float textFontHeightPixels;
   
   pkCheckBox() {
      
   super(PhabletSignaturePad.theMainActivity);

   // CREATECHECKBOX x, y, id, isVisible, isChecked, text

   setId(Integer.valueOf(PhabletSignaturePad.controlDetails[3]));
   
   x = Integer.valueOf(PhabletSignaturePad.controlDetails[1]);
   
   y = Integer.valueOf(PhabletSignaturePad.controlDetails[2]);

   textSize = new Rect();
   
   textSizeF = new RectF();
   
   textPath = new Path();
   
   setIncludeFontPadding(false);
   
   setText(PhabletSignaturePad.controlDetails[6].replaceAll("\"", "").trim());

   currentRequestedVisibility = "1".equals(PhabletSignaturePad.controlDetails[4]) ? android.view.View.VISIBLE : android.view.ViewGroup.INVISIBLE;

   if ( 1 == PhabletSignaturePad.controlUpdatesShowing )
      setVisibility(currentRequestedVisibility);
   else
      setVisibility(android.view.ViewGroup.INVISIBLE);

   setChecked(("1".equals(PhabletSignaturePad.controlDetails[5])));
   
   setOnClickListener(this);

   fontSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PT,PhabletSignaturePad.properties.fontSize,PhabletSignaturePad.theMainActivity.getBaseContext().getResources().getDisplayMetrics());
   
   typeFace = Typeface.create(PhabletSignaturePad.properties.fontFamily,Typeface.NORMAL);

   setTypeface(typeFace);
   
   setTextSize(TypedValue.COMPLEX_UNIT_PT,fontSize);
   
   int resID = 0;

   if ( Build.VERSION.SDK_INT <= 10 ) {
     // pre-Honeycomb has a different way of setting the CheckBox button drawable
      resID = Resources.getSystem().getIdentifier("btn_check", "drawable", "android");
   } else {
     // starting with Honeycomb, retrieve the theme-based indicator as CheckBox button drawable
     TypedValue value = new TypedValue();
     PhabletSignaturePad.theMainActivity.getBaseContext().getTheme().resolveAttribute(android.R.attr.listChoiceIndicatorMultiple, value, true);
     resID = value.resourceId;
   }

   Drawable cbDrawable = getResources().getDrawable(resID);
   
   checkBoxHeight = cbDrawable.getIntrinsicHeight();
   checkBoxWidth = cbDrawable.getIntrinsicWidth();
   checkBoxMinimumHeight = cbDrawable.getMinimumHeight();

   measure(0,0);

   return;
   }


   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

   Paint paint = getPaint();
   paint.setTypeface(typeFace);
   paint.setTextSize(fontSize);
   String text = (String)getText();
  
   paint.getTextBounds(text + "MMMM",0,text.length() + 4,textSize);
 
   width = (int)((float)(textSize.right - textSize.left) * 1.25) + checkBoxWidth;
   
   height = Math.max(checkBoxMinimumHeight,(int)((double)(textSize.bottom - textSize.top) / 0.75));

   setMeasuredDimension(width,height);

   paint.getTextPath(text, 0, text.length(), 0.0f, 0.0f, textPath);

   textPath.computeBounds(textSizeF, true);

   textFontHeightPixels = paint.getTextSize();

   return;
   }
   
   public int X() {
   return x;
   }

   public int Y() {
   return y;
   }

   public int Width() {
   return width;
   }

   public int Height() {
   return height;
   }
 
   public int TextX() {
   int rv = (int)textSizeF.left;
   if ( 0.5 > textSizeF.left - rv )
      return rv;
   return ++rv;
   }

   public int TextY() {
   int rv = (int)textSizeF.top;
   if ( 0.5 > textSizeF.top - rv )
      return rv;
   return ++rv;
   }
   
   public int TextWidth() {
   int rv = (int)textSizeF.width();
   if ( 0.5 > textSizeF.width() - rv )
      return rv;
   return ++rv;
   }
   
   public int TextHeight() {
   int rv = (int)textSizeF.height();
   if ( 0.5 > textSizeF.height() - rv )
      return rv;
   return ++rv;
   }
   
   public void setX(int x) {
   this.x = x;
   return;
   }

   public void setY(int y) {
   this.y = y;
   return;
   }

   public String Text() {
   return (String)getText();
   }
   
   public void setText(String v) {
   super.setText(v);
   return;
   }
   
   public void setWidth(int width) {
   this.width = width;
   return;
   }

   public void setHeight(int height) {
   this.height = height;
   return;
   }

   public void setRequestedVisibility(int visibility) {
   currentRequestedVisibility = visibility;
   return;
   }

   public void restoreVisibility() {
   this.setVisibility(currentRequestedVisibility);
   return;
   }

   public int ScaledX() {
   return (int)((double)x * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledY() {
   return (int)((double)y * PhabletSignaturePad.scaleObjectsY);
   }

   public int ScaledWidth() {
   return (int)((double)width * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledHeight() {
   return (int)((double)height * PhabletSignaturePad.scaleObjectsY);
   }
   
   public float InchesX() {
   return PhabletSignaturePad.ToInches(x);
   }

   public float InchesY() {
   return PhabletSignaturePad.ToInches(y);
   }
   
   public float InchesWidth() {
   return PhabletSignaturePad.ToInches(width);
   }

   public float InchesHeight() {
   return PhabletSignaturePad.ToInches(height);
   }

   public float TextFontHeightPixels() { return textFontHeightPixels; }

   @Override
   public void onClick(View v) {
   if ( isChecked() ) 
      PhabletSignaturePad.sendEvent(String.valueOf(PhabletSignaturePad.EVENT_OPTIONSELECTED) + " " + getId());
   else
      PhabletSignaturePad.sendEvent(String.valueOf(PhabletSignaturePad.EVENT_OPTIONUNSELECTED) + " " + getId());
   return;
   }
   
}
