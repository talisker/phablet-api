package com.innovisionate.phabletsignaturepad;

import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
//import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.widget.EditText;

//public class pkEntryField extends AppCompatEditText implements pkUIObject {
public class pkEntryField extends EditText implements pkUIObject {

   private int x,y,width,height;
   private int currentRequestedVisibility;

   private RectF textSizeF;
   private float textFontHeightPixels;

   private Typeface typeFace;

   private String currentValue;

   pkEntryField() {

   super(PhabletSignaturePad.theMainActivity);

   // CREATEENTRYFIELD x, y, width, id, isVisible, text

   setId(Integer.valueOf(PhabletSignaturePad.controlDetails[4]));

   x = Integer.valueOf(PhabletSignaturePad.controlDetails[1]);

   y = Integer.valueOf(PhabletSignaturePad.controlDetails[2]);

   width = Integer.valueOf(PhabletSignaturePad.controlDetails[3]);

   currentRequestedVisibility = "1".equals(PhabletSignaturePad.controlDetails[5]) ? android.view.View.VISIBLE : android.view.ViewGroup.INVISIBLE;

   textSizeF = new RectF();

   setIncludeFontPadding(false);

   if ( 1 == PhabletSignaturePad.controlUpdatesShowing )
      setVisibility(currentRequestedVisibility);
   else
      setVisibility(android.view.ViewGroup.INVISIBLE);

   typeFace = Typeface.create(PhabletSignaturePad.properties.fontFamily,Typeface.NORMAL);

   setTypeface(typeFace);

   setTextSize(TypedValue.COMPLEX_UNIT_PT,PhabletSignaturePad.properties.fontSize);

   textFontHeightPixels = getTextSize();

   currentValue = PhabletSignaturePad.controlDetails[6].replaceAll("\"", "").trim();

   setText(currentValue);

   setInputType(1);

   setTextScaleX(1.0f);

   measure(0,0);

   addTextChangedListener(new TextWatcher() {

      public void afterTextChanged(Editable s) {
         PhabletSignaturePad.sendEvent(String.valueOf(PhabletSignaturePad.EVENT_TEXTCHANGED) + " " +
                  getId() + " î" + currentValue + "ì î" + s.toString() + "ì");
         currentValue = s.toString();
      }

      public void beforeTextChanged(CharSequence s, int start,int count, int after) {
      }

      public void onTextChanged(CharSequence s, int start,int before, int count) {
      }

   });

   return;
   }

   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
   calcTextF();
   super.onMeasure(widthMeasureSpec,heightMeasureSpec);
   return;
   }

   private void calcTextF() {
   Paint paint = getPaint();
   paint.setTypeface(typeFace);
   paint.setTextSize(getTextSize());
   Path textPath = new Path();
   String text = getText().toString();
   if ( 0 == text.length() )
      text = "MMMM";
   paint.getTextPath(text, 0, text.length(), 0.0f, 0.0f, textPath);
   textPath.computeBounds(textSizeF, true);
   return;
   }

   public int X() {
      return x;
   }

   public int Y() {
      return y - getMeasuredHeight() / 3;
   }

   public int Width() {
      return Math.max(width,getMeasuredWidth());
   }

   public int Height() { return getMeasuredHeight(); }

   public int TextX() {

   calcTextF();

   int rv = (int)textSizeF.left;

   if ( 0.5 > textSizeF.left - rv )
      return rv;

   return ++rv;
   }

   public int TextY() {

   calcTextF();

   int rv = (int)textSizeF.top;
   if ( 0.5 > textSizeF.top - rv )
      return rv;
   return ++rv;
   }

   public int TextWidth() {

   calcTextF();

   int rv = (int)textSizeF.width();
   if ( 0.5 > textSizeF.width() - rv )
      return rv;
   return ++rv;
   }

   public int TextHeight() {

   calcTextF();

   int rv = (int)textSizeF.height();
   if ( 0.5 > textSizeF.height() - rv )
      return rv;
   return ++rv;
   }

   public void setX(int x) {
   this.x = x;
   return;
   }

   public void setY(int y) {
   this.y = y;
   return;
   }

   public void setWidth(int width) {
   this.width = width;
   return;
   }

   public void setHeight(int height) {
   this.height = height;
   return;
   }

   public void setRequestedVisibility(int visibility) {
      currentRequestedVisibility = visibility;
   }

   public void restoreVisibility() {
      this.setVisibility(currentRequestedVisibility);
   }

   public String Text() {
      return getText().toString();
   }

   public void setText(String v) {
   super.setText(v);
   return;
   }

   public int ScaledX() {
      return (int)((double)x * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledY() {
      return (int)((double)y * PhabletSignaturePad.scaleObjectsY);
   }

   public int ScaledWidth() {
      return (int)((double)width * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledHeight() {
      return (int)((double)height * PhabletSignaturePad.scaleObjectsY);
   }

   public float InchesX() {
      return PhabletSignaturePad.ToInches(x);
   }

   public float InchesY() {
      return PhabletSignaturePad.ToInches(y);
   }

   public float InchesWidth() {
      return PhabletSignaturePad.ToInches(width);
   }

   public float InchesHeight() {
      return PhabletSignaturePad.ToInches(height);
   }

   public float TextFontHeightPixels() { return textFontHeightPixels; }
}
