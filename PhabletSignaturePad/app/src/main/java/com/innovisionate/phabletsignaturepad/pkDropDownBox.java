package com.innovisionate.phabletsignaturepad;

import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
//import android.support.v7.widget.AppCompatSpinner;
import android.widget.Spinner;
import android.widget.TextView;

//public class pkDropDownBox extends AppCompatSpinner implements pkUIObject, AdapterView.OnItemSelectedListener {
public class pkDropDownBox extends Spinner implements pkUIObject, AdapterView.OnItemSelectedListener {

   private int x,y,width,height;
   private int currentRequestedVisibility;

   private int dropDownBoxWidth,dropDownBoxHeight;
   private int dropDownBoxMinimumHeight;
   
   private String [] theItems;
   private pkSpinnerAdapter theAdapter;
   private Paint thePaint;
   private Rect theTextSize;
   private RectF textSizeF;
   private Path textPath;
   private float textFontHeightPixels;
   
   private String maxString;

   private Typeface typeFace;
   private float fontSize;
   
   pkDropDownBox() {
      
   super(PhabletSignaturePad.theMainActivity);

   // CREATEDROPDOWNBOX x, y, id, isVisible items selectedItem

   theItems = PhabletSignaturePad.controlDetails[5].trim().split(",");
   
   setId(Integer.valueOf(PhabletSignaturePad.controlDetails[3]));
   
   x = Integer.valueOf(PhabletSignaturePad.controlDetails[1]);
   
   y = Integer.valueOf(PhabletSignaturePad.controlDetails[2]);

   theAdapter = new pkSpinnerAdapter(theItems);
   
   setAdapter(theAdapter);

   String selectedItem = PhabletSignaturePad.controlDetails[6].trim();
   
   if ( 0 < selectedItem.length() ) {
      int k = theAdapter.getPosition(selectedItem);
      if ( -1 < k )
         setSelection(k);
   }

   currentRequestedVisibility = "1".equals(PhabletSignaturePad.controlDetails[4]) ? android.view.View.VISIBLE : android.view.ViewGroup.INVISIBLE;

   if ( 1 == PhabletSignaturePad.controlUpdatesShowing )
      setVisibility(currentRequestedVisibility);
   else
      setVisibility(android.view.ViewGroup.INVISIBLE);

   thePaint = new Paint();
   theTextSize = new Rect();
   textSizeF = new RectF();
   textPath = new Path();
   
   maxString = "";
   
   for ( int k = 0; k < theItems.length; k++ ) {
      if ( maxString.length() < theItems[k].length() ) 
         maxString = theItems[k];
   }
   
   setOnItemSelectedListener(this);

   typeFace = Typeface.create(PhabletSignaturePad.properties.fontFamily,Typeface.NORMAL);

   DisplayMetrics displayMetrics = PhabletSignaturePad.theMainActivity.getBaseContext().getResources().getDisplayMetrics();

   fontSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PT,PhabletSignaturePad.properties.fontSize,displayMetrics);

   TypedValue value = new TypedValue();
   
   PhabletSignaturePad.theMainActivity.getBaseContext().getTheme().resolveAttribute(android.R.attr.listPreferredItemHeight, value, true);

   TypedValue.coerceToString(value.type, value.data);

   dropDownBoxHeight = (int)value.getDimension(displayMetrics);

   dropDownBoxMinimumHeight = dropDownBoxHeight;
   
   measure(0,0);

   return;
   }

   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

//   View v = getSelectedView();
//
//   ((TextView)v).setTypeface(typeFace);
//   ((TextView)v).setTextSize(TypedValue.COMPLEX_UNIT_PT,fontSize);

      //theAdapter.getView();

   thePaint.setTypeface(typeFace);
   thePaint.setTextSize(fontSize);
   thePaint.getTextBounds(maxString + "M",0,maxString.length() + 1,theTextSize);
   
   width = (int)((float)(theTextSize.right - theTextSize.left) * 1.75) + dropDownBoxWidth;
   
   height = Math.max(dropDownBoxMinimumHeight,theTextSize.bottom - theTextSize.top);
   
   setMeasuredDimension(width,height);

   thePaint.getTextPath(maxString + "M", 0, maxString.length() + 1, 0.0f, 0.0f, textPath);

   textPath.computeBounds(textSizeF, true);

   textFontHeightPixels = thePaint.getTextSize();

   return;
   }
   
   public int X() {
   return x;
   }

   public int Y() {
   return y;
   }

   public int Width() {
   return width;
   }

   public int Height() {
   return height;
   }
   
   public int TextX() {
   int rv = (int)textSizeF.left;
   if ( 0.5 > textSizeF.left - rv )
      return rv;
   return ++rv;
   }

   public int TextY() {
   int rv = (int)textSizeF.top;
   if ( 0.5 > textSizeF.top - rv )
      return rv;
   return ++rv;
   }
   
   public int TextWidth() {
   int rv = (int)textSizeF.width();
   if ( 0.5 > textSizeF.width() - rv )
      return rv;
   return ++rv;
   }
   
   public int TextHeight() {
   int rv = (int)textSizeF.height();
   if ( 0.5 > textSizeF.height() - rv )
      return rv;
   return ++rv;
   }
   
   public void setX(int x) {
   this.x = x;
   return;
   }

   public void setY(int y) {
   this.y = y;
   return;
   }

   public void setWidth(int width) {
   this.width = width;
   return;
   }

   public void setHeight(int height) {
   this.height = height;
   return;
   }

   public void setRequestedVisibility(int visibility) {
   currentRequestedVisibility = visibility;
   return;
   }

   public void restoreVisibility() {
   this.setVisibility(currentRequestedVisibility);
   return;
   }

   public String Text() {
   if ( -1 == getSelectedItemPosition() )
      return "";
   return (String)getSelectedItem();
   }
   
   public void setText(String v) {
   if ( -1 == getPosition(v) )
      return;
   setSelection(getPosition(v));
   return;
   }
   
   public int ScaledX() {
   return (int)((double)x * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledY() {
   return (int)((double)y * PhabletSignaturePad.scaleObjectsY);
   }

   public int ScaledWidth() {
   return (int)((double)width * PhabletSignaturePad.scaleObjectsX);
   }

   public int ScaledHeight() {
   return (int)((double)height * PhabletSignaturePad.scaleObjectsY);
   }
   
   public float InchesX() {
   return PhabletSignaturePad.ToInches(x);
   }

   public float InchesY() {
   return PhabletSignaturePad.ToInches(y);
   }
   
   public float InchesWidth() {
   return PhabletSignaturePad.ToInches(width);
   }

   public float InchesHeight() {
   return PhabletSignaturePad.ToInches(height);
   }

   public float TextFontHeightPixels() { return textFontHeightPixels; }

   public int getPosition(String item) {
   return theAdapter.getPosition(item);
   }
   
   public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
   PhabletSignaturePad.sendEvent(String.valueOf(PhabletSignaturePad.EVENT_ITEMSELECTED) + " " + getId() + " î" + theAdapter.getItem((int)arg3).toString() + "ì");
   return;
   }

   public void onNothingSelected(AdapterView<?> arg0) {
   return;
   }

}
