package com.innovisionate.phabletsignaturepad;

import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class pkSpinnerAdapter extends ArrayAdapter<String> {

   private Typeface typeFace;
   private float fontSize;
   private String [] theItems;
   
   public pkSpinnerAdapter(String [] values) {

   super(PhabletSignaturePad.theMainActivity,android.R.layout.simple_spinner_item,values);

   theItems = values.clone();

   setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

   fontSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PT,PhabletSignaturePad.properties.fontSize,PhabletSignaturePad.theMainActivity.getBaseContext().getResources().getDisplayMetrics());

   typeFace = Typeface.create(PhabletSignaturePad.properties.fontFamily,Typeface.NORMAL);

   return;
   }

   @Override
   public View getView(int position, View convertView, ViewGroup parent) {

   TextView view = (TextView) super.getView(position, convertView, parent);

   view.setTypeface(typeFace);
   view.setTextSize(fontSize);

   return view;
   }

   @Override
   public View getDropDownView(int position, View convertView, ViewGroup parent) {

   TextView v = new TextView(PhabletSignaturePad.theMainActivity.getBaseContext());

   //Paint paint = v.getPaint();
   //paint.setTypeface(typeFace);
   //paint.setTextSize(fontSize);
   //Rect textSize = new Rect();
   //paint.getTextBounds(theItems[position],0,theItems[position].length(),textSize);

   v.setTypeface(typeFace);
   v.setTextSize(fontSize);

   v.setText(theItems[position]);

   //v.setHeight((int)(1.5 * (textSize.bottom - textSize.top)));
   return v;
   }
   
}
