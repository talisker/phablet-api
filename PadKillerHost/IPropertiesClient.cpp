// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PadKillerHost.h"

   long __stdcall _IGPropertiesClient::QueryInterface(REFIID riid,void **ppv) {
   *ppv = NULL; 
 
   if ( riid == IID_IUnknown )
      *ppv = static_cast<IUnknown*>(this); 
   else

   if ( riid == IID_IGPropertiesClient )
      *ppv = this;
   else

   if ( riid == IID_IGPropertyPageClient )
      return pIGPropertyPageClient -> QueryInterface(riid,ppv);
   else

      return E_NOINTERFACE;
 
   static_cast<IUnknown*>(*ppv) -> AddRef();
  
   return S_OK; 
   }
 
   unsigned long __stdcall _IGPropertiesClient::AddRef() {
   return 1;
   }
 
   unsigned long __stdcall _IGPropertiesClient::Release() {
   return 1;
   }


   HRESULT _IGPropertiesClient::SavePrep() {
   properties.szwBackgroundFileName[0] = L'\0';
   return S_OK;
   }


   HRESULT _IGPropertiesClient::InitNew() {
   return Loaded();
   }


   HRESULT _IGPropertiesClient::Loaded() {
   return S_OK;
   }


   HRESULT _IGPropertiesClient::Saved() {
   return S_OK;
   }


   HRESULT _IGPropertiesClient::IsDirty() {
   return S_FALSE;
   }

   HRESULT _IGPropertiesClient::GetClassID(BYTE *pCLSID) {
   return E_NOTIMPL;
   }
