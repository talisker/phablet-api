// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#define ID_CONFIGURE                10
#define ID_EXIT                     11
#define ID_START                    12
#define ID_STOP                     13

#define IDD_DEVICE_CONFIGURE        10
#define IDDI_USE_ENTIRE_SCREEN      11
#define IDDI_GROUP_NOT_ENTIRE       12
#define IDDI_PIXEL_WIDTH_LABEL      13
#define IDDI_PIXEL_WIDTH            14
#define IDDI_PIXEL_HEIGHT_LABEL     15
#define IDDI_PIXEL_HEIGHT           16
#define IDDI_IGNORE_HEARTBEAT       18

#define IDDI_POSITION_RESET         21
#define IDDI_POSITION_NOTE          23
#define IDDI_SIZE_NOTE              24
#define IDDI_RESET_NOTE             25

#define WM_SET_BACKGROUND              (WM_USER + 1)
#define WM_SIGNATURE_START             (WM_USER + 2)
#define WM_SIGNATURE_STOP              (WM_USER + 3)
#define WM_START_COMMAND_ACCEPTANCE    (WM_USER + 4)
#define WM_START_EVENTS_PUBLISHER      (WM_USER + 5)
#define WM_START_CONTROL_INTERFACE     (WM_USER + 6)
#define WM_INITIALIZE_CONTROL          (WM_USER + 7)
#define WM_CREATE_CONTROL              (WM_USER + 8)
#define WM_DESTROY_CONTROL             (WM_USER + 9)
#define WM_MOVE_AND_RESIZE_CONTROL     (WM_USER + 10)
#define WM_MOVE_CONTROL                (WM_USER + 11)
#define WM_ADD_IMAGE                   (WM_USER + 12)
#define WM_SHUTDOWN                    (WM_USER + 13)
#define WM_LOAD_SAVED_CONTROLS         (WM_USER + 14)
#define WM_MOVE_SAVED_CONTROLS         (WM_USER + 15)
#define WM_FIRE_DEVICE_READY           (WM_USER + 16)
#define WM_SHOW_CONTROL                (WM_USER + 17)
#define WM_HIDE_CONTROL                (WM_USER + 18)
#define WM_SHOW_CONTROL_LIST           (WM_USER + 19)
#define WM_HIDE_CONTROL_LIST           (WM_USER + 20)
#define WM_SHOW_PDF                    (WM_USER + 21)
#define WM_OTHER_INITIALIZATION        (WM_USER + 22)
#define WM_SET_CONTROL_TEXT            (WM_USER + 23)
#define WM_SET_OPTION_STATE            (WM_USER + 24)
#define WM_SELECT_CONTROL_ITEM         (WM_USER + 25)

#define IDDI_COMMAND_BUTTON_START      100

#define TIMER_CLIENT_CONNECTED               1
#define TIMER_CLIENT_ABANDONED               2

#define TIMER_DELAY_EXPECT_HEARTBEAT      5000

#define MINIMUM_EVENT_SPACING_DURATION    100

