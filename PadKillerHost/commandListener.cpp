// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PadKillerHost.h"

   unsigned int __stdcall commandListener(void *) {

   addrinfo addressInfo;

   addrinfo *pResolvedAddressInfo;

   memset(&addressInfo,0,sizeof(addrinfo));

   addressInfo.ai_flags = AI_PASSIVE;
   addressInfo.ai_family = AF_INET;
   addressInfo.ai_socktype = SOCK_STREAM;
   addressInfo.ai_protocol = IPPROTO_TCP;
   addressInfo.ai_addrlen = 0;
   addressInfo.ai_addr = NULL;
   addressInfo.ai_canonname = NULL;
   addressInfo.ai_next = NULL;

   getaddrinfo(NULL,COMMAND_LISTENER_PORT_A,&addressInfo,&pResolvedAddressInfo);

   commandListenerSocket = INVALID_SOCKET;

   for ( addrinfo *p = pResolvedAddressInfo; p; p = p -> ai_next ) {
      if ( ! ( SOCK_STREAM == p -> ai_socktype ) )
         continue;
      commandListenerSocket = socket(p -> ai_family,p -> ai_socktype,p -> ai_protocol);
      if ( ! ( INVALID_SOCKET == commandListenerSocket ) ) {
         if ( ! ( SOCKET_ERROR == bind(commandListenerSocket,p -> ai_addr,(int)p -> ai_addrlen) ) ) 
            break;
         commandListenerSocket = INVALID_SOCKET;
      }
   }

   if ( INVALID_SOCKET == commandListenerSocket ) {
      Beep(2000,100);
      return 0;
   }

   if ( SOCKET_ERROR == listen(commandListenerSocket,SOMAXCONN) ) {
      Beep(2000,100);
      return 0;
   }

   commandListenerShutdown = 0L;

   while ( ! commandListenerShutdown ) {

      sockaddr_in clientSockAddr = {0};

      int clientSockAddrSize = sizeof(sockaddr);

      commandClientSocket = accept(commandListenerSocket,(sockaddr *)&clientSockAddr,&clientSockAddrSize);

      if ( INVALID_SOCKET == commandClientSocket )
         break;

      SetTimer(hwndMain,TIMER_CLIENT_CONNECTED,TIMER_DELAY_EXPECT_HEARTBEAT,NULL);

      bool clientConnected = true;

      while ( clientConnected && ! commandListenerShutdown ) {

         memset(szwCommand,0,COMMAND_SIZE * sizeof(WCHAR));

         long rc = recv(commandClientSocket,(char *)szwCommand,8,MSG_WAITALL);

         if ( 0 > rc ) {
            commandListenerShutdown = 1L;
            PostQuitMessage(0L);
            break;
         }

         long byteCount = _wtol(szwCommand);

         memset(szwCommand,0,(byteCount + 1));

         rc = recv(commandClientSocket,(char *)szwCommand,byteCount,MSG_WAITALL);

         if ( 0 > rc ) {
            commandListenerShutdown = 1L;
            PostMessage(hwndMain,WM_SHUTDOWN,0L,0L);
            break;
         }

         if ( 0 == _wcsnicmp(szwCommand,L"disconnect",10) ) {
            send(commandClientSocket,(char *)L"ok",4,0);
            commandListenerShutdown = 1L;
            PostMessage(hwndMain,WM_SHUTDOWN,0L,0L);
            break;
         }

         memset(szwResponse,0,RESPONSE_SIZE * sizeof(WCHAR));

         WCHAR szwCommandBackup[64];
         
         wcsncpy(szwCommandBackup,szwCommand,64);

         enum pkCommands commandId = (pkCommands)_wtol(szwCommand);

         if ( holdOneEvent == commandId ) {
            bool doHold = true;
            if ( holdOneEvent == SETPARAMETER ) {
               WCHAR szwParameter[64];
               wcsncpy(szwParameter,szwCommand,64);
               wcstok(szwParameter,L" ,",NULL);
               enum pkParameters theParameter = (pkParameters)_wtol(wcstok(NULL,L" ,",NULL));
               if ( ! ( holdOneEventParameter == theParameter ) )
                  doHold = false;
            }
            if ( doHold ) {
               holdOneEvent = NOCOMMAND;
               holdOneEventParameter = PARM_NOPARAMETER;
               wcscpy(szwResponse,L"ok");
               send(commandClientSocket,(char *)szwResponse,2 * (DWORD)wcslen(szwResponse),0L);
               continue;
            }
         }

         switch ( commandId ) {

         case BACKGROUND:
         case RECEIVEIMAGE: {
            WCHAR *p = wcstok(szwCommand,L" ,",NULL);
            p = wcstok(NULL,L" ,",NULL);
            if ( p ) {
               WCHAR *px = p;
               while ( *px && isdigit(*px) ) px++;
               if ( *px )
                  p = NULL;
            }
            long expectedFileSize = _wtol(p);
            if ( RECEIVEIMAGE == commandId ) {
               p = wcstok(NULL,L" ,",NULL);
               if ( p ) {
                  imageLocationX = _wtol(p);
                  p = wcstok(NULL,L" ,",NULL);
                  imageLocationY = _wtol(p);
                  p = wcstok(NULL,L" ,",NULL);
                  imageWidth = _wtol(p);
                  p = wcstok(NULL,L" ,",NULL);
                  imageHeight = _wtol(p);
               } else {
                  imageLocationX = -1L;
                  imageLocationY = -1L;
                  imageWidth = -1L;
                  imageHeight = -1L;
               }
            } else {
               imageLocationX = -1L;
               imageLocationY = -1L;
               imageWidth = -1L;
               imageHeight = -1L;
            }

            WCHAR szwTemp[MAX_PATH];

            if ( recieveFile(expectedFileSize,szwTemp) ) {

               swprintf(properties.szwBackgroundFileName,L"%ls\\pad",szwApplicationDataDirectory);
         
               CopyFileW(szwTemp,properties.szwBackgroundFileName,FALSE);
               
               DeleteFileW(szwTemp);

               if ( RECEIVEIMAGE == commandId )
                  PostMessage(hwndMain,WM_ADD_IMAGE,0L,(LPARAM)properties.szwBackgroundFileName);
               else
                  PostMessage(hwndMain,WM_SET_BACKGROUND,0L,(LPARAM)properties.szwBackgroundFileName);

               requestorThreadId = GetCurrentThreadId();

               WaitForSingleObject(hProcessingDone,INFINITE);

               requestorThreadId = uiThreadId;

            }

            szwResponse[0] = L'\0';

            }
            break;

         case START: 
            if ( ! hBitmapBackground ) {
#ifdef _DEBUG
               //SetWindowPos(hwndMain,HWND_TOP,rcMultiMonitorOffset.left + cxScreen/2 - properties.cxDefault/2,cyScreen/2 - properties.cyDefault/2,properties.cxDefault,properties.cyDefault,0L);
#else
               SetWindowPos(hwndMain,HWND_TOPMOST,rcMultiMonitorOffset.left + cxScreen/2 - properties.cxDefault/2,cyScreen/2 - properties.cyDefault/2,properties.cxDefault,properties.cyDefault,0L);
#endif
            }

            PostMessage(hwndMain,WM_SIGNATURE_START,0L,0L);
            requestorThreadId = GetCurrentThreadId();
            WaitForSingleObject(hProcessingDone,INFINITE);
            requestorThreadId = uiThreadId;
            break;

         case STOP: 
            PostMessage(hwndMain,WM_SIGNATURE_STOP,0L,0L);
            requestorThreadId = GetCurrentThreadId();
            WaitForSingleObject(hProcessingDone,INFINITE);
            requestorThreadId = uiThreadId;
            break;

         case CLEARHOTSPOTS: {
            for ( hotSpot *pHotSpot : hotSpots )
               delete pHotSpot;
            hotSpots.clear();
            }
            break;

         case HOTSPOT: {
            wcstok(szwCommand,L" ",NULL);
            RECT rc;
            rc.left = _wtol(wcstok(NULL,L" ",NULL));
            rc.top = _wtol(wcstok(NULL,L" ",NULL));
            rc.right = _wtol(wcstok(NULL,L" ",NULL));
            rc.bottom = _wtol(wcstok(NULL,L" ",NULL));
            hotSpots.insert(hotSpots.end(),new hotSpot(&rc,_wtol(wcstok(NULL,L" ",NULL))));
            }
            break;

         case CREATEBUTTON:
         case CREATEDROPDOWNBOX:
         case CREATECHECKBOX:
         case CREATERADIOBUTTON:
         case CREATELABEL:
         case CREATETEXTBOX:
         case CREATEENTRYFIELD: {
            PostMessage(hwndMain,WM_CREATE_CONTROL,(WPARAM)commandId,0L);
            requestorThreadId = GetCurrentThreadId();
            WaitForSingleObject(hProcessingDone,INFINITE);
            requestorThreadId = uiThreadId;
            }
            break;

         case DESTROYCONTROL: {
            PostMessage(hwndMain,WM_DESTROY_CONTROL,0,0L);
            requestorThreadId = GetCurrentThreadId();
            WaitForSingleObject(hProcessingDone,INFINITE);
            requestorThreadId = uiThreadId;
            }
            break;

         case CLEAREVERYTHING: {
            if ( pSignatureGraphic ) {
               delete pSignatureGraphic;
               pSignatureGraphic = new signatureGraphic(true);
            }
            swprintf(szwCommand,L" %ld -1",DESTROYCONTROL);
            PostMessage(hwndMain,WM_DESTROY_CONTROL,0,0L);
            requestorThreadId = GetCurrentThreadId();
            WaitForSingleObject(hProcessingDone,INFINITE);
            requestorThreadId = uiThreadId;
            }
            break;

         case POSITION: {
            wcstok(szwCommand,L" ,",NULL);
            long x = _wtol(wcstok(NULL,L" ,",NULL));
            long y = _wtol(wcstok(NULL,L" ,",NULL));
            SetWindowPos(hwndMain,HWND_TOP,rcMultiMonitorOffset.left + x,y,0,0,SWP_NOSIZE);
            }
            break;

         case RESIZE: {
            wcstok(szwCommand,L" ,",NULL);
            long cx = _wtol(wcstok(NULL,L" ,",NULL));
            long cy = _wtol(wcstok(NULL,L" ,",NULL));
            if ( ! isLocalHostClient ) {
               long x = GetSystemMetrics(SM_CXSCREEN) / 2 - cx / 2;
               long y = GetSystemMetrics(SM_CYSCREEN) / 2 - cy / 2;
               SetWindowPos(hwndMain,HWND_TOP,rcMultiMonitorOffset.left + x,y,cx,cy,0L);
            } else
               SetWindowPos(hwndMain,HWND_TOP,rcMultiMonitorOffset.left + 0,0,cx,cy,SWP_NOMOVE);
            }
            break;

         case SETOPTIONSTATE: {
            wcscpy(szwCommand,szwCommandBackup);
            PostMessage(hwndMain,WM_SET_OPTION_STATE,0,0L);
            requestorThreadId = GetCurrentThreadId();
            WaitForSingleObject(hProcessingDone,INFINITE);
            requestorThreadId = uiThreadId;
            if ( ! szwResponse[0] )
               swprintf(szwResponse,L"The control was not found");
            }
            break;

         case SELECTITEM: {
            wcscpy(szwCommand,szwCommandBackup);
            PostMessage(hwndMain,WM_SELECT_CONTROL_ITEM,0,0L);
            requestorThreadId = GetCurrentThreadId();
            WaitForSingleObject(hProcessingDone,INFINITE);
            requestorThreadId = uiThreadId;
            if ( ! szwResponse[0] )
               swprintf(szwResponse,L"The control was not found");
            }
            break;

#if 0
         case BOUNDS: {
            strtok(szCommand," ,");
            long x = atol(strtok(NULL," ,"));
            long y = atol(strtok(NULL," ,"));
            long cx = atol(strtok(NULL," ,"));
            long cy = atol(strtok(NULL," ,"));
            boundsExplicitlySet = 1L;
            if ( ! isLocalHostClient ) {
               x = GetSystemMetrics(SM_CXSCREEN) / 2 - cx / 2;
               y = GetSystemMetrics(SM_CYSCREEN) / 2 - cy / 2;
            }
            SetWindowPos(hwndMain,HWND_TOP,rcMultiMonitorOffset.left + x,y,cx,cy,0L);
            }
            break;
#endif

         case HIDE: {
            ShowWindow(hwndMain,SW_HIDE);
            }
            break;

         case SHOW: {
            ShowWindow(hwndMain,SW_SHOW);
            }
            break;

         case SETPARAMETER: {

            wcstok(szwCommand,L" ,",NULL);
            WCHAR *p = wcstok(NULL,L" ,",NULL);
            wcscpy(szwCommandBackup,p + wcslen(p) + 1);
            pkParameters theParameter = (pkParameters)_wtol(p);

            switch ( theParameter ) {

            case PARM_BOUNDS_PIXELS: {
               long x = _wtol(wcstok(NULL,L" ,",NULL));
               long y = _wtol(wcstok(NULL,L" ,",NULL));
               long cx = _wtol(wcstok(NULL,L" ,",NULL)) - x;
               long cy = _wtol(wcstok(NULL,L" ,",NULL)) - y;
               if ( ! isLocalHostClient ) {
                  x = GetSystemMetrics(SM_CXSCREEN) / 2 - cx / 2;
                  y = GetSystemMetrics(SM_CYSCREEN) / 2 - cy / 2;
               }
               SetWindowPos(hwndMain,HWND_TOP,rcMultiMonitorOffset.left + x,y,cx,cy,0L);
               }
               break;

            case PARM_BOUNDS_INCHES: {
Beep(2000,100);
Beep(2000,100);
Beep(2000,100);
               }
               break;

            case PARM_HEIGHT_PIXELS: {
               long cy = _wtol(wcstok(NULL,L" ,",NULL));
               RECT rcCurrent;
               GetWindowRect(hwndMain,&rcCurrent);
               SetWindowPos(hwndMain,HWND_TOP,0,0,rcCurrent.right - rcCurrent.left,cy,SWP_NOMOVE);
               }
               break;

            case PARM_WIDTH_PIXELS: {
               long cx = _wtol(wcstok(NULL,L" ,",NULL));
               RECT rcCurrent;
               GetWindowRect(hwndMain,&rcCurrent);
               SetWindowPos(hwndMain,HWND_TOP,0,0,cx,rcCurrent.bottom - rcCurrent.top,SWP_NOMOVE);
               }
               break;

            case PARM_CLIENT_ADDRESS: {
               while ( *p ) p++;
               p++;
               wcscpy(szwClientAddress,p);
               if ( 0 == wcscmp(szwClientAddress,L"127.0.0.1") )
                  isLocalHostClient = true;
               }
               break;

            case PARM_INK_WEIGHT: {
               long v = _wtol(wcstok(NULL,L" ,",NULL));
               properties.inkWeight = v;
               if ( pGDIPlusPen )
                  delete pGDIPlusPen;
               if ( pGDIPlusGraphic )
                  delete pGDIPlusGraphic;
               COLORREF color = (COLORREF)properties.inkColor;
               float weight = (float)properties.inkWeight;
               pGDIPlusPen = new Gdiplus::Pen(Gdiplus::Color(GetRValue(color),GetGValue(color),GetBValue(color)),weight);
               pGDIPlusPen -> SetLineCap(Gdiplus::LineCapSquare,Gdiplus::LineCapSquare,Gdiplus::DashCapFlat);
               pGDIPlusGraphic = new Gdiplus::Graphics(hdcGDIPlus);
               pGDIPlusGraphic -> SetSmoothingMode(Gdiplus::SmoothingMode::SmoothingModeHighQuality);
               }
               break;

            case PARM_INK_COLOR: {
               long r = _wtol(wcstok(NULL,L" ,",NULL));
               long g = _wtol(wcstok(NULL,L" ,",NULL));
               long b = _wtol(wcstok(NULL,L" ,",NULL));
               properties.inkColor = RGB((BYTE)r,(BYTE)g,(BYTE)b);
               if ( pGDIPlusPen )
                  delete pGDIPlusPen;
               if ( pGDIPlusGraphic )
                  delete pGDIPlusGraphic;
               COLORREF color = (COLORREF)properties.inkColor;
               float weight = (float)properties.inkWeight;
               pGDIPlusPen = new Gdiplus::Pen(Gdiplus::Color(GetRValue(color),GetGValue(color),GetBValue(color)),weight);
               pGDIPlusPen -> SetLineCap(Gdiplus::LineCapSquare,Gdiplus::LineCapSquare,Gdiplus::DashCapFlat);
               pGDIPlusGraphic = new Gdiplus::Graphics(hdcGDIPlus);
               pGDIPlusGraphic -> SetSmoothingMode(Gdiplus::SmoothingMode::SmoothingModeHighQuality);
               }
               break;

            case PARM_SHOW_INK:
               properties.showInk = _wtol(wcstok(NULL,L" ,",NULL));
               break;

            case PARM_SEND_POINTS:
               sendPoints = _wtol(wcstok(NULL,L" ,",NULL));
               if ( 1L == sendPoints )
                  eventSpacingDuration = 0L;
               else
                  eventSpacingDuration = MINIMUM_EVENT_SPACING_DURATION;
               break;

            case PARM_FONT_SIZE:
            case PARM_FONT_FAMILY: {
//
//NTC: Deleting a font already used by a control messes up the text on the control. For now, I will allow this small handle leak, or
// maybe a control will delete it's font when it is destroyed (?)
//
//               if ( hFontControls )
//                  DeleteObject(hFontControls);
//
               if ( PARM_FONT_SIZE == theParameter )
                  properties.fontSize = (float)_wtof(wcstok(NULL,L" ,",NULL));

               else if ( PARM_FONT_FAMILY == theParameter )
                  wcscpy(properties.szwFontFamily,wcstok(NULL,L" ,",NULL));

               LOGFONTW baseSystemFont = {0};

               GetObjectW(GetStockObject(DEFAULT_GUI_FONT),sizeof(LOGFONTW),&baseSystemFont);

               HDC hdc = GetDC(hwndMain);

               control::fontSizeDefault = properties.fontSize;

               control::fontSizePixelsDefault = MulDiv((int)control::fontSizeDefault,GetDeviceCaps(hdc,LOGPIXELSY),72);

               baseSystemFont.lfHeight = -control::fontSizePixelsDefault;

               ReleaseDC(hwndMain,hdc);

               if ( properties.szwFontFamily[0] )
                  wcscpy(baseSystemFont.lfFaceName,properties.szwFontFamily);

               control::hFontDefault = CreateFontIndirectW(&baseSystemFont);

               }
               break;

            case PARM_CONTROL_BOUNDS: {
               wcscpy(szwCommand,szwCommandBackup);
               PostMessage(hwndMain,WM_MOVE_AND_RESIZE_CONTROL,0,0L);
               requestorThreadId = GetCurrentThreadId();
               WaitForSingleObject(hProcessingDone,INFINITE);
               requestorThreadId = uiThreadId;
               if ( ! szwResponse[0] )
                  swprintf(szwResponse,L"The control was not found");
               }
               break;

            case PARM_CONTROL_POSITION: {
               wcscpy(szwCommand,szwCommandBackup);
               PostMessage(hwndMain,WM_MOVE_CONTROL,0,0L);
               requestorThreadId = GetCurrentThreadId();
               WaitForSingleObject(hProcessingDone,INFINITE);
               requestorThreadId = uiThreadId;
               if ( ! szwResponse[0] )
                  swprintf(szwResponse,L"The control was not found");
               }
               break;

            case PARM_CONTROL_TEXT: {
               wcscpy(szwCommand,szwCommandBackup);
               PostMessage(hwndMain,WM_SET_CONTROL_TEXT,0,0L);
               requestorThreadId = GetCurrentThreadId();
               WaitForSingleObject(hProcessingDone,INFINITE);
               requestorThreadId = uiThreadId;
               if ( ! szwResponse[0] )
                  swprintf(szwResponse,L"The control was not found");
               }
               break;

            default:
               swprintf(szwResponse,L"Unknown value or parameter identifier (%ld)",theParameter);
               break;
            }

            }
            break;

         case QUERYPARAMETER: {

            wcstok(szwCommand,L" ,",NULL);

            pkParameters theParameter = (pkParameters)_wtol(wcstok(NULL,L" ,",NULL));

            switch ( theParameter ) {

            case PARM_WIDTH_PIXELS:
            case PARM_HEIGHT_PIXELS:
            case PARM_WIDTH_INCHES:
            case PARM_HEIGHT_INCHES:
            case PARM_BOUNDS_PIXELS:
            case PARM_BOUNDS_INCHES: {

               RECT rcCurrent;
               GetWindowRect(hwndMain,&rcCurrent);

               long cx = rcCurrent.right - rcCurrent.left;
               long cy = rcCurrent.bottom - rcCurrent.top;

               if ( PARM_WIDTH_PIXELS == theParameter || PARM_HEIGHT_PIXELS == theParameter ) {
                  if ( PARM_WIDTH_PIXELS == theParameter )
                     swprintf(szwResponse,L"%ld",cx);
                  else
                     swprintf(szwResponse,L"%ld",cy);
                  break;
               }

               if ( PARM_WIDTH_INCHES == theParameter || PARM_HEIGHT_INCHES == theParameter ) {
                  if ( PARM_WIDTH_INCHES == theParameter )
                     swprintf(szwResponse,L"%f",horizontalInchesFromPixels(cx));
                  else
                     swprintf(szwResponse,L"%f",verticalInchesFromPixels(cy));
                  break;
               }

               if ( PARM_BOUNDS_PIXELS == theParameter )
                  swprintf(szwResponse,L"0 0 %ld %ld",cx,cy);
               else
                  swprintf(szwResponse,L"0.0 0.0 %f %f",horizontalInchesFromPixels(cx),verticalInchesFromPixels(cy));
               }
               break;

            case PARM_DEVICE_WIDTH_PIXELS:
            case PARM_DEVICE_HEIGHT_PIXELS: {
               //
               //NTC: 11-02-2017: These are not in the prior case statement because there may
               // be a possibility in the future that "device" is not the same size as the particular
               // "view" on the device (which is also why the "bounds" logic in the above exists).
               //
               if ( PARM_DEVICE_WIDTH_PIXELS == theParameter )
                  swprintf(szwResponse,L"%ld",properties.cxDefault);
               else
                  swprintf(szwResponse,L"%ld",properties.cyDefault);
               }
               break;

            case PARM_CLIENT_ADDRESS: {
               swprintf(szwResponse,L"%ls",szwClientAddress);
               }
               break;

            case PARM_SHOW_INK:
               swprintf(szwResponse,L"%ld",properties.showInk);
               break;

            case PARM_SEND_POINTS:
               swprintf(szwResponse,L"%ld",sendPoints);
               break;

            case PARM_IMAGE: {

               HDC hdcTarget = CreateCompatibleDC(NULL);

               HBITMAP hbmTarget = CreateBitmap(rcDisplay.right - rcDisplay.left,rcDisplay.bottom - rcDisplay.top,1,GetDeviceCaps(hdcTarget,BITSPIXEL),NULL);

               HGDIOBJ oldObj = SelectObject(hdcTarget,hbmTarget);

               SendMessage(hwndMain,WM_PAINT,0L,(LPARAM)hdcTarget);

               SelectObject(hdcTarget,oldObj);

               DeleteDC(hdcTarget);

               char szBitmap[MAX_PATH];

               strcpy(szBitmap,_tempnam(NULL,NULL));

               BSTR bstrFMS = SysAllocStringLen(NULL,MAX_PATH);

               MultiByteToWideChar(CP_ACP,0,szBitmap,-1,bstrFMS,MAX_PATH);

               Gdiplus::Bitmap *pBitmap = Gdiplus::Bitmap::FromHBITMAP(hbmTarget,NULL);

               pBitmap -> Save(bstrFMS,&pTheEncoder -> Clsid,NULL);

               delete pBitmap;

               DeleteObject(hbmTarget);

               FILE *fImage = fopen(szBitmap,"rb");
               fseek(fImage,0,SEEK_END);
               long countBytes = ftell(fImage);
               fseek(fImage,0,SEEK_SET);

               swprintf(szwResponse,L"recieve %ld bytes",countBytes);

               send(commandClientSocket,(char *)szwResponse,2 * (DWORD)wcslen(szwResponse),0L);

               recv(commandClientSocket,(char *)szwResponse,4,0L);

               BYTE *pBinary = new BYTE[countBytes];
   
               fread(pBinary,1,countBytes,fImage);

               fclose(fImage);

               long totalBytes = 0L;

               for ( totalBytes = 0; totalBytes + 512 < countBytes; totalBytes += 512 ) {
                  send(commandClientSocket,(char *)pBinary + totalBytes,512,0L);
                  recv(commandClientSocket,(char *)szwResponse,4,0L);
               }

               if ( totalBytes < countBytes ) {
                  send(commandClientSocket,(char *)pBinary + totalBytes,countBytes - totalBytes,0L);
                  recv(commandClientSocket,(char *)szwResponse,4,0L);
               }

               recv(commandClientSocket,(char *)szwResponse,4,0L);

               delete [] pBinary;

               DeleteFileA(szBitmap);

               continue;

               }
               break;

            case PARM_FONT_SIZE:
            case PARM_FONT_FAMILY: {

               LOGFONTW baseSystemFont = {0};

               if ( ! control::hFontDefault )
                  GetObjectW((HFONT)GetStockObject(DEFAULT_GUI_FONT),sizeof(LOGFONTW),&baseSystemFont);
               else
                  GetObjectW(control::hFontDefault,sizeof(LOGFONTW),&baseSystemFont);

               if ( PARM_FONT_FAMILY == theParameter )
                  swprintf(szwResponse,L"%ls",baseSystemFont.lfFaceName);
               else {
                  HDC hdc = GetDC(hwndMain);
                  float fontSize = (float)((double)baseSystemFont.lfHeight * 72.0 / (double)GetDeviceCaps(hdc,LOGPIXELSY));
                  ReleaseDC(hwndMain,hdc);

                  if ( 0.0 > fontSize )
                     swprintf(szwResponse,L"%f",-fontSize);
                  else
                     swprintf(szwResponse,L"%f",fontSize);
               }

               }
               break;

            case PARM_CONTROL_TEXT:
            case PARM_CONTROL_BOUNDS:
            case PARM_CONTROL_POSITION:
            case PARM_CONTROL_TEXT_BOUNDS:
            case PARM_CONTROL_TEXT_LINE:
            case PARM_CONTROL_TEXT_FONT_HEIGHT: {

               long eventId = _wtol(wcstok(NULL,L" ,",NULL));

               szwResponse[0] = L'\0';

               control *pControl = NULL;

               for ( control *pc : control::controls ) {
                  if ( pc -> eventId == eventId ) {
                     pControl = pc;
                     break;
                  }
               }

               if ( ! pControl ) {
                  swprintf(szwResponse,L"the control with id %ld is not found",eventId);
                  break;
               }

               if ( PARM_CONTROL_TEXT == theParameter ) {
                  swprintf(szwResponse,L"%ls",pControl -> szwText);
                  break;
               }

               if ( PARM_CONTROL_BOUNDS == theParameter ) {
                  RECT rcParent,rcControl;
                  GetWindowRect(hwndMain,&rcParent);
                  GetWindowRect(pControl -> hwndControl,&rcControl);
                  long x = (long)((double)(rcControl.left - rcParent.left) / cxScale);
                  long y = (long)((double)(rcControl.top - rcParent.top) / cyScale);
                  long cx = (long)((double)(rcControl.right - rcControl.left) / cxScale);
                  long cy = (long)((double)(rcControl.bottom - rcControl.top) / cyScale);
                  swprintf(szwResponse,L"%ld %ld %ld %ld",x,y,x + cx,y + cy);
                  break;
               }

               if ( PARM_CONTROL_POSITION == theParameter ) {
                  RECT rcParent,rcControl;
                  GetWindowRect(hwndMain,&rcParent);
                  GetWindowRect(pControl -> hwndControl,&rcControl);
                  long x = (long)((double)(rcControl.left - rcParent.left) / cxScale);
                  long y = (long)((double)(rcControl.top - rcParent.top) / cyScale);
                  swprintf(szwResponse,L"%ld %ld",x,y);
                  break;
               }

               if ( PARM_CONTROL_TEXT_BOUNDS == theParameter ) {
                  SIZEL size;
                  pControl -> getSize(&size);
                  swprintf(szwResponse,L"0 0 %ld %ld",size.cx,size.cy);
                  break;
               }

               if ( PARM_CONTROL_TEXT_FONT_HEIGHT == theParameter ) {
                  swprintf(szwResponse,L"%ld",pControl -> fontHeightPixels());
                  break;
               }

swprintf(szwResponse,L"<unknown>");
MessageBoxW(NULL,szwResponse,L"",MB_OK);

               }
               break;

            default: 
               swprintf(szwResponse,L"Unknown value or parameter identifier (%ld)",theParameter);
               break;

            }
            break;

            }
            break;

         case CLEARINK: {
            if ( pSignatureGraphic ) {
               delete pSignatureGraphic;
               pSignatureGraphic = new signatureGraphic(true);
            }
            InvalidateRect(hwndMain,NULL,TRUE);
            RedrawWindow(hwndMain,NULL,NULL,RDW_ERASENOW | RDW_UPDATENOW);
            }
            break;

         case CLEARSETTINGS: {
            properties.initialize();
            if ( pSignatureGraphic ) {
               delete pSignatureGraphic;
               pSignatureGraphic = new signatureGraphic(true);
            }
            swprintf(szwCommand,L" %ld -1",DESTROYCONTROL);
            PostMessage(hwndMain,WM_DESTROY_CONTROL,0,0L);
            requestorThreadId = GetCurrentThreadId();
            WaitForSingleObject(hProcessingDone,INFINITE);
            requestorThreadId = uiThreadId;
            for ( std::pair<long,WCHAR *> pair : control::controlCreationStrings )
               delete [] pair.second;
            control::controlCreationStrings.clear();
            for ( std::pair<long,WCHAR *> pair : control::controlMotionStrings )
               delete [] pair.second;
            control::controlMotionStrings.clear();
            if ( hBitmapBackground )
               DeleteObject(hBitmapBackground);
            hBitmapBackground = NULL;
            pIGProperties -> Save();
            InvalidateRect(hwndMain,NULL,TRUE);
            RedrawWindow(hwndMain,NULL,NULL,RDW_ERASENOW | RDW_UPDATENOW);
            }
            break;

         case CLEARBACKGROUND: {
            memset(properties.szwBackgroundFileName,0,sizeof(properties.szwBackgroundFileName));
            if ( hBitmapBackground )
               DeleteObject(hBitmapBackground);
            hBitmapBackground = NULL;
            pIGProperties -> Save();
            InvalidateRect(hwndMain,NULL,TRUE);
            RedrawWindow(hwndMain,NULL,NULL,RDW_ERASENOW | RDW_UPDATENOW);
            }
            break;

         case HIDECONTROL: {
            PostMessage(hwndMain,WM_HIDE_CONTROL,0L,0L);
            requestorThreadId = GetCurrentThreadId();
            WaitForSingleObject(hProcessingDone,INFINITE);
            requestorThreadId = uiThreadId;
            }
            break;

         case SHOWCONTROL: {
            PostMessage(hwndMain,WM_SHOW_CONTROL,0L,0L);
            requestorThreadId = GetCurrentThreadId();
            WaitForSingleObject(hProcessingDone,INFINITE);
            requestorThreadId = uiThreadId;
            }
            break;

         case HIDECONTROLLIST: {
            PostMessage(hwndMain,WM_HIDE_CONTROL_LIST,0L,0L);
            requestorThreadId = GetCurrentThreadId();
            WaitForSingleObject(hProcessingDone,INFINITE);
            requestorThreadId = uiThreadId;
            }
            break;

         case SHOWCONTROLLIST: {
            PostMessage(hwndMain,WM_SHOW_CONTROL_LIST,0L,0L);
            requestorThreadId = GetCurrentThreadId();
            WaitForSingleObject(hProcessingDone,INFINITE);
            requestorThreadId = uiThreadId;
            }
            break;

         case DISPLAYPDF: {

            static WCHAR szwTemp[MAX_PATH];
            WCHAR *p = wcstok(szwCommand,L" ,",NULL);
            p = wcstok(NULL,L" ,",NULL);

            long expectedFileSize = _wtol(p);

            if ( recieveFile(expectedFileSize,szwTemp) ) {

               PostMessage(hwndMain,WM_SHOW_PDF,0,(LPARAM)szwTemp);
               requestorThreadId = GetCurrentThreadId();
               WaitForSingleObject(hProcessingDone,INFINITE);
               requestorThreadId = uiThreadId;

            }

            }
            break;

         default:
            break;
         }

         if ( ! szwResponse[0] )
            wcscpy(szwResponse,L"ok");

         send(commandClientSocket,(char *)szwResponse,2 * (DWORD)wcslen(szwResponse),0L);

      }

   }

   shutdown(commandListenerSocket,SD_BOTH);
   closesocket(commandListenerSocket);
   commandListenerSocket = INVALID_SOCKET;

   commandListenerShutdown = 0L;

   if ( ! ( INVALID_SOCKET == commandClientSocket ) ) {
      shutdown(commandClientSocket,SD_BOTH);
      closesocket(commandClientSocket);
   }

   commandClientSocket = INVALID_SOCKET;

   return 0L;
   }


   long recieveFile(long expectedFileSize,WCHAR *pszwResult) {

   bool fileRecievingFailed = false;

   swprintf(szwResponse,L"send %ld bytes",expectedFileSize);

   send(commandClientSocket,(char *)szwResponse,2 * (DWORD)wcslen(szwResponse),0L);

   long totalBytes = 0;
   BYTE *pBinary = new BYTE[expectedFileSize];
   memset(pBinary,0,expectedFileSize * sizeof(BYTE));
   while ( totalBytes < expectedFileSize ) {
      long rc = recv(commandClientSocket,(char *)pBinary + totalBytes,expectedFileSize - totalBytes,0L);
      if ( 0 > rc ) {
         fileRecievingFailed = true;
         break;
      }
      totalBytes += rc;
   }

   if ( fileRecievingFailed ) {
      delete [] pBinary;
      return 0L;
   }

   swprintf(pszwResult,L"%ls",_wtempnam(NULL,NULL));//"%s\\pad",szApplicationDataDirectory);

   FILE *fFile = _wfopen(pszwResult,L"wb");
   fwrite(pBinary,1,expectedFileSize,fFile);
   fclose(fFile);

   delete [] pBinary;

   return 1L;
   }