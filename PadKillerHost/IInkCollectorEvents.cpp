// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PadKillerHost.h"

   // IUnknown

   long __stdcall IInkCollectorEvents::QueryInterface(REFIID riid,void **ppv) {

   if ( riid == IID_IUnknown )
      *ppv = static_cast<IUnknown *>(this);
   else

   if ( riid == DIID__IInkCollectorEvents )
      *ppv = static_cast<IInkCollectorEvents *>(this);
   else

      return E_NOINTERFACE;

   AddRef();
   return S_OK;
   }
   unsigned long __stdcall IInkCollectorEvents::AddRef() {
   refCount++;
   return refCount;
   }
   unsigned long __stdcall IInkCollectorEvents::Release() { 
   refCount--;
   return refCount;
   }
 
   // IDispatch

   STDMETHODIMP IInkCollectorEvents::GetTypeInfoCount(UINT * pctinfo) { 
   *pctinfo = 0;
   return S_OK;
   } 


   long __stdcall IInkCollectorEvents::GetTypeInfo(UINT itinfo,LCID lcid,ITypeInfo **pptinfo) { 
   *pptinfo = NULL; 
   if ( itinfo != 0 ) 
      return DISP_E_BADINDEX; 
   return E_NOTIMPL; 
   } 
 

   STDMETHODIMP IInkCollectorEvents::GetIDsOfNames(REFIID riid,OLECHAR** rgszNames,UINT cNames,LCID lcid, DISPID* rgdispid) { 
   return E_NOTIMPL;
   }


   STDMETHODIMP IInkCollectorEvents::Invoke(DISPID dispidMember,REFIID riid,LCID lcid, 
                                                         WORD wFlags,DISPPARAMS *pDispParms,VARIANT *pVarResult,
                                                         EXCEPINFO *pexcepinfo,UINT *puArgErr) { 
   switch ( dispidMember ) {

   case DISPID_ICEStroke:
      return Stroke((IInkCursor *)pDispParms -> rgvarg[2].pdispVal,(IInkStrokeDisp *)pDispParms -> rgvarg[1].pdispVal,pDispParms -> rgvarg[0].pboolVal);

   case DISPID_ICECursorDown:
      return CursorDown((IInkCursor *)pDispParms -> rgvarg[1].pdispVal,(IInkStrokeDisp *)pDispParms -> rgvarg[0].pdispVal);

   case DISPID_ICECursorButtonDown:
      return CursorButtonDown((IInkCursor *)pDispParms -> rgvarg[1].pdispVal,(IInkCursorButton *)pDispParms -> rgvarg[0].pdispVal);

   case DISPID_ICENewInAirPackets:
      return NewInAirPackets(NULL,0L,NULL);

   case DISPID_IPEMouseDown:
      return MouseDown((InkMouseButton)pDispParms -> rgvarg[4].lVal,(InkShiftKeyModifierFlags)pDispParms -> rgvarg[3].lVal,pDispParms -> rgvarg[2].lVal,pDispParms -> rgvarg[1].lVal,pDispParms -> rgvarg[0].pboolVal);

   case DISPID_IPEMouseUp:
      return MouseDown((InkMouseButton)pDispParms -> rgvarg[4].lVal,(InkShiftKeyModifierFlags)pDispParms -> rgvarg[3].lVal,pDispParms -> rgvarg[2].lVal,pDispParms -> rgvarg[1].lVal,pDispParms -> rgvarg[0].pboolVal);

   case DISPID_IPEMouseMove:
      return MouseMove((InkMouseButton)pDispParms -> rgvarg[4].lVal,(InkShiftKeyModifierFlags)pDispParms -> rgvarg[3].lVal,pDispParms -> rgvarg[2].lVal,pDispParms -> rgvarg[1].lVal,pDispParms -> rgvarg[0].pboolVal);

   case DISPID_ICENewPackets:
      return NewPackets((IInkCursor *)pDispParms -> rgvarg[3].pdispVal,(IInkStrokeDisp *)pDispParms -> rgvarg[2].pdispVal,pDispParms -> rgvarg[1].lVal,&pDispParms -> rgvarg[0]);

   case DISPID_ICECursorOutOfRange:
      break;

   default:
      break;

   }
#if 0
    {	
   DISPID_ICEStroke	= 1,
	DISPID_ICECursorDown	= 2
	DISPID_ICENewPackets	= 3
	DISPID_ICENewInAirPackets	= 4
	DISPID_ICECursorButtonDown	= 5
	DISPID_ICECursorButtonUp	= 6
	DISPID_ICECursorInRange	= 7
	DISPID_ICECursorOutOfRange	= 8
	DISPID_ICESystemGesture	= 9
	DISPID_ICEGesture	= 10
	DISPID_ICETabletAdded	= 11
	DISPID_ICETabletRemoved	= 12
	DISPID_IOEPainting	= 13
	DISPID_IOEPainted	= 14
	DISPID_IOESelectionChanging	= 15
	DISPID_IOESelectionChanged	= 16
	DISPID_IOESelectionMoving	= 17
	DISPID_IOESelectionMoved	= 18
	DISPID_IOESelectionResizing	= 19
	DISPID_IOESelectionResized	= 20
	DISPID_IOEStrokesDeleting	= 21
	DISPID_IOEStrokesDeleted	= 22
	DISPID_IPEChangeUICues	= 23
	DISPID_IPEClick	= 24
	DISPID_IPEDblClick	= 25
	DISPID_IPEInvalidated	= 26
	DISPID_IPEMouseDown	= 27
	DISPID_IPEMouseEnter	= 28
	DISPID_IPEMouseHover	= 29
	DISPID_IPEMouseLeave	= 30
	DISPID_IPEMouseMove	= 31
	DISPID_IPEMouseUp	= 32
	DISPID_IPEMouseWheel	= 33
	DISPID_IPESizeModeChanged	= 34
	DISPID_IPEStyleChanged	= ( DISPID_IPESizeModeChanged + 1 ) ,
	DISPID_IPESystemColorsChanged	= ( DISPID_IPEStyleChanged + 1 ) ,
	DISPID_IPEKeyDown	= ( DISPID_IPESystemColorsChanged + 1 ) ,
	DISPID_IPEKeyPress	= ( DISPID_IPEKeyDown + 1 ) ,
	DISPID_IPEKeyUp	= ( DISPID_IPEKeyPress + 1 ) ,
	DISPID_IPEResize	= ( DISPID_IPEKeyUp + 1 ) ,
	DISPID_IPESizeChanged	= ( DISPID_IPEResize + 1 ) 
#endif
   return E_NOTIMPL;
   }

   HRESULT IInkCollectorEvents::Stroke(IInkCursor * pCursor,IInkStrokeDisp *pStroke,VARIANT_BOOL *pCancel) {
   strokeCount++;
   if ( pCancel )
      *pCancel = VARIANT_FALSE;
   return S_OK;
   }

   HRESULT IInkCollectorEvents::CursorDown(IInkCursor *pCursor,IInkStrokeDisp *pStroke) {
   strokeCount++;
   return S_OK;
   }

   HRESULT IInkCollectorEvents::NewPackets(IInkCursor *pCursor,IInkStrokeDisp *pStroke,long packetCount,VARIANT *pPacketData) {

#if 0
   long strokeId = strokeCount - 1;

   if ( oldStrokeNumber != strokeId )
      fire_PenPoint(0,0);

   long n = 0;
   SAFEARRAY *pValues = (SAFEARRAY *)((VARIANT *)pPacketData -> byref) -> parray;

   for ( long k = 0; k < packetCount; k++ ) {

      long pX,pY;

      long index = 0;
      SafeArrayGetElement(&pValues[k],&index,&pX);

      index = 1;
      SafeArrayGetElement(&pValues[k],&index,&pY);

      pIInkRenderer -> InkSpaceToPixel((LONG_PTR)hdcClient,&pX,&pY);

      fire_PenPoint(pX,pY);

   }
#endif

   return S_OK;
   }


   HRESULT IInkCollectorEvents::DblClick(VARIANT_BOOL * Cancel ) {
   return S_OK;
   }


   HRESULT IInkCollectorEvents::MouseMove(InkMouseButton button,InkShiftKeyModifierFlags Shift,long pX,long pY,VARIANT_BOOL *pCancel ) {

   if ( ! IMF_Left == button )
      return S_OK;

   long strokeId = strokeCount - 1;

   if ( sendPoints && ! ( oldStrokeNumber == strokeId ) )
      fire_PenDown(pX,pY);

   oldStrokeNumber = strokeId;

   fire_PenPoint(pX,pY);

   return S_OK;
   }

   HRESULT IInkCollectorEvents::MouseDown(InkMouseButton Button,InkShiftKeyModifierFlags Shift,long x,long y,VARIANT_BOOL * Cancel ) {
   strokeCount++;
   for ( std::list<hotSpot *>::iterator it = hotSpots.begin(); it != hotSpots.end(); it++ ) {
      if ( (*it) -> contains(x,y) ) {
         fire_OptionSelected((*it) -> optionNumber);
         return S_OK;
      }
   }
   static long lastX = -1L, lastY = -1L;
   if ( lastX == x && lastY == y )
      return S_OK;
   lastX = x;
   lastY = y;
   fire_PenDown(x,y);
   return S_OK;
   }

   HRESULT IInkCollectorEvents::MouseUp(InkMouseButton Button,InkShiftKeyModifierFlags Shift,long x,long y,VARIANT_BOOL * Cancel ) {
   strokeCount++;
   fire_PenUp(x,y);
   return S_OK;
   }

   HRESULT IInkCollectorEvents::MouseWheel(InkMouseButton Button,InkShiftKeyModifierFlags Shift,long Delta,long x,long y,VARIANT_BOOL * Cancel ) {
   return S_OK;
   }

   HRESULT IInkCollectorEvents::NewInAirPackets(IInkCursor * pCursor,long packetCount,VARIANT * pPacketData ) {
   return S_OK;
   }

   HRESULT IInkCollectorEvents::CursorButtonDown(IInkCursor * pCursor,IInkCursorButton * pButton ) {
   return S_OK;
   }

   HRESULT IInkCollectorEvents::CursorButtonUp(IInkCursor * pCursor,IInkCursorButton * pButton ) {
   return S_OK;
   }

   HRESULT IInkCollectorEvents::CursorInRange(IInkCursor * pCursor,VARIANT_BOOL NewCursor,VARIANT & ButtonsState ) {
   return S_OK;
   }

   HRESULT IInkCollectorEvents::CursorOutOfRange(IInkCursor * pCursor ) {
   return S_OK;
   }

   HRESULT IInkCollectorEvents::SystemGesture(IInkCursor * Cursor,InkSystemGesture Id,long x,long y,long Modifier,BSTR Character,long CursorMode ) {
   return S_OK;
   }

   HRESULT IInkCollectorEvents::Gesture(IInkCursor * Cursor,IInkStrokes * Strokes,VARIANT & Gestures,VARIANT_BOOL * Cancel ) {
   return S_OK;
   }

   HRESULT IInkCollectorEvents::TabletAdded(IInkTablet * Tablet ) {
   return S_OK;
   }

   HRESULT IInkCollectorEvents::TabletRemoved(long TabletId ) {
   return S_OK;
   }
