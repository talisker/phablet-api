// Copyright 2017 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PadKillerHost.h"


   _IConnectionPointContainer::_IConnectionPointContainer() 
   { 
   return;
   }

   _IConnectionPointContainer::~_IConnectionPointContainer() {
   return;
   }


   HRESULT _IConnectionPointContainer::QueryInterface(REFIID riid,void **ppv) {
   return ::QueryInterface(riid,ppv);
   }


   STDMETHODIMP_(ULONG) _IConnectionPointContainer::AddRef() {
   return ::AddRef();
   }

   STDMETHODIMP_(ULONG) _IConnectionPointContainer::Release() {
   return ::Release();
   }


   STDMETHODIMP _IConnectionPointContainer::EnumConnectionPoints(IEnumConnectionPoints **ppEnum) {

   _IConnectionPoint *connectionPoints[1];

   *ppEnum = NULL;
 
   if ( enumConnectionPoints ) 
      delete enumConnectionPoints;
 
   connectionPoints[0] = &connectionPoint;
   enumConnectionPoints = new _IEnumConnectionPoints(connectionPoints,1);
 
   return enumConnectionPoints -> QueryInterface(IID_IEnumConnectionPoints,(void **)ppEnum);
   }
 
 
   STDMETHODIMP _IConnectionPointContainer::FindConnectionPoint(REFIID riid,IConnectionPoint **ppCP) {
   *ppCP = NULL;
//FIXME
#if 0
   if ( riid == IID_ISignaturePadEvents )
      return connectionPoint.QueryInterface(IID_IConnectionPoint,(void **)ppCP);
#endif
   return CONNECT_E_NOCONNECTION;
   }


   void _IConnectionPointContainer::fire_PenUp() {
//FIXME
#if 0
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   connectionPoint.EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      ISignaturePadEvents * pClient = reinterpret_cast<ISignaturePadEvents *>(connectData.pUnk);
      pClient -> PenUp();
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
#endif
   return;
   }


   void _IConnectionPointContainer::fire_PenDown() {
//FIXME
#if 0
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   connectionPoint.EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      ISignaturePadEvents * pClient = reinterpret_cast<ISignaturePadEvents *>(connectData.pUnk);
      pClient -> PenDown();
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
#endif
   return;
   }


   void _IConnectionPointContainer::fire_PenPoint(long x,long y) {
//FIXME
#if 0
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   connectionPoint.EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      ISignaturePadEvents * pClient = reinterpret_cast<ISignaturePadEvents *>(connectData.pUnk);
      pClient -> PenPoint(x,y);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
#endif
   return;
   }

   void _IConnectionPointContainer::fire_OptionSelected(long optionNumber) {
//FIXME
#if 0
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   connectionPoint.EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      ISignaturePadEvents * pClient = reinterpret_cast<ISignaturePadEvents *>(connectData.pUnk);
      pClient -> OptionSelected(optionNumber);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
#endif
   return;
   }


