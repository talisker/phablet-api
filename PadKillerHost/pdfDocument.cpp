// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PadKillerHost.h"

   HWND hwndHTMLHost = NULL;

   RECT rcHTMLHost = {0};

   pdfDocument::pdfDocument(char *pszFileName) : 

      pIOleClientSite_HTML_Host(NULL),
      pIOleDocumentSite_HTML_Host(NULL),
      pIOleInPlaceSite_HTML_Host(NULL),
      pIOleInPlaceFrame_HTML_Host(NULL),

      pIOleObject_HTML(NULL),
      pIOleInPlaceObject_HTML(NULL),
      pIOleInPlaceActiveObject_HTML(NULL),

      refCount(0)

   { 
   memset(szPDFFile,0,sizeof(szPDFFile)); 
   strcpy(szPDFFile,pszFileName);
   return;
   };

   pdfDocument::~pdfDocument() {
   DeleteFile(szPDFFile);
   return;
   }

   long __stdcall pdfDocument::QueryInterface(REFIID riid,void **ppv) {

   if ( ! ppv )
      return E_POINTER;

   *ppv = NULL;

   if ( IID_IUnknown == riid )
      *ppv = static_cast<IUnknown *>(this);
   else

#if 0
   if ( IID_IConnectionPointContainer == riid ) 
      *ppv = static_cast<IConnectionPointContainer *>(&connectionPointContainer);
   else

   if ( IID_IConnectionPoint == riid ) 
      *ppv = static_cast<IConnectionPoint *>(&connectionPoint);
   else

   if ( IID_IGPropertyPageClient == riid )
      return pIGPropertyPageClient -> QueryInterface(riid,ppv);
   else

   if ( IID_IGPropertiesClient == riid )
      *ppv = static_cast<IGPropertiesClient *>(this);
   else

   if ( riid == DIID_DWebBrowserEvents2 )
      return pDWebBrowserEvents_HTML_Host -> QueryInterface(riid,ppv);
   else

   if ( IID_IElementBehaviorFactory == riid )
      return pIElementBehaviorFactory -> QueryInterface(riid,ppv);
   else

   if ( IID_IElementBehavior == riid )
      return pIElementBehavior -> QueryInterface(riid,ppv);
   else

   if ( IID_IHTMLPainter == riid ) 
      return pIHTMLPainter -> QueryInterface(riid,ppv);
   else
#endif

   if ( IID_IOleInPlaceFrame == riid ) 
      *ppv = static_cast<IOleInPlaceFrame *>(pIOleInPlaceFrame_HTML_Host);
   else

   if ( IID_IOleInPlaceSite == riid ) 
      *ppv = static_cast<IOleInPlaceSite *>(pIOleInPlaceSite_HTML_Host);
   else

   if ( IID_IOleInPlaceSiteEx == riid ) 
      *ppv = static_cast<IOleInPlaceSiteEx *>(pIOleInPlaceSite_HTML_Host);
   else

   if ( IID_IOleDocumentSite == riid ) 
      *ppv = static_cast<IOleDocumentSite *>(pIOleDocumentSite_HTML_Host);
   else

      return E_NOINTERFACE;

   AddRef();
   return S_OK;
   }
   unsigned long __stdcall pdfDocument::AddRef() {
   return ++refCount;
   }
   unsigned long __stdcall pdfDocument::Release() { 
   return --refCount;
   }


   bool pdfDocument::show() {

   OleInitialize(NULL);

   hwndHTMLHost = hwndMain;

   GetClientRect(hwndHTMLHost,&rcHTMLHost);

   HRESULT rc = CoCreateInstance(CLSID_WebBrowser,NULL,CLSCTX_INPROC_SERVER,IID_IWebBrowser2,reinterpret_cast<void **>(&pIWebBrowser));

   CoInternetSetFeatureEnabled(FEATURE_LOCALMACHINE_LOCKDOWN,SET_FEATURE_ON_PROCESS,FALSE);

   pIOleInPlaceFrame_HTML_Host = new _IOleInPlaceFrame(this,hwndHTMLHost);
   pIOleInPlaceSite_HTML_Host = new _IOleInPlaceSite(this,pIOleInPlaceFrame_HTML_Host);
   pIOleClientSite_HTML_Host = new _IOleClientSite(this,pIOleInPlaceSite_HTML_Host,pIOleInPlaceFrame_HTML_Host);
   pIOleDocumentSite_HTML_Host = new _IOleDocumentSite(this,pIOleClientSite_HTML_Host);

   pIWebBrowser -> QueryInterface(IID_IOleObject,reinterpret_cast<void **>(&pIOleObject_HTML));
   
   pIOleObject_HTML -> QueryInterface(IID_IOleInPlaceObject,reinterpret_cast<void **>(&pIOleInPlaceObject_HTML));

   pIOleObject_HTML -> SetClientSite(pIOleClientSite_HTML_Host);

   VARIANT target,vEmpty,vFlags;

   VariantInit(&vEmpty);
   VariantInit(&target);
   VariantInit(&vFlags);

   target.vt = VT_BSTR;
   target.bstrVal = L"_self";

   vFlags.vt = VT_I4;
   vFlags.lVal = navNoReadFromCache | navNoHistory;

   rc = pIWebBrowser -> put_Resizable(TRUE);

   rc = pIWebBrowser -> put_AddressBar(VARIANT_FALSE);

   rc = pIWebBrowser -> put_FullScreen(VARIANT_FALSE);

   wchar_t homePage[MAX_PATH];
   wchar_t rootName[MAX_PATH];

   MultiByteToWideChar(CP_ACP,0,szPDFFile,-1,rootName,MAX_PATH);

   swprintf(homePage,L"file://%s",rootName);

   rc = pIWebBrowser -> Navigate(homePage,&vFlags,&target,&vEmpty,&vEmpty);

   pIOleInPlaceObject_HTML -> SetObjectRects(&rcHTMLHost,&rcHTMLHost);

   pIOleObject_HTML -> DoVerb(OLEIVERB_SHOW,NULL,pIOleClientSite_HTML_Host,0,hwndHTMLHost,&rcHTMLHost);

   return true;
   }


   void pdfDocument::hide() {

#if 0
   if ( pIConnectionPoint_HTML ) {
      pIConnectionPoint_HTML -> Unadvise(connectionCookie_HTML);
      pIConnectionPoint_HTML -> Release();
      pIConnectionPoint_HTML = NULL;
   }

   if ( pDWebBrowserEvents_HTML_Host ) 
      delete pDWebBrowserEvents_HTML_Host;

   pDWebBrowserEvents_HTML_Host = NULL;
#endif

   pIOleObject_HTML -> Close(OLECLOSE_NOSAVE);

   pIOleObject_HTML -> SetClientSite(NULL);

   pIOleInPlaceObject_HTML -> Release();

   if ( pIOleInPlaceActiveObject_HTML )
      pIOleInPlaceActiveObject_HTML -> Release();

   pIOleObject_HTML -> Release();

#if 0
   if ( pIElementBehaviorFactory )
      delete pIElementBehaviorFactory;

   pIElementBehaviorFactory = NULL;

   if ( pIElementBehavior )
      delete pIElementBehavior;

   pIElementBehavior = NULL;

   if ( pIHTMLPainter )
      delete pIHTMLPainter;
   
   pIHTMLPainter = NULL;

   if ( pDocumentElement )
      pDocumentElement -> Release();

   pDocumentElement = NULL;

   if ( pIWebBrowserDocument ) 
      pIWebBrowserDocument -> Release();

   pIWebBrowserDocument = NULL;

#endif

   long k = pIWebBrowser -> Release();

   delete pIOleInPlaceFrame_HTML_Host;
   delete pIOleInPlaceSite_HTML_Host;
   delete pIOleClientSite_HTML_Host;
   delete pIOleDocumentSite_HTML_Host;

   CoFreeUnusedLibraries();

   CoUninitialize();

   return;
   }

#include "interfacesToSupportAnEmbeddedObject_IOleClientSite.cpp"
#include "interfacesToSupportAnEmbeddedObject_IOleDocumentSite.cpp"
#include "interfacesToSupportAnEmbeddedObject_IOleInPlaceFrame.cpp"
#include "interfacesToSupportAnEmbeddedObject_IOleInPlaceSite.cpp"
