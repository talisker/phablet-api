// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PadKillerHost.h"

#ifndef GET_X_LPARAM
#define GET_X_LPARAM(lParam)	((int)(short)LOWORD(lParam))
#endif
#ifndef GET_Y_LPARAM
#define GET_Y_LPARAM(lParam)	((int)(short)HIWORD(lParam))
#endif

enum moveAction {
   none,
   move,
/*
   sizeUp,
   sizeUpRight,
*/
   sizeRight,
   sizeDownRight,
   sizeDown
/*
   sizeDownLeft,
   sizeLeft,
   sizeUpLeft
*/
} theMoveAction;

#define MOVE   1

   void setMoveAction(long mouseX,long mouseY,long cx,long cy);

   bool isMoving = false;
   bool isSizing = false;

   LRESULT CALLBACK handler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   switch ( msg ) {
   case WM_CREATE: {
      }
      return (LRESULT)0L;

   case WM_TIMER:

      if ( TIMER_CLIENT_CONNECTED == wParam ) {
         KillTimer(hwndMain,TIMER_CLIENT_CONNECTED);
         SetTimer(hwndMain,TIMER_CLIENT_ABANDONED,TIMER_CLIENT_ABANDONED_TIMEOUT,NULL);
         initialConnectionPeriodExpired = true;
         break;
      }
   
      if ( TIMER_CLIENT_ABANDONED == wParam ) {
         if ( properties.stayAlive )
            break;
         ShowWindow(hwnd,SW_HIDE);
         PostQuitMessage(0L);
      }

      break;

   case WM_RBUTTONUP: {

      if ( isUnderConfiguration ) 
         break;

      if ( hOptionsMenu )
         DestroyMenu(hOptionsMenu);

      hOptionsMenu = CreatePopupMenu();

      MENUITEMINFO menuItem = {0};

      menuItem.cbSize = sizeof(MENUITEMINFO);
      menuItem.fMask = MIIM_ID | MIIM_TYPE | MIIM_STATE;
      menuItem.fType = MFT_STRING;
      menuItem.fState = MFS_ENABLED;

      menuItem.wID = ID_CONFIGURE;
      menuItem.dwTypeData = "Configure";
      menuItem.cch = (DWORD)strlen(menuItem.dwTypeData);
      menuItem.fState = MFS_ENABLED;

      InsertMenuItem(hOptionsMenu,1,MF_BYPOSITION,&menuItem);

      menuItem.wID = ID_START;
      menuItem.dwTypeData = "Start";
      menuItem.cch = (DWORD)strlen(menuItem.dwTypeData);
      menuItem.fState = MFS_ENABLED;

      InsertMenuItem(hOptionsMenu,2,MF_BYPOSITION,&menuItem);

      menuItem.wID = ID_STOP;
      menuItem.dwTypeData = "Stop";
      menuItem.cch = (DWORD)strlen(menuItem.dwTypeData);
      menuItem.fState = MFS_ENABLED;

      InsertMenuItem(hOptionsMenu,3,MF_BYPOSITION,&menuItem);

      menuItem.wID = ID_EXIT;
      menuItem.dwTypeData = "Exit";
      menuItem.cch = (DWORD)strlen(menuItem.dwTypeData);
      menuItem.fState = MFS_ENABLED;

      InsertMenuItem(hOptionsMenu,4,MF_BYPOSITION,&menuItem);

      TrackPopupMenu(hOptionsMenu,TPM_LEFTALIGN,rcDisplay.left + LOWORD(lParam),rcDisplay.top + HIWORD(lParam),0,hwnd,NULL);
      }

      break;

   case WM_MOUSEMOVE: {

      if ( ! isUnderConfiguration )
         break;

      RECT rcCurrent;

      GetWindowRect(hwnd,&rcCurrent);

      long x = GET_X_LPARAM(lParam);

      long y = GET_Y_LPARAM(lParam);

      long cx = rcCurrent.right - rcCurrent.left;
      long cy = rcCurrent.bottom - rcCurrent.top;

      if ( isMoving ) {

         long dx = rcCurrent.left + x - rcMoving.left;
         long dy = rcCurrent.top + y - rcMoving.top;

         SetWindowPos(hwnd,HWND_TOP,rcCurrent.left + dx,rcCurrent.top + dy,0,0,SWP_NOSIZE);

         rcMoving.left += dx;
         rcMoving.top += dy;

         break;

      }

      if ( isSizing ) {

         long dx = x - rcMoving.left;
         long dy = y - rcMoving.top;

         switch ( theMoveAction ) {
         case sizeRight:
         case sizeDownRight:
         case sizeDown:
            if ( theMoveAction == sizeRight )
               dy = 0;
            if ( theMoveAction == sizeDown )
               dx = 0;
            SetWindowPos(hwnd,HWND_TOP,0,0,cx + dx,cy + dy,SWP_NOMOVE);
            rcMoving.left = x;
            rcMoving.top = y;
            break;

/*
         case sizeLeft:
         case sizeUpLeft:
         case sizeUp:
         case sizeDownLeft:
            SetWindowPos(hwnd,HWND_TOP,rcCurrent.left + dx,rcCurrent.top + dy,cx + dx,cy + dy,0L);
            rcMoving.left = x;
            rcMoving.top = y;
            break;
*/
         }

         char szNumber[32];
         sprintf(szNumber,"%ld",cx + dx);
         SetDlgItemText(hwndConfigure,IDDI_PIXEL_WIDTH,szNumber);
         sprintf(szNumber,"%ld",cy + dy);
         SetDlgItemText(hwndConfigure,IDDI_PIXEL_HEIGHT,szNumber);

         break;
      }

      setMoveAction(x,y,cx,cy);

      }
      break;

   case WM_LBUTTONDOWN:

      if ( ! isUnderConfiguration ) 
         break;

      GetWindowRect(hwnd,&rcMoving);

      rcMoving.left += GET_X_LPARAM(lParam);
      rcMoving.top += GET_Y_LPARAM(lParam);

      switch ( theMoveAction ) {
      case none:
         isMoving = true;
         isSizing = false;
         SetCursor(LoadCursor(NULL,IDC_HAND));
         break;

      case move:
         break;

      default:
         isMoving = false;
         isSizing = true;

         rcMoving.left = GET_X_LPARAM(lParam);
         rcMoving.top = GET_Y_LPARAM(lParam);

         TRACKMOUSEEVENT tmEvent = {0};

         tmEvent.cbSize = sizeof(TRACKMOUSEEVENT);

         tmEvent.dwFlags = TME_LEAVE;
         tmEvent.hwndTrack = hwnd;

         TrackMouseEvent(&tmEvent);

         SetCapture(hwnd);

      }

      break;

   case WM_MOUSELEAVE:
   case WM_LBUTTONUP:

      isMoving = false;
      isSizing = false;

      if ( ! isUnderConfiguration ) 
         break;

      SetCursor(LoadCursor(NULL,IDC_ARROW));

      ReleaseCapture();

      break;
      
   case WM_COMMAND: {

      long commandId = LOWORD(wParam);

      if ( BN_CLICKED == HIWORD(wParam) ) {

         long buttonStyle = (long)GetWindowLongPtr((HWND)lParam,GWL_STYLE);

         if ( buttonStyle & BS_RADIOBUTTON ) {

            SendMessage((HWND)lParam,BM_SETCHECK,BST_CHECKED,0L);   

            fire_OptionSelected(LOWORD(wParam) - IDDI_COMMAND_BUTTON_START);

            long myGroup = radioButton::radioButtonGroups[(HWND)lParam];

            for ( std::pair<HWND,long> pair : radioButton::radioButtonGroups) {
               if ( (HWND)lParam == pair.first )
                  continue;
               if ( pair.second == myGroup ) {
                  SendMessage(pair.first,BM_SETCHECK,BST_UNCHECKED,0L);
                  fire_OptionUnSelected((DWORD)GetWindowLongPtr(pair.first,GWL_ID) - IDDI_COMMAND_BUTTON_START);
               }
            }

         }

         if ( buttonStyle & BS_AUTOCHECKBOX ) {

            if ( BST_CHECKED == SendMessage((HWND)lParam,BM_GETCHECK,0L,0L) )
               fire_OptionSelected(LOWORD(wParam) - IDDI_COMMAND_BUTTON_START);
            else
               fire_OptionUnSelected(LOWORD(wParam) - IDDI_COMMAND_BUTTON_START);

         }

      }

      char szClass[128];

      long controlStyle = (long)GetWindowLongPtr((HWND)lParam,GWL_STYLE);

      GetClassName((HWND)lParam,szClass,64);

      if ( 0 == _stricmp("button",szClass) && 0L == ( controlStyle & BS_AUTOCHECKBOX ) && 0L == ( controlStyle & BS_RADIOBUTTON ) )
         fire_OptionSelected(LOWORD(wParam) - IDDI_COMMAND_BUTTON_START);

      if ( 0 == _stricmp("edit",szClass) ) {

         control *pControl = NULL;

         for ( control *pc: control::controls ) {
            if ( pc -> hwndControl == (HWND)lParam ) {
               pControl = pc;
               break;
            }
         }

         switch ( HIWORD(wParam) ) {

         case EN_CHANGE:

            if ( pControl ) {

               WCHAR szwOldText[MAX_PATH];

               wcsncpy(szwOldText,pControl -> szwText,MAX_PATH);
            
               pControl -> updateText();

               fire_TextChanged(LOWORD(wParam) - IDDI_COMMAND_BUTTON_START,szwOldText,pControl -> szwText);

            }

            break;

         default:
            break;
         }

      }

      if ( CBN_SELCHANGE == HIWORD(wParam) ) {
         WCHAR szwItem[128];
         memset(szwItem,0,sizeof(szwItem));
         SendMessageW(GetDlgItem(hwnd,LOWORD(wParam)),CB_GETLBTEXT,SendMessage(GetDlgItem(hwnd,LOWORD(wParam)),CB_GETCURSEL,0L,0L),(LPARAM)szwItem);
         fire_ItemSelected(LOWORD(wParam) - IDDI_COMMAND_BUTTON_START,szwItem);
         break;
      }

      //if ( IDDI_COMMAND_BUTTON_START <= commandId ) {
      //   fire_OptionSelected(commandId - IDDI_COMMAND_BUTTON_START);
      //   return (LRESULT)0L;
      //}

      switch ( LOWORD(wParam) ) {
      case ID_CONFIGURE: {
         IUnknown *pIUnknown = NULL;
         pIGPropertiesClient -> QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));
         pIGProperties -> ShowProperties(NULL,pIUnknown);
         pIUnknown -> Release();
         pIGProperties -> Save();
         }
         break;

      case ID_START:
         start();
         break;

      case ID_STOP:
         stop();
         break;

      case ID_EXIT:
         PostMessage(hwndMain,WM_QUIT,0L,0L);
         break;

      default:
         break;
      }
      }
      break;

   case WM_SIZE:
   case WM_MOVE:
      GetWindowRect(hwnd,&rcDisplay);
      if ( pPDFDocument && pPDFDocument -> pIOleInPlaceObject_HTML ) 
         pPDFDocument -> pIOleInPlaceObject_HTML -> SetObjectRects(&rcDisplay,&rcDisplay);
      break;

   case WM_PAINT: {

      if ( pPDFDocument )
         break;

      PAINTSTRUCT ps = {0};

      BeginPaint(hwnd,&ps);

      HDC hdcForImage = (HDC)lParam;

      if ( hdcForImage ) {

         if ( hBitmapBackground ) {
            HDC hdcSource = CreateCompatibleDC(NULL);
            HGDIOBJ oldObj = SelectObject(hdcSource,hBitmapBackground);
            StretchBlt(hdcForImage,0,0,rcDisplay.right - rcDisplay.left,rcDisplay.bottom - rcDisplay.top,hdcSource,0,0,cxBitmapBackground,cyBitmapBackground,SRCCOPY);
            SelectObject(hdcSource,oldObj);
            DeleteDC(hdcSource);
         } else
            BitBlt(hdcForImage,0,0,rcDisplay.right - rcDisplay.left,rcDisplay.bottom - rcDisplay.top,NULL,0,0,WHITENESS);

         if ( pSignatureGraphic ) {

            long *pX = pSignatureGraphic -> pSignatureDataX;
            long *pY = pSignatureGraphic -> pSignatureDataY;

            COLORREF color = (COLORREF)properties.inkColor;

            float weight = (float)properties.inkWeight;

            Gdiplus::Pen *pPen = new Gdiplus::Pen(Gdiplus::Color(GetRValue(color),GetGValue(color),GetBValue(color)),weight);

            pPen -> SetLineCap(Gdiplus::LineCapSquare,Gdiplus::LineCapSquare,Gdiplus::DashCapFlat);

            Gdiplus::Graphics *pGraphics = new Gdiplus::Graphics(hdcForImage);

            bool nextIsMove = false;

            long lastSignatureX = 0L;
            long lastSignatureY = 0L;

            for ( long k = 0; k < pSignatureGraphic -> totalPoints; k++ ) {
               long x = *pX;
               long y = *pY;
               pX++;
               pY++;
               if ( 0 == x && 0 == y ) {
                  nextIsMove = true;
                  continue;
               }
               if ( ! nextIsMove ) 
                  pGraphics -> DrawLine(pPen,lastSignatureX,lastSignatureY,x,y);
               nextIsMove = false;
               lastSignatureX = x;
               lastSignatureY = y;
            }

            delete pGraphics;
            delete pPen;

         }

         EndPaint(hwnd,&ps);

         break;

      }

      if ( hBitmapBackground ) {

         HDC hdcSource = CreateCompatibleDC(NULL);

         HGDIOBJ oldObj = SelectObject(hdcSource,hBitmapBackground);

         BOOL rc = StretchBlt(ps.hdc,0,0,rcDisplay.right - rcDisplay.left,rcDisplay.bottom - rcDisplay.top,hdcSource,0,0,cxBitmapBackground,cyBitmapBackground,SRCCOPY);

         for ( control *pControl : control::controls )
            RedrawWindow(pControl -> hwndControl,NULL,NULL,RDW_UPDATENOW);

         SelectObject(hdcSource,oldObj);

         DeleteDC(hdcSource);

      } else

         BitBlt(ps.hdc,ps.rcPaint.left,ps.rcPaint.top,ps.rcPaint.right - ps.rcPaint.left,ps.rcPaint.bottom - ps.rcPaint.top,NULL,0,0,WHITENESS);

      EndPaint(hwnd,&ps);

      }
      break;

   case WM_DESTROY:
      if ( pPDFDocument ) {
         pPDFDocument -> hide();
         delete pPDFDocument;
         pPDFDocument = NULL;
      }
      break;

   case WM_CTLCOLORBTN:
   case WM_CTLCOLORSTATIC: {
      HDC hdc = (HDC)wParam;
      SetBkMode(hdc,TRANSPARENT);
      }
      return (LRESULT)GetStockObject(HOLLOW_BRUSH);

   case WM_SET_BACKGROUND:
      setBackground((WCHAR *)lParam);
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      break;

   case WM_ADD_IMAGE:
      addBackground((WCHAR *)lParam);
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      break;

   case WM_SIGNATURE_START:
      start();
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      break;

   case WM_SIGNATURE_STOP:
      stop();
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      break;

   case WM_START_EVENTS_PUBLISHER: {
      unsigned int threadAddr;
      hEventsPublisher = (HANDLE)_beginthreadex(NULL,0L,publishEvents,NULL,CREATE_SUSPENDED,&threadAddr);
      ResumeThread(hEventsPublisher);
      }
      break;

   case WM_START_COMMAND_ACCEPTANCE: {
      unsigned int threadAddr;
      hCommandListener = (HANDLE)_beginthreadex(NULL,0,commandListener,NULL,CREATE_SUSPENDED,&threadAddr);
      ResumeThread(hCommandListener);
      }
      break;

   case WM_START_CONTROL_INTERFACE: {
      unsigned int threadAddr;
      hControlInterface = (HANDLE)_beginthreadex(NULL,0,controlListener,NULL,CREATE_SUSPENDED,&threadAddr);
      ResumeThread(hControlInterface);
      }
      break;

   case WM_MOVE_SAVED_CONTROLS: 
   case WM_LOAD_SAVED_CONTROLS: {

      std::list<WCHAR *> commands;

      WCHAR *pCommandSource = properties.buttonCreationStrings;

      if ( WM_MOVE_SAVED_CONTROLS == msg )
         pCommandSource =  properties.buttonMotionStrings;

      WCHAR *p = wcstok(pCommandSource,L";",NULL);
      while ( p ) {
         if ( *p )
            commands.insert(commands.end(),p);
         p = wcstok(NULL,L";",NULL);
      }

      for ( WCHAR *pCommand : commands ) {
         wcscpy(szwCommand,pCommand);
         if ( WM_MOVE_SAVED_CONTROLS == msg )
            handler(hwnd,WM_MOVE_AND_RESIZE_CONTROL,0L,0L);
         else
            handler(hwnd,WM_CREATE_CONTROL,0L,0L);
      }

      commands.clear();

      }
      break;

   case WM_INITIALIZE_CONTROL: {

      control *pControl = (control *)lParam;

      SIZEL size;

      pControl -> getSize(&size);

      SetWindowPos(pControl -> hwndControl,HWND_TOP,0,0,size.cx,size.cy,SWP_NOMOVE);

      if ( 1 == pControl -> isVisible )
         ShowWindow(pControl -> hwndControl,SW_SHOW);

      }
      break;

   // CREATEBUTTON x, y, id, isVisible, text
   // CREATEDROPDOWNBOX x, y, id, isVisible, items selectedItem
   // CREATECHECKBOX x, y, id, isVisible, isChecked, text
   // CREATERADIOBUTTON x, y, id, isVisible, isChecked, groupNumber, text
   // CREATELABEL x, y, id, isVisible, text, fontFace, fontSize
   // CREATETEXTBOX x, y, width, maxHeight, id, isVisible, text, fontFace, fontSize
   // CREATEENTRYFIELD x, y, width, id, isVisible, text, fontFace, fontSize
   
   case WM_CREATE_CONTROL: {

      control *pControl = NULL;
      long optionNumber = -1L;

      WCHAR *pszwCreationString = new WCHAR[wcslen(szwCommand) + 1];
      memset(pszwCreationString,0,(wcslen(szwCommand) + 1) * sizeof(WCHAR));
      wcscpy(pszwCreationString,szwCommand);

      switch ( wParam ) {

      case CREATEBUTTON: {
         wcstok(szwCommand,L" ",NULL);
         RECT rc = {0};
         rc.left = _wtol(wcstok(NULL,L" ,",NULL));
         rc.top = _wtol(wcstok(NULL,L" ,",NULL));
         optionNumber = _wtol(wcstok(NULL,L" ,",NULL));
         long isVisible = _wtol(wcstok(NULL,L" ,",NULL));
         WCHAR *p = wcstok(NULL,L"��",NULL);
         pControl = new button(&rc,p,optionNumber,isVisible);
         }
         break;

      case CREATECHECKBOX:
      case CREATERADIOBUTTON: {
         wcstok(szwCommand,L" ",NULL);
         RECT rc = {0};
         rc.left = _wtol(wcstok(NULL,L" ,",NULL));
         rc.top = _wtol(wcstok(NULL,L" ,",NULL));
         optionNumber = _wtol(wcstok(NULL,L" ,",NULL));
         long isVisible = _wtol(wcstok(NULL,L" ,",NULL));
         long isChecked = _wtol(wcstok(NULL,L" ,",NULL));
         if ( CREATECHECKBOX == wParam ) 
            pControl = new checkBox(&rc,wcstok(NULL,L"��",NULL),optionNumber,isVisible,isChecked);
         else {
            long groupNumber = _wtol(wcstok(NULL,L" ,",NULL));
            pControl = new radioButton(&rc,wcstok(NULL,L"��",NULL),optionNumber,isVisible,isChecked,groupNumber);
         }
         }
         break;

      case CREATEDROPDOWNBOX: {
         wcstok(szwCommand,L" ",NULL);
         RECT rc = {0};
         rc.left = _wtol(wcstok(NULL,L" ,",NULL));
         rc.top = _wtol(wcstok(NULL,L" ,",NULL));
         optionNumber = _wtol(wcstok(NULL,L" ,",NULL));
         long isVisible = _wtol(wcstok(NULL,L" ,",NULL));
         WCHAR *pszwItems = wcstok(NULL,L"��",NULL);
         WCHAR *pszwSelectedItem = wcstok(NULL,L"��",NULL);
         pControl = new dropDownList(&rc,pszwItems,pszwSelectedItem,optionNumber,isVisible);
         }
         break;

      case CREATELABEL: {
         wcstok(szwCommand,L" ",NULL);
         RECT rc = {0};
         rc.left = _wtol(wcstok(NULL,L" ,",NULL));
         rc.top = _wtol(wcstok(NULL,L" ,",NULL));
         optionNumber = _wtol(wcstok(NULL,L" ,",NULL));
         WCHAR *pszwIsVisible = wcstok(NULL,L" ,",NULL);
         long isVisible = _wtol(pszwIsVisible);
         WCHAR *pszwText = NULL;
         WCHAR *pszwFont = NULL;
         WCHAR *pszwFontSize = NULL;
         WCHAR *pszwEmptyText = wcsstr(pszwIsVisible + wcslen(pszwIsVisible) + 1,L"��");
         if ( NULL == pszwEmptyText ) {
            pszwText = wcstok(NULL,L"��",NULL);
            pszwFont = NULL;
            pszwFontSize = NULL;
            if ( NULL == wcsstr(pszwText + wcslen(pszwText) + 1,L"\"\"") ) {
               pszwFont = wcstok(NULL,L" \"",NULL);
               pszwFontSize = wcstok(NULL,L" ",NULL);
            }
         } else {
            pszwEmptyText += 2;
            if ( NULL == wcsstr(pszwEmptyText,L"\"\"") ) {
               pszwFont = wcstok(pszwEmptyText,L" \"",NULL);
               pszwFontSize = wcstok(NULL,L" ",NULL);
            }
         }
         pControl = new label(&rc,0L,pszwText,optionNumber,isVisible);
         if ( pszwFont )
            pControl -> setFont(pszwFont,(float)_wtof(pszwFontSize));
         }
         break;

      case CREATEENTRYFIELD: {
         wcstok(szwCommand,L" ",NULL);
         RECT rc = {0};
         rc.left = _wtol(wcstok(NULL,L" ,",NULL));
         rc.top = _wtol(wcstok(NULL,L" ,",NULL));
         long width = _wtol(wcstok(NULL,L" ,",NULL));
         optionNumber = _wtol(wcstok(NULL,L" ,",NULL));
         WCHAR *pszwIsVisible = wcstok(NULL,L" ,",NULL);
         long isVisible = _wtol(pszwIsVisible);
         WCHAR *pszwText = NULL;
         WCHAR *pszwFont = NULL;
         WCHAR *pszwFontSize = NULL;
         WCHAR *pszwEmptyText = wcsstr(pszwIsVisible + wcslen(pszwIsVisible) + 1,L"��");
         if ( NULL == pszwEmptyText ) {
            pszwText = wcstok(NULL,L"��",NULL);
            pszwFont = NULL;
            pszwFontSize = NULL;
            if ( NULL == wcsstr(pszwText + wcslen(pszwText) + 1,L"\"\"") ) {
               pszwFont = wcstok(NULL,L" \"",NULL);
               pszwFontSize = wcstok(NULL,L" ",NULL);
            }
         } else {
            pszwEmptyText += 2;
            if ( NULL == wcsstr(pszwEmptyText,L"\"\"") ) {
               pszwFont = wcstok(pszwEmptyText,L" \"",NULL);
               pszwFontSize = wcstok(NULL,L" ",NULL);
            }
         }
         pControl = new entryField(&rc,pszwText,width,optionNumber,isVisible);
         if ( pszwFont )
            pControl -> setFont(pszwFont,(float)_wtof(pszwFontSize));
         }
         break;

      case CREATETEXTBOX: {
         wcstok(szwCommand,L" ",NULL);
         RECT rc = {0};
         rc.left = _wtol(wcstok(NULL,L" ,",NULL));
         rc.top = _wtol(wcstok(NULL,L" ,",NULL));
         long width = _wtol(wcstok(NULL,L" ,",NULL));
         long maxHeight = _wtol(wcstok(NULL,L" ,",NULL));
         optionNumber = _wtol(wcstok(NULL,L" ,",NULL));
         long isVisible = _wtol(wcstok(NULL,L" ,",NULL));
         WCHAR *pszwText = wcstok(NULL,L"��",NULL);
         WCHAR *pszwFont = NULL;
         WCHAR *pszwFontSize = NULL;
         if ( NULL == wcsstr(pszwText + wcslen(pszwText) + 1,L"\"\"") ) {
            pszwFont = wcstok(NULL,L" \"",NULL);
            pszwFontSize = wcstok(NULL,L" ",NULL);
         }
         pControl = new textBox(&rc,pszwText,width,maxHeight,optionNumber,isVisible);
         if ( pszwFont )
            pControl -> setFont(pszwFont,(float)_wtof(pszwFontSize));
         }
         break;

      }

      if ( pControl ) {
         control::controls.insert(control::controls.end(),pControl);
         control::controlCreationStrings[optionNumber] = pszwCreationString;
      } else
         delete [] pszwCreationString;

      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);

      }
      break;

   case WM_DESTROY_CONTROL: {

      pkCommands commandId = (pkCommands)_wtol(wcstok(szwCommand,L" ",NULL));
      long optionNumber = _wtol(wcstok(NULL,L" ",NULL));
      std::list<control *> toRemove;
      for ( control *pControl : control::controls ) {
         if ( -1 == optionNumber || pControl -> eventId == optionNumber ) {
            ShowWindow(pControl -> hwndControl,SW_HIDE);
            DestroyWindow(pControl -> hwndControl);
            toRemove.insert(toRemove.end(),pControl);
            if ( ! ( -1 == optionNumber ) )
               break;
         }
      }
      for ( control *pControl : toRemove ) {
         if ( ! ( -1 == optionNumber ) ) {
            WCHAR *pbcs = control::controlCreationStrings[pControl -> eventId];
            delete [] pbcs;
            control::controlCreationStrings.erase(pControl -> eventId);
            pbcs = control::controlMotionStrings[pControl -> eventId];
            delete [] pbcs;
            control::controlMotionStrings.erase(pControl -> eventId);
         }
         control::controls.remove(pControl);
         delete pControl;
      }
      toRemove.clear();
      if ( -1 == optionNumber ) {
         if ( hBitmapBackground )
            DeleteObject(hBitmapBackground);
         hBitmapBackground = NULL;
         InvalidateRect(hwndMain,NULL,TRUE);
         RedrawWindow(hwndMain,NULL,NULL,RDW_UPDATENOW);
      }
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      RedrawWindow(hwnd,NULL,NULL,RDW_ERASE | RDW_UPDATENOW);
      }
      break;

   case WM_MOVE_CONTROL: {
      WCHAR *pMoveString = new WCHAR[wcslen(szwCommand) + 1];
      memset(pMoveString,0,(wcslen(szwCommand) + 1) * sizeof(WCHAR));
      wcscpy(pMoveString,szwCommand);
      long eventId = _wtol(wcstok(szwCommand,L" ,",NULL));
      long x = _wtol(wcstok(NULL,L" ,",NULL));
      long y = _wtol(wcstok(NULL,L" ,",NULL));
      control::controlMotionStrings[eventId] = pMoveString;
      for ( control *pControl : control::controls ) {
         if ( pControl -> eventId == eventId ) {
            pControl -> setLocation(x,y);
            wcscpy(szwResponse,L"ok");
            break;
         }
      }
      for ( control *pControl : control::controls ) {
         InvalidateRect(pControl -> hwndControl,NULL,TRUE);
         RedrawWindow(pControl -> hwndControl,NULL,NULL,RDW_UPDATENOW | RDW_ERASENOW);
      }
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      }
      break;

   case WM_MOVE_AND_RESIZE_CONTROL: {
      WCHAR *pMoveString = new WCHAR[wcslen(szwCommand) + 1];
      memset(pMoveString,0,(wcslen(szwCommand) + 1) * sizeof(WCHAR));
      wcscpy(pMoveString,szwCommand);
      long eventId = _wtol(wcstok(szwCommand,L" ,",NULL));
      long x = _wtol(wcstok(NULL,L" ,",NULL));
      long y = _wtol(wcstok(NULL,L" ,",NULL));
      long cx = _wtol(wcstok(NULL,L" ,",NULL)) - x;
      long cy = _wtol(wcstok(NULL,L" ,",NULL)) - y;
      control::controlMotionStrings[eventId] = pMoveString;
      for ( control *pControl : control::controls ) {
         if ( pControl -> eventId == eventId ) {
            pControl -> setBounds(x,y,cx,cy);
            wcscpy(szwResponse,L"ok");
            break;
         }
      }
      for ( control *pControl : control::controls ) {
         InvalidateRect(pControl -> hwndControl,NULL,TRUE);
         RedrawWindow(pControl -> hwndControl,NULL,NULL,RDW_UPDATENOW | RDW_ERASENOW);
      }
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      }
      break;

   case WM_SET_CONTROL_TEXT: {
      long eventId = _wtol(wcstok(szwCommand,L" ,",NULL));
      WCHAR *pszwText = wcstok(NULL,L"��",NULL);
      for ( control *pControl : control::controls ) {
         if ( pControl -> eventId == eventId ) {
            pControl -> setText(pszwText);
            wcscpy(szwResponse,L"ok");
            break;
         }
      }
      for ( control *pControl : control::controls ) {
         InvalidateRect(pControl -> hwndControl,NULL,TRUE);
         RedrawWindow(pControl -> hwndControl,NULL,NULL,RDW_UPDATENOW | RDW_ERASENOW);
      }
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      }
      break;

   case WM_SET_OPTION_STATE: {
      wcstok(szwCommand,L" ,",NULL);
      long eventId = _wtol(wcstok(NULL,L" ,",NULL));
      long optionState = _wtol(wcstok(NULL,L" ,",NULL));
      for ( control *pControl : control::controls ) {
         if ( pControl -> eventId == eventId ) {
            pControl -> setOptionState(optionState);
            wcscpy(szwResponse,L"ok");
            break;
         }
      }
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      }
      break;

   case WM_SELECT_CONTROL_ITEM: {
      wcstok(szwCommand,L" ,",NULL);
      long eventId = _wtol(wcstok(NULL,L" ,",NULL));
      WCHAR *pszwItem = wcstok(NULL,L"��",NULL);
      for ( control *pControl : control::controls ) {
         if ( pControl -> eventId == eventId ) {
            pControl -> selectItem(pszwItem);
            wcscpy(szwResponse,L"ok");
            break;
         }
      }
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      }
      break;

   case WM_FIRE_DEVICE_READY:
      fire_DeviceReady();
      break;

   case WM_HIDE_CONTROL: {
      pkCommands commandId = (pkCommands)_wtol(wcstok(szwCommand,L" ",NULL));
      long controlId = _wtol(wcstok(NULL,L" ",NULL));
      for ( control *pControl : control::controls ) {
         if ( -1 == controlId || pControl -> eventId == controlId ) {
            ShowWindow(pControl -> hwndControl,SW_HIDE);
            if ( -1L == controlId )
               continue;
            break;
         }
      }
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      RedrawWindow(hwnd,NULL,NULL,RDW_ERASE | RDW_UPDATENOW);
      }
      break;

   case WM_SHOW_CONTROL: {
      pkCommands commandId = (pkCommands)_wtol(wcstok(szwCommand,L" ",NULL));
      long controlId = _wtol(wcstok(NULL,L" ",NULL));
      for ( control *pControl : control::controls ) {
         if ( -1 == controlId || pControl -> eventId == controlId ) {
            ShowWindow(pControl -> hwndControl,SW_SHOW);
            if ( -1L == controlId )
               continue;
            break;
         }
      }
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      RedrawWindow(hwnd,NULL,NULL,RDW_ERASE | RDW_UPDATENOW);
      }
      break;

   case WM_HIDE_CONTROL_LIST: {
      pkCommands commandId = (pkCommands)_wtol(wcstok(szwCommand,L" ",NULL));
      WCHAR *p = wcstok(NULL,L":",NULL);
      while ( p ) {
         long controlId = _wtol(p);
         for ( control *pControl : control::controls ) {
            if ( pControl -> eventId == controlId ) {
               ShowWindow(pControl -> hwndControl,SW_HIDE);
               break;
            }
         }
         p = wcstok(NULL,L":",NULL);
      }
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      RedrawWindow(hwnd,NULL,NULL,RDW_ERASE | RDW_UPDATENOW);
      }
      break;

   case WM_SHOW_CONTROL_LIST: {
      pkCommands commandId = (pkCommands)_wtol(wcstok(szwCommand,L" ",NULL));
      WCHAR *p = wcstok(NULL,L":",NULL);
      while ( p ) {
         long controlId = _wtol(p);
         for ( control *pControl : control::controls ) {
            if ( pControl -> eventId == controlId ) {
               ShowWindow(pControl -> hwndControl,SW_SHOW);
               break;
            }
         }
         p = wcstok(NULL,L":",NULL);
      }
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      RedrawWindow(hwnd,NULL,NULL,RDW_ERASE | RDW_UPDATENOW);
      }
      break;

   case WM_SHUTDOWN:
      PostMessage(hwnd,WM_QUIT,0L,0L);
      break;

   case WM_SHOW_PDF:
      if ( pPDFDocument ) {
         pPDFDocument -> hide();
         delete pPDFDocument;
      }
      pPDFDocument = NULL;
      if ( lParam ) {
         pPDFDocument = new pdfDocument((char *)lParam);
         pPDFDocument -> show();
      }
      if ( ! ( requestorThreadId == uiThreadId ) )
         ReleaseSemaphore(hProcessingDone,1,NULL);
      break;

   case WM_OTHER_INITIALIZATION:
#if 0
      if ( properties.szPDFFileName[0] ) {
         pPDFDocument = new pdfDocument(properties.szPDFFileName);
         pPDFDocument -> show();
      }
#endif
      break;

   default:
      break;

   }

   return DefWindowProc(hwnd,msg,wParam,lParam);
   }


   void setMoveAction(long x,long y,long cx,long cy) {

   theMoveAction = none;

/*
   if ( x < 8 ) {
      theMoveAction = sizeLeft;
      if ( y < 8 )
         theMoveAction = sizeUpLeft;
      else if ( abs(y - cy) < 8 )
         theMoveAction = sizeDownLeft;
   } else */
   if ( abs(x - cx) < 8 ) {
      theMoveAction = sizeRight;
      /*if ( y < 8 )
         theMoveAction = sizeUpRight;
      else */if ( abs(y - cy) < 8 )
         theMoveAction = sizeDownRight;
   } /*else if ( y < 8 ) {
      theMoveAction = sizeUp;
   } */else if ( abs(y - cy) < 8 ) {
      theMoveAction = sizeDown;
   }

   switch ( theMoveAction ) {

   case move:
      SetCursor(LoadCursor(NULL,IDC_HAND));
      break;

   //case sizeUp:
   case sizeDown:
      SetCursor(LoadCursor(NULL,IDC_SIZENS));
      break;
/*
   case sizeDownLeft:
   case sizeUpRight:
      SetCursor(LoadCursor(NULL,IDC_SIZENESW));
      break;
*/
   case sizeRight:
   //case sizeLeft:
      SetCursor(LoadCursor(NULL,IDC_SIZEWE));
      break;

   //case sizeUpLeft:
   case sizeDownRight:
      SetCursor(LoadCursor(NULL,IDC_SIZENWSE));
      break;

   default:
      SetCursor(LoadCursor(NULL,IDC_ARROW));
      break;
   }

   return;
   }