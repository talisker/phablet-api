// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#include "PadKillerHost.h"

#include <CommCtrl.h>

   LRESULT CALLBACK handler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

   struct hotSpot {
   hotSpot(RECT *p,long on) : optionNumber(on) { memcpy(&rcHotSpot,p,sizeof(RECT)); };
   bool contains(long x,long y) { return rcHotSpot.left < x && x < rcHotSpot.right && rcHotSpot.top < y && y < rcHotSpot.bottom; };
   RECT rcHotSpot;
   long optionNumber;
   };


   struct control {

   control(WCHAR *pszwClassName,long exStyle,long style,WCHAR *pszwText,RECT *pLocation,long id,long isv) {

      eventId = id;

      isVisible = isv;

      if ( pszwText )
         wcscpy(szwText,pszwText);
      else
         memset(szwText,0,sizeof(szwText));

      wcscpy(szwClassName,pszwClassName);

      hwndControl = CreateWindowExW(exStyle,pszwClassName,pszwText,style | WS_CHILD,(int)((double)pLocation -> left * cxScale),(int)((double)pLocation -> top * cyScale),0,0,hwndMain,(HMENU)(UINT_PTR)(IDDI_COMMAND_BUTTON_START + eventId),NULL,NULL);

      hFont = hFontDefault;
      fontSize = fontSizeDefault;
      fontSizePixels = fontSizePixelsDefault;

      SendMessage(hwndControl,WM_SETFONT,(WPARAM)hFont,0L);

      return;
   }

   virtual ~control() { };


   void setFont(WCHAR *pszwFontFamily,float fs) {

      LOGFONTW baseSystemFont = {0};

      GetObjectW(GetStockObject(DEFAULT_GUI_FONT),sizeof(LOGFONTW),&baseSystemFont);

      HDC hdc = GetDC(hwndControl);

      fontSize = fs;

      if ( 0.0 == fontSize )
         fontSize = properties.fontSize;

      fontSizePixels = MulDiv((int)fontSize,GetDeviceCaps(hdc,LOGPIXELSY),72);

      baseSystemFont.lfHeight = -fontSizePixels;

      ReleaseDC(hwndControl,hdc);

      if ( pszwFontFamily && pszwFontFamily[0] )
         wcscpy(baseSystemFont.lfFaceName,pszwFontFamily);
      else
         wcscpy(baseSystemFont.lfFaceName,properties.szwFontFamily);

      hFont = CreateFontIndirectW(&baseSystemFont);

      SendMessage(hwndControl,WM_SETFONT,(WPARAM)hFont,0L);

      handler(hwndMain,WM_INITIALIZE_CONTROL,0L,(LPARAM)this);

      return;
   }

   void setLocation(long x,long y) {
      SetWindowPos(hwndControl,HWND_TOP,(int)((double)x * cxScale),(int)((double)y * cyScale),0L,0L,SWP_NOSIZE);
   }

   void setBounds(long x,long y,long cx,long cy) {
      SetWindowPos(hwndControl,HWND_TOP,(int)((double)x * cxScale),(int)((double)y * cyScale),(int)((double)cx * cxScale),(int)((double)cy * cyScale),0L);
   }

   virtual void setText(WCHAR *pszwText) {
      if ( pszwText && pszwText[0] )
         wcscpy(szwText,pszwText);
      else
         szwText[0] = L'\0';
      SetWindowTextW(hwndControl,szwText);
      handler(hwndMain,WM_INITIALIZE_CONTROL,0L,(LPARAM)this);
      return;
   }

   virtual void updateText() {
      GetWindowTextW(hwndControl,szwText,sizeof(szwText) / 2);
      return;
   }

   virtual void setOptionState(long optionState) {
      return;
   }

   virtual void selectItem(WCHAR *pszItem) {
      return;
   }

   virtual void getSize(SIZEL *pSizel) = 0;

   float fontHeight() { return fontSize; };

   long fontHeightPixels() { return fontSizePixels; };

   HWND hwndControl;
   long eventId;
   HFONT hFont;
   float fontSize;
   long fontSizePixels;
   long isVisible;

   WCHAR szwText[4096];

   WCHAR szwClassName[64];

   static HFONT hFontDefault;
   static float fontSizeDefault;
   static long fontSizePixelsDefault;

   static std::list<control *> controls;

   static std::map<long,WCHAR *> controlCreationStrings;
   static std::map<long,WCHAR *> controlMotionStrings;

   };


   struct button : public control {

   button(RECT *pLocation,WCHAR *pszwLabel,long on,long isVisible) : control(L"button",0L,BS_PUSHBUTTON,pszwLabel,pLocation,on,isVisible) {
      handler(hwndMain,WM_INITIALIZE_CONTROL,0L,(LPARAM)this);
   };

   virtual void getSize(SIZEL *pSize) {

      RECT rcText = {0};

      rcText.right = 256;
      rcText.bottom = 256;

      HDC hdc = GetDC(hwndControl);
      HGDIOBJ oldFont = SelectObject(hdc,hFont);
      long textHeight = DrawTextExW(hdc,szwText,-1,&rcText,DT_CALCRECT | DT_WORDBREAK | DT_NOCLIP | DT_EDITCONTROL,NULL);
      SelectObject(hdc,oldFont);
      ReleaseDC(hwndControl,hdc);

      pSize -> cx = (int)((double)(rcText.right - rcText.left) / 0.5);
      pSize -> cy = (int)((double)textHeight / 0.80);

      return;
   }

   virtual void setOptionState(long optionState) {
      SendMessage(hwndControl,BM_SETCHECK,1 == optionState ? BST_CHECKED : BST_UNCHECKED,0L);
      return;
   }

   };


   struct label : public control {

   label(RECT *pLocation,long style,WCHAR *pszwLabel,long on,long isVisible) : control(L"Edit",0L,style,pszwLabel,pLocation,on,isVisible) {

      long n = (DWORD)wcslen(szwText) + 1;

      WCHAR *pszwText = new WCHAR[n];

      memset(pszwText,0,n * sizeof(WCHAR));

      wcscpy(pszwText,szwText);

      WCHAR *p = wcsstr(pszwText,L"\\n");

      while ( p ) {
         *p = 0x0D;
         *(p + 1) = 0x0A;
         p = wcsstr(p,L"\\n");
      }

      wcscpy(szwText,pszwText);

      delete [] pszwText;

      SetWindowTextW(hwndControl,szwText);

      handler(hwndMain,WM_INITIALIZE_CONTROL,0L,(LPARAM)this);
   }

   virtual void getSize(SIZEL *pSize) {

      RECT rcText = {0};

      rcText.right = LONG_MAX;
      rcText.bottom = LONG_MAX;

      HDC hdc = GetDC(hwndControl);
      HGDIOBJ oldFont = SelectObject(hdc,hFont);
      long textHeight = DrawTextExW(hdc,szwText,-1,&rcText,DT_CALCRECT | DT_NOCLIP | DT_EDITCONTROL,NULL);
      SelectObject(hdc,oldFont);
      ReleaseDC(hwndControl,hdc);

      pSize -> cx = (long)((double)(rcText.right - rcText.left) * 1.05);
      pSize -> cy = textHeight;

      return;
   }

   };


   struct entryField : public label {

   entryField(RECT *pLocation,WCHAR *pszwText,long w,long on,long isVisible) : label(pLocation,WS_TABSTOP | ES_LEFT | ES_AUTOHSCROLL,pszwText,on,isVisible) {
      width = w;
      SetWindowLongPtr(hwndControl,GWL_EXSTYLE,(DWORD)GetWindowLongPtr(hwndControl,GWL_EXSTYLE) | WS_EX_CLIENTEDGE);
      handler(hwndMain,WM_INITIALIZE_CONTROL,0L,(LPARAM)this);
      return;
   }

   virtual void getSize(SIZEL *pSize) {

      RECT rcText = {0};

      rcText.right = LONG_MAX;
      rcText.bottom = LONG_MAX;

      WCHAR *pszwText = szwText;
   
      if ( ! pszwText[0] )
         pszwText = L"M";

      HDC hdc = GetDC(hwndControl);
      HGDIOBJ oldFont = SelectObject(hdc,hFont);
      long textHeight = DrawTextExW(hdc,pszwText,-1,&rcText,DT_CALCRECT | DT_NOCLIP | DT_EDITCONTROL,NULL);
      SelectObject(hdc,oldFont);
      ReleaseDC(hwndControl,hdc);

      pSize -> cx = width;
      pSize -> cy = (long)((double)textHeight * 1.10);

      return;
   }

   virtual void setText(WCHAR *pszwText) {
      label::setText(pszwText);
      SendMessage(hwndControl,EM_SETSEL,(WPARAM)wcslen(szwText),(LPARAM)wcslen(szwText));
      return;
   }

   long width{0L};

   };


   struct textBox : public label {

   textBox(RECT *pLocation,WCHAR *pszwLabel,long w,long mh,long on,long isVisible) : label(pLocation,ES_MULTILINE | ES_AUTOVSCROLL | 0*ES_READONLY,pszwLabel,on,isVisible) {
      width = w;
      maxHeight = mh;
      handler(hwndMain,WM_INITIALIZE_CONTROL,0L,(LPARAM)this);
   };

   virtual void getSize(SIZEL *pSize) {

      RECT rcText = {0};

      rcText.right = width;
      rcText.bottom = LONG_MAX;

      HDC hdc = GetDC(hwndControl);
      HGDIOBJ oldFont = SelectObject(hdc,hFont);
      long textHeight = DrawTextExW(hdc,szwText,-1,&rcText,DT_CALCRECT | DT_NOCLIP | DT_WORDBREAK | DT_EDITCONTROL,NULL);
      SelectObject(hdc,oldFont);
      ReleaseDC(hwndControl,hdc);

      pSize -> cx = width;
      pSize -> cy = min(textHeight,maxHeight);

      if ( maxHeight < textHeight )
         SetWindowLongPtr(hwndControl,GWL_STYLE,(DWORD)GetWindowLongPtr(hwndControl,GWL_STYLE) | WS_VSCROLL);
      else
         SetWindowLongPtr(hwndControl,GWL_STYLE,(DWORD)GetWindowLongPtr(hwndControl,GWL_STYLE) & ~WS_VSCROLL);

      return;
   }

   long width{0L};
   long maxHeight{0L};

   };


   struct dropDownList : public control {

   dropDownList(RECT *pLocation,WCHAR *pszwItems,WCHAR *pszwSelectedItem,long on,long isVisible) : control(L"combobox",0L,CBS_DROPDOWNLIST,NULL,pLocation,on,isVisible) {

      memset(szwWidestItem,0,sizeof(szwWidestItem));

      WCHAR *pszwLocalItems = new WCHAR[wcslen(pszwItems) + 1];

      wcscpy(pszwLocalItems,pszwItems);

      WCHAR *p = wcstok(pszwLocalItems,L",",NULL);

      DWORD maxItemWidth = 0L;

      while ( p ) {
         SendMessageW(hwndControl,CB_ADDSTRING,(WPARAM)0,(LPARAM)p);
         if ( 0 == wcscmp(p,pszwSelectedItem) )
            SendMessage(hwndControl,CB_SETCURSEL,SendMessage(hwndControl,CB_GETCOUNT,0L,0L) - 1,0L);
         if ( maxItemWidth < (DWORD)wcslen(p) ) {
            maxItemWidth = (DWORD)wcslen(p);
            wcscpy(szwWidestItem,p);
         }
         p = wcstok(NULL,L",",NULL);
      }

      delete [] pszwLocalItems;

      handler(hwndMain,WM_INITIALIZE_CONTROL,0L,(LPARAM)this);

   };

   virtual void getSize(SIZEL *pSize) {

      RECT rcText = {0};

      rcText.right = LONG_MAX;
      rcText.bottom = LONG_MAX;

      HDC hdc = GetDC(hwndControl);
      HGDIOBJ oldFont = SelectObject(hdc,hFont);
      long textHeight = DrawTextExW(hdc,szwWidestItem,-1,&rcText,DT_CALCRECT | DT_WORDBREAK | DT_NOCLIP | DT_EDITCONTROL,NULL);
      SelectObject(hdc,oldFont);
      ReleaseDC(hwndControl,hdc);

      COMBOBOXINFO comboBoxInfo = {0};

      comboBoxInfo.cbSize = sizeof(comboBoxInfo);

      SendMessage(hwndControl,CB_GETCOMBOBOXINFO,0L,(LPARAM)&comboBoxInfo);

      pSize -> cx = rcText.right - rcText.left +  2 * (comboBoxInfo.rcButton.right - comboBoxInfo.rcButton.left);
      pSize -> cy = 2 * (long)SendMessage(hwndControl,CB_GETCOUNT,0L,0L) * (comboBoxInfo.rcItem.bottom - comboBoxInfo.rcItem.top);

      return;
   }

   virtual void selectItem(WCHAR *pszwItem) {
      SendMessage(hwndControl,CB_SETCURSEL,(WPARAM)SendMessageW(hwndControl,CB_FINDSTRING,(WPARAM)-1,(LPARAM)pszwItem),(LPARAM)0L);
      return;
   }

   WCHAR szwWidestItem[256];

   };


   struct checkBox : public button {

   checkBox(RECT *pLocation,WCHAR *pszwLabel,long on,long isVisible,long isChecked) : button(pLocation,pszwLabel,on,isVisible) {
      SetWindowLongPtr(hwndControl,GWL_STYLE,(GetWindowLongPtr(hwndControl,GWL_STYLE) & ~BS_PUSHBUTTON) | BS_AUTOCHECKBOX);
      SendMessage(hwndControl,BM_SETCHECK,1 == isChecked ? BST_CHECKED : BST_UNCHECKED,0L);
      handler(hwndMain,WM_INITIALIZE_CONTROL,0L,(LPARAM)this);
      return;
   }

   virtual void getSize(SIZEL *pSize) {

      RECT rcText = {0};

      rcText.right = LONG_MAX;
      rcText.bottom = LONG_MAX;

      HDC hdc = GetDC(hwndControl);
      HGDIOBJ oldFont = SelectObject(hdc,hFont);
      long textHeight = DrawTextExW(hdc,szwText,-1,&rcText,DT_CALCRECT | DT_WORDBREAK | DT_NOCLIP | DT_EDITCONTROL,NULL);
      SelectObject(hdc,oldFont);
      ReleaseDC(hwndControl,hdc);

      pSize -> cx = (long)((double)(rcText.right - rcText.left) * 1.05) + 2 * GetSystemMetrics(SM_CXMENUCHECK);
      pSize -> cy = textHeight;

      return;
   }

   };

   struct radioButton : public button {

   radioButton(RECT *pLocation,WCHAR *pszwLabel,long on,long isVisible,long isChecked,long groupNumber) : button(pLocation,pszwLabel,on,isVisible) {
      SetWindowLongPtr(hwndControl,GWL_STYLE,(GetWindowLongPtr(hwndControl,GWL_STYLE) & ~BS_PUSHBUTTON) | BS_RADIOBUTTON);
      SendMessage(hwndControl,BM_SETCHECK,1 == isChecked ? BST_CHECKED : BST_UNCHECKED,0L);
      handler(hwndMain,WM_INITIALIZE_CONTROL,0L,(LPARAM)this);
      radioButtonGroups[hwndControl] = groupNumber;
      return;
   }

   virtual ~radioButton() {
      radioButtonGroups.erase(hwndControl);
      return;
   }

   virtual void getSize(SIZEL *pSize) {

      RECT rcText = {0};

      rcText.right = LONG_MAX;
      rcText.bottom = LONG_MAX;

      HDC hdc = GetDC(hwndControl);
      HGDIOBJ oldFont = SelectObject(hdc,hFont);
      long textHeight = DrawTextExW(hdc,szwText,-1,&rcText,DT_CALCRECT | DT_WORDBREAK | DT_NOCLIP | DT_EDITCONTROL,NULL);
      SelectObject(hdc,oldFont);
      ReleaseDC(hwndControl,hdc);

      pSize -> cx = (long)((double)(rcText.right - rcText.left) * 1.05) + 2 * GetSystemMetrics(SM_CXMENUCHECK);
      pSize -> cy = textHeight;

      return;
   }

   static std::map<HWND,long> radioButtonGroups;

   };