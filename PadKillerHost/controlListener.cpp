// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PadKillerHost.h"

   char szControl[64];
   char szControlResponse[256];

#define CONTROL_SIZE 64
#define CONTROL_RESPONSE_SIZE 256

   unsigned int __stdcall controlListener(void *) {

   addrinfo addressInfo;

   addrinfo *pResolvedAddressInfo;

   memset(&addressInfo,0,sizeof(addrinfo));

   addressInfo.ai_flags = AI_PASSIVE;
   addressInfo.ai_family = AF_INET;
   addressInfo.ai_socktype = SOCK_STREAM;
   addressInfo.ai_protocol = IPPROTO_TCP;
   addressInfo.ai_addrlen = 0;
   addressInfo.ai_addr = NULL;
   addressInfo.ai_canonname = NULL;
   addressInfo.ai_next = NULL;

   getaddrinfo(NULL,CONTROL_LISTENER_PORT_A,&addressInfo,&pResolvedAddressInfo);

   controlListenerSocket = INVALID_SOCKET;

   for ( addrinfo *p = pResolvedAddressInfo; p; p = p -> ai_next ) {
      if ( ! ( SOCK_STREAM == p -> ai_socktype ) )
         continue;
      controlListenerSocket = socket(p -> ai_family,p -> ai_socktype,p -> ai_protocol);
      if ( ! ( INVALID_SOCKET == controlListenerSocket ) ) {
         if ( ! ( SOCKET_ERROR == bind(controlListenerSocket,p -> ai_addr,(int)p -> ai_addrlen) ) ) 
            break;
         controlListenerSocket = INVALID_SOCKET;
      }
   }

   if ( INVALID_SOCKET == controlListenerSocket ) {
      Beep(2000,100);
      return 0;
   }

   if ( SOCKET_ERROR == listen(controlListenerSocket,SOMAXCONN) ) {
      Beep(2000,100);
      return 0;
   }

   memset(szControl,0,CONTROL_SIZE);
   memset(szControlResponse,0,CONTROL_RESPONSE_SIZE);

   controlInterfaceShutdown = 0L;

   while ( ! controlInterfaceShutdown ) {

      sockaddr_in clientSockAddr = {0};

      int clientSockAddrSize = sizeof(sockaddr);

      controlClientSocket = accept(controlListenerSocket,(sockaddr *)&clientSockAddr,&clientSockAddrSize);

      if ( INVALID_SOCKET == controlClientSocket )
         break;

      sprintf(szControlResponse,"heartbeats: %ld ms",TIMER_CLIENT_ABANDONED_TIMEOUT);

      send(controlClientSocket,szControlResponse,(DWORD)strlen(szControlResponse),0);

      SetTimer(hwndMain,TIMER_CLIENT_CONNECTED,TIMER_DELAY_EXPECT_HEARTBEAT,NULL);

      bool clientConnected = true;

      while ( clientConnected && ! controlInterfaceShutdown ) {

         memset(szControl,0,CONTROL_SIZE);
         memset(szControlResponse,0,CONTROL_RESPONSE_SIZE);

         if ( initialConnectionPeriodExpired ) {
            KillTimer(hwndMain,TIMER_CLIENT_ABANDONED);
            SetTimer(hwndMain,TIMER_CLIENT_ABANDONED,TIMER_CLIENT_ABANDONED_TIMEOUT,NULL);
         }

         long rc = recv(controlClientSocket,szControl,4,MSG_WAITALL);

         if ( 0 > rc ) {
            controlInterfaceShutdown = 1L;
            PostQuitMessage(0L);
            break;
         }

         long byteCount = atol(szControl);

         memset(szControl,0,byteCount + 1);

         rc = recv(controlClientSocket,szControl,byteCount,MSG_WAITALL);

         if ( 0 > rc ) {
            controlInterfaceShutdown = 1L;
            PostMessage(hwndMain,WM_SHUTDOWN,0L,0L);
            break;
         }

         if ( 0 == _strnicmp(szControl,"disconnect",10) ) {
            send(controlClientSocket,"ok",2,0);
            controlInterfaceShutdown = 1L;
            PostMessage(hwndMain,WM_SHUTDOWN,0L,0L);
            break;
         }

         strcpy(szControlResponse,"ok");

         send(controlClientSocket,szControlResponse,2,0L);

      }

   }

   shutdown(controlListenerSocket,SD_BOTH);
   closesocket(controlListenerSocket);
   controlListenerSocket = INVALID_SOCKET;

   controlInterfaceShutdown = 0L;

   if ( ! ( INVALID_SOCKET == controlClientSocket ) ) {
      shutdown(controlClientSocket,SD_BOTH);
      closesocket(controlClientSocket);
   }

   controlClientSocket = INVALID_SOCKET;

   return 0L;
   }