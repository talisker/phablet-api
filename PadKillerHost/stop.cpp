// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PadKillerHost.h"

   HRESULT stop() {
   if ( ! pIInkCollector )
      return E_UNEXPECTED;
   pIInkCollector -> put_Enabled(VARIANT_FALSE);
   pIInkCollector -> SetEventInterest(ICEI_AllEvents,VARIANT_FALSE);
   return S_OK;
   }