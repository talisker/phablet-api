// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PadKillerHost.h"
#include <gdiplus.h>
#include <stdio.h>

   HRESULT setBackground(WCHAR *pszwFileName) {

   if ( pszwFileName ) {

      BSTR fms = SysAllocString(pszwFileName);

      Gdiplus::Bitmap *pBitmap = new Gdiplus::Bitmap(fms);

      cxBitmapBackground = pBitmap -> GetWidth();
      cyBitmapBackground = pBitmap -> GetHeight();

      if ( hBitmapBackground )
         DeleteObject(hBitmapBackground);

      pBitmap -> GetHBITMAP(NULL,&hBitmapBackground);

      delete pBitmap;

      SysFreeString(fms);

   }

   if ( hdcGDIPlus )
      ReleaseDC(hwndMain,hdcGDIPlus);

   if ( pGDIPlusPen )
      delete pGDIPlusPen;

   if ( pGDIPlusGraphic )
      delete pGDIPlusGraphic;

   hdcGDIPlus = GetDC(hwndMain);

   if ( ! hBitmapBackground ) {

      HDC hdcTarget = CreateCompatibleDC(NULL);

      cxBitmapBackground = properties.cxDefault;
      cyBitmapBackground = properties.cyDefault;

      cxScale = (double)(rcDisplay.right - rcDisplay.left) / (double)cxBitmapBackground;
      cyScale = (double)(rcDisplay.bottom - rcDisplay.top) / (double)cyBitmapBackground;

      hBitmapBackground = CreateCompatibleBitmap(hdcTarget,properties.cxDefault,properties.cyDefault);
      HGDIOBJ oldObj = SelectObject(hdcTarget,hBitmapBackground);
      BitBlt(hdcTarget,0,0,properties.cxDefault,properties.cyDefault,NULL,0,0,WHITENESS);
      SelectObject(hdcTarget,oldObj);
      DeleteDC(hdcTarget);

   }

   COLORREF color = (COLORREF)properties.inkColor;

   float weight = (float)properties.inkWeight;

   pGDIPlusPen = new Gdiplus::Pen(Gdiplus::Color(GetRValue(color),GetGValue(color),GetBValue(color)),weight);

   pGDIPlusPen -> SetLineCap(Gdiplus::LineCapSquare,Gdiplus::LineCapSquare,Gdiplus::DashCapFlat);

   pGDIPlusGraphic = new Gdiplus::Graphics(hdcGDIPlus);

   pGDIPlusGraphic -> SetSmoothingMode(Gdiplus::SmoothingMode::SmoothingModeHighQuality);

   InvalidateRect(hwndMain,NULL,TRUE);

   RedrawWindow(hwndMain,NULL,NULL,RDW_UPDATENOW | RDW_ERASENOW);

   return S_OK;
   }


   HRESULT addBackground(WCHAR *pszwFileName) {

   BSTR fms = SysAllocString(pszwFileName);

   Gdiplus::Bitmap *pBitmap = new Gdiplus::Bitmap(fms);

   long cx = pBitmap -> GetWidth();
   long cy = pBitmap -> GetHeight();

   HBITMAP hBitmapPartial = NULL;

   pBitmap -> GetHBITMAP(NULL,&hBitmapPartial);

   delete pBitmap;

   HDC hdcSource = CreateCompatibleDC(NULL);
   HGDIOBJ oldObj = SelectObject(hdcSource,hBitmapPartial);
   HDC hdcTarget = GetDC(hwndMain);
   HGDIOBJ oldObj2 = SelectObject(hdcTarget,hBitmapBackground);

   BOOL rc = FALSE;
   rc = BitBlt(hdcTarget,imageLocationX,imageLocationY,imageWidth,imageHeight,hdcSource,0,0,SRCCOPY);
#if 0
         for ( std::list<button *>::iterator it = buttons.begin(); it != buttons.end(); it++ ) {
            RedrawWindow((*it) -> hwnd,NULL,NULL,RDW_UPDATENOW);
         }
#endif

   SelectObject(hdcTarget,oldObj2);
   ReleaseDC(hwndMain,hdcTarget);

   SelectObject(hdcSource,oldObj);
   DeleteDC(hdcSource);

   return S_OK;
   }