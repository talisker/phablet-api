#pragma once

   class IInkCollectorEvents;
   struct IInkCollector;
   struct IInkRenderer;

   struct hotSpot;
   struct IGProperties;
   class _IGPropertiesClient;
   class _IGPropertyPageClient;

   struct signatureGraphic;

   class pdfDocument;

   struct props {

      struct props() { initialize(); };

      void initialize() {
         x = -32768;
         y = -32768;
         cxDefault = 512;
         cyDefault = 256;
         inkColor = RGB(0,0,0);
         inkWeight = 4;
         showInk = 1L;
         fontSize = 12.0f;
         monitorNumber = 1L;
         useEntire = false;
         stayAlive = false;
         memset(szwFontFamily,0,sizeof(szwFontFamily));
         memset(buttonCreationStrings,0,sizeof(buttonCreationStrings));
         memset(buttonMotionStrings,0,sizeof(buttonMotionStrings));
         memset(szwBackgroundFileName,0,sizeof(szwBackgroundFileName));
         return;
      }

      long inkColor;
      long inkWeight;
      long showInk;
      long cxDefault,cyDefault;
      bool useEntire;
      long monitorNumber;
      float fontSize;
      bool stayAlive;
      WCHAR szwFontFamily[64];
      WCHAR buttonCreationStrings[256];
      WCHAR buttonMotionStrings[256];
      WCHAR szwBackgroundFileName[MAX_PATH];
      long x,y;

   };

#ifdef DEFINE_DATA

   HMODULE hModule = NULL;
   HWND hwndMain = NULL;
   HFONT hGUIFont = NULL;
   HMENU hOptionsMenu = NULL;

   DWORD uiThreadId = -1L;
   DWORD requestorThreadId = -1L;

   HANDLE hCommandListener = NULL;
   SOCKET commandListenerSocket = INVALID_SOCKET;
   SOCKET commandClientSocket = INVALID_SOCKET;
   SOCKET commandListenerShutdown = 0L;

   HANDLE hEventsPublisher = NULL;
   SOCKET eventClientListenerSocket = INVALID_SOCKET;
   SOCKET eventClientSocket = INVALID_SOCKET;
   SOCKET eventPublisherShutdown = 0L;

   HANDLE hControlInterface = NULL;
   SOCKET controlListenerSocket = INVALID_SOCKET;
   SOCKET controlClientSocket = INVALID_SOCKET;
   SOCKET controlInterfaceShutdown = 0L;

   long cxScreen = 0L;
   long cyScreen = 0L;

   IInkCollectorEvents *pIInkCollectorEvents = NULL;
   IInkCollector *pIInkCollector = NULL;
   IInkRenderer *pIInkRenderer = NULL;

   IConnectionPointContainer* pIConnectionPointContainer = NULL;
   IConnectionPoint *pIConnectionPoint = NULL;
   DWORD dwConnectionCookie = 0L;

   RECT rcDisplay = {0};

   HBITMAP hBitmapBackground = NULL;
   long cxBitmapBackground = 0L;
   long cyBitmapBackground = 0L;

   double cxScale = 1.0L;
   double cyScale = 1.0L;

   double xPixelsToInches = 1.0;
   double yPixelsToInches = 1.0;

   WCHAR szwApplicationDataDirectory[MAX_PATH];

   long lastSignatureX = 0L;
   long lastSignatureY = 0L;

   HDC hdcGDIPlus = NULL;
   Gdiplus::Graphics *pGDIPlusGraphic = NULL;
   Gdiplus::Pen *pGDIPlusPen = NULL;
   Gdiplus::ImageCodecInfo *pTheEncoder = NULL;
   Gdiplus::EncoderParameters *pTheEncoderParameters = NULL;

   std::list<hotSpot *> hotSpots;

   long boundsExplicitlySet = 0L;

   HANDLE hProcessingDone = NULL;

   WCHAR szwCommand[COMMAND_SIZE];
   WCHAR szwResponse[RESPONSE_SIZE];
   WCHAR szwEvent[EVENT_SIZE];

   WCHAR szwClientAddress[MAX_PATH];

   bool isLocalHostClient = false;

   struct props properties;

   long sendPoints = 1L;

   long eventSpacingDuration = 0L;
   long optionEventSpacingDuration = MINIMUM_EVENT_SPACING_DURATION;
   long penDownEventSpacingDuration = MINIMUM_EVENT_SPACING_DURATION;

   enum pkCommands holdOneEvent = NOCOMMAND;
   enum pkParameters holdOneEventParameter = PARM_NOPARAMETER;

   long imageLocationX = -1L, imageLocationY = -1L, imageWidth = -1L, imageHeight = -1L;

   bool initialConnectionPeriodExpired = false;

   IGProperties *pIGProperties = NULL;
   _IGPropertiesClient *pIGPropertiesClient = NULL;
   _IGPropertyPageClient *pIGPropertyPageClient = NULL;

   signatureGraphic *pSignatureGraphic = NULL;

   long monitorIndex = -1L;
   HMONITOR hFoundMonitor = NULL;
   RECT rcMultiMonitorOffset = {0};

   pdfDocument *pPDFDocument = NULL;

   bool isUnderConfiguration = false;
   RECT rcMoving = {0};

   HWND hwndConfigure = NULL;

#else

   extern HMODULE hModule;
   extern HWND hwndMain;
   extern HFONT hGUIFont;
   extern HMENU hOptionsMenu;

   extern DWORD uiThreadId;
   extern DWORD requestorThreadId;

   extern HANDLE hCommandListener;
   extern SOCKET commandListenerSocket;
   extern SOCKET commandClientSocket;
   extern SOCKET commandListenerShutdown;

   extern HANDLE hEventsPublisher;
   extern SOCKET eventClientListenerSocket;
   extern SOCKET eventClientSocket;
   extern SOCKET eventPublisherShutdown;

   extern HANDLE hControlInterface;
   extern SOCKET controlListenerSocket;
   extern SOCKET controlClientSocket;
   extern SOCKET controlInterfaceShutdown;

   extern long cxScreen;
   extern long cyScreen;

   extern IInkCollectorEvents *pIInkCollectorEvents;

   extern IInkCollector *pIInkCollector;
   extern IInkRenderer *pIInkRenderer;

   extern IConnectionPointContainer *pIConnectionPointContainer;
   extern IConnectionPoint *pIConnectionPoint;
   extern DWORD dwConnectionCookie;

   extern RECT rcDisplay;

   extern HBITMAP hBitmapBackground;
   extern long cxBitmapBackground;
   extern long cyBitmapBackground;
   
   extern double cxScale;
   extern double cyScale;

   extern double xPixelsToInches;
   extern double yPixelsToInches;

   extern WCHAR szwApplicationDataDirectory[];

   extern long lastSignatureX;
   extern long lastSignatureY;

   extern HDC hdcGDIPlus;
   extern Gdiplus::Graphics *pGDIPlusGraphic;
   extern Gdiplus::Pen *pGDIPlusPen;
   extern Gdiplus::ImageCodecInfo *pTheEncoder;
   extern Gdiplus::EncoderParameters *pTheEncoderParameters;

   extern std::list<hotSpot *> hotSpots;

   extern long boundsExplicitlySet;

   extern HANDLE hProcessingDone;

   extern WCHAR szwCommand[];
   extern WCHAR szwResponse[];
   extern WCHAR szwEvent[];

   extern WCHAR szwClientAddress[];

   extern bool isLocalHostClient;

   extern struct props properties;

   extern long sendPoints;

   extern long eventSpacingDuration;
   extern long optionEventSpacingDuration;
   extern long penDownEventSpacingDuration;

   extern enum pkCommands holdOneEvent;
   extern enum pkParameters holdOneEventParameter;

   extern long imageLocationX,imageLocationY,imageWidth,imageHeight;

   extern bool initialConnectionPeriodExpired;

   extern IGProperties *pIGProperties;
   extern _IGPropertiesClient *pIGPropertiesClient;
   extern _IGPropertyPageClient *pIGPropertyPageClient;

   extern signatureGraphic *pSignatureGraphic;

   extern long monitorIndex;
   extern HMONITOR hFoundMonitor;

   extern RECT rcMultiMonitorOffset;

   extern pdfDocument *pPDFDocument;

   extern bool isUnderConfiguration;
   extern RECT rcMoving;

   extern HWND hwndConfigure;

#endif

