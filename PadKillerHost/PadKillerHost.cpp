// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#define DEFINE_DATA

#include "PadKillerHost.h"
#include <ShlObj.h>
#include <msinkaut_i.c>

#include "Properties_i.c"

   HFONT control::hFontDefault = NULL;
   float control::fontSizeDefault = 0.0;
   long control::fontSizePixelsDefault = 0L;

   std::list<control *> control::controls;
   std::map<long,WCHAR *> control::controlCreationStrings;
   std::map<long,WCHAR *> control::controlMotionStrings;

   std::map<HWND,long> radioButton::radioButtonGroups;

   int GetCommonAppDataLocation(HWND hwnd,WCHAR *);

   int __stdcall WinMain(HINSTANCE hInst,HINSTANCE hInstancePrevious,LPSTR lpCmdLine,int nCmdShow) {

   commandListenerShutdown = 0L;

   eventPublisherShutdown = 0L;

   CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);

   WSADATA wsaData;

   if ( WSAStartup(MAKEWORD(2,2),&wsaData) ) {
      Beep(2000,100);
      return 0;
   }

   GetCommonAppDataLocation(NULL,szwApplicationDataDirectory);

   swprintf(szwApplicationDataDirectory + wcslen(szwApplicationDataDirectory),L"\\pkDevice");

   HRESULT rc = CoCreateInstance(CLSID_InnoVisioNateProperties,NULL,CLSCTX_ALL,IID_IGProperties,reinterpret_cast<void **>(&pIGProperties));

   pIGPropertiesClient = new _IGPropertiesClient();

   pIGPropertyPageClient = new _IGPropertyPageClient();

   pIGProperties -> Advise(static_cast<IGPropertiesClient *>(pIGPropertiesClient));

   pIGProperties -> Add(L"properties",NULL);

   pIGProperties -> DirectAccess(L"properties",TYPE_BINARY,&properties,sizeof(properties));

   WCHAR szwTemp[MAX_PATH];

   swprintf(szwTemp,L"%ls\\%ls.settings",szwApplicationDataDirectory,L"pkDevice");

   BSTR bstrFileName = SysAllocString(szwTemp);

   pIGProperties -> put_FileName(bstrFileName);

   short bSuccess;

   pIGProperties -> LoadFile(&bSuccess);

   SysFreeString(bstrFileName);

   if ( bSuccess ) {

      hFoundMonitor = NULL;

      monitorIndex = -1L;

      EnumDisplayMonitors(NULL,NULL,enumFindMonitor,(LPARAM)properties.monitorNumber - 1);

      if ( hFoundMonitor ) {
         MONITORINFO monitorInfo = {0};
         monitorInfo.cbSize = sizeof(MONITORINFO);
         GetMonitorInfo(hFoundMonitor,&monitorInfo);
         rcMultiMonitorOffset = monitorInfo.rcWork;
      } else
         SystemParametersInfo(SPI_GETWORKAREA,0,&rcMultiMonitorOffset,0);

      if ( properties.useEntire ) {
         properties.x = 0;
         properties.y = 0;
      }
      
   } else
      SystemParametersInfo(SPI_GETWORKAREA,0,&rcMultiMonitorOffset,0);

   hModule = hInst;

   WNDCLASS gClass = {0};

   gClass.style = CS_BYTEALIGNCLIENT | CS_BYTEALIGNWINDOW;
   gClass.lpfnWndProc = handler;
   gClass.cbClsExtra = 32;
   gClass.cbWndExtra = 32;
   gClass.hInstance = hModule;
   gClass.hIcon = NULL;
   gClass.hCursor = NULL;
   gClass.hbrBackground = 0;
   gClass.lpszMenuName = NULL;
   gClass.lpszClassName = "padkiller";

   RegisterClass(&gClass);

   hProcessingDone = CreateSemaphore(NULL,0,1,NULL);

   hwndMain = CreateWindowEx(WS_EX_CLIENTEDGE,"padkiller","",WS_POPUP | WS_SYSMENU | WS_VISIBLE,0,0,0,0,NULL,NULL,NULL,NULL);

   hGUIFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

   cxScreen = rcMultiMonitorOffset.right - rcMultiMonitorOffset.left;

   cyScreen = rcMultiMonitorOffset.bottom - rcMultiMonitorOffset.top;

   if ( properties.cxDefault > cxScreen )
      properties.cxDefault = cxScreen;

   if ( properties.cyDefault > cyScreen )
      properties.cyDefault = cyScreen;

   HDC hdc = GetDC(hwndMain);

   double screenWidth = (double)GetDeviceCaps(hdc,HORZSIZE);

   screenWidth = screenWidth / 10.0 / 2.54;

   double screenHeight = (double)GetDeviceCaps(hdc,VERTSIZE);

   screenHeight = screenHeight / 10.0 / 2.54;

   xPixelsToInches = screenWidth / (double)cxScreen; // ( double)GetDeviceCaps(hdc,LOGPIXELSX) / screenWidth;

   yPixelsToInches = screenHeight / (double)cyScreen; // ( double)GetDeviceCaps(hdc,LOGPIXELSY) / screenHeight;

   if ( -32768 == properties.x || 0 == properties.x ) {
      properties.x = rcMultiMonitorOffset.left + cxScreen / 2 - properties.cxDefault / 2;
      properties.y = rcMultiMonitorOffset.top + cyScreen / 2 - properties.cyDefault / 2;
   }

   SetWindowPos(hwndMain,HWND_TOP,properties.x,properties.y,properties.cxDefault,properties.cyDefault,0L);

   LOGFONTW baseSystemFont = {0};

   GetObjectW(GetStockObject(DEFAULT_GUI_FONT),sizeof(LOGFONTW),&baseSystemFont);

   control::fontSizeDefault = properties.fontSize;

   control::fontSizePixelsDefault = MulDiv((int)control::fontSizeDefault,GetDeviceCaps(hdc,LOGPIXELSY),72);

   baseSystemFont.lfHeight = -control::fontSizePixelsDefault;

   ReleaseDC(hwndMain,hdc);

   if ( properties.szwFontFamily[0] )
      wcscpy(baseSystemFont.lfFaceName,properties.szwFontFamily);

   control::hFontDefault = CreateFontIndirectW(&baseSystemFont);

   ULONG_PTR gdiplusToken;

   Gdiplus::GdiplusStartupInput gdiplusStartupInput = 0L;

   Gdiplus::GdiplusStartup(&gdiplusToken,&gdiplusStartupInput,NULL);

   hdcGDIPlus = GetDC(hwndMain);

   COLORREF color = (COLORREF)properties.inkColor;

   float weight = (float)properties.inkWeight;

   pGDIPlusPen = new Gdiplus::Pen(Gdiplus::Color(GetRValue(color),GetGValue(color),GetBValue(color)),weight);

   pGDIPlusPen -> SetLineCap(Gdiplus::LineCapSquare,Gdiplus::LineCapSquare,Gdiplus::DashCapFlat);

   pGDIPlusGraphic = new Gdiplus::Graphics(hdcGDIPlus);

   pGDIPlusGraphic -> SetSmoothingMode(Gdiplus::SmoothingMode::SmoothingModeHighQuality);

   UINT arraySize;
   
   UINT countEncoders = 0L;

   Gdiplus::GetImageEncodersSize(&countEncoders,&arraySize);

   Gdiplus::ImageCodecInfo *pEncoders = (Gdiplus::ImageCodecInfo *)new BYTE[arraySize];

   Gdiplus::GetImageEncoders(countEncoders,arraySize,pEncoders);
   
   for ( UINT k = 0; k < countEncoders; k++ ) {
      if ( 0 == _wcsicmp(pEncoders[k].MimeType,L"image/jpeg") ) {
         pTheEncoder = &pEncoders[k];
#if 0
         if ( 0 == wcsicmp(preferredImageMimeType,L"image/jpeg") ) {
         Bitmap *fms = new Bitmap(1,1);
         UINT encoderParameterSize = fms -> GetEncoderParameterListSize(&pTheEncoder -> Clsid);
         if ( encoderParameterSize ) {
            pEncoderParameters = (EncoderParameters *)new BYTE[encoderParameterSize];
            fms -> GetEncoderParameterList(&pTheEncoder -> Clsid,encoderParameterSize,pEncoderParameters);
            for ( long j = 0; j < pEncoderParameters -> Count; j++ ) {
               if ( EncoderQuality /*EncoderColorDepth*/ == pEncoderParameters -> Parameter[j].Guid ) {
                  encoderQuality = 50L;
                  //pEncoderParameters -> Parameter[j].Value = &encoderQuality;
               }
            }
         }
         delete fms;
         }
#endif
         break;
      }

   }

   PostMessage(hwndMain,WM_START_COMMAND_ACCEPTANCE,0L,0L);

   PostMessage(hwndMain,WM_START_EVENTS_PUBLISHER,0L,0L);

   PostMessage(hwndMain,WM_START_CONTROL_INTERFACE,0L,0L);

//   PostMessage(hwndMain,WM_LOAD_SAVED_CONTROLS,0L,0L);
//   PostMessage(hwndMain,WM_MOVE_SAVED_CONTROLS,0L,0L);

   PostMessage(hwndMain,WM_OTHER_INITIALIZATION,0L,0L);

   if ( properties.szwBackgroundFileName[0] )
      PostMessage(hwndMain,WM_SET_BACKGROUND,0L,(LPARAM)properties.szwBackgroundFileName);

   PostMessage(hwndMain,WM_FIRE_DEVICE_READY,0L,0L);

   MSG qMessage;

   uiThreadId = GetCurrentThreadId();

   requestorThreadId = uiThreadId;

   while ( GetMessage(&qMessage,(HWND)NULL,0L,0L) ) {
      TranslateMessage(&qMessage);
      DispatchMessage(&qMessage);
   }

   DestroyWindow(hwndMain);

   commandListenerShutdown = 1L;

   if ( ! ( INVALID_SOCKET == commandListenerSocket ) || ! ( INVALID_SOCKET == commandClientSocket ) ) {
      if ( ! ( INVALID_SOCKET == commandListenerSocket ) ) {
         shutdown(commandListenerSocket,SD_BOTH);
         closesocket(commandListenerSocket);
      }
      if ( ! ( INVALID_SOCKET == commandClientSocket ) ) {
         shutdown(commandClientSocket,SD_BOTH);
         closesocket(commandClientSocket);
      }
      WaitForSingleObject(hCommandListener,INFINITE);
      commandListenerSocket = INVALID_SOCKET;
      commandClientSocket = INVALID_SOCKET;    
   }

   memset(properties.buttonCreationStrings,0,sizeof(properties.buttonCreationStrings));

//
//NTC: 7-27-2014: For the time being, buttons are not recreated on the pad as if they are in non-volatile memory. There is no
// event mechanism to do anything with the buttons.
//
#if 0

   for ( std::map<long,char *>::iterator it = buttonCreationStrings.begin(); it != buttonCreationStrings.end(); it++ ) {
      sprintf(properties.buttonCreationStrings + strlen(properties.buttonCreationStrings),"%s;",(*it).second);
      delete [] (*it).second;
   }

#endif

   control::controlCreationStrings.clear();

   memset(properties.buttonMotionStrings,0,sizeof(properties.buttonMotionStrings));

#if 0

   for ( std::map<long,char *>::iterator it = buttonMotionStrings.begin(); it != buttonMotionStrings.end(); it++ ) {
      sprintf(properties.buttonMotionStrings + strlen(properties.buttonMotionStrings),"%s;",(*it).second);
      delete [] (*it).second;
   }

#endif

   control::controlMotionStrings.clear();

   pIGProperties -> Save();

   if ( ! ( INVALID_SOCKET == controlListenerSocket ) ) {
      controlInterfaceShutdown = 1L;
      shutdown(controlListenerSocket,SD_BOTH);
      closesocket(controlListenerSocket);
      WaitForSingleObject(hControlInterface,INFINITE);
      controlListenerSocket = INVALID_SOCKET;
   }

   if ( ! ( INVALID_SOCKET == eventClientListenerSocket ) ) {
      eventPublisherShutdown = 1L;
      shutdown(eventClientListenerSocket,SD_BOTH);
      closesocket(eventClientListenerSocket);
      WaitForSingleObject(hEventsPublisher,INFINITE);
      eventClientListenerSocket = INVALID_SOCKET;
   }

   CloseHandle(hProcessingDone);

   if ( pSignatureGraphic )
      delete pSignatureGraphic;

   Gdiplus::GdiplusShutdown(gdiplusToken);

   return 0L;
   }


   float horizontalInchesFromPixels(long pixels) { 
   return (float)(xPixelsToInches * (double)pixels); 
   }

   float verticalInchesFromPixels(long pixels) { 
   return (float)(yPixelsToInches * (double)pixels); 
   }


   BOOL CALLBACK enumMonitors(HMONITOR hm,HDC,RECT *,LPARAM lp) {
   monitorIndex++;
   if ( (LPARAM)hm == lp )
      return FALSE;
   return TRUE;
   }

   BOOL CALLBACK enumFindMonitor(HMONITOR hm,HDC,RECT *,LPARAM lp) {
   monitorIndex++;
   if ( monitorIndex == lp ) {
      hFoundMonitor = hm;
      return FALSE;
   }
   return TRUE;
   }

   int GetLocation(HWND hwnd,long key,WCHAR *szwFolderLocation) {

   ITEMIDLIST *ppItemIDList;
   IShellFolder *pIShellFolder;
   LPCITEMIDLIST pcParentIDList;

   szwFolderLocation[0] = L'\0';

   HRESULT rc = SHGetFolderLocation(hwnd,key,NULL,0,&ppItemIDList);

   rc = SHBindToParent(ppItemIDList, IID_IShellFolder, (void **) &pIShellFolder, &pcParentIDList);

   STRRET strRet;
   rc = pIShellFolder -> GetDisplayNameOf(pcParentIDList,SHGDN_FORPARSING,&strRet);
   pIShellFolder -> Release();

   wcscpy(szwFolderLocation,strRet.pOleStr);

   return 0;
   }

   int __cdecl GetCommonAppDataLocation(HWND hwnd,WCHAR *szwFolderLocation) {
   GetLocation(hwnd,CSIDL_COMMON_APPDATA,szwFolderLocation);
   return 0;
   }


   unsigned int __stdcall publishEvents(void *) {

   addrinfo addressInfo;
   addrinfo *pResolvedAddressInfo;

   memset(&addressInfo,0,sizeof(addrinfo));

   addressInfo.ai_flags = AI_PASSIVE;
   addressInfo.ai_family = AF_INET;
   addressInfo.ai_socktype = SOCK_STREAM;
   addressInfo.ai_protocol = IPPROTO_TCP;
   addressInfo.ai_addrlen = 0;
   addressInfo.ai_addr = NULL;
   addressInfo.ai_canonname = NULL;
   addressInfo.ai_next = NULL;

   getaddrinfo(NULL,EVENT_SERVER_PORT_A,&addressInfo,&pResolvedAddressInfo);

   eventClientListenerSocket = socket(pResolvedAddressInfo -> ai_family,pResolvedAddressInfo -> ai_socktype,pResolvedAddressInfo -> ai_protocol);

   if ( INVALID_SOCKET == eventClientListenerSocket ) {
      Beep(2000,100);
      return 0;
   }

   if ( SOCKET_ERROR == bind(eventClientListenerSocket,pResolvedAddressInfo -> ai_addr,(int)pResolvedAddressInfo -> ai_addrlen) ) {
      Beep(2000,100);
      return 0;
   }

   if ( SOCKET_ERROR == listen(eventClientListenerSocket,SOMAXCONN) ) {
      Beep(2000,100);
      return 0;
   }

   while ( ! eventPublisherShutdown ) {

      sockaddr_in clientSockAddr = {0};

      int clientSockAddrSize = sizeof(sockaddr);

      eventClientSocket = accept(eventClientListenerSocket,(sockaddr *)&clientSockAddr,&clientSockAddrSize);

      if ( INVALID_SOCKET == eventClientSocket )
         break;

      PostMessage(hwndMain,WM_FIRE_DEVICE_READY,0L,0L);

   }

   if ( ! ( INVALID_SOCKET == eventClientSocket ) ) {
      shutdown(eventClientSocket,SD_BOTH);
      closesocket(eventClientSocket);
   }

   if ( ! ( INVALID_SOCKET == eventClientListenerSocket ) ) {
      shutdown(eventClientListenerSocket,SD_BOTH);
      closesocket(eventClientListenerSocket);
   }

   eventPublisherShutdown = 0L;

   eventClientSocket = INVALID_SOCKET;
   eventClientListenerSocket = INVALID_SOCKET;

   return 0L;
   }