// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#include <Ws2tcpip.h>
#include <windows.h>
#include <stdio.h>
#include <process.h>
#include <OleCtl.h>

#include <list>
#include <map>

#include <olectl.h>
#include <exDisp.h>
#include <mshtml.h>

#ifndef RC_INVOKED
#include <Gdiplus.h>
#endif

#define COMMAND_SIZE    32768
#define RESPONSE_SIZE   32768
#define EVENT_SIZE      32768

#define _MIDL_USE_GUIDDEF_

#include <msinkaut.h>

#include "resource.h"

#include "PadKiller\PadKillerDefines.h"

#include "globals.h"

#include "controls.h"

#include "signatureGraphic.h"

#include "Properties_i.h"

   class _IGPropertiesClient : public IGPropertiesClient {

   public:

      _IGPropertiesClient() : refCount(0) {};
      ~_IGPropertiesClient() {};

      //   IUnknown

      STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
      STDMETHOD_ (ULONG, AddRef)();
      STDMETHOD_ (ULONG, Release)();

      STDMETHOD(SavePrep)();
      STDMETHOD(InitNew)();
      STDMETHOD(Loaded)();
      STDMETHOD(Saved)();
      STDMETHOD(IsDirty)();
      STDMETHOD(GetClassID)(BYTE *pCLSID);

   private:

      long refCount;

   };

   class _IGPropertyPageClient : public IGPropertyPageClient {

   public:
      
      _IGPropertyPageClient() {};
      ~_IGPropertyPageClient();

//      IPropertyPageClient
 
      STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
      STDMETHOD_ (ULONG, AddRef)();
      STDMETHOD_ (ULONG, Release)();

      STDMETHOD(BeforeAllPropertyPages)();
      STDMETHOD(GetPropertyPagesInfo)(long* countPages,SAFEARRAY** stringDescriptions,SAFEARRAY** pHelpDirs,SAFEARRAY** pSizes);
      STDMETHOD(CreatePropertyPage)(long indexNumber,HWND,RECT*,BOOL,HWND *pHwndPropertyPage);
      STDMETHOD(IsPageDirty)(long,BOOL*);
      STDMETHOD(Help)(BSTR);
      STDMETHOD(TranslateAccelerator)(long,long*);
      STDMETHOD(Apply)();
      STDMETHOD(AfterAllPropertyPages)(BOOL);
      STDMETHOD(DestroyPropertyPage)(long indexNumber);

      STDMETHOD(GetPropertySheetHeader)(void *pHeader);
      STDMETHOD(get_PropertyPageCount)(long *pCount);
      STDMETHOD(GetPropertySheets)(void *pSheets);

   private:

   };

class pdfDocument : public IUnknown {

public:

   pdfDocument(char *pszFileName);
   ~pdfDocument();

   //   IUnknown

   STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
   STDMETHOD_ (ULONG, AddRef)();
   STDMETHOD_ (ULONG, Release)();

   bool show();
   void hide();

   IOleObject *pIOleObject_HTML;
   IOleInPlaceActiveObject *pIOleInPlaceActiveObject_HTML;
   IOleInPlaceObject *pIOleInPlaceObject_HTML;
   IWebBrowser2 *pIWebBrowser;

private:

#define EMBEDDED_OBJECT_EMBEDDER_CLASS pdfDocument

#include "interfacesToSupportAnEmbeddedObject.h"

   char szPDFFile[MAX_PATH];

   long refCount;

};

HRESULT setBackground(WCHAR *pszFileName);
HRESULT addBackground(WCHAR *pszFilename);
HRESULT start();
HRESULT stop();

void fire_PenUp(long x,long y);
void fire_PenDown(long x,long y);
void fire_PenPoint(long x,long y);
void fire_OptionSelected(long optionNumber);
void fire_OptionUnSelected(long optionNumber);
void fire_DeviceReady();
void fire_ConfigurationChanged();
void fire_TextChanged(long optionNumber,WCHAR *pszOldText,WCHAR *pszNewText);
void fire_ItemSelected(long optionNumber,WCHAR *pszItem);

unsigned int __stdcall commandListener(void *);
unsigned int __stdcall publishEvents(void *);
unsigned int __stdcall controlListener(void *);

long recieveFile(long fileSize,WCHAR *pszwResult);

   class IInkCollectorEvents : _IInkCollectorEvents {

   public:

      IInkCollectorEvents(HWND hwnd) : hwndClient(hwnd), hdcClient(GetDC(hwnd)), refCount(0), strokeCount(1L), oldStrokeNumber(-1L) {};
      ~IInkCollectorEvents() { ReleaseDC(hwndClient,hdcClient); };

      //   IUnknown

      STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
      STDMETHOD_ (ULONG, AddRef)();
      STDMETHOD_ (ULONG, Release)();

      // IDispatch

      STDMETHOD(GetTypeInfoCount)(UINT *pctinfo);
      STDMETHOD(GetTypeInfo)(UINT itinfo, LCID lcid, ITypeInfo **pptinfo);
      STDMETHOD(GetIDsOfNames)(REFIID riid, LPOLESTR *rgszNames, UINT cNames, LCID lcid, DISPID *rgdispid);
      STDMETHOD(Invoke)(DISPID dispidMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS *pdispparams, VARIANT *pvarResult, EXCEPINFO *pexcepinfo, UINT *puArgErr);

      // _IInkCollectorEvents

      STDMETHOD(Stroke)(IInkCursor * Cursor,IInkStrokeDisp * Stroke,VARIANT_BOOL * Cancel );

      STDMETHOD(CursorDown)(IInkCursor * Cursor,IInkStrokeDisp * Stroke );

      STDMETHOD(NewPackets)(IInkCursor * Cursor,IInkStrokeDisp * Stroke,long PacketCount,VARIANT * PacketData );

      STDMETHOD(DblClick)(VARIANT_BOOL * Cancel );

      STDMETHOD(MouseMove)(InkMouseButton Button,InkShiftKeyModifierFlags Shift,long pX,long pY,VARIANT_BOOL * Cancel );

      STDMETHOD(MouseDown)(InkMouseButton Button,InkShiftKeyModifierFlags Shift,long pX,long pY,VARIANT_BOOL * Cancel );

      STDMETHOD(MouseUp)(InkMouseButton Button,InkShiftKeyModifierFlags Shift,long pX,long pY,VARIANT_BOOL * Cancel );

      STDMETHOD(MouseWheel)(InkMouseButton Button,InkShiftKeyModifierFlags Shift,long Delta,long x,long y,VARIANT_BOOL * Cancel );

      STDMETHOD(NewInAirPackets)(IInkCursor * pCursor,long packetCount,VARIANT * pPacketData );

      STDMETHOD(CursorButtonDown)(IInkCursor * pCursor,IInkCursorButton * pButton );

      STDMETHOD(CursorButtonUp)(IInkCursor * pCursor,IInkCursorButton * pButton );

      STDMETHOD(CursorInRange)(IInkCursor * pCursor,VARIANT_BOOL NewCursor,VARIANT & ButtonsState );

      STDMETHOD(CursorOutOfRange)(IInkCursor * pCursor );

      STDMETHOD(SystemGesture)(IInkCursor * Cursor,InkSystemGesture Id,long x,long y,long Modifier,BSTR Character,long CursorMode );

      STDMETHOD(Gesture)(IInkCursor * Cursor,IInkStrokes * Strokes,VARIANT & Gestures,VARIANT_BOOL * Cancel );

      STDMETHOD(TabletAdded)(IInkTablet * Tablet );

      STDMETHOD(TabletRemoved)(long TabletId );

      HWND hwndClient;
      HDC hdcClient;

      long refCount;
      long strokeCount;
      long oldStrokeNumber;

   };

   LRESULT CALLBACK handler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);
   LRESULT CALLBACK configureHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

   void SaveBitmapFile(HDC hdcSource,HBITMAP hBitmap,char *pszBitmapFileName);

   BOOL CALLBACK enumMonitors(HMONITOR hm,HDC,RECT *,LPARAM lp);
   BOOL CALLBACK enumFindMonitor(HMONITOR hm,HDC,RECT *,LPARAM lp);

   float horizontalInchesFromPixels(long pixels);
   float verticalInchesFromPixels(long pixels);