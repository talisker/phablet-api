// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PadKillerHost.h"

   long isInitialized = 0L;

   HRESULT start() {

   IInkTablet *pIInkTablet = NULL;

   HRESULT rc;

   if ( properties.showInk ) {
      if ( ! pSignatureGraphic )
         pSignatureGraphic = new signatureGraphic(true);
      pSignatureGraphic -> addPoint(0,0);
   } else {
      if ( pSignatureGraphic )
         delete pSignatureGraphic;
      pSignatureGraphic = NULL;
   }

   if ( ! isInitialized ) {

      isInitialized = 1L;

      if ( ! pIInkCollector ) {

         rc = CoCreateInstance(CLSID_InkCollector,NULL,CLSCTX_ALL,IID_IInkCollector,reinterpret_cast<void **>(&pIInkCollector));

         if ( ! ( S_OK == rc ) ) {
            char szMessage[1024];
            sprintf(szMessage,"The Windows compatible TabletPC OS Components are not installed on this computer.\n\nWindows Error: %ld",rc);
            MessageBox(NULL,szMessage,"Error",MB_ICONEXCLAMATION | MB_TOPMOST);
            exit(0);
         }

         pIInkCollectorEvents = new IInkCollectorEvents(hwndMain);

         IUnknown *pIUnknown = NULL;

         pIInkCollectorEvents -> QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));

         pIInkCollector -> QueryInterface(IID_IConnectionPointContainer,reinterpret_cast<void **>(&pIConnectionPointContainer));

         if ( pIConnectionPointContainer ) {
            HRESULT hr = pIConnectionPointContainer -> FindConnectionPoint(DIID__IInkCollectorEvents,&pIConnectionPoint);
            if ( pIConnectionPoint )
               hr = pIConnectionPoint -> Advise(pIUnknown,&dwConnectionCookie);
         }

         IInkTablets *pIInkTablets = NULL;

         rc = CoCreateInstance(CLSID_InkTablets,NULL,CLSCTX_ALL,IID_IInkTablets,reinterpret_cast<void **>(&pIInkTablets));

         pIInkTablets -> get_DefaultTablet(&pIInkTablet);

         pIInkTablets -> Release();

      }

      rc = pIInkCollector -> put_hWnd((LONG_PTR)hwndMain);

      rc = pIInkCollector -> put_Enabled(VARIANT_FALSE);

      rc = pIInkCollector -> SetAllTabletsMode(VARIANT_FALSE);
      
      rc = pIInkCollector -> SetEventInterest(ICEI_AllEvents,VARIANT_FALSE);

      rc = pIInkCollector -> put_AutoRedraw(VARIANT_FALSE);

      rc = pIInkCollector -> put_DynamicRendering(VARIANT_FALSE);

      rc = pIInkCollector -> get_Renderer(&pIInkRenderer);

#if 0
      IInkDrawingAttributes *pIInkDrawingAttributes = NULL;

      pIInkCollector -> get_DefaultDrawingAttributes(&pIInkDrawingAttributes);

      pIInkDrawingAttributes -> put_AntiAliased(VARIANT_TRUE);

      pIInkDrawingAttributes -> put_FitToCurve(VARIANT_TRUE);
      
      pIInkDrawingAttributes -> put_Transparency(0);

      pIInkCollector -> putref_DefaultDrawingAttributes(pIInkDrawingAttributes);
#endif

   }

   IInkRectangle *pIInkRectangle = NULL;

   rc = CoCreateInstance(CLSID_InkRectangle,NULL,CLSCTX_ALL,IID_IInkRectangle,reinterpret_cast<void **>(&pIInkRectangle));

   rc = pIInkRectangle -> SetRectangle(rcDisplay.top,rcDisplay.left,rcDisplay.bottom,rcDisplay.right);

   rc = pIInkCollector -> SetWindowInputRectangle(pIInkRectangle);

   rc = pIInkRectangle -> Release();
      
   rc = pIInkCollector -> SetEventInterest(ICEI_AllEvents,VARIANT_TRUE);

   rc = pIInkCollector -> put_CollectionMode(ICM_InkAndGesture);

   rc = pIInkCollector -> put_Enabled(VARIANT_TRUE);

   return S_OK;
   }

