// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PadKillerHost.h"

   void getScreenSize();

   LRESULT CALLBACK configureHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   switch ( msg ) {

   case WM_INITDIALOG:

      if ( properties.useEntire )
         SendDlgItemMessage(hwnd,IDDI_USE_ENTIRE_SCREEN,BM_SETCHECK,BST_CHECKED,0L);
      else
         SendDlgItemMessage(hwnd,IDDI_USE_ENTIRE_SCREEN,BM_SETCHECK,BST_UNCHECKED,0L);

      if ( properties.stayAlive )
         SendDlgItemMessage(hwnd,IDDI_IGNORE_HEARTBEAT,BM_SETCHECK,BST_CHECKED,0L);
      else
         SendDlgItemMessage(hwnd,IDDI_IGNORE_HEARTBEAT,BM_SETCHECK,BST_UNCHECKED,0L);

      char szX[32];
      sprintf(szX,"%ld",properties.cxDefault);
      SetDlgItemText(hwnd,IDDI_PIXEL_WIDTH,szX);
      sprintf(szX,"%ld",properties.cyDefault);
      SetDlgItemText(hwnd,IDDI_PIXEL_HEIGHT,szX);
      configureHandler(hwnd,WM_COMMAND,MAKEWPARAM(IDDI_USE_ENTIRE_SCREEN,0),0L);
      SetWindowPos(hwnd,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);

      isUnderConfiguration = true;
      
      hwndConfigure = hwnd;

      break;

   case WM_COMMAND:

      switch ( LOWORD(wParam) ) {

      case IDDI_USE_ENTIRE_SCREEN:
         properties.useEntire = (BST_CHECKED == SendDlgItemMessage(hwnd,IDDI_USE_ENTIRE_SCREEN,BM_GETCHECK,0L,0L));
         EnableWindow(GetDlgItem(hwnd,IDDI_GROUP_NOT_ENTIRE),! properties.useEntire);
         EnableWindow(GetDlgItem(hwnd,IDDI_PIXEL_WIDTH_LABEL),! properties.useEntire);
         EnableWindow(GetDlgItem(hwnd,IDDI_PIXEL_WIDTH),! properties.useEntire);
         EnableWindow(GetDlgItem(hwnd,IDDI_PIXEL_HEIGHT_LABEL),! properties.useEntire);
         EnableWindow(GetDlgItem(hwnd,IDDI_PIXEL_HEIGHT),! properties.useEntire);
         EnableWindow(GetDlgItem(hwnd,IDDI_POSITION_RESET),! properties.useEntire);
         EnableWindow(GetDlgItem(hwnd,IDDI_POSITION_NOTE),! properties.useEntire);
         EnableWindow(GetDlgItem(hwnd,IDDI_SIZE_NOTE),! properties.useEntire);
         EnableWindow(GetDlgItem(hwnd,IDDI_RESET_NOTE),! properties.useEntire);
         break;

#if 0
      case IDDI_POSITION_MOVE: {

         GetWindowRect(hwndMain,&rcMoving);

         rcMoving.left = rcMoving.left + (rcMoving.right - rcMoving.left) / 2;
         rcMoving.top = rcMoving.top + (rcMoving.bottom - rcMoving.top) / 2;

         SetCursorPos(rcMoving.left,rcMoving.top);

         SetCursor(LoadCursor(NULL,IDC_HAND));

         }
         break;
#endif

      case IDDI_POSITION_RESET:

         getScreenSize();

         properties.x = rcMultiMonitorOffset.left + cxScreen / 2 - properties.cxDefault / 2;
         properties.y = rcMultiMonitorOffset.top + cyScreen / 2 - properties.cyDefault / 2;

         SetWindowPos(hwndMain,HWND_TOP,properties.x,properties.y,properties.cxDefault,properties.cyDefault,0L);

         break;

      default:
         break;
      }
   
      break;

   case WM_DESTROY: {

      properties.useEntire = (BST_CHECKED == SendDlgItemMessage(hwnd,IDDI_USE_ENTIRE_SCREEN,BM_GETCHECK,0L,0L));
      properties.stayAlive = (BST_CHECKED == SendDlgItemMessage(hwnd,IDDI_IGNORE_HEARTBEAT,BM_GETCHECK,0L,0L));

      getScreenSize();

      if ( properties.useEntire ) {
         properties.cxDefault = cxScreen;
         properties.cyDefault = cyScreen;
      } else {
         char szX[32];
         GetDlgItemText(hwnd,IDDI_PIXEL_WIDTH,szX,32);
         properties.cxDefault = atol(szX);
         GetDlgItemText(hwnd,IDDI_PIXEL_HEIGHT,szX,32);
         properties.cyDefault = atol(szX);
      }

      fire_ConfigurationChanged();

      isUnderConfiguration = false;

      }
      break;

   default:
      break;
   }
   

   return (LRESULT)0L;
   }

   void getScreenSize() {

   RECT rcPad = {0};

   GetWindowRect(hwndMain,&rcPad);

   properties.x = rcPad.left;
   properties.y = rcPad.top;

   HMONITOR hMonitor = MonitorFromRect(&rcPad,MONITOR_DEFAULTTOPRIMARY);

   monitorIndex = -1L;

   EnumDisplayMonitors(NULL,NULL,enumMonitors,(LPARAM)hMonitor);

   properties.monitorNumber = monitorIndex + 1;

   if ( hMonitor ) {
      MONITORINFO monitorInfo = {0};
      monitorInfo.cbSize = sizeof(MONITORINFO);
      GetMonitorInfo(hMonitor,&monitorInfo);
      rcMultiMonitorOffset = monitorInfo.rcWork;
   } else
      SystemParametersInfo(SPI_GETWORKAREA,0,&rcMultiMonitorOffset,0);

   cxScreen = rcMultiMonitorOffset.right - rcMultiMonitorOffset.left;
   cyScreen = rcMultiMonitorOffset.bottom - rcMultiMonitorOffset.top;

   return;
   }