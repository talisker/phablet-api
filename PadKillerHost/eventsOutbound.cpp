// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <time.h>

#include "PadKillerHost.h"

long lastEventSent = -1L;
long lastPenDownEventSent = -1L;
long lastOptionEventSent = -1L;

#define CHECK_DURATION                                                     \
   if ( eventSpacingDuration ) {                                           \
      if ( (long)(GetTickCount() - lastEventSent) < eventSpacingDuration ) \
         return;                                                           \
   }                                                                       \
   lastEventSent = GetTickCount();

   bool IsInBounds(long x,long y) { return 0 < x && x < properties.cxDefault && 0 < y && y < properties.cyDefault; };

   void fire_PenUp(long x,long y) {

   if ( INVALID_SOCKET == eventClientSocket )
      return;

   CHECK_DURATION

   swprintf(szwEvent,L"%ld %ld %ld",(long)PENUP,x,y);
   send(eventClientSocket,(char *)szwEvent,2 * (DWORD)wcslen(szwEvent),0L);
   recv(eventClientSocket,(char *)szwEvent,EVENT_SIZE * sizeof(WCHAR),0L);
   if ( pSignatureGraphic )
      pSignatureGraphic -> addPoint(0,0);
   return;
   }

   void fire_PenDown(long x,long y) {

   if ( INVALID_SOCKET == eventClientSocket )
      return;

   CHECK_DURATION

   if ( ! ( -1L == lastPenDownEventSent ) && penDownEventSpacingDuration ) {
      if ( (long)(GetTickCount() - lastPenDownEventSent) < penDownEventSpacingDuration )
         return;
   }

   lastPenDownEventSent = GetTickCount();

   swprintf(szwEvent,L"%ld %ld %ld",(long)PENDOWN,x,y);
   send(eventClientSocket,(char *)szwEvent,2 * (DWORD)wcslen(szwEvent),0L);
   recv(eventClientSocket,(char *)szwEvent,EVENT_SIZE * sizeof(WCHAR),0L);
   lastSignatureX = 0;
   lastSignatureY = 0;
   if ( pSignatureGraphic )
      pSignatureGraphic -> addPoint(0,0);

   return;
   }

   void fire_PenPoint(long x,long y) {
   
   for ( std::list<hotSpot *>::iterator it = hotSpots.begin(); it != hotSpots.end(); it++ ) {
      if ( (*it) -> contains(x,y) ) {
         lastSignatureX = 0L;
         lastSignatureY = 0L;
         return;
      }
   }

   if ( eventClientSocket && sendPoints ) {
      //CHECK_DURATION
      swprintf(szwEvent,L"%ld %ld %ld",(long)PENPOINT,x,y);
      send(eventClientSocket,(char *)szwEvent,2 * (DWORD)wcslen(szwEvent),0L);
      recv(eventClientSocket,(char *)szwEvent,EVENT_SIZE * sizeof(WCHAR),0L);
   }

   if ( ! IsInBounds(x,y) ) {
      lastSignatureX = 0L;
      lastSignatureY = 0L;
      return;
   }

   if ( 0 == lastSignatureX ) {
      lastSignatureX = x;
      lastSignatureY = y;
      return;
   }

   if ( 0 == x && 0 == y ) {
      lastSignatureX = 0;
      lastSignatureY = 0;
      return;
   }

   if ( properties.showInk ) {
      pGDIPlusGraphic -> DrawLine(pGDIPlusPen,lastSignatureX,lastSignatureY,x,y);
      if ( pSignatureGraphic )
         pSignatureGraphic -> addPoint(x,y);
   }

   lastSignatureX = x;
   lastSignatureY = y;

   return;
   }

   void fire_OptionUnSelected(long optionNumber) {

   swprintf(szwEvent,L"%ld %ld",(long)OPTIONUNSELECTED,optionNumber);
   send(eventClientSocket,(char *)szwEvent,2 * (DWORD)wcslen(szwEvent),0L);
   recv(eventClientSocket,(char *)szwEvent,EVENT_SIZE * sizeof(WCHAR),0L);

   return;
   }

   void fire_OptionSelected(long optionNumber) {

   if ( INVALID_SOCKET == eventClientSocket )
      return;

   if ( ! ( -1L == lastOptionEventSent ) && optionEventSpacingDuration ) {
      if ( (long)(GetTickCount() - lastOptionEventSent) < optionEventSpacingDuration )
         return;
   }

   lastOptionEventSent = GetTickCount();

   swprintf(szwEvent,L"%ld %ld",(long)OPTIONSELECTED,optionNumber);
   send(eventClientSocket,(char *)szwEvent,2 * (DWORD)wcslen(szwEvent),0L);
   recv(eventClientSocket,(char *)szwEvent,EVENT_SIZE * sizeof(WCHAR),0L);

   return;
   }

   void fire_DeviceReady() {
   if ( INVALID_SOCKET == eventClientSocket )
      return;
   swprintf(szwEvent,L"%ld",(long)DEVICEREADY);
   send(eventClientSocket,(char *)szwEvent,2 * (DWORD)wcslen(szwEvent),0L);
   recv(eventClientSocket,(char *)szwEvent,EVENT_SIZE * sizeof(WCHAR),0L);
   return;
  }

   void fire_ConfigurationChanged() {
   if ( INVALID_SOCKET == eventClientSocket )
      return;
   swprintf(szwEvent,L"%ld",(long)CONFIGURATIONCHANGED);
   send(eventClientSocket,(char *)szwEvent,2 * (DWORD)wcslen(szwEvent),0L);
   recv(eventClientSocket,(char *)szwEvent,EVENT_SIZE * sizeof(WCHAR),0L);
   return;
   }


   void fire_TextChanged(long optionNumber,WCHAR *pszwOldText,WCHAR *pszwNewText) {

   if ( INVALID_SOCKET == eventClientSocket )
      return;

   holdOneEvent = SETPARAMETER;
   holdOneEventParameter = PARM_CONTROL_TEXT;

   swprintf(szwEvent,L"%ld %ld �l%s� �l%s�",(long)TEXTCHANGED,optionNumber,pszwOldText,pszwNewText);
   send(eventClientSocket,(char *)szwEvent,2 * (DWORD)wcslen(szwEvent),0L);
   recv(eventClientSocket,(char *)szwEvent,EVENT_SIZE * sizeof(WCHAR),0L);

   return;
   }


   void fire_ItemSelected(long optionNumber,WCHAR *pszwItem) {

   if ( INVALID_SOCKET == eventClientSocket )
      return;

   holdOneEvent = SELECTITEM;

   swprintf(szwEvent,L"%ld %ld �%ls�",(long)ITEMSELECTED,optionNumber,pszwItem);
   send(eventClientSocket,(char *)szwEvent,2 * (DWORD)wcslen(szwEvent),0L);
   recv(eventClientSocket,(char *)szwEvent,EVENT_SIZE * sizeof(WCHAR),0L);

   return;
   }