// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#define PREALLOCATED_SIGNATURE_GRAPHIC_SIZE  17638

struct signatureGraphic {

   signatureGraphic(bool isPreallocated = false) { 
      memset(this,0,sizeof(signatureGraphic));
      if ( isPreallocated ) {
         isIndependentOfList = true;
         pSignatureDataX = new long[PREALLOCATED_SIGNATURE_GRAPHIC_SIZE];
         pSignatureDataY = new long[PREALLOCATED_SIGNATURE_GRAPHIC_SIZE];
      }
      };

   ~signatureGraphic() { 
      if ( pSignatureDataX ) delete [] pSignatureDataX; 
      if ( pSignatureDataY ) delete [] pSignatureDataY; 
   };

   void addPoint(long x,long y) { 
      if ( PREALLOCATED_SIGNATURE_GRAPHIC_SIZE == totalPoints ) 
         return;
      pSignatureDataX[totalPoints] = x; 
      pSignatureDataY[totalPoints] = y; 
      totalPoints++; 
   };

   long totalPoints;
   bool isIndependentOfList;
   long *pSignatureDataX;
   long *pSignatureDataY;
};

