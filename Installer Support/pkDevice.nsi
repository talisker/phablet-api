
!define DONT_SIGN

!define ARCH x64

!define CONFIGURATION Release

!define OUTFILE pkDevice.exe

!define CERTIFICATE   <pathAndCertificate>
!define CERTPASSWORD  <certificatePassword>

;
; DON'T forget to set the above variables as appropriate
;

!define PRODUCT_NAME "PK Device"

!define PRODUCT_VERSION "4.00"
!define PRODUCT_PUBLISHER "InnoVisioNate"
!define PRODUCT_WEB_SITE "http://www.innovisionate.com"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\pkHostApp.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_MINOR_VERSION "0"
!define PRODUCT_MAJOR_VERSION "2"

!include "MUI.nsh"

!include "WinVer.nsh"

!include WordFunc.nsh
!insertmacro VersionCompare
!include LogicLib.nsh

!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"
;!define MUI_WELCOMEFINISHPAGE_BITMAP "..\Resources\VisioLogger-Logo.bmp"

!insertmacro MUI_PAGE_WELCOME

!insertmacro MUI_PAGE_INSTFILES

;!define MUI_FINISHPAGE_RUN "$INSTDIR\VisioLogger.exe"

!define MUI_FINISHPAGE_RUN_NOTCHECKED

!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

!define MULTIUSER_EXECUTIONLEVEL Admin

!include MultiUser.nsh

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"

InstallDir "$PROGRAMFILES\InnoVisioNate\pkDevice"

InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""

ShowInstDetails show
ShowUnInstDetails show

!ifdef INNER

  OutFile "$%temp%\tempinstaller.exe"

!else

  !system "$\"${NSISDIR}\makensis$\" /DINNER pkDevice.nsi" = 0

  !system "$%temp%\tempinstaller.exe" = 2

!ifndef DONT_SIGN
  !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%temp%\PKDeviceUninst.exe"'
!endif

  OutFile "${OUTFILE}"

!endif

Function .onInit

!ifdef INNER
  WriteUninstaller "$%temp%\PKDeviceUninst.exe"
  Quit
!endif

!insertmacro MUI_LANGDLL_DISPLAY
!insertmacro MULTIUSER_INIT

   SetShellVarContext all

!if 'x64' == '${ARCH}'

   StrCpy $INSTDIR "$PROGRAMFILES64\InnoVisioNate\pkDevice"

   SetRegView 64
   
!else

   StrCpy "$INSTDIR" $PROGRAMFILES\InnoVisioNate\pkDevice"

   SetRegView 32
   
!endif

   SetOutPath "$INSTDIR"

   Return
   
FunctionEnd


Section "InnoVisioNate" SEC01
  
  SetOutPath "$INSTDIR"

  SetOverwrite ifnewer

!ifndef DONT_SIGN
!ifdef INNER
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkControl.exe"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkHostApp.exe"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkListener.exe"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\Properties.ocx"'
!endif
!endif

  ExecWait '"$INSTDIR\pkControl.exe" /uninstall'

  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkControl.exe"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkHostApp.exe"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkListener.exe"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\Properties.ocx"
  
  ExecWait '"$INSTDIR\pkControl.exe" /install'

  ExecWait '"$INSTDIR\pkControl.exe" /start'

  ExecWait '"$INSTDIR\pkControl.exe" /register "$INSTDIR\properties.ocx"'
  
  CreateDirectory "$APPDATA\pkDevice"
  
  ExecWait '"$INSTDIR\pkControl.exe" /grantfullaccess "$APPDATA\pkDevice"'
  
SectionEnd

Section -Post

!ifndef INNER
  SetOutPath $INSTDIR
  File "$%temp%\PKDeviceUninst.exe"
  !system 'del "$%temp%\PKDeviceUninst.exe"'
  !system 'del "$%temp%\tempinstaller.exe"'
!endif

  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\pkHostApp.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\PKDeviceUninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\pkHostApp.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "VersionMajor" "${PRODUCT_MAJOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "VersionMinor" "${PRODUCT_MINOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "MajorVersion" "${PRODUCT_MAJOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "MinorVersion" "${PRODUCT_MINOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "InstallLocation" "$INSTDIR"

SectionEnd


Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) has been removed."
FunctionEnd

Function un.onInit
!insertmacro MULTIUSER_UNINIT
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Are you sure you want to remove $(^Name)?" IDYES +2
  Abort
FunctionEnd

Section Uninstall

  ExecWait '"$INSTDIR\PKControl" /uninstall'

  Delete "$INSTDIR\Properties.ocx"
  Delete "$INSTDIR\pkControl.exe"
  Delete "$INSTDIR\pkHostApp.exe"
  Delete "$INSTDIR\pkListener.exe"
  Delete "$INSTDIR\PKUninst.exe"

  RMDir "$INSTDIR"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
  DeleteRegKey HKLM "Software\InnoVisioNate\PK"
  SetAutoClose true
  
  System::Call 'Shell32::SHChangeNotify(i 0x8000000, i 0, i 0, i 0)'

SectionEnd

