
#include "PKControl.h"


   HRESULT shutdownService() {

   char szServerName[] = "localhost";
   wchar_t wszServerName[] = L"localhost";

   char  szCommand[4096];

   WSADATA wsaData;
   WSAStartup(MAKEWORD(2,2),&wsaData);

   addrinfo addressInfo = {0};
   addrinfo *pResolvedAddressInfo = NULL;

   addressInfo.ai_flags = 0L;
   addressInfo.ai_family = AF_UNSPEC;
   addressInfo.ai_socktype = SOCK_STREAM;
   addressInfo.ai_protocol = IPPROTO_TCP;
   addressInfo.ai_addrlen = 0;
   addressInfo.ai_addr = NULL;
   addressInfo.ai_canonname = NULL;
   addressInfo.ai_next = NULL;

   if ( getaddrinfo(szServerName,SERVICE_PORT_A,&addressInfo,&pResolvedAddressInfo) ) 
      return E_FAIL;

   long rc = 0L;

   long connectionSocket = INVALID_SOCKET;

   for ( addrinfo *p = pResolvedAddressInfo; p; p = p -> ai_next ) {
      if ( ! ( SOCK_STREAM == p -> ai_socktype ) )
         continue;
      connectionSocket = socket(p -> ai_family,p -> ai_socktype,p -> ai_protocol);
      if ( ! ( INVALID_SOCKET == connectionSocket ) ) {
         if ( SOCKET_ERROR != connect(connectionSocket,p -> ai_addr,p -> ai_addrlen) )
            break;
      }
      connectionSocket = INVALID_SOCKET;
   }

   freeaddrinfo(pResolvedAddressInfo);

   if ( rc ) 
      return E_FAIL;

   send(connectionSocket,"shutdown",8,0);

   memset(szCommand,0,sizeof(szCommand));
   rc = recv(connectionSocket,szCommand,1024,0);

   return 0;
   }