// Copyright 2017,2018,2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PKControl.h"

#include "PadKiller\pkListener.h"

   extern "C" int GrantFullAccess(char *pszDirectory);
   extern "C" int RegisterDLL(char *pszDLL);

   extern "C" int main(int argc,char *argv[]) {

   if ( 2 > argc ) {
      printf("Usage: "
                  "\n\nTo install the PKListener service:\n\n\tPKControl /install"
                  "\n\nTo uninstall the service:\n\n\tPKControl /uninstall\n"
                  "\n\nTo Restart the service:\n\n\tPKControl /restart\n"
                  "\n\nTo Start the service:\n\n\tPKControl /start\n"
                  "\n\nTo Stop the service:\n\n\tPKControl /stop\n");

      _getch();
      exit(0);
   }

   wchar_t *wargv[3] = {L"",L"",L""};

   for ( long k = 0; k < argc && k < 3; k++ ) {
      wargv[k] = new wchar_t[strlen(argv[k]) + 1];
      MultiByteToWideChar(CP_ACP,0,argv[k],-1,wargv[k],(DWORD)strlen(argv[k]) + 1);
   }

   if ( strstr(argv[1],"grantfullaccess") ) {
      GrantFullAccess(argv[2]);
      return 0L;
   }

   if ( strstr(argv[1],"register") ) {
      RegisterDLL(argv[2]);
      return 0L;
   }

   SC_HANDLE hServiceManager;
   SC_HANDLE hService;
   wchar_t szPath[MAX_PATH];

   memset(szPath,0,sizeof(szPath));

   szPath[0] = '\"';

   GetModuleFileName(NULL,szPath + 1,MAX_PATH);
 
   wchar_t *p = wcsrchr(szPath,'\\');
   if ( ! p )
      p = wcsrchr(szPath,'/');
   if ( p ) {
      *p = '\0';
      wsprintf(szPath + wcslen(szPath),L"\\pkListener.exe");
   }

   szPath[wcslen(szPath) + 1] = '\0';
   szPath[wcslen(szPath)] = '\"';

   hServiceManager = OpenSCManager(NULL,NULL,SC_MANAGER_ALL_ACCESS);

   if ( 0 == _wcsicmp(wargv[1],L"/install") ) {

      hService = OpenService(hServiceManager,SERVICE_NAME,SERVICE_ALL_ACCESS);

      if ( hService ) {
         SERVICE_STATUS serviceStatus = {0};
         BOOL b = ControlService(hService,SERVICE_CONTROL_STOP,&serviceStatus);
         if ( ! b && ! ( 1060 == GetLastError() ) )
            wprintf(L"The service \"" SERVICE_NAME L"\" exists and an attempt was made to stop it, which failed. The last error is: %ld\ncontinuing...\n",GetLastError());
         b = DeleteService(hService);
         if ( ! b )
            wprintf(L"The service \"" SERVICE_NAME L"\" exists and an attempt was made to delete it, which failed. The last error is: %ld\ncontinuing...\n",GetLastError());
         CloseServiceHandle(hService); 
      }

      hService = CreateService( hServiceManager,SERVICE_NAME,SERVICE_NAME,SERVICE_ALL_ACCESS,
                                    SERVICE_WIN32_OWN_PROCESS | 0*SERVICE_INTERACTIVE_PROCESS,
                                    SERVICE_AUTO_START,SERVICE_ERROR_NORMAL,szPath,NULL,NULL,NULL,NULL,NULL);

      if ( ! hService ) {
         wprintf(L"There was an error attempting to create the service \"" SERVICE_NAME L"\" with the executable at %s.\nWindows returned the error code %ld.\n",szPath,GetLastError());
         CloseServiceHandle(hServiceManager);
         return 0;
      }

      SERVICE_DESCRIPTION serviceDescription;
      wchar_t szDescription[2048];
      wsprintf(szDescription,L"Instructions for using this service on port %s",SERVICE_PORT_W);
      serviceDescription.lpDescription = szDescription;

      long rc = ChangeServiceConfig2(hService,SERVICE_CONFIG_DESCRIPTION,(void *)&serviceDescription);

      BOOL b = StartService(hService,0,NULL);
      if ( ! b )
         wprintf(L"\nThere was an error starting the service \"" SERVICE_NAME L"\".\nWindows reported the error: %ld\n",GetLastError());
      else
         wprintf(L"\nThe service \"" SERVICE_NAME L"\" was started successfully.\n");

      CloseServiceHandle(hService); 

      wprintf(L"The service \"" SERVICE_NAME L"\" was installed successfully\n"); 

   }

   if ( 0 == _wcsicmp(wargv[1],L"/uninstall") ) {

      //shutdownService();

      hService = OpenService(hServiceManager,SERVICE_NAME,SERVICE_ALL_ACCESS);

      if ( hService ) {
         SERVICE_STATUS serviceStatus;
         ControlService(hService,SERVICE_CONTROL_STOP,&serviceStatus);
         BOOL rc = DeleteService(hService);
         long k = GetLastError();
         CloseServiceHandle(hService); 
         wprintf(L"\nThe service \"" SERVICE_NAME L"\" was uninstalled successfully\n"); 
      } else
         if ( ! 1060 == GetLastError() )
            wprintf(L"\nThere was an error opening the service \"" SERVICE_NAME L"\" handle. Windows reported the error: %ld\n",GetLastError());
         else
            wprintf(L"\nThe service \"" SERVICE_NAME L"\" does not exist.\n");

   }

   if ( 0 == _wcsicmp(wargv[1],L"/restart") || 0 == _wcsicmp(wargv[1],L"/stop") ) {

//      shutdownService();

      hService = OpenService(hServiceManager,SERVICE_NAME,SERVICE_STOP);

      if ( ! hService ) {
         if ( 1060 == GetLastError() )
            wprintf(L"\nThe service \"" SERVICE_NAME L"\" does not exist\n");
         else
            wprintf(L"\nThere was an error opening the service \"" SERVICE_NAME L"\" handle.\nWindows reported the error: %ld\n",GetLastError());
         CloseServiceHandle(hServiceManager);
         return 0;
      }

      SERVICE_STATUS serviceStatus = {0};
      BOOL b = ControlService(hService,SERVICE_CONTROL_STOP,&serviceStatus);
      if ( ! b )
         wprintf(L"\nThere was an error stopping the service \"" SERVICE_NAME L"\".\nWindows reported the error: %ld\n",GetLastError());
      else
         wprintf(L"\nThe service \"" SERVICE_NAME L"\" was stopped successfully.\n");

   }

   if ( 0 == _wcsicmp(wargv[1],L"/restart") || 0 == _wcsicmp(wargv[1],L"/start") ) {

      //shutdownService();

      hService = OpenService(hServiceManager,SERVICE_NAME,SERVICE_START);

      if ( ! hService ) {
         if ( 1060 == GetLastError() )
            wprintf(L"\nThe service \"" SERVICE_NAME L"\" does not exist\n");
         else
            wprintf(L"\nThere was an error opening the service \"" SERVICE_NAME L"\" handle.\nWindows reported the error: %ld\n",GetLastError());
         CloseServiceHandle(hServiceManager);
         return 0;
      }

      BOOL b = StartService(hService,0,NULL);
      if ( ! b )
         wprintf(L"\nThere was an error starting the service \"" SERVICE_NAME L"\".\nWindows reported the error: %ld\n",GetLastError());
      else
         wprintf(L"\nThe service \"" SERVICE_NAME L"\" was started successfully.\n");

   }

   CloseServiceHandle(hServiceManager);

   return 0;
   }

#include "GrantFullAccess.cpp"
#include "RegisterDLL.cpp"